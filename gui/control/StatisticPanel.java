package gui.control;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.LineBorder;

import org.jdesktop.swingworker.SwingWorker;

import gui.Viewer;
import nenok.Adapter;
import nenok.lc.JoinTree;

/**
 * This panel displays statistics about local computation. 
 *
 * @author Marc Pouly
 * @version $LastChangedRevision: 566 $<br/>$LastChangedDate: 2008-03-26 14:40:58 +0100 (Mi, 26 Mrz 2008) $
 */

public class StatisticPanel extends JPanel {
	
	private Viewer viewer;
	private JTable opTable = null;
	private JTable runTable = null;
	private JButton collectButton = null;
	private JButton distributeButton = null;
	
	/**
	 * Constructor:
	 * @param viewer The main application.
	 */
	
	public StatisticPanel(Viewer viewer) {
		super();
		this.viewer = viewer;
		initialize();
	}
	
	/**
	 * Constructor:
	 * @param viewer The main application.
	 * @param tree The jointree that delivers data to fill this panel.
	 */
	
	public StatisticPanel(Viewer viewer, JoinTree tree) {
		this(viewer);
		update(tree);
	}

	/**
	 * This method initializes the component.
	 */
	
	private void initialize() {
		GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
		gridBagConstraints2.gridx = 1;
		gridBagConstraints2.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints2.insets = new java.awt.Insets(5,5,10,10);
		gridBagConstraints2.weightx = 0.5;
		gridBagConstraints2.anchor = java.awt.GridBagConstraints.CENTER;
		gridBagConstraints2.ipadx = 0;
		gridBagConstraints2.gridy = 4;
		GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
		gridBagConstraints11.gridx = 0;
		gridBagConstraints11.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints11.insets = new java.awt.Insets(5,10,10,5);
		gridBagConstraints11.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints11.weightx = 0.5;
		gridBagConstraints11.gridy = 4;
		GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
		gridBagConstraints1.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints1.weighty = 1.0;
		gridBagConstraints1.gridx = 0;
		gridBagConstraints1.gridy = 3;
		gridBagConstraints1.insets = new java.awt.Insets(5,10,5,10);
		gridBagConstraints1.gridwidth = 2;
		gridBagConstraints1.weightx = 1.0;
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.insets = new java.awt.Insets(10,10,5,10);
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.weightx = 1.0;
		this.setLayout(new GridBagLayout());
		this.setSize(300, 200);
		this.add(getOpTable(), gridBagConstraints);
		this.add(getRunTable(), gridBagConstraints1);
		this.add(getCollectButton(), gridBagConstraints11);
		this.add(getDistributeButton(), gridBagConstraints2);
	}

	/**
	 * @return The operation statistic table.
	 */
	
	private JTable getOpTable() {
		if (opTable == null) {
			opTable = new JTable();
			opTable.setBackground(this.getBackground());
			opTable.setShowGrid(true);
			opTable.setGridColor(Color.BLACK);
			opTable.setBorder(new LineBorder(Color.BLACK));
	        opTable.setFocusable(false);
	        opTable.setEnabled(false);
	        opTable.setRequestFocusEnabled(false);
	        opTable.setRowSelectionAllowed(false);
	        opTable.setModel(new javax.swing.table.DefaultTableModel(
	                new Object [][] {
	                    {" # Comb:", " # Marg:", " # Div:"},
	                    {" -", " -", " -"},
	                },
	                new String [] {
	                    "Title 1", "Title 2", ""
	                }
	            ));	        
		}
		return opTable;
	}

	/**	
	 * @return The runtime statistic table.
	 */
	
	private JTable getRunTable() {
		if (runTable == null) {
			runTable = new JTable();
			runTable.setBackground(this.getBackground());
	        runTable.setFocusable(false);
	        runTable.setRequestFocusEnabled(false);
	        runTable.setRowSelectionAllowed(false);
	        runTable.setModel(new javax.swing.table.DefaultTableModel(
	                new Object [][] {
	                    {"Collect Time:", "-"},
	                    {"Distribute Time:", "-"},
	                    {"Allocator:", "-"},
	                    {"Communication Costs:", "-"}
	                },
	                new String [] {
	                    "Title 1", "Title 2"
	                }
	            ) {
	        	
	        	/**
	        	 * @see javax.swing.table.DefaultTableModel#isCellEditable(int, int)
	        	 */
	        	
	        	@Override
	        	public boolean isCellEditable(int arg0, int arg1) {
	        		return false;
	        	}
	        	
	        });
	        runTable.setShowGrid(false);
		}
		return runTable;
	}

	/**
     * Delegator method to fill the table.
     * @param tree The jointree whose information is displayed.
     */
    
    public void update(JoinTree tree) {

            opTable.setValueAt(" -", 1,0);
            opTable.setValueAt(" -", 1,1);
            opTable.setValueAt(" -", 1,2);
            
            runTable.setValueAt("-", 0,1);
            runTable.setValueAt("-", 1,1);
            runTable.setValueAt("-", 2,1);
            runTable.setValueAt("-", 3,1);
                        
        if(tree != null) {
            
            Adapter factory = tree.getAdapter(); 
            
            opTable.setValueAt(" "+factory.getCombinations(), 1,0);
            opTable.setValueAt(" "+factory.getMarginalizations(), 1,1);
            opTable.setValueAt(" "+factory.getInverses(), 1,2);
            
            if(tree.isCollected()) {
                runTable.setValueAt(tree.getCollectTime()+" ms", 0,1);
            }
            
            if(tree.isDistributed()) {
                runTable.setValueAt(tree.getDistributeTime()+" ms", 1,1);
            }
        }
    }

    /**
	 * @return The collect button.
	 */
    
	protected JButton getCollectButton() {
		if (collectButton == null) {
			collectButton = new JButton();
			collectButton.setText("Collect");
			collectButton.setEnabled(false);
			collectButton.setFocusPainted(false);
			collectButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					final JoinTree tree = viewer.getJointreePanel().getTreeSelection();
					if(tree == null) {
						return;
					}
					
					getCollectButton().setEnabled(false);
					setCursor(Viewer.HOURGLASS);
					
					/*
					 * This swing worker performs heavy computations in background and
					 * allows the user to continue working on other swing components.
					 */
					
					new SwingWorker<JoinTree, Object>() {
						
						@Override
						protected JoinTree doInBackground() throws Exception {
							tree.collect();
							return tree;
						}
						
						@Override
						protected void done() {
							try {
								update(get());
								viewer.repaintFrames();
								setCursor(Viewer.NORMAL);
								getDistributeButton().setEnabled(true);
							} catch (Exception e) {
								viewer.error("SwingWorker Exception", e);
							} 
						}
					}.execute();
				}
			});
		}
		return collectButton;
	}

	/**
	 * @return The distribute button.
	 */
	
	protected JButton getDistributeButton() {
		if (distributeButton == null) {
			distributeButton = new JButton();
			distributeButton.setText("Distribute");
			distributeButton.setEnabled(false);
			distributeButton.setFocusPainted(false);
			distributeButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					final JoinTree tree = viewer.getJointreePanel().getTreeSelection();
					if(tree == null) {
						return;
					}
					
		            getDistributeButton().setEnabled(false);
					setCursor(Viewer.HOURGLASS);
					
					/*
					 * This swing worker performs heavy computations in background and
					 * allows the user to continue working on other swing components.
					 */
					
					new SwingWorker<JoinTree, Object>() {
						
						@Override
						protected JoinTree doInBackground() throws Exception {
							tree.distribute();
							return tree;
						}
						
						@Override
						protected void done() {
							try {
								update(get());
								viewer.repaintFrames();
								setCursor(Viewer.NORMAL);
							} catch (Exception e) {
								viewer.error("SwingWorker Exception", e);
							} 
						}
					}.execute();
				}
			});
		}
		return distributeButton;
	}
	
    /**
     * Updates the buttons within this container.
     * @param tree The jointree that is currently selected.
     */
	
    protected void updateButtons(JoinTree tree) {
		if(tree == null) {
			getCollectButton().setEnabled(false);
			getDistributeButton().setEnabled(false);
		} else {
			getCollectButton().setEnabled(!tree.isCollected());
			getDistributeButton().setEnabled(!tree.isDistributed() && tree.isCollected());
		}
    }

}
