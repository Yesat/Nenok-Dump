package gui.control;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.MatteBorder;

import gui.Viewer;
import gui.dialogs.ParserDialog;
import nenok.Knowledgebase;

/**
 * Control panel that lists local knowledgebases.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 566 $<br/>$LastChangedDate: 2008-03-26 14:40:58 +0100 (Mi, 26 Mrz 2008) $
 */

public class LocalPanel extends JPanel {
	
	private Viewer viewer;
	private JScrollPane scroller = null;
	private JList<Knowledgebase> kbList = null;
	private JButton newButton = null;
	private ParserDialog dialog;

	/**
	 * Constructor:
	 */
	
	public LocalPanel() {
		super();
		initialize();
	}
	
	/**
	 * Constructor:
	 * @param viewer The main application.
	 */
	
	public LocalPanel(Viewer viewer) {
		super();
		this.viewer = viewer;
		initialize();	
	}
	
	/**
	 * Adds user knowledgebases to the internal list.
	 * @param kbs The knowledgebases to add.
	 */
	
	public void addKnowledgebases(Knowledgebase... kbs) {
		DefaultListModel<Knowledgebase> model = (DefaultListModel<Knowledgebase>)kbList.getModel();
		for(Knowledgebase kb : kbs) {
			model.addElement(kb);
		}
		if(kbs.length > 0) {
			kbList.setSelectedIndex(0);
		}
	}

	/**
	 * Changes selection in the internal knowledgebase list.
	 * @param visible <code>true</code> if this component is currently visible.
	 */
	
	public void changeSelection(boolean visible) {
		if(!visible) {
			kbList.clearSelection();
			viewer.getKnowledgebasePanel().updateTable(null);
		} else if(kbList.getModel().getSize() > 0) {
			kbList.setSelectedIndex(0);
			Knowledgebase kb = kbList.getModel().getElementAt(0);
			viewer.getKnowledgebasePanel().updateTable(kb);
		}
	}
	
	/**
	 * Initializes all graphical components.
	 */
	
	private void initialize() {
		GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
		gridBagConstraints1.gridx = 1;
		gridBagConstraints1.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints1.insets = new java.awt.Insets(10,0,10,10);
		gridBagConstraints1.anchor = java.awt.GridBagConstraints.NORTH;
		gridBagConstraints1.gridy = 1;
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(10,10,10,10);
		this.setLayout(new GridBagLayout());
		this.setSize(300, 200);
		this.add(getScroller(), gridBagConstraints);
		this.add(getNewButton(), gridBagConstraints1);
	}

	/**
	 * @return The list scroller.
	 */
	
	private JScrollPane getScroller() {
		if (scroller == null) {
			scroller = new JScrollPane();
			scroller.setViewportView(getList());
		}
		return scroller;
	}

	/**
	 * @return The list of knowledgebases.
	 */
	
	private JList<?> getList() {
		if (kbList == null) {
			kbList = new JList<Knowledgebase>(new DefaultListModel<Knowledgebase>());
			kbList.setBorder(new MatteBorder(5,5,5,5,kbList.getBackground()));
			kbList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
			kbList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
				public void valueChanged(javax.swing.event.ListSelectionEvent e) {
					if(kbList.getSelectedValue() != null) {
						Knowledgebase kb = kbList.getSelectedValue();
						viewer.getKnowledgebasePanel().updateTable(kb);
					}
				}
			});
		}
		return kbList;
	}

	/**
	 * @return The button to parse local knowledgebases.
	 */
	
	private JButton getNewButton() {
		if (newButton == null) {
			newButton = new JButton();
			newButton.setText("...");
			newButton.setFocusPainted(false);
			newButton.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12));
			newButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(dialog == null) {
						dialog = new ParserDialog(viewer, ParserDialog.Mode.Knowledgebase);
					} else {
						dialog.setVisible(true);
					}
					Knowledgebase kb = dialog.getKnowledgebase();
					dialog.setVisible(false);
					if(kb != null) {
						DefaultListModel<Knowledgebase> model = (DefaultListModel<Knowledgebase>)kbList.getModel();
						model.addElement(kb);
						int index = model.indexOf(kb);
						kbList.setSelectedIndex(index);
						kbList.scrollRectToVisible(kbList.getCellBounds(index,index));
					}
				}
			});
		}
		return newButton;
	}
}
