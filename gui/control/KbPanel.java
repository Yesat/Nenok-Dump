package gui.control;

import gui.Viewer;
import gui.dialogs.NetworkDialog;
import gui.dialogs.QueryDialog;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.HashSet;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import nenok.Architecture;
import nenok.Knowledgebase;
import nenok.LCFactory;
import nenok.lc.JoinTree;
import nenok.va.Domain;

import org.jdesktop.swingworker.SwingWorker;

/**
 * Control panel that displays knowledgebase infos.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 566 $<br/>$LastChangedDate: 2008-03-26 14:40:58 +0100 (Mi, 26 Mrz 2008) $
 */

public class KbPanel extends JPanel {
	
	private Knowledgebase kb;
	private Hashtable<Knowledgebase, HashSet<Domain>> knowledgebases;
	
	private final Viewer viewer;
	private final LCFactory factory = new LCFactory();
	
	private JTable kbTable = null;
	private JComboBox<?> archCombo = null;
	private JButton queryButton = null;
	private JButton jtButton = null;
	private JButton netButton = null;
	
	/**
	 * Constructor:
	 * @param viewer The main application.
	 */
	
	public KbPanel(Viewer viewer) {
		super();
		this.viewer = viewer;
		knowledgebases = new Hashtable<Knowledgebase, HashSet<Domain>>();
		initialize();
		updateTable(null);
	}	
	
	/**
	 * @return The knowledgebase information table.
	 */
	
	private JTable getTable() {
		if (kbTable == null) {
			kbTable = new JTable();
	        kbTable.setModel(new javax.swing.table.DefaultTableModel(
	            new Object [][] {
	                {"Size:", "-"},
	                {"Domain Size:", "-"},
	                {"Content Type:", "-"}
	            },
	            new String [] {
	                "Title 1", "Title 2"
	            }
	        ) {

	        	/**
	        	 * @see javax.swing.table.DefaultTableModel#isCellEditable(int, int)
	        	 */
	        	
	            public boolean isCellEditable(int rowIndex, int columnIndex) {
	                return false;
	            }
	            
	        });
	        kbTable.setAutoscrolls(false);
	        kbTable.setEditingColumn(2);
	        kbTable.setEditingRow(3);
	        kbTable.setFocusable(false);
	        kbTable.setGridColor(new java.awt.Color(0, 0, 0));
	        kbTable.setRowSelectionAllowed(false);
	        kbTable.setShowHorizontalLines(false);
	        kbTable.setShowVerticalLines(false);
	        kbTable.setBackground(this.getBackground());
		}
		return kbTable;
	}

	/**
	 * @return The architecture combobox.	
	 */
	
	private JComboBox<?> getArchCombo() {
		if (archCombo == null) {
			archCombo = new JComboBox<Architecture>(Architecture.values());
			archCombo.setRequestFocusEnabled(false);
			archCombo.setFocusable(false);
		}
		return archCombo;
	}

	/**
	 * @return The query button.
	 */
	
	private JButton getQueryButton() {
		if (queryButton == null) {
			queryButton = new JButton();
			queryButton.setText("Queries");
			queryButton.setFocusPainted(false);
			queryButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if(kb == null) {
			    		return;
			    	}

					QueryDialog cp = new QueryDialog(viewer, kb.getDomain(), knowledgebases.get(kb));

					if(cp.getQueryList() != null) {
						knowledgebases.get(kb).clear();
						knowledgebases.get(kb).addAll(cp.getQueryList());					
					}					
				}
			});
		}
		return queryButton;
	}

	/**
	 * @return The jointree button.	
	 */
	
	private JButton getJointreeButton() {
		if (jtButton == null) {
			jtButton = new JButton();
			jtButton.setText("Build Join Tree");
			jtButton.setFocusPainted(false);
			jtButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					
					factory.setArchitecture((Architecture)(archCombo.getSelectedItem()));							
					setCursor(Viewer.HOURGLASS);
					jtButton.setEnabled(false);
					queryButton.setEnabled(false);
					netButton.setEnabled(false);
					
					/*
					 * This swing worker performs heavy computations in background and
					 * allows the user to continue working on other swing components.
					 */
					
					new SwingWorker<JoinTree, Object>() {
						
						@Override
						protected JoinTree doInBackground() throws Exception {
							return factory.create(kb, knowledgebases.get(kb));
						}
						
						@Override
						protected void done() {
							try {
								viewer.getJointreePanel().addToTree(kb.toString(), get());
							} catch (Exception e) {
								viewer.error("SwingWorker Exception", e);
							}
							
							setCursor(Viewer.NORMAL);
							jtButton.setEnabled(true);
							queryButton.setEnabled(true);
							netButton.setEnabled(true);
						}
					}.execute();					
				}
			});
		}
		return jtButton;
	}

	/**
	 * @return The network button.
	 */
	
	private JButton getNetworkButton() {
		if (netButton == null) {
			netButton = new JButton();
			netButton.setText("Network");
			netButton.setFocusPainted(false);
			netButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {						
					Thread thread = new Thread(new Runnable() {
						private JInternalFrame frame;
						public void run() {
							setCursor(Viewer.HOURGLASS); 
							frame = new NetworkDialog(kb, viewer);
							viewer.addDrawingFrame(frame);
							frame.setVisible(true);
							setCursor(Viewer.NORMAL);
						}
					});
					SwingUtilities.invokeLater(thread);
				}
			});
		}
		return netButton;
	}

	/**
	 * Updates the knowledgebase information table:
	 * @param kb The knowledgebase whose information will be displayed.
	 */
	
	public void updateTable(Knowledgebase kb) {
		
		this.kb = kb;
				
		kbTable.setValueAt("-", 0,1);
        kbTable.setValueAt("-", 1,1);
        kbTable.setValueAt("-", 2,1);
                
        if(kb == null) {  
            netButton.setEnabled(false);
            jtButton.setEnabled(false);
            queryButton.setEnabled(false);
            return;
        } 
        
		if(!knowledgebases.containsKey(kb)) {
			knowledgebases.put(kb, new HashSet<Domain>());
		}
        
        netButton.setEnabled(true);
        jtButton.setEnabled(true);
        queryButton.setEnabled(true);
  
        kbTable.setValueAt(kb.size(), 0,1);
        kbTable.setValueAt(kb.getDomain().size(), 1,1);
        try {
			if(kb.getType() != null) {
			    kbTable.setValueAt(kb.getType().getSimpleName(), 2,1);
			}
		} catch (Exception e) {
			viewer.error("Communication Problem.", e);
			kbTable.setValueAt("-", 2,1);
		} 
	}

	/**
	 * Initializes this component:
	 */
	
	private void initialize() {
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.ipadx = 0;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.insets = new java.awt.Insets(5,10,5,10);
		gridBagConstraints.weightx = 1.0;
		this.add(getTable(), gridBagConstraints);
		
		GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
		gridBagConstraints2.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints2.gridx = 0;
		gridBagConstraints2.gridy = 2;
		gridBagConstraints2.insets = new java.awt.Insets(5,10,5,5);
		gridBagConstraints2.weightx = 1.0;
		this.add(getArchCombo(), gridBagConstraints2);
		
		GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
		gridBagConstraints3.gridx = 1;
		gridBagConstraints3.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints3.insets = new java.awt.Insets(5,5,5,10);
		gridBagConstraints3.gridy = 2;
		this.add(getQueryButton(), gridBagConstraints3);
		
		GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
		gridBagConstraints5.gridy = 3;
		gridBagConstraints5.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints5.insets = new java.awt.Insets(5,10,10,5);
		gridBagConstraints5.gridx = 0;
		this.add(getJointreeButton(), gridBagConstraints5);
		
		GridBagConstraints gridBagConstraints6 = new GridBagConstraints();
		gridBagConstraints6.gridx = 1;
		gridBagConstraints6.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints6.insets = new java.awt.Insets(5,5,10,10);
		gridBagConstraints6.gridy = 3;
		this.add(getNetworkButton(), gridBagConstraints6);
	}
}
