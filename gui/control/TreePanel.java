package gui.control;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.border.MatteBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.jdesktop.swingworker.SwingWorker;

import gui.Viewer;
import gui.dialogs.FlatDialog;
import gui.dialogs.HyperDialog;
import gui.dialogs.ParserDialog;
import nenok.Knowledgebase;
import nenok.lc.JoinTree;

/**
 * This is the control panel unit for join trees.
 * 
 * @author Marc Pouly
 */

public class TreePanel extends JPanel {
	
	private final Viewer viewer;
	private JScrollPane scroller = null;
	private JTree jtTree = null;
	private JTable table = null;
	private JPopupMenu popup = null;
	private JMenuItem removeItem = null;
	private JButton newButton = null;
	private JComboBox<Modes> combo = null;
	private JButton showButton = null;
	private JPanel buttonPanel = null;
	
	private enum Modes {
		
		/**
		 * Tree mode:
		 */
		
		Tree, 
		
		/**
		 * Hypergraph mode:
		 */
		
		Hypergraph}
		
	/**
	 * Constructor:
	 * @param viewer The main frame of the application.
	 */
	
	public TreePanel(Viewer viewer) {
		super();
		this.viewer = viewer;
		initialize();
		updateTable(null);
		
		/*
		 * Popup Menu:
		 */
		
		popup = new JPopupMenu();
		
		removeItem = new JMenuItem("Remove");
		removeItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Object obj = getTree().getLastSelectedPathComponent();
				if(obj != null) {
					DefaultMutableTreeNode node = (DefaultMutableTreeNode)obj;
					DefaultMutableTreeNode parent = (DefaultMutableTreeNode)node.getParent();
			        DefaultTreeModel model = (DefaultTreeModel)jtTree.getModel();
			        model.removeNodeFromParent(node);
			        if(parent.getChildCount() == 0) {
			        	model.removeNodeFromParent(parent);
			        }
				}
			} 
		});
		
		popup.add(removeItem);	
	}

	/**
	 * Initializes this panel.
	 */
	
	private void initialize() {
		GridBagConstraints gridBagConstraints6 = new GridBagConstraints();
		gridBagConstraints6.gridx = 0;
		gridBagConstraints6.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints6.gridwidth = 3;
		gridBagConstraints6.insets = new java.awt.Insets(5,10,10,10);
		gridBagConstraints6.anchor = java.awt.GridBagConstraints.SOUTH;
		gridBagConstraints6.gridy = 6;
		GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
		gridBagConstraints3.gridx = 2;
		gridBagConstraints3.anchor = java.awt.GridBagConstraints.NORTH;
		gridBagConstraints3.insets = new java.awt.Insets(10,5,0,10);
		gridBagConstraints3.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints3.gridy = 1;
		GridBagConstraints gridBagConstraints51 = new GridBagConstraints();
		gridBagConstraints51.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints51.weighty = 0.5;
		gridBagConstraints51.gridx = 0;
		gridBagConstraints51.gridy = 5;
		gridBagConstraints51.insets = new java.awt.Insets(5,10,5,10);
		gridBagConstraints51.gridwidth = 3;
		gridBagConstraints51.anchor = java.awt.GridBagConstraints.CENTER;
		gridBagConstraints51.weightx = 1.0;
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weighty = 0.5;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridheight = 1;
		gridBagConstraints.insets = new java.awt.Insets(10,10,0,5);
		gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
		gridBagConstraints.gridwidth = 1;
		gridBagConstraints.weightx = 1.0;
		this.setLayout(new GridBagLayout());
		this.setSize(300, 300);
		this.setMinimumSize(new java.awt.Dimension(342,300));
		this.add(getScroller(), gridBagConstraints);
		this.add(getTable(), gridBagConstraints51);
		this.add(getNewButton(), gridBagConstraints3);
		this.add(getButtonPanel(), gridBagConstraints6);
	}

	/**
	 * @return The scroller of the jointree list.
	 */
	
	private JScrollPane getScroller() {
		if (scroller == null) {
			scroller = new JScrollPane();
			scroller.setPreferredSize(new java.awt.Dimension(0,120));
			scroller.setMinimumSize(new java.awt.Dimension(0,120));
			scroller.setViewportView(getTree());
		}
		return scroller;
	}

	/**
	 * @return The jointree list.
	 */
	
	private JTree getTree() {
		if (jtTree == null) {
			jtTree = new JTree();
			jtTree.putClientProperty("JTree.lineStyle", "None");
			jtTree.setBorder(new MatteBorder(5,5,5,5,jtTree.getBackground()));
			jtTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
			jtTree.setBorder(new MatteBorder(3,3,3,3,jtTree.getBackground()));
	        DefaultTreeModel model = (DefaultTreeModel)jtTree.getModel();
	        DefaultMutableTreeNode root = new DefaultMutableTreeNode("");
	        jtTree.setRootVisible(false);
	        model.setRoot(root);
	        jtTree.scrollPathToVisible(new TreePath(root.getPath()));
			jtTree.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
				public void valueChanged(javax.swing.event.TreeSelectionEvent e) {
					JoinTree tree = getTreeSelection();
					updateButtons(tree);
					updateTable(tree);
					viewer.getStatisticPanel().update(tree);
					viewer.getStatisticPanel().updateButtons(tree);
				}
			});
	 
			jtTree.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					int mask = InputEvent.BUTTON1_MASK - 1;
			        int mods = e.getModifiers() & mask;
			        if(mods != 0 && getTreeSelection() != null) {
			        	popup.show(e.getComponent(), e.getX(), e.getY());
			        }
				}
			});
		}
		return jtTree;
	}

	/**
	 * @return The table displaying jointree infos.
	 */
	
	private JTable getTable() {
		if (table == null) {
			table = new JTable();			
	        table.setModel(new javax.swing.table.DefaultTableModel(
	            new Object [][] {
	                {"Vertices:", null},
	                {"Treewidth:", null},
	                {"Construction Time:", null},
	                {"Construction Algo:", null}
	            },
	            new String [] {
	                "Title 1", "Title 2"
	            }
	        ){
	        	
	        	/**
	        	 * @see javax.swing.table.DefaultTableModel#isCellEditable(int, int)
	        	 */
	        	
	        	@Override
	        	public boolean isCellEditable(int arg0, int arg1) {
	        		return false;
	        	}
	        	
	        });
	        table.setFocusable(false);
	        table.setRowSelectionAllowed(false);
	        table.setShowHorizontalLines(false);
	        table.setShowVerticalLines(false);
	        table.setBackground(this.getBackground());
		}
		return table;
	}
	
    /**
     * Adds a new jointree to the jointree list.
     * @param kb The knowledgebase.
     * @param tree The jointree to add.
     */

	public void addToTree(Knowledgebase kb, JoinTree tree) {
		addToTree(kb.toString(), tree);
	}
	
    /**
     * Adds a new jointree to the jointree list.
     * @param kb The name of the knowledgebase.
     * @param tree The jointree to add.
     */
	
    public void addToTree(String kb, JoinTree tree) {
        
        DefaultTreeModel model = (DefaultTreeModel)jtTree.getModel();
        DefaultMutableTreeNode root = (DefaultMutableTreeNode)model.getRoot();
        
        for(int i = 0; i < root.getChildCount(); i++) {
        	String name = (String)((DefaultMutableTreeNode)root.getChildAt(i)).getUserObject();
       
            if(name.equals(kb)) {
                DefaultMutableTreeNode node = new DefaultMutableTreeNode(tree);
                model.insertNodeInto(node, (DefaultMutableTreeNode)root.getChildAt(i), root.getChildAt(i).getChildCount());
                TreePath path = new TreePath(node.getPath());
                getTree().scrollPathToVisible(path);
                getTree().setSelectionPath(path);
                return;
            }
        }
        
        DefaultMutableTreeNode node = new DefaultMutableTreeNode(kb);
        model.insertNodeInto(node, root, root.getChildCount());
        addToTree(kb, tree);
    }
    
    /**
     * @return The currently selected jointree.
     */
    
    public JoinTree getTreeSelection() {
    	DefaultMutableTreeNode node = (DefaultMutableTreeNode)getTree().getLastSelectedPathComponent();
    	if(node == null) {
    		return null;
    	}
    	Object select = node.getUserObject();
    	return (select instanceof JoinTree) ? (JoinTree)select : null;
    }
    
    /**
     * Updates the buttons within this container.
     * @param tree The jointree that is currently selected.
     */
    
    private void updateButtons(JoinTree tree) {
    	getShowButton().setEnabled(tree != null);
    }
    
    /**
     * Updates the jointree information table.
     * @param tree The jointree that is currently selected.
     */
    
	private void updateTable(final JoinTree tree) {
		if(tree == null) {			
            table.setValueAt("-", 0,1);
            table.setValueAt("-", 1,1);
            table.setValueAt("-", 2,1);
            table.setValueAt("-", 3,1);
            return;
		}
		
		showButton.setEnabled(false);
				
		/*
		 * This swing worker performs heavy computations in background and
		 * allows the user to continue working on other swing components.
		 */
		
		new SwingWorker<JoinTree, Object>() {
			
			private int nodes;
			private int domain;
			
			@Override
			protected JoinTree doInBackground() throws Exception {
				nodes = tree.countNodes();
				domain = tree.getTreeWidth();
				return tree;
			}
			
			@Override
			protected void done() {
		        table.setValueAt(nodes, 0,1);
		        table.setValueAt(domain, 1,1);
		        table.setValueAt(tree.getConstructionAlgorithm().getConstructionTime()+" ms", 2,1);
		        table.setValueAt(tree.getConstructionAlgorithm().getName(), 3,1); 
				showButton.setEnabled(true);
			}
		}.execute();
	}
	
	/**
	 * @return The button to import jointrees.
	 */
	
	private JButton getNewButton() {
		if (newButton == null) {
			newButton = new JButton();
			newButton.setText("...");
			newButton.setFocusPainted(false);
			newButton.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12));
			newButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					ParserDialog dialog = new ParserDialog(viewer, ParserDialog.Mode.JoinTree);
					JoinTree tree = dialog.getJoinTree();
					if(tree != null) {
						addToTree(dialog.getFileName(), dialog.getJoinTree());
					}
				}
			});
		}
		return newButton;
	}

	/**
	 * @return Combobox that shows different display modes.	
	 */
	
	private JComboBox<Modes> getCombo() {
		if (combo == null) {
			combo = new JComboBox<Modes>(Modes.values());
			combo.setRequestFocusEnabled(false);
			combo.setFocusable(false);
		}
		return combo;
	}

	/**
	 * @return The button to display jointrees.
	 */
	
	private JButton getShowButton() {
		if (showButton == null) {
			showButton = new JButton();
			showButton.setText("Show");
			showButton.setFocusPainted(false);
			showButton.setEnabled(false);
			showButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					final JoinTree tree = getTreeSelection();
					if(tree == null) {
						return;
					}
					Modes d = (Modes)combo.getSelectedItem();
					if(d.equals(Modes.Tree)) {
						Thread thread = new Thread(new Runnable() {
							private JInternalFrame frame;
							public void run() {
								setCursor(Viewer.HOURGLASS);
								frame = new FlatDialog(tree, viewer);
								viewer.addDrawingFrame(frame);
								frame.setVisible(true);
								setCursor(Viewer.NORMAL);
							}
						});
						SwingUtilities.invokeLater(thread);
					}
					if(d.equals(Modes.Hypergraph)) {
						JInternalFrame frame = new HyperDialog(tree, viewer);
						frame.setVisible(true);
						viewer.addDrawingFrame(frame);
					}
				}
			});
		}
		return showButton;
	}

	/**
	 * @return The button panel.	
	 */
	
	private JPanel getButtonPanel() {
		if (buttonPanel == null) {
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
			gridBagConstraints2.insets = new java.awt.Insets(0,5,0,0);
			gridBagConstraints2.gridy = 0;
			gridBagConstraints2.fill = java.awt.GridBagConstraints.VERTICAL;
			gridBagConstraints2.anchor = java.awt.GridBagConstraints.CENTER;
			gridBagConstraints2.gridx = 1;
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.fill = java.awt.GridBagConstraints.BOTH;
			gridBagConstraints1.gridx = 0;
			gridBagConstraints1.gridy = 0;
			gridBagConstraints1.weightx = 1.0;
			gridBagConstraints1.insets = new java.awt.Insets(0,0,0,5);
			buttonPanel = new JPanel();
			buttonPanel.setLayout(new GridBagLayout());
			buttonPanel.add(getCombo(), gridBagConstraints1);
			buttonPanel.add(getShowButton(), gridBagConstraints2);
		}
		return buttonPanel;
	}
}
