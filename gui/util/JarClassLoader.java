package gui.util;

import gui.dialogs.ParserDialog;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import nenok.Utilities;
import nenok.parser.JT_Parser;
import nenok.parser.KB_Parser;
import nenok.parser.Parser;

/**
 * Loads a class dynamically from a JAR file.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 562 $<br/>$LastChangedDate: 2008-03-26 14:38:24 +0100 (Mi, 26 Mrz 2008) $
 */

public class JarClassLoader extends MultiClassLoader {

	private String name;
	private JarResources resources;
	
	/**
	 * Constructor:
	 * @param jar The name of the jar file.
	 */
	
	public JarClassLoader(String jar) {
		this.name = jar;
		resources = new JarResources(jar);
	}
	
	/**
	 * 
	 */
	
	protected byte[] loadClassBytes(String className) {
		className = formatClassName (className);
		return (resources.getResource (className));
	}
	
	/**
	 * @param file The file for which a parser is searched.
	 * @param mode Indicates either knowledgebase or tree parsing.
	 * @return The {@link Parser} class that has been loaded dynamically.
	 * @throws IOException Exception caused by stream creation.
	 * @throws ClassNotFoundException Exception caused when the JAR file is not found.
	 */
	
	public Parser loadParser(File file, ParserDialog.Mode mode) throws IOException, ClassNotFoundException {
		
		/*
		 * Read in jar file.
		 */
		
		ZipFile zf = new ZipFile(name);
		Enumeration<?> e = zf.entries();
		
		Hashtable<String, Integer> content = new Hashtable<String, Integer>();
		
		while (e.hasMoreElements()) {
			ZipEntry ze=(ZipEntry)e.nextElement();
			content.put(ze.getName(),new Integer((int)ze.getSize()));
		}
		zf.close();
		
		ArrayList<String> names = new ArrayList<String>();

		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(name));
		ZipInputStream zis = new ZipInputStream(bis);
		ZipEntry ze = null;
		while ((ze = zis.getNextEntry())!=null) {
			if (!ze.isDirectory()) {
				names.add(ze.getName());
			}
		}
		
		zis.close();
		
		for(String name : names) {
			if(name.endsWith(".class")) {
				name = name.replace(".class", "");
				name = name.replace(System.getProperty("file.separator"), ".");
				name = name.replace("/", ".");
				Class<?> cl = this.loadClass(name);
				
				/*
				 * For knowledgebases:
				 */
				
				if(mode == ParserDialog.Mode.Knowledgebase && Utilities.interfaceOf(KB_Parser.class, cl)) {
					try {
						Parser parser = (Parser)cl.newInstance();
						if(parser.accept(file)) {
							return parser;
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					} 
				}
				
				/*
				 * For jointrees:
				 */
				
				if(mode == ParserDialog.Mode.JoinTree && Utilities.interfaceOf(JT_Parser.class, cl)) {
					try {
						Parser parser = (Parser)cl.newInstance();
						if(parser.accept(file)) {
							return parser;
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					} 
				}				
			}
		}
		
		throw new ClassNotFoundException("No parser found that accepts the given file.");
	}
}