package gui.util;

import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.Icon;
import javax.swing.JPanel;

/**
 * JPanel that displays GIF files.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 562 $<br/>$LastChangedDate: 2008-03-26 14:38:24 +0100 (Mi, 26 Mrz 2008) $
 */

public class ImageCanvas extends JPanel {

	private Icon icon;
			
	/**
	 * Constructor:
	 */
	
	public ImageCanvas() {
		super();
	}
	
	/**
	 * Constructor:
	 * @param icon The icon to display.
	 */

	public ImageCanvas(Icon icon) {
		super();
		this.icon = icon;
		this.setOpaque(true);
	}
		
	/**
	 * 
	 */

    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D)g.create(); 
        if (icon != null) {
            Composite oldComposite = g2d.getComposite();
            icon.paintIcon(this, g2d, 0, 0);
            g2d.setComposite(oldComposite);
        }
        g2d.dispose();
    }
}
