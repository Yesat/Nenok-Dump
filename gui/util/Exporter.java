package gui.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import nenok.Knowledgebase;
import nenok.lc.JoinTree;
import nenok.lc.Node;
import nenok.va.Valuation;
import nenok.va.Variable;

/**
 * Exports a jointree image into a file.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 562 $<br/>$LastChangedDate: 2008-03-26 14:38:24 +0100 (Mi, 26 Mrz 2008) $
 */

public class Exporter {
	
	/**
	 * Saves the jointree in DOT fomat.
	 * @param file The file to which the tree is exported.
	 * @param tree The join tree to export.
	 * @param color Color filled nodes.
	 * @throws IOException Exception caused by file access problems.
	 */
	
	public static void saveTreeAsDOT(File file, JoinTree tree, boolean color) throws IOException {
		
		if(tree == null) {
			return;
		}
		
		PrintStream writer = new PrintStream(file);
		String result = "digraph Jointree {\n";
		ArrayList<Node> nodes = new ArrayList<Node>();
		
		for(Node node : tree) {
			nodes.add(node);
		}
		
		// Draw nodes & links:
		for (int i = 0; i < nodes.size(); i++) {
			if (nodes.get(i).getChild() == null) {
				continue;
			}
			result += "\t" + nodes.indexOf(nodes.get(i).getChild()) + " -> " + i
					+ " [arrowhead=none];\n";
		}
		
		// Draw labels:
		for (int i = 0; i < nodes.size(); i++) {
			result += "\t"
						+ i
						+ " ["
						+ "label=\""
						+ nodes.get(i).getLabel()
						+ "\", ";
			if(color && nodes.get(i).isFull()) {
				result += "style=filled, fillcolor=gold2, ";
			}
			result += "fontsize=12" + "];\n";
		}
		result += "}";
		writer.print(result);
		writer.close();
	}
	
	/**
	 * Saves the valuation network in DOT fomat.
	 * @param file The file to which the network is exported.
	 * @param kb The knowledgebase to create the network
	 * @throws Exception Exception caused by file access problems.
	 */

	public static void saveNetworkAsDOT(File file, Knowledgebase kb) throws Exception {
		
		if(kb == null) {
			return;
		}
		
		PrintStream writer = new PrintStream(file);
		String result = "digraph ValNet {\n";
				
		List<Variable> variables = kb.getDomain().asList();
		Valuation[] valuations = kb.toArray();
		
		// Draw valuation-variable links:
		for(int i = 0; i < valuations.length; i++) {
			for(Variable var : valuations[i].label()) {
				result += "\t" + i + " -> " + "v"+ variables.indexOf(var)
				+ " [arrowhead=none];\n";
			}
		}
		
		// Draw variables:
		for (int i = 0; i < variables.size(); i++) {
			result += "\t"
						+ "v"+i
						+ " ["
						+ "label=\""
						+ variables.get(i)
						+ "\", "
					    + "style=filled, "
					    + "shape=circle, "
						+ "fontsize=12" + "];\n";
		}
		
		// Draw valuations:
		for (int i = 0; i < valuations.length; i++) {
			result += "\t"
						+ i
						+ " ["
						+ "label=\""
						+ "Val " + i
						+ "\", "
					    + "style=filled, "
					    + "shape=box, "
						+ "fontsize=12" + "];\n";
		}
		
		result += "}";
		writer.print(result);
		writer.close();
	}
}
