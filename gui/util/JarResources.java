package gui.util;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * JarResources: JarResources maps all resources included in a Zip or Jar file. 
 * Additionaly, it provides a method to extract one as a blob.
 * 
 * @author Jack Harich - 8/18/97
 * @author John D. Mitchell - 99.03.04
 * @author Marc Pouly
 * @version $LastChangedRevision: 562 $<br/>$LastChangedDate: 2008-03-26 14:38:24 +0100 (Mi, 26 Mrz 2008) $
 */

public final class JarResources {

    // JAR resource mapping tables
    private Hashtable<String, Integer> htSizes=new Hashtable<String, Integer>();  
    private Hashtable<String, byte[]> htJarContents = new Hashtable<String, byte[]>();

    private String jarFileName;
    
    /** 
     * Constructor:
     * It extracts all resources from a JAR into an internal hashtable, keyed by resource names.
     * @param jar A JAR or ZIP file.
     */
    
    public JarResources(String jar) {
    	this.jarFileName=jar;
    	init();
	}

    /**
     * Extracts a jar resource as a blob.
     * @param name a resource name.
     * @return The ressource as byte array.
     */
    
    public byte[] getResource(String name) {
    	return htJarContents.get(name);
	}
    
    /**
     * Initializes internal hash tables with Jar file resources.
     */

    private void init() {
    	try {
	    
	    	// Extracts just sizes only. 
		    ZipFile zf = new ZipFile(jarFileName);
		    Enumeration<?> e=zf.entries();
		    while (e.hasMoreElements()) {
		    	ZipEntry ze=(ZipEntry)e.nextElement();
		    	htSizes.put(ze.getName(),new Integer((int)ze.getSize()));
			}
		    zf.close();

		    // Extract resources and put them into the hashtable.
		    FileInputStream fis = new FileInputStream(jarFileName);
		    BufferedInputStream bis = new BufferedInputStream(fis);
		    ZipInputStream zis = new ZipInputStream(bis);
		    ZipEntry ze = null;
		    while ((ze=zis.getNextEntry()) != null) {
				if (ze.isDirectory()) {
				    continue;
				}
		
				int size=(int)ze.getSize(); // -1 means unknown size.
				if (size == -1) {
				    size=(htSizes.get(ze.getName())).intValue();
				}
	
				byte[] b = new byte[size];
				int rb = 0;
				int chunk = 0;
				while ((size - rb) > 0){
				    chunk = zis.read(b,rb,size - rb);
				    if (chunk == -1) {
				    	break;
					}
				    rb+=chunk;
				}
	
				// Add to internal resource hashtable
				htJarContents.put(ze.getName(),b);
		    }
		    
		    zis.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
