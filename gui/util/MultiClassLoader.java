package gui.util;

import java.util.Hashtable;

/**
 * A simple test class loader capable of loading from multiple sources, such as
 * local files or a URL. This class is derived from an article by Chuck McManis
 * http://www.javaworld.com/javaworld/jw-10-1996/indepth.src.html with large
 * modifications.
 * 
 * @author Jack Harich - 8/18/97
 * @author John D. Mitchell - 99.03.04
 * @author Marc Pouly
 * @version $LastChangedRevision: 562 $<br/>$LastChangedDate: 2008-03-26 14:38:24 +0100 (Mi, 26 Mrz 2008) $
 */

public abstract class MultiClassLoader extends ClassLoader {

	private char classNameReplacementChar;
	private Hashtable<String, Class<?>> classes = new Hashtable<String, Class<?>>();

	/**
	 * Constructor:
	 */

	public MultiClassLoader() {}

	/**
	 * This is a simple version for external clients since they will always want
	 * the class resolved before it is returned to them.
	 */

	public Class<?> loadClass(String className) throws ClassNotFoundException {
		return (loadClass(className, true));
	}

	/**
	 * This is a simple version for external clients since they will always want
	 * the class resolved before it is returned to them.
	 */

	public synchronized Class<?> loadClass(String className, boolean resolveIt) throws ClassNotFoundException {

		// Check our local cache of classes
		Class<?> result = classes.get(className);
		if (result != null) {
			return result;
		}

		// Check with the primordial class loader
		try {
			result = super.findSystemClass(className);
			return result;
		} catch (ClassNotFoundException e) {
			// Do nothing !!!
		}

		// Try to load it from preferred source:
		byte[] classBytes = loadClassBytes(className);
		if (classBytes == null) {
			throw new ClassNotFoundException();
		}

		// Define it (parse the class file)
		result = defineClass(className, classBytes, 0, classBytes.length);
		if (result == null) {
			throw new ClassFormatError();
		}

		// Resolve if necessary
		if (resolveIt)
			resolveClass(result);

		classes.put(className, result);
		return result;
	}	

	/**
	 * 
	 * @param className The name of the class.
	 * @return The class as byte array.
	 */
	
	protected abstract byte[] loadClassBytes(String className);

	/**
	 * Transforms the class name in Java format.
	 * @param className The name of the class.
	 * @return The class name in Java format.
	 */
	
	protected String formatClassName(String className) {
		if (classNameReplacementChar == '\u0000') {
			// '/' is used to map the package to the path
			return className.replace('.', '/') + ".class";
		}
		// Replace '.' with custom char, such as '_'
		return className.replace('.', classNameReplacementChar) + ".class";
	}
}
