package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.util.Enumeration;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.plaf.FontUIResource;

import gui.control.KbPanel;
import gui.control.LocalPanel;
import gui.control.StatisticPanel;
import gui.control.TreePanel;
import gui.dialogs.AboutDialog;
import gui.dialogs.Dialog;
import gui.dialogs.ErrorDialog;
import gui.dialogs.ValuationDialog;
import nenok.Knowledgebase;
import nenok.Utilities;
import nenok.lc.JoinTree;
import nenok.va.Valuation;
import net.infonode.gui.laf.InfoNodeLookAndFeel;

/**
 * Main component of the NENOK GUI.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 563 $<br/>$LastChangedDate: 2008-03-26 14:38:32 +0100 (Mi, 26 Mrz 2008) $
 */

public class Viewer extends JFrame {
	
	private Dimension screen;
	private JMenuBar menuBar = null;
	private JDesktopPane desktop = null;
	private KbPanel kbPanel = null;
	private JInternalFrame jtFrame = null;
	private TreePanel jtPanel = null;
	private StatisticPanel statPanel = null;
	private JMenu viewMenu = null;
	private JInternalFrame kbFrame = null;
	private JInternalFrame localFrame = null;
	private JInternalFrame statisticFrame = null;
	
	/**
	 * The hourglass cursor:
	 */

    public static final Cursor HOURGLASS = new Cursor(Cursor.WAIT_CURSOR);
    
    /**
     * The standard arrow cursor.
     */
    
    public static final Cursor NORMAL = new Cursor(Cursor.DEFAULT_CURSOR);
		
	/**
     * Static bloc to handle Look and Feel.
     */
    
    static {
        try {
        	
        	// Set look and feel:
            UIManager.put("ModelPanelUI","hypergraph.hyperbolic.ModelPanelUI");
            UIManager.setLookAndFeel(new InfoNodeLookAndFeel());
            
            // Reduces the font size of all components:
            for (Enumeration<?> e = UIManager.getDefaults().keys(); e.hasMoreElements();) {
				Object key = e.nextElement();
				Object value = UIManager.get(key);
				if (value instanceof Font) {
					Font f = (Font) value;
					UIManager.put(key, new FontUIResource(f.getName(), f.getStyle(), f.getSize() - 1));
				}
			}
            
            /*
			 * Adapt tree icons:
			 */
        	
            //ClassLoader loader = ClassLoader.getSystemClassLoader();
			//UIManager.put("Tree.openIcon", new ImageIcon(loader.getResource("open.gif")));
			//UIManager.put("Tree.closedIcon", new ImageIcon(loader.getResource("close.gif")));
            
        } catch(Exception e) {
            System.out.println("Look and Feel Initialitation Error.");
            System.exit(1);
        }
    }
    
    /**
     * Main method:
     * @param args Standard input parameter.
     */
    
    public static void main(String[] args) {
        try {
            Viewer viewer = new Viewer();
            viewer.setVisible(true);
            while(viewer.isShowing()) {
                Thread.sleep(3000);
            }  
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /*
     * Display methods:
     */
   
    /**
     * Static method to display the main frame.
     * @param kbs Knowledgebases that are initially contained in the viewer.
     */
    
    public static void display(Knowledgebase... kbs) {
    	try {			  
			Viewer frame = new Viewer();
			LocalPanel panel = (LocalPanel)frame.getLocalFrame().getContentPane();
			panel.addKnowledgebases(kbs);
			frame.setVisible(true);
			while(frame.isShowing()) {
				Thread.sleep(3000);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
        
    /**
     * Static method to display the main frame.
     * @param kbs Knowledgebases that are initially contained in the viewer.
     * @param trees Jointrees that are initially contained in the viewer.
     */
    
    public static void display(Knowledgebase[] kbs, JoinTree[] trees) {
    	try {			
			Viewer frame = new Viewer();
			
			TreePanel panel1 = (TreePanel)frame.getJointreeFrame().getContentPane();
			for(JoinTree tree : trees) {
				panel1.addToTree(tree.getConstructionData().knowledgebase.toString(), tree);
			}
			
			LocalPanel panel2 = (LocalPanel)frame.getLocalFrame().getContentPane();
			panel2.addKnowledgebases(kbs);
			
			frame.setVisible(true);
			while(frame.isShowing()) {
				Thread.sleep(3000);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    } 
    
    /**
     * Static method to display the main frame.
     * @param trees Jointrees that are initially contained in the viewer.
     */
    
    public static void display(JoinTree... trees) {
    	try {			
			Viewer frame = new Viewer();
			
			TreePanel panel1 = (TreePanel)frame.getJointreeFrame().getContentPane();
			for(JoinTree tree : trees) {
				panel1.addToTree(tree.getConstructionData().knowledgebase.toString(), tree);
			}
						
			frame.setVisible(true);
			while(frame.isShowing()) {
				Thread.sleep(3000);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    } 
    
	/**
	 * Internal Constructor:
	 * Use <code>Viewer.display(...)</code> instead.
	 */
    
	private Viewer() {
		super();
		initialize();
        screen = Toolkit.getDefaultToolkit().getScreenSize();
        
        // Compute default window size:
        this.setSize(
        		(int)(screen.width*0.95), 
        		(int)(screen.height*0.85));
        this.setLocation(
        		(screen.width - this.getWidth()) / 2, 
        		(screen.height - this.getHeight()) / 3);
        
        // Set window to maximum size at the beginning:
        this.setExtendedState(MAXIMIZED_BOTH);
	}
	
	/**
	 * This method initializes the main frame.
	 */
	
	private void initialize() {
		this.setJMenuBar(getMenu());
		this.setContentPane(getDesktop());
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setTitle("NENOK");
	}
	
	/**
	 * Initializes the menubar.
	 */
	
	private JMenuBar getMenu() {
		if (menuBar == null) {
			menuBar = new JMenuBar();
	        this.setJMenuBar(menuBar);
	        
	        JMenu fileMenu = new JMenu("File");
	        menuBar.add(fileMenu);
	        
			viewMenu = new JMenu("View");
			menuBar.add(viewMenu);		
						
			viewMenu.add(new JSeparator());
			
			JMenuItem arrange = new JMenuItem("Arrange Controls");
			arrange.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						kbFrame.setIcon(false);
						jtFrame.setIcon(false);
						statisticFrame.setIcon(false);
					} catch (PropertyVetoException e1) {}
					localFrame.setLocation(0,-1); 
					kbFrame.setLocation(0,getLocalFrame().getHeight()-2);
					jtFrame.setLocation(0, getLocalFrame().getHeight()+getKnowledgebaseFrame().getHeight()-3);
					statisticFrame.setLocation(0, getLocalFrame().getHeight()+getKnowledgebaseFrame().getHeight()+getJointreeFrame().getHeight()-4);
				}
			});
			viewMenu.add(arrange);
			
			/*
			 * Radio Buttons:
			 */
																	
	        //JMenu graphMenu = new JMenu("Graph");
	        //menuBar.add(graphMenu);       
	        JMenu helpMenu = new JMenu("Help");
	        menuBar.add(helpMenu);
	        
	        // Exit Menu:
	        JMenuItem exitMenuItem = new JMenuItem("Exit");
	        exitMenuItem.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent e) {
	                dispose();
	            }
	        });
	        fileMenu.add(exitMenuItem);
	        	        	                
	        // About Menu:
	        JMenuItem aboutMenuItem = new JMenuItem("About ...");
	        aboutMenuItem.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent e) {
	            	new AboutDialog(Viewer.this);
	            }
	        });
	        helpMenu.add(aboutMenuItem);
		}
		return menuBar;
	}
	
	/**
	 * This method initializes the desktop.
	 */
	
	private JDesktopPane getDesktop() {
		if (desktop == null) {
			desktop = new JDesktopPane();
			desktop.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
			desktop.add(getJointreeFrame(), null);
			desktop.add(getLocalFrame(), null);
			desktop.add(getKnowledgebaseFrame(), null);
			desktop.add(getStatisticFrame(), null);
		}
		return desktop;
	}
	
	/**
	 * Adds a new internal drawing frame to the desktop.
	 * @param frame The internal frame to add.
	 */
	
	public void addDrawingFrame(JInternalFrame frame) {
		for(Component c : desktop.getComponents()) {
			if(!(c instanceof JInternalFrame)) {
				continue;
			}
			JInternalFrame f = (JInternalFrame)c;
			if(Utilities.superclassOf(Dialog.class, f.getClass())) {
				try {
					f.setIcon(true);
				} catch (PropertyVetoException e) {}
			}
		}
				
		desktop.add(frame);
		try {
			frame.setSelected(true);
		} catch (PropertyVetoException e) {}
	}
		
	/**
	 * Adds a new internal valuation frame to the desktop.
	 * @param val The valuation to be displayed.
	 */
	
	public void addValuationFrame(Valuation val) {
		
		if(val == null) {
			return;
		}
		
		JInternalFrame frame = new ValuationDialog(val, this);
	    frame.setLocation((desktop.getWidth() - frame.getWidth()) / 2, 100);
		
		for(Component c : desktop.getComponents()) {
			if(c instanceof ValuationDialog) {
				JInternalFrame f = (JInternalFrame)c;
				f.dispose();
			}
		}
		
		desktop.add(frame);
		
		try {
			frame.setSelected(true);
		} catch (PropertyVetoException e) {}
		
    }
	
	/**
	 * Repaints all internal frames.
	 */
	
	public void repaintFrames() {
		for(JInternalFrame frame : desktop.getAllFrames()) {
			Container c = frame.getContentPane();
			c.repaint();
		}
	}
	
	/**
	 * @return The local knowledgebase controll frame.
	 */
	
	private JInternalFrame getLocalFrame() {
		if (localFrame == null) {
			localFrame = new JInternalFrame();
			localFrame.setLocation(0,-1);
			localFrame.setVisible(true);
			localFrame.setSize(300,120);
			localFrame.setTitle("Local Knowledgebase List");
			localFrame.setContentPane(new LocalPanel(this));
			localFrame.setBorder(new LineBorder(Color.BLACK));
		}
		return localFrame;
	}
	
	/**
	 * @return The knowledgebase controll frame.
	 */
	
	private JInternalFrame getKnowledgebaseFrame() {
		if (kbFrame == null) {
			kbFrame = new JInternalFrame();
			kbFrame.setBounds(new java.awt.Rectangle(0,0,0,0));
			kbFrame.setLocation(0,getLocalFrame().getHeight()-2);
			kbFrame.setVisible(true);
			kbFrame.setSize(300,150);
			kbFrame.setTitle("Knowledgebase Infos");
			kbFrame.setContentPane(getKnowledgebasePanel());
			kbFrame.setBorder(new LineBorder(Color.BLACK));
			kbFrame.setIconifiable(true);
		}
		return kbFrame;
	}
	
	/**
	 * @return The jointree control frame.
	 */
	
	private JInternalFrame getJointreeFrame() {
		if (jtFrame == null) {
			jtFrame = new JInternalFrame();
			jtFrame.setBounds(new java.awt.Rectangle(0,0,0,0));
			jtFrame.setTitle("Join Trees");
			jtFrame.setContentPane(getJointreePanel());
			jtFrame.setSize(300,260);
			jtFrame.setVisible(true);
			jtFrame.setBorder(new LineBorder(Color.BLACK));
			jtFrame.setLocation(0, getLocalFrame().getHeight()+getKnowledgebaseFrame().getHeight()-3);
			jtFrame.setIconifiable(true);
		}
		return jtFrame;
	}
	
	/**
	 * @return The jointree control frame.
	 */
	
	private JInternalFrame getStatisticFrame() {
		if (statisticFrame == null) {
			statisticFrame = new JInternalFrame();
			statisticFrame.setBounds(new java.awt.Rectangle(0,0,0,0));
			statisticFrame.setTitle("Runtime Statistic");
			statisticFrame.setContentPane(getStatisticPanel());
			statisticFrame.setSize(300,175);
			statisticFrame.setVisible(true);
			statisticFrame.setBorder(new LineBorder(Color.BLACK));
			statisticFrame.setLocation(0, getLocalFrame().getHeight()+getKnowledgebaseFrame().getHeight()+getJointreeFrame().getHeight()-4);
			statisticFrame.setIconifiable(true);
		}
		return statisticFrame;
	}	
		
	/**
	 * @return The statistic control panel.
	 */
	
	public StatisticPanel getStatisticPanel() {
		if (statPanel == null) {
			statPanel = new StatisticPanel(this);
		}
		return statPanel;
	}
	
	/*
	 * Getter &amp; Setter:
	 */
	
	/**
	 * @return The knowledgebase controll panel.
	 */
	
	public KbPanel getKnowledgebasePanel() {
		if (kbPanel == null) {
			kbPanel = new KbPanel(this);
		}
		return kbPanel;
	}

	/**
	 * @return The jointree controll panel.
	 */
	
	public TreePanel getJointreePanel() {
		if (jtPanel == null) {
			jtPanel = new TreePanel(this);
		}
		return jtPanel;
	}
	
	/**
	 * Displays error dialogs.
	 * @param type The type of error.
	 * @param e The exception to fill the frame components.
	 */
	
	public void error(final String type, final Exception e) {
		new ErrorDialog(Viewer.this, type, e);
	}
		  
	/**
	 * Displays error dialogs.
	 * @param type The type of error.
	 * @param message The error message.
	 */
	
	public void error(final String type, final String message) {
		new ErrorDialog(Viewer.this, type, message);
	}	
	
	/*
	 * Frame dimensions:
	 */
	
	/**
	 * @return The total width of the controll panel.
	 */
	
	public int getControllPanelWidth() {
		return getLocalFrame().getWidth();
	}

	/**
	 * @return The total height of the controll panel.
	 */
	
	public int getControllPanelHeight() {
		return getLocalFrame().getHeight() +
		getKnowledgebaseFrame().getHeight() +
		getJointreeFrame().getHeight() +
		getStatisticFrame().getHeight();
	}
}
