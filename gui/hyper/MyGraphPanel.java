package gui.hyper;

import hypergraph.graph.input.CSSColourParser;
import hypergraph.graphApi.AttributeManager;
import hypergraph.graphApi.Edge;
import hypergraph.graphApi.Graph;
import hypergraph.graphApi.GraphException;
import hypergraph.graphApi.Node;
import hypergraph.hyperbolic.ModelPanel;
import hypergraph.hyperbolic.ModelPoint;
import hypergraph.visualnet.DefaultGraphLayoutModel;
import hypergraph.visualnet.DefaultGraphSelectionModel;
import hypergraph.visualnet.GraphLayout;
import hypergraph.visualnet.GraphLayoutEvent;
import hypergraph.visualnet.GraphLayoutListener;
import hypergraph.visualnet.GraphLayoutModel;
import hypergraph.visualnet.GraphSelectionEvent;
import hypergraph.visualnet.GraphSelectionListener;
import hypergraph.visualnet.GraphSelectionModel;
import hypergraph.visualnet.TreeLayout;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

/**
 * 
 * 
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 564 $<br/>$LastChangedDate: 2008-03-26 14:39:05 +0100 (Mi, 26 Mrz 2008) $
 */

public class MyGraphPanel extends ModelPanel implements MouseListener, GraphLayoutListener, GraphSelectionListener {
	
	/**
	 * The attribute name for background colour of nodes. 
	 */
	
	public static final String NODE_BACKGROUND = "node.bkcolor";
	
	/**
	 * The attribute name for an icon of a node. 
	 */
	
	public static final String NODE_ICON = "node.icon";
	
	/**
	 * The attribute name for foreground colour of edges. 
	 */
	
	public static final String EDGE_TEXTCOLOR = "edge.textcolor";
	
	/**
	 * 
	 */
	
	public static final String EDGE_LINECOLOR = "edge.linecolor";
	
	/**
	 * 
	 */
	
	public static final String EDGE_LINEWIDTH = "edge.linewidth";
	
	/**
	 * 
	 */
	
	public static final String EDGE_STROKE = "edge.stroke";

	/**
	 * 
	 */
	
	public static final String NODE_EXPANDED = "NODE_EXPANDED";
	
	/**
	 * 
	 */
	
	public final ExpandAction expandAction = new ExpandAction(true);
	
	/**
	 * 
	 */
	
	public final ExpandAction shrinkAction = new ExpandAction(false);
	
	private Graph graph;
	private Graph visibleGraph;
	private GraphLayout graphLayout;
	private GraphSelectionModel selectionModel;
	private Node hoverNode;
	private Node lastMouseClickNode;
	
	private MyDefaultNodeRenderer _nodeRenderer;
	private MyDefaultEdgeRenderer _edgeRenderer;
	
	/**
	 * Constructor:
	 * @param graph The graph to display.
	 */
	
	public MyGraphPanel(Graph graph) {
		super();
		this.graph = graph;
		initVisibleGraph();
		createGraphLayout();
		createGraphSelectionModel();	
		selectionModel.addSelectionEventListener(this);
		setNodeRenderer(new MyDefaultNodeRenderer());
		setEdgeRenderer(new MyDefaultEdgeRenderer());
		initDefaultAttributes();
	}
	
	/**
	 * Initializes attributes such as colors, line strokes, etc.
	 */
	
	protected void initDefaultAttributes() {
		AttributeManager amgr = graph.getAttributeManager();
		String colorString = getPropertyManager().getString("hypergraph.hyperbolic.line.color");
		Color color = CSSColourParser.stringToColor(colorString);
		if (color == null || colorString == null)
			color = Color.LIGHT_GRAY;
		amgr.setAttribute(EDGE_LINECOLOR,graph,color);
		amgr.setAttribute(EDGE_STROKE,graph,null);
		amgr.setAttribute(EDGE_LINEWIDTH,graph,new Float(1));
	}
	
	/**
	 * 
	 *
	 */
	
	public void initVisibleGraph() {
		visibleGraph = graph;
	}
	
	/**
	 * 
	 *
	 */
	
	public void createGraphLayout() {
		GraphLayout layout;
		try {
			layout = (GraphLayout) getPropertyManager().getClass("hypergraph.visualnet.layout.class").newInstance();
		} catch (Exception e) {
			layout = new TreeLayout(); 
		}
		setGraphLayout(layout);
	}
	
	/**
	 * @see hypergraph.hyperbolic.ModelPanel#loadProperties(java.io.InputStream)
	 */
	
	public void loadProperties(InputStream is) throws IOException {
		super.loadProperties(is);
		createGraphLayout();
		initDefaultAttributes();
	}
	
	/**
	 * 
	 * @param layout
	 */
	
	public void setGraphLayout(GraphLayout layout) {
		graphLayout = layout;		
		GraphLayoutModel glm = new DefaultGraphLayoutModel();
		graphLayout.initialize(this,graph);
		graphLayout.setGraphLayoutModel(glm);
		graphLayout.layout();
		graphLayout.getGraphLayoutModel().addLayoutEventListener(this);
	}
	
	/**
	 * @return The graph layout.
	 */
	
	public GraphLayout getGraphLayout() {
		return graphLayout;
	}
	
	/**
	 * @return The Panel's Graph.
	 */
	
	public Graph getGraph() {
	    return graph;
	}
	
	/**
	 * 
	 */
	
	private void createGraphSelectionModel() {
		setGraphSelectionModel(new DefaultGraphSelectionModel(graph));
	}
	
	/**
	 * @param gsm The Graph Selection Model to set.
	 */
	
	public void setGraphSelectionModel(GraphSelectionModel gsm) {
		selectionModel = gsm;
	}
	
	/**
	 * @return The current Graph Selection Model.
	 */
	
	public GraphSelectionModel getSelectionModel() {
		return selectionModel;
	}
			
	/**
	 * @param nodeRenderer The node renderer to set.
	 */
	
	public void setNodeRenderer(MyDefaultNodeRenderer nodeRenderer) {
		this._nodeRenderer = nodeRenderer;
	}
	
	/**
	 * @return The current node renderer.
	 */
	
	public MyDefaultNodeRenderer getNodeRenderer() {
		return _nodeRenderer;
	}
	
	/**
	 * @see hypergraph.visualnet.GraphLayoutListener#valueChanged(hypergraph.visualnet.GraphLayoutEvent)
	 */
	
	public void valueChanged(GraphLayoutEvent e) {
		repaint();
	}
	
	/**
	 * 
	 */

	private void checkLayout() {
		if (!getGraphLayout().isValid()) 
			getGraphLayout().layout();
	}
	
	/**
	 * @return The iterator for visible nodes.
	 */
	
	public Iterator<?> getVisibleNodeIterator() {
		return visibleGraph.getNodes().iterator();
	}
	
	/**
	 * @return The iterator for visible edges.
	 */
	
	public Iterator<?> getVisibleEdgeIterator() {
		return visibleGraph.getEdges().iterator();
	}
	
	/**
	 * @see java.awt.Component#paint(java.awt.Graphics)
	 */
	
	public void paint(Graphics g) {
		
	    /*
	     * Make sure that neither the graph nor the graph layout are changed while painting
	     */
	    
		synchronized(graph) {
			checkLayout();
			Graphics2D g2 = (Graphics2D) g;
			if (getUI().isDraft()) {	
				g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_OFF);
				g2.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_SPEED);
				g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS,RenderingHints.VALUE_FRACTIONALMETRICS_OFF);
			} else {
				g2.setRenderingHint(
					RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);
			}
			super.paint(g);	
			GraphLayoutModel glm;
			glm = getGraphLayout().getGraphLayoutModel();
			for (Iterator<?> i = getVisibleEdgeIterator(); i.hasNext(); ) {
				Edge edge = (Edge) i.next();
				_edgeRenderer.configure(this,edge);
				paintRenderer(g, _edgeRenderer);
			}		
			for (Iterator<?> i = getVisibleNodeIterator(); i.hasNext(); ) {
				Node node = (Node) i.next();
				ModelPoint mp = glm.getNodePosition(node);
				_nodeRenderer.configure(this,mp,node);
				paintRenderer(g, _nodeRenderer);
			}		
		}
	}
	
	/**
	 * @return The hover node.
	 */
	
	public Node getHoverNode() {
		return hoverNode;
	}

	/**
	 * Sets the hover node.
	 * @param node The hover node to set.
	 * @param repaint Is repaint needed?
	 */
	
	protected void setHoverNode(Node node, boolean repaint) {
		hoverNode = node;
		if (repaint)
			repaint();	
	}
	
	/**
	 * 
	 * @param node
	 * @return boolean
	 */
	
	public boolean hasExpander(Node node){
		if (!getGraphLayout().isExpandingEnabled())
			return false;
		AttributeManager amgr = visibleGraph.getAttributeManager();
		ExpandAction action = (ExpandAction) amgr.getAttribute(NODE_EXPANDED,node);
		return action != null;
	}
	
	/**
	 * 
	 * @param node
	 * @return boolean
	 */
	
	public boolean isExpanded(Node node) {
		AttributeManager amgr = visibleGraph.getAttributeManager();
		ExpandAction action = (ExpandAction) amgr.getAttribute(NODE_EXPANDED,node);
		if (action == null)
			return false;
		return action == shrinkAction;
	}
	
	/**
	 * 
	 * @param node
	 */
	
	public void expandNode(Node node) {
		AttributeManager amgr = visibleGraph.getAttributeManager();
		// get all outgoing edges in the original (!) graph.
		Iterator<?> iter = graph.getEdges(node).iterator();
		while (iter.hasNext()) {
			Edge edge = (Edge) iter.next();
			if (edge.getSource() != node)
				continue; // only outgoing edges
			try {
				visibleGraph.addElement(edge);
			} catch (GraphException ge) {
			}
			Node target = edge.getOtherNode(node);
			if (graph.getEdges(target).size() != 0)
				amgr.setAttribute(NODE_EXPANDED,target,expandAction);
		}
//		Iterator iter = graph.getOutgoingEdgesIterator(node);
//		while (iter.hasNext()) {
//			Edge edge = (Edge) iter.next();
//			visibleGraph.addEdge(edge);
//			Node target = edge.getOtherNode(node);
//			if (graph.getOutgoingEdges(target).size() != 0)
//				amgr.setAttribute(NODE_EXPANDED,target,expandAction);
//		}
		amgr.setAttribute(NODE_EXPANDED,node,shrinkAction);
	}
	
	/**
	 * 
	 * @param node
	 */
	
	public void shrinkNode(Node node) {
		AttributeManager amgr = visibleGraph.getAttributeManager();
		// get all outgoing edges in the original (!) graph.
		Iterator<?> iter = graph.getEdges(node).iterator();
		while (iter.hasNext()) {
			Edge edge = (Edge) iter.next();
			if (edge.getSource() != node)
				continue; // only outgoing edges
			Node target = edge.getOtherNode(node);
			visibleGraph.removeElement(target);
		}
		amgr.setAttribute(NODE_EXPANDED,node,expandAction);		
	}
	
	/**
	 * 
	 * @param node
	 */
	
	public void centerNode(Node node) {
		GraphLayoutModel glm = getGraphLayout().getGraphLayoutModel();
		getUI().center(glm.getNodePosition(node),this);		
	}

	
	/**
	 * Returns the node at the position <code>point</code>.
	 * @param point The point.
	 * @return The node at this point.
	 */
	
	public Node getNode(Point point) {
		GraphLayoutModel glm = getGraphLayout().getGraphLayoutModel();
		Point p = new Point();
		MyDefaultNodeRenderer nr = new MyDefaultNodeRenderer();
		// make sure that neither the graph nor the graph layout are changed while getting the nearest node.
		synchronized(graph) {
		    synchronized(glm) {
		        for (Iterator<?> i = getVisibleNodeIterator(); i.hasNext(); ) {
		            Node node = (Node) i.next();
		            nr.configure(this,glm.getNodePosition(node),node);
		            Component c = nr.getComponent();
		            p.setLocation(point.getX()-c.getX(), point.getY() - c.getY());
		            if ( c.contains(p) ) 
		                return node;
		        }
		    }
		}
		return null;			
	}

	/**
	 * Is called when the user clicked on a node.
	 * The default implementation centers the node if it is clicked once.
	 * @param clickCount
	 * @param node
	 */
	
	public void nodeClicked(int clickCount, Node node) {
		if (clickCount == 1) {
			lastMouseClickNode = node;
			if (lastMouseClickNode != null)
				centerNode(lastMouseClickNode);
		}
	}
	
	/**
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	
	public void mouseClicked(MouseEvent e) {
		setHoverNode(null,false);
		Node node = getNode(e.getPoint());
		if (node != null) {
			if (e.getClickCount() == 1)
				nodeClicked(1,node);
			if (e.getClickCount() == 2 && lastMouseClickNode != null) 
				nodeClicked(2,lastMouseClickNode);
			MyDefaultNodeRenderer nodeRenderer = new MyDefaultNodeRenderer();
			GraphLayoutModel glm = getGraphLayout().getGraphLayoutModel();
			nodeRenderer.configure(this,glm.getNodePosition(node),node);
			if (nodeRenderer.expanderHit(e.getPoint())) {
				AttributeManager amgr = visibleGraph.getAttributeManager();
				ExpandAction action = (ExpandAction) amgr.getAttribute(NODE_EXPANDED,node);
				action.actionPerformed(new ActionEvent(node,0,""));				
				return;
			}
		}
		super.mouseClicked(e);		
	}
	
	/**
	 * @see java.awt.event.MouseMotionListener#mouseMoved(java.awt.event.MouseEvent)
	 */
	
	public void mouseMoved(MouseEvent e) {
		Node node = getNode(e.getPoint());
		setHoverNode(node,true);
	}	
	
	/**
	 * @see hypergraph.visualnet.GraphSelectionListener#valueChanged(hypergraph.visualnet.GraphSelectionEvent)
	 */
	
	public void valueChanged(GraphSelectionEvent e) {
		repaint();	
	}
	
	/**
	 * @author Jens Kanschik
	 * @version 1.0
	 */
	
	public class ExpandAction implements ActionListener {
		private boolean expand;
		
		/**
		 * 
		 * @param expand
		 */
		
		public ExpandAction(boolean expand) {
			this.expand = expand;
		}
		
		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		
		public void actionPerformed(ActionEvent e) {
			Node node = (Node) e.getSource();
			if (expand)
				expandNode(node);
			else
				shrinkNode(node);
			repaint();
		}
	}
	
	/**
	 * @return The edge renderer.
	 */
	
	public MyDefaultEdgeRenderer getEdgeRenderer() {
		return _edgeRenderer;
	}

	/**
	 * @param renderer The edge renderer to set.
	 */
	
	public void setEdgeRenderer(MyDefaultEdgeRenderer renderer) {
		_edgeRenderer = renderer;
	}

}