package gui.hyper;

import hypergraph.graphApi.AttributeManager;
import hypergraph.graphApi.Edge;
import hypergraph.hyperbolic.DefaultLineRenderer;
import hypergraph.hyperbolic.Isometry;
import hypergraph.hyperbolic.ModelPoint;
import hypergraph.hyperbolic.TextRenderer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;


/**
 * @author Jens Kanschik
 * @version $LastChangedRevision: 564 $<br/>$LastChangedDate: 2008-03-26 14:39:05 +0100 (Mi, 26 Mrz 2008) $
 */

public class MyDefaultEdgeRenderer extends DefaultLineRenderer {

	TextRenderer labelRenderer;
	BasicStroke lineStroke;
	
	/**
	 * 
	 * @param mp
	 * @param edge
	 */
	
	public void configure(MyGraphPanel mp, Edge edge) {
		AttributeManager attrMgr = mp.getGraph().getAttributeManager();
		ModelPoint mp1 = mp.getGraphLayout().getGraphLayoutModel().getNodePosition(edge.getSource());
		ModelPoint mp2 = mp.getGraphLayout().getGraphLayoutModel().getNodePosition(edge.getTarget());
		if (mp1 != null && mp2 != null) { 
			configure(mp,mp1,mp2);	
			if (edge.getLabel() != null && edge.getLabel().length()>0) {
				labelRenderer = mp.getTextRenderer();
				ModelPoint center = (ModelPoint) mp1.clone();
				Isometry isom = mp.getModel().getTranslation(mp1,mp2,0.5);
				isom.apply(center);
				labelRenderer.configure(mp, center, edge.getLabel());
				Color colour=null;
				if (attrMgr != null)
					colour = (Color) attrMgr.getAttribute(MyGraphPanel.EDGE_TEXTCOLOR,edge);
				if (colour != null)
					labelRenderer.setColor(colour);
			} else
				labelRenderer = null;
		}
		if (attrMgr != null) { 
			Color colour=null;
			colour = (Color) attrMgr.getAttribute(MyGraphPanel.EDGE_LINECOLOR,edge);
			if (colour != null) {
				if( edge.getSource().equals(mp.getHoverNode()) ||
					edge.getTarget().equals(mp.getHoverNode()))
					colour = colour.darker().darker();
				setColor(colour);
			}
			float[] stroke = (float[]) attrMgr.getAttribute(MyGraphPanel.EDGE_STROKE,edge); 
			float lineWidth = ((Float) attrMgr.getAttribute(MyGraphPanel.EDGE_LINEWIDTH,edge)).floatValue();
			if (stroke != null && stroke.length >0)
				lineStroke = new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 10.0f, stroke , 0.0f);
			else if (stroke != null && stroke.length == 0)
				lineStroke = null;
			else
				lineStroke = new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
		}
	}

	/**
	 * @see java.awt.Component#paint(java.awt.Graphics)
	 */
	
	public void paint(Graphics g) {
		if (lineStroke == null)
			return;
		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(lineStroke);
		super.paint(g);
		if (labelRenderer != null) {
			g.translate(labelRenderer.getComponent().getX(),labelRenderer.getComponent().getY());	
			labelRenderer.getComponent().paint(g);
		}
	}
}
