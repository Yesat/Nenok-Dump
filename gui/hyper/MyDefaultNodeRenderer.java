package gui.hyper;

import hypergraph.graphApi.AttributeManager;
import hypergraph.graphApi.Graph;
import hypergraph.graphApi.Node;
import hypergraph.hyperbolic.ModelPanel;
import hypergraph.hyperbolic.ModelPoint;
import hypergraph.hyperbolic.StubRenderer;
import hypergraph.hyperbolic.TextRenderer;
import hypergraph.visualnet.DefaultNodeRenderer;
import hypergraph.visualnet.GraphPanel;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ComponentEvent;

import javax.swing.Icon;

/**
 * This class implements the panel to display hypergraphs.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 564 $<br/>$LastChangedDate: 2008-03-26 14:39:05 +0100 (Mi, 26 Mrz 2008) $
 */

public class MyDefaultNodeRenderer extends DefaultNodeRenderer {
	
    private TextRenderer textRenderer;
	private MyGraphPanel gmp;
	private Node gnode;
	private Icon icon;
	
	/**
	 * Constructor:
	 * @return mygraphPanel
	 */
	
	public MyGraphPanel getMyGraphPanel() {
		return gmp;
	}
	
	/**
	 * @see hypergraph.hyperbolic.Renderer#configure(hypergraph.hyperbolic.ModelPanel)
	 */
	
	public void configure(ModelPanel c) {
		gmp = (MyGraphPanel) c;
		textRenderer = c.getTextRenderer();
		textRenderer.configure(c);
	}
	
	/**
	 * @see hypergraph.visualnet.NodeRenderer#configure(hypergraph.hyperbolic.ModelPanel, hypergraph.hyperbolic.ModelPoint, hypergraph.graphApi.Node)
	 */
	
	public void configure(ModelPanel c, ModelPoint mp, Node node) {
		this.gmp = (MyGraphPanel) c;
		this.gnode = node;
		textRenderer = c.getTextRenderer();
		if (node == null)
			textRenderer.configure(c, null, null);
		else {
			int iconWidth = 0;
			int iconHeight =0;
			int expanderWidth = 0;
			// initializing call to configure of the text renderer	
			textRenderer.configure(c, mp, node.getLabel());	
			Component textComponent = textRenderer.getComponent();
			// reading some attributes for the node
			Graph graph = ((MyGraphPanel) c).getGraph();
			AttributeManager attrMgr = graph.getAttributeManager();
			Color textColor = Color.black;
			if (textColor != null)
				textRenderer.setColor(textColor);
			Color fillColor = (Color) attrMgr.getAttribute(GraphPanel.NODE_BACKGROUND,node);
			setBackground(fillColor);
			if (((MyGraphPanel)c).getSelectionModel().isElementSelected(node))
				setFont(getFont().deriveFont(Font.BOLD));
			// adjust the size to have space for the +/- sign to expand if necessary
			if (getMyGraphPanel().hasExpander(node))
				expanderWidth = 10;
			// adjust the size to have space for the icon if necessary
			icon = (Icon) attrMgr.getAttribute(MyGraphPanel.NODE_ICON,node);
			if (icon != null) {
				iconHeight = icon.getIconHeight();
				iconWidth = icon.getIconWidth();
			}
			setSize(iconWidth + textComponent.getWidth() + expanderWidth,
					Math.max(iconHeight,textComponent.getHeight()));
			int borderx = getWidth() - textComponent.getWidth();
			int bordery = getHeight() - textComponent.getHeight();
			setLocation(textComponent.getX()-borderx/2,textComponent.getY()-bordery/2);
			textComponent.setLocation(getX()+iconWidth,getY()+(getHeight()-textComponent.getHeight())/2);
			if (((MyGraphPanel)c).getHoverNode() == node)
				setFont(getFont().deriveFont(Font.BOLD));
		}
	}
	
	/**
	 * @see java.awt.MenuContainer#getFont()
	 */
	
	public Font getFont() {
		return textRenderer.getComponent().getFont();
	}
	
	/**
	 * @see java.awt.Component#setFont(java.awt.Font)
	 */
	
	public void setFont(Font font) {
		textRenderer.getComponent().setFont(font);
	}
	
	/**
	 * @see hypergraph.hyperbolic.Renderer#getComponent()
	 */
	
	public Component getComponent() {
		if (textRenderer.getComponent() instanceof StubRenderer)
			return textRenderer.getComponent();
		return this;		
	}
	
	/**
	 * 
	 * @param g
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 */
	
	public void paintExpander(Graphics g,int x, int y, int w, int h) {
		g.setColor(Color.lightGray);
		g.drawRect(x,y,w,h);
		g.drawLine(x+2,y+h/2,x+w-2,y+h/2); // horizontal line
		if (!getMyGraphPanel().isExpanded(gnode))
			g.drawLine(x+w/2,y+2,x+w/2,y+h-2); // vertical line
	}
	
	/**
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	
	public void paintComponent(Graphics g) {
		if (getBackground() != null) {
			g.setColor(getBackground());
			g.fillRect(0,0,getWidth(),getHeight());
		}
		if (getMyGraphPanel().hasExpander(gnode))
			paintExpander(g,getWidth()-9,0,8,8);
		if (icon != null) {
			icon.paintIcon(this,g,0,(getHeight()-icon.getIconHeight())/2);
			g.translate(icon.getIconWidth(),0);
		}
		textRenderer.getComponent().paint(g);		
	}
	
	/**
	 * 
	 * @param p
	 * @return boolean
	 */
	
	public boolean expanderHit(Point p) {
		if (!getMyGraphPanel().hasExpander(gnode))
			return false;
		int x = p.x-getX()-getWidth();
		int y = p.y-getY();
		if (x>=-10 && x<=0 && y>=0 && y<=10)
			return true;
		return false;
	}
	
	/**
	 * @see java.awt.event.ComponentListener#componentHidden(java.awt.event.ComponentEvent)
	 */
	
	public void componentHidden(ComponentEvent e) {}
	
	/**
	 * @see java.awt.event.ComponentListener#componentMoved(java.awt.event.ComponentEvent)
	 */
	
	public void componentMoved(ComponentEvent e) {}
	
	/**
	 * @see java.awt.event.ComponentListener#componentResized(java.awt.event.ComponentEvent)
	 */
	
	public void componentResized(ComponentEvent e) {}
	
	/**
	 * @see java.awt.event.ComponentListener#componentShown(java.awt.event.ComponentEvent)
	 */
	
	public void componentShown(ComponentEvent e) {}
	
}
