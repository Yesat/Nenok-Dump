package gui.dialogs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyVetoException;
import java.io.File;
import java.util.Hashtable;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.MatteBorder;

import edu.uci.ics.jung.graph.ArchetypeVertex;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.Vertex;
import edu.uci.ics.jung.graph.decorators.GradientEdgePaintFunction;
import edu.uci.ics.jung.graph.decorators.VertexFontFunction;
import edu.uci.ics.jung.graph.decorators.VertexPaintFunction;
import edu.uci.ics.jung.graph.decorators.VertexShapeFunction;
import edu.uci.ics.jung.graph.decorators.VertexStringer;
import edu.uci.ics.jung.graph.impl.SparseVertex;
import edu.uci.ics.jung.graph.impl.UndirectedSparseEdge;
import edu.uci.ics.jung.graph.impl.UndirectedSparseGraph;
import edu.uci.ics.jung.utils.UserData;
import edu.uci.ics.jung.visualization.DefaultGraphLabelRenderer;
import edu.uci.ics.jung.visualization.ISOMLayout;
import edu.uci.ics.jung.visualization.Layout;
import edu.uci.ics.jung.visualization.PickSupport;
import edu.uci.ics.jung.visualization.PluggableRenderer;
import edu.uci.ics.jung.visualization.ShapePickSupport;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.CrossoverScalingControl;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ScalingControl;
import gui.Viewer;
import gui.util.Exporter;
import nenok.Knowledgebase;
import nenok.va.Domain;
import nenok.va.Valuation;
import nenok.va.Variable;

/**
 * The dialog that displays valuation networks.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 565 $<br/>$LastChangedDate: 2008-03-26 14:39:54 +0100 (Mi, 26 Mrz 2008) $
 */

public class NetworkDialog extends Dialog {
	
	private Graph graph;
	private Layout layout;
	private VisualizationViewer vv;
	private Knowledgebase kb;
	private DefaultModalGraphMouse mouse;
	private boolean labeling = true;
	
	private static final Color GREEN = new Color(50,205,50);
	
	/**
	 * Constructor:
	 * @param kb The knowledgebase to represent.
	 * @param viewer The parent component.
	 */
	
	public NetworkDialog(Knowledgebase kb, Viewer viewer) {
		super(viewer);
		this.kb = kb;
		this.setTitle("Valuation Network");
		this.extendMenu();
		this.setContentPane(getCanvas());
	}
		
	/**
	 * Redraws the canvas when window size changes.
	 */
	
	public void setMaximum(boolean max) throws PropertyVetoException {
		super.setMaximum(max);
    	layout.resize(this.getSize());
	}

	/**
	 * @return The canvas that draws the join tree.
	 */
	
	protected JPanel getCanvas() {
		
		if(vv != null) {
			return vv;
		}

		/*
		 * Built tree:
		 */
		
    	buildGraph();
  	
        /*
         * Layout:
         */
    	
    	PluggableRenderer pr = new PluggableRenderer();
        layout = new ISOMLayout(graph);
        
        vv =  new VisualizationViewer(layout, pr); 
        vv.setPickSupport(new ShapePickSupport());
        
    	layout.resize(normal);
    	
    	/*
    	 * Scaling:
    	 */
    	
        ScalingControl scaler = new CrossoverScalingControl();
        scaler.scale(vv, 1.2f, new Point(0,0)); // empirically !
                        
        /*
         * Determine Graph center:
         */
        
        Dimension size = layout.getCurrentSize();
        double x = size.getWidth() / 2 + 10;
        double y = size.getHeight() / 2 + 50;
        
        /*
         * Position:
         */
        
    	Point2D start = new Point2D.Double(x,y);
    	Point2D target = vv.inverseLayoutTransform(new Point2D.Double(normal.width / 2, normal.height / 2));
        double dx = target.getX() - start.getX();
        double dy = target.getY() - start.getY();
    	vv.getLayoutTransformer().translate(dx, dy);
    		
        /*
         * Background color:
         */
        
        vv.setBackground(Color.WHITE);
		vv.setBorder(new MatteBorder(5,5,5,5,vv.getBackground()));
        
        /*
         * Edge style:
         */
        
        pr.setEdgePaintFunction(new GradientEdgePaintFunction(new Color(229, 229, 229), new Color(0, 64, 0), vv, vv));
        
        pr.setVertexShapeFunction(new VertexShapeFunction() {
        	public Shape getShape(Vertex v) {
        		if(v.getUserDatum("Variable") != null) {
        			return new Ellipse2D.Double(-5,-5,10,10);
        		}
        		return new Rectangle2D.Double(-5,-5,10,10);
        	}
        });
        
        /*
         * Vertex labels &amp; color:
         */
        
        pr.setGraphLabelRenderer(new DefaultGraphLabelRenderer(Color.BLACK, Color.BLACK));
        pr.setVertexStringer(new VertexStringer() {
        	public String getLabel(ArchetypeVertex v) {
        		if(v.getUserDatum("Variable") != null && labeling) {
            		Variable var = (Variable)v.getUserDatum("Variable");
                    return "<html><div width='100'>"+var.toString()+"</div></html>"; 
        		}
        		return "";
        	}
        });
        pr.setVertexFontFunction(new VertexFontFunction() {
        	private final Font font = new java.awt.Font("Dialog", java.awt.Font.PLAIN, 10);
        	public Font getFont(Vertex arg0) {
        		return font;
        	}
        });
        
        /*
         * Vertex colors:
         */
        
        pr.setVertexPaintFunction(new VertexPaintFunction() {
        	
        	public Color getFillPaint(Vertex v) {
        		if(v.getUserDatum("Variable") != null) {
        			return Color.BLUE;
        		}
        		return GREEN;
			}

			public Paint getDrawPaint(Vertex v) {
				return Color.BLACK;
			}
        	
        });
        
        /*
         * Mouse listener:
         */
                        
        mouse = new DefaultModalGraphMouse() {

        	private PickSupport pickSupport = vv.getPickSupport();
 
        	public void mouseClicked(MouseEvent e) {	        		
				Point2D p = vv.inverseViewTransform(e.getPoint());
        		Vertex v = pickSupport.getVertex(p.getX(), p.getY());
        		if(v == null) {
        			return;
        		}
        
				if(v.getUserDatum("Valuation") != null) {
					Valuation label = (Valuation)v.getUserDatum("Valuation");
					Valuation val = null;
					val = label;
					
					viewer.addValuationFrame(val);
				}
        	}        	
        };
        
        vv.setGraphMouse(mouse);
        vv.suspend();
       
        return vv;
	}
	
	/**
	 * Initializes the menubar.
	 */
	
	private void extendMenu() {
        JMenu modeMenu = new JMenu("Mode");
        getMenu().add(modeMenu);
        		
		// Labeling Checkbox:
        JCheckBoxMenuItem labelItem = new JCheckBoxMenuItem("Labels");
        labelItem.setSelected(true);
        labelItem.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				labeling = !labeling;
				vv.repaint();
			}
		});
		modeMenu.add(labelItem);
		
		// Separator:
		modeMenu.add(new JSeparator());
		
		// Freeze Checkbox:
        final JCheckBoxMenuItem freezeItem = new JCheckBoxMenuItem("Freeze");
        freezeItem.setSelected(false);
        freezeItem.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(freezeItem.isSelected()) {
					mouse.setMode(ModalGraphMouse.Mode.PICKING);
				} else {
					mouse.setMode(ModalGraphMouse.Mode.TRANSFORMING);
				}
			}
		});
		modeMenu.add(freezeItem);
	}
	
	/**
	 * @see gui.dialogs.Dialog#saveDOT(File)
	 */
	
	@Override
	protected void saveDOT(File file) throws Exception {
		Exporter.saveNetworkAsDOT(file, kb);
	}
	
	/**
	 * Builds the valuation network graph.
	 */
	
	private void buildGraph() {
		
		if(kb == null) {
			return;
		}
		
		graph = new UndirectedSparseGraph();
		
		Hashtable<Variable, Vertex> vertices = new Hashtable<Variable, Vertex>();
		
		/*
		 * Variables:
		 */
		
		Domain dom = kb.getDomain();
		for(Variable var : dom) {
			Vertex vertex = new SparseVertex();
			vertices.put(var, vertex);
			vertex.setUserDatum("Variable", var, UserData.SHARED);
			graph.addVertex(vertex);
		}
		
		/*
		 * Valuations:
		 */
		
		for(Valuation val : kb.toArray()) {
			Vertex vertex = new SparseVertex();
			vertex.setUserDatum("Valuation", val, UserData.SHARED);
			graph.addVertex(vertex);
			
			dom = val.label();
			for(Variable var : dom) {
				graph.addEdge(new UndirectedSparseEdge(vertex, vertices.get(var)));
			}
		}
	}
}
