package gui.dialogs;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.MatteBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import gui.Viewer;
import nenok.va.Domain;
import nenok.va.Variable;

/**
 * A dialog that allows the user to specify queries.
 *
 * @author Marc Pouly
 * @version $LastChangedRevision: 565 $<br/>$LastChangedDate: 2008-03-26 14:39:54 +0100 (Mi, 26 Mrz 2008) $
 */

public class QueryDialog extends JDialog {
	
	private Panel panel;
	
	/**
	 * Constructor:
	 * @param viewer The main application.
	 * @param dom The total domain from which queries are taken.
	 * @param queries A list of already existing queries.
	 */
	
	public QueryDialog(Viewer viewer, Domain dom, Collection<Domain> queries) {
		super(viewer, true);
		this.setTitle("Queries");
		this.setBackground(java.awt.Color.white);
		this.setBounds(new java.awt.Rectangle(0,0,0,0));
		this.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12));
		this.setResizable(false);
		this.panel = new Panel(dom, queries);
		this.setContentPane(panel);
		this.setSize(new Dimension(410,300));
        this.setLocation((viewer.getWidth() - this.getWidth()) / 2, (viewer.getHeight() - this.getHeight()) / 3);
		this.setVisible(true);
		this.pack();
	}
	
	/**
	 * @return The queries that has been defined by this dialog.
	 */
	
	public Collection<Domain> getQueryList() {
		return panel.result;
	}
	
	/**
	 * Dialog that allows to add queries dynamically via a GUI.
	 * 
	 * @author Marc Pouly
	 */

	public class Panel extends JPanel {

		private Domain dom;
		private HashSet<Domain> result;
		private Hashtable<JCheckBox, Variable> buttons = new Hashtable<JCheckBox, Variable>();
		
		private JScrollPane left = null;
		private JPanel buttonPanel = null;
		private JButton create = null;
		private JButton remove = null;
		private JButton okButton = null;
		private JScrollPane right = null;
		private JList<Domain> list = null;
				
		/**
		 * Constructor:
		 * @param dom The variables to build domains.
		 * @param queries Already existing queries.
		 */
		
		public Panel(Domain dom, Collection<Domain> queries) {
			super();
			this.dom = dom;
			this.result = new HashSet<Domain>();
			initialize();
			if(queries != null) {
				for(Domain query : queries) {
					addQuery(query);
				}
			}
		}	
		
		/**
		 * @return The scroller for the variable checkboxes.
		 */
		
		private JScrollPane getLeft() {
			if (left == null) {
				left = new JScrollPane();
				left.setBackground(Color.RED);
				left.setViewportView(getButtonPanel());
				left.setPreferredSize(new Dimension(150,150));
			}
			return left;
		}

		/**
		 * Initializes this component.
		 */
		
		private void initialize() {
			
			// OK Button
			GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
			gridBagConstraints4.insets = new java.awt.Insets(5,5,10,5);
			gridBagConstraints4.gridx = 1;
			gridBagConstraints4.gridy = 2;
			
			// Right Panel:
			GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
			gridBagConstraints3.fill = java.awt.GridBagConstraints.BOTH;
			gridBagConstraints3.gridx = 2;
			gridBagConstraints3.gridy = 0;
			gridBagConstraints3.gridheight = 2;
			gridBagConstraints3.insets = new java.awt.Insets(10,5,0,10);
			
			// Create Button:
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.gridx = 1;
			gridBagConstraints1.gridy = 0;
			gridBagConstraints1.weighty = 0.5;
			gridBagConstraints1.anchor = GridBagConstraints.SOUTH;
			gridBagConstraints1.fill = java.awt.GridBagConstraints.HORIZONTAL;
			gridBagConstraints1.insets = new java.awt.Insets(10,5,5,5);
			
			// Remove Button:
			GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
			gridBagConstraints5.gridx = 1;
			gridBagConstraints5.gridy = 1;
			gridBagConstraints5.weighty = 0.5;
			gridBagConstraints5.anchor = GridBagConstraints.NORTH;
			gridBagConstraints5.fill = java.awt.GridBagConstraints.HORIZONTAL;
			gridBagConstraints5.insets = new java.awt.Insets(5,5,10,5);
			
			// Left Panel:
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
			gridBagConstraints.gridx = 0;
			gridBagConstraints.gridy = 0;
			gridBagConstraints.gridheight = 2;
			gridBagConstraints.insets = new java.awt.Insets(10,10,0,5);
			
			
			setLayout(new GridBagLayout());
			this.add(getLeft(), gridBagConstraints);
			add(getCreateButton(), gridBagConstraints1);
			add(getRemoveButton(), gridBagConstraints5);
			this.add(getRight(), gridBagConstraints3);
			this.add(getOKButton(), gridBagConstraints4);
		}

		/**
		 * @return The checkbox panel for the variables. 
		 */
		
		private JPanel getButtonPanel() {
			if (buttonPanel == null) {
				buttonPanel = new JPanel();
				buttonPanel.setBackground(Color.WHITE);
				buttonPanel.setBorder(new MatteBorder(5,5,5,5,buttonPanel.getBackground()));
				buttonPanel.setLayout(new BoxLayout(getButtonPanel(), BoxLayout.Y_AXIS));
				for(Variable v : dom) {
					JCheckBox check = new JCheckBox(v.toString());
					check.setBackground(Color.WHITE);
					check.setFocusable(false);
					buttonPanel.add(check);
					buttons.put(check, v);
				}
			}
			return buttonPanel;
		}

		/**
		 * @return Button to create queries from the chosen variables.
		 */
		
		private JButton getCreateButton() {
			if (create == null) {
				create = new JButton();
				create.setText("Create");
				create.setFocusable(false);
				create.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						ArrayList<Variable> vars = new ArrayList<Variable>();
						for(Component c : buttonPanel.getComponents()) {
							if(c instanceof JCheckBox && ((JCheckBox)c).isSelected()) {
								vars.add(buttons.get(c));
								((JCheckBox)c).setSelected(false);
							}
						}
						addQuery(new Domain(vars));
					}
				});
			}
			return create;
		}
		
		/**
		 * @return Button to remove queries.
		 */
		
		private JButton getRemoveButton() {
			if (remove == null) {
				remove = new JButton();
				remove.setText("Remove");
				remove.setFocusable(false);
				remove.setEnabled(false);
				remove.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						DefaultListModel<?> model = (DefaultListModel<?>)list.getModel();
						Object obj = getList().getSelectedValue();
						if(obj != null) {
							Domain query = (Domain)obj;
							model.removeElement(query);
							result.remove(query);	
							if(model.size() > 0) {
								getList().setSelectedIndex(0);
							}
						}
					}
				});
			}
			return remove;
		}
		
		/**
		 * @return Button to close this window.
		 */
		
		private JButton getOKButton() {
			if (okButton == null) {
				okButton = new JButton();
				okButton.setText(" OK ");
				okButton.setFocusable(false);
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						QueryDialog.this.dispose();
					}
				});
			}
			return okButton;
		}
		
		/**
		 * Adds a given variables to the right hand list.
		 * @param query The query to add.
		 */
		
		private void addQuery(Domain query) {
			DefaultListModel<Domain> model = (DefaultListModel<Domain>)list.getModel();
			for (int i = 0; i < model.size(); i++) {
				Domain d = model.get(i);
				if(query.equals(d)) {
					return;
				}
			}
			model.addElement(query);
			result.add(query);
		}
		
		/**
		 * @return The scroller for the query list.
		 */
		
		private JScrollPane getRight() {
			if (right == null) {
				right = new JScrollPane();
				right.setPreferredSize(new Dimension(150,150));
				right.setViewportView(getList());
			}
			return right;
		}

		/**
		 * @return The list of built queries.	
		 */
		
		private JList<Domain> getList() {
			if (list == null) {
				list = new JList<Domain>();
				list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				list.setBorder(new MatteBorder(5,5,5,5,list.getBackground()));
				list.addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent arg0) {
						remove.setEnabled(list.getSelectedValue() != null);
					}
				});
				DefaultListModel<Domain> model = new DefaultListModel<Domain>();
				list.setModel(model);
				if(result != null) {
					for(Domain query : result) {
						model.addElement(query);
					}
				}
			}
			return list;
		}

	}
}
