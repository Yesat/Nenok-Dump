package gui.dialogs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;

import gui.Viewer;
import nenok.Utilities;
import nenok.va.Domain;
import nenok.va.Scalability;
import nenok.va.VAException;
import nenok.va.Valuation;

/**
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 565 $<br/>$LastChangedDate: 2008-03-26 14:39:54 +0100 (Mi, 26 Mrz 2008) $
 */

public class ValuationDialog extends JInternalFrame {
	
	/**
	 * Constructor:
	 * @param val The valuation object to show.
	 * @param viewer The main frame.
	 */
		
	public ValuationDialog(Valuation val, Viewer viewer) {
		super();
		
		this.setBackground(Color.WHITE);
		this.setBounds(new java.awt.Rectangle(0,0,0,0));
		this.setBorder(new LineBorder(Color.BLACK));
		this.setTitle("Valuation");
		this.setVisible(true);
		this.setResizable(true);
		this.setClosable(true);
		this.setIconifiable(true);
		
		if(val instanceof Scalability) {
			val = ((Scalability)val).scale();
		}
		
		ViewerPanel canvas = new ViewerPanel(val, viewer);
		this.setContentPane(canvas);
		
		Dimension dim = this.getPreferredSize();
		if(dim.width > 800) {
			dim.setSize(800, dim.height);
		}
		if(dim.height > 600) {
			dim.setSize(dim.width, 600);
		}				
		this.setSize(dim);
	}
	
	/**
	 * Displays valuation objects according to their display methods.
	 * 
	 * @author Marc Pouly
	 */

	private class ViewerPanel extends JPanel {

		private Valuation val;
		private Viewer viewer;
		
		private JLabel pLabel = null;
		private JComboBox<Domain> combo = null;
		private JTabbedPane tabs = null;

		/**
		 * Constructor:
		 * @param val
		 * @param viewer
		 */
		
		public ViewerPanel(Valuation val, Viewer viewer) {
			super();
			this.val = val;
			this.viewer = viewer;
			initialize();
		}

		/**
		 * This method initializes the component.
		 */
		
		private void initialize() {
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
			gridBagConstraints2.gridx = 0;
			gridBagConstraints2.insets = new java.awt.Insets(5,10,10,5);
			gridBagConstraints2.fill = java.awt.GridBagConstraints.NONE;
			gridBagConstraints2.gridy = 1;
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.fill = java.awt.GridBagConstraints.BOTH;
			gridBagConstraints1.weighty = 1.0;
			gridBagConstraints1.gridx = 0;
			gridBagConstraints1.gridwidth = 2;
			gridBagConstraints1.gridy = 0;
			gridBagConstraints1.insets = new java.awt.Insets(10,10,5,10);
			gridBagConstraints1.weightx = 1.0;
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
			gridBagConstraints.gridx = 1;
			gridBagConstraints.gridy = 1;
			gridBagConstraints.insets = new java.awt.Insets(5,5,10,10);
			gridBagConstraints.weightx = 1.0;
			pLabel = new JLabel();
			pLabel.setText("Marginalization:");
			pLabel.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 12));
			this.setLayout(new GridBagLayout());
			this.setSize(300, 200);
			this.setRequestFocusEnabled(false);
			this.add(getTabs(), gridBagConstraints1);
			this.add(pLabel, gridBagConstraints2);
			this.add(getCombo(), gridBagConstraints);
		}

		/**
		 * This method initializes combo	
		 * 	
		 * @return javax.swing.JComboBox	
		 */
		
		private JComboBox<Domain> getCombo() {
			if (combo == null) {
				combo = new JComboBox<Domain>();
				combo.setRequestFocusEnabled(false);
				combo.setFocusable(false);
				
	            Domain label = val.label();
	            for(Domain dom : Domain.powerSet(label)) {
	                if(!dom.equals(label)) {
	                    this.combo.addItem(dom);
	                }
	            }
	            
	            combo.addActionListener(new ActionListener() {
	                public void actionPerformed(ActionEvent event) {
	                    Domain query = (Domain)combo.getSelectedItem();
	                    if(query == null) {
	                    	return;
	                    }
	                    try {
	                        Valuation v = val.marginalize(query);
	                    	if(val instanceof Scalability) {
	                        	viewer.addValuationFrame(((Scalability)v).scale());
	                        } else {
	                        	viewer.addValuationFrame(v);
	                        }
	                    } catch(VAException e) {
	                    	viewer.error("Communication Error", e);
	                        return;
	                    }
	                }
	            });
	            
	            
			}
			return combo;
		}

		/**
		 * This method initializes tabs	
		 * 	
		 * @return javax.swing.JTabbedPane	
		 */
		
		private JTabbedPane getTabs() {
			if (tabs == null) {
				tabs = new JTabbedPane();
				tabs.setRequestFocusEnabled(false);
				tabs.setFocusable(false);
				
		        if(val != null) {
		        	
		        	/*
		        	 * Representors:
		        	 */
		        	
		        	java.util.List<Method> methods = Utilities.getRepresentators(val);
		        	
		            /*
		             * If no display method is defined:
		             */
		            
		            if(methods.size() == 0) {       
		            	
		                JTextArea editor = new JTextArea("No display methods defined ...");
		                editor.setEditable(false);
		                editor.setFocusable(false);
		                editor.setLineWrap(false);
		                editor.setFont(new java.awt.Font("Courier New", java.awt.Font.BOLD, 15));
		                editor.setBackground(Color.WHITE);
		                editor.setBorder(new CompoundBorder(new LineBorder(Color.BLACK), new MatteBorder(10,10,10,10,Color.WHITE)));
		                
		                JScrollPane scroller = new JScrollPane(editor);
		                scroller.setBorder(new MatteBorder(10,10,10,10,this.getBackground()));
		                scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		                scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		                
		                tabs.addTab("Warning", scroller);
		  
		            }
		        	
		        	
		            for(Method m : methods) {
		            	
		            	// Graphical Representors:
		            	if(Utilities.superclassOf(JComponent.class, m.getReturnType())) {
			                
		            		JComponent comp = null;
			                try {
			                    comp = (JComponent)m.invoke(val, new Object[] {});
			                } catch (Exception e) {
			                    e.printStackTrace();
			                    return tabs;
			                } 
			                
			                if(comp == null) {
			                    continue;
			                }
			                
			                comp.setBackground(Color.WHITE);
			                
			                JScrollPane scroller = new JScrollPane(comp);
			                scroller.setBorder(new MatteBorder(10,10,10,10,this.getBackground()));
			                scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			                scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			                tabs.addTab("Graphic", scroller);
		                
		            	}
		            	
		            	// Text Representors:
		            	else if(m.getReturnType().equals(String.class)) {
			                String content = "";
			                try {
			                    content = (String)m.invoke(val, new Object[] {});
			                } catch (Exception e) {
			                    e.printStackTrace();
			                    continue;
			                } 
			                
			                if(content == null) {
			                	continue;
			                }
			                                
			                JTextArea editor = new JTextArea(content);
			                editor.setEditable(false);
			                editor.setFocusable(false);
			                editor.setLineWrap(false);
			                editor.setFont(new java.awt.Font("Courier New", java.awt.Font.BOLD, 15));
			                editor.setBackground(Color.WHITE);
			                editor.setBorder(new CompoundBorder(new LineBorder(Color.BLACK), new MatteBorder(10,10,10,10,Color.WHITE)));
			                
			                JScrollPane scroller = new JScrollPane(editor);
			                scroller.setBorder(new MatteBorder(10,10,10,10,this.getBackground()));
			                scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			                scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			                
			                tabs.addTab("Text", scroller);
		            	
		            	} else {
		            		viewer.error("Display invocation error", "The return type of a display method must either be String or a subtype of JComponent.");
		            	}
		            }
		        }
			}
			return tabs;
		}

	}
}
