package gui.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSeparator;
import javax.swing.border.MatteBorder;

import edu.uci.ics.jung.graph.ArchetypeVertex;
import edu.uci.ics.jung.graph.Edge;
import edu.uci.ics.jung.graph.Vertex;
import edu.uci.ics.jung.graph.decorators.GradientEdgePaintFunction;
import edu.uci.ics.jung.graph.decorators.VertexFontFunction;
import edu.uci.ics.jung.graph.decorators.VertexPaintFunction;
import edu.uci.ics.jung.graph.decorators.VertexStringer;
import edu.uci.ics.jung.graph.impl.DirectedSparseEdge;
import edu.uci.ics.jung.graph.impl.DirectedSparseVertex;
import edu.uci.ics.jung.graph.impl.SparseTree;
import edu.uci.ics.jung.utils.UserData;
import edu.uci.ics.jung.visualization.DefaultGraphLabelRenderer;
import edu.uci.ics.jung.visualization.PickSupport;
import edu.uci.ics.jung.visualization.PluggableRenderer;
import edu.uci.ics.jung.visualization.ShapePickSupport;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.contrib.TreeLayout;
import edu.uci.ics.jung.visualization.control.CrossoverScalingControl;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ScalingControl;
import gui.Viewer;
import gui.util.Exporter;
import nenok.lc.JoinTree;
import nenok.lc.Node;
import nenok.va.Domain;
import nenok.va.Identity;
import nenok.va.Valuation;

/**
 * Displays jointrees by use of the JUNG library.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 565 $<br/>$LastChangedDate: 2008-03-26 14:39:54 +0100 (Mi, 26 Mrz 2008) $
 */

public class FlatDialog extends Dialog {
	
	private JoinTree tree;
    private SparseTree graph;
    private TreeLayout layout;
    private PickSupport pickSupport;
    private VisualizationViewer vv;   
	private boolean none = false, labeling = true;
	
	/*
	 * Constants:
	 */
	
	private static final Color FULL = new Color(255, 215, 0);
	
	/*
	 * Mouse-Listeners:
	 */
	
	private DefaultModalGraphMouse mouse;
	
	/*
	 * Other components:
	 */
	
	private ArrayList<Vertex> leaves = new ArrayList<Vertex>();

	/**
	 * Constructor:
	 * @param tree The jointree to display.
	 * @param viewer The main application.
	 */
	
	public FlatDialog(JoinTree tree, Viewer viewer) {
		super(viewer);
		this.setLayout(new BorderLayout());
		this.tree = tree;
		this.setTitle("Join Tree");	
		this.extendMenu();
		
		/*
		 * Build tree:
		 */
		
		buildTree(tree.getRoot(), null);
		
		/*
		 * Prepare canvas:
		 */
		
		PluggableRenderer pr = new PluggableRenderer();
		layout = new TreeLayout(graph);
		vv =  new VisualizationViewer(layout, pr); 
		
        /*
         * Background color:
         */
        
        vv.setBackground(Color.WHITE);
		vv.setBorder(new MatteBorder(5,5,5,5,vv.getBackground()));
		
		/*
		 * Pick Support:
		 */
		
		pickSupport = new ShapePickSupport();
		vv.setPickSupport(pickSupport);
		
		/*
		 * Define content pane:
		 */
		
		this.setContentPane(vv);
		
        
        /*
         * Edge style:
         */
        
        pr.setEdgePaintFunction(new GradientEdgePaintFunction(new Color(229, 229, 229), new Color(0, 64, 0), vv, vv));
        
        /*
         * Vertex labels &amp; color:
         */
        
        pr.setGraphLabelRenderer(new DefaultGraphLabelRenderer(Color.BLACK, Color.BLACK));
        pr.setVertexStringer(new VertexStringer() {
        	public String getLabel(ArchetypeVertex v) {
        		if(!labeling) {
        			return "";
        		}
        		Domain dom = ((Node)v.getUserDatum("Node")).getLabel();
                return "<html><div width='100'>"+dom.toString()+"</div></html>"; 
        	}
        });
        pr.setVertexFontFunction(new VertexFontFunction() {
        	private final Font font = new java.awt.Font("Dialog", java.awt.Font.PLAIN, 10);
        	public Font getFont(Vertex arg0) {
        		return font;
        	}
        });
        
        /*
         * Vertex colors:
         */
        
        pr.setVertexPaintFunction(new VertexPaintFunction() {
        	
        	/**
        	 * @see edu.uci.ics.jung.graph.decorators.VertexPaintFunction#getFillPaint(edu.uci.ics.jung.graph.Vertex)
        	 */
        	
        	public Color getFillPaint(Vertex v) {				
        		Valuation content = ((Node)v.getUserDatum("Node")).getContent();						
				if(content instanceof Identity) {
					return vv.getBackground();
				}			
				return FULL;		
			}

        	/**
        	 * @see edu.uci.ics.jung.graph.decorators.VertexPaintFunction#getDrawPaint(edu.uci.ics.jung.graph.Vertex)
        	 */
        	
			public Paint getDrawPaint(Vertex v) {
				return Color.BLACK;
			}
        	
        });
		        
        /*
         * Mouse listener:
         */
                        
        mouse = new DefaultModalGraphMouse() {
 
        	public void mouseClicked(MouseEvent e) {
        				        		  				
				Point2D p = vv.inverseViewTransform(e.getPoint());
        		Vertex v = pickSupport.getVertex(p.getX(), p.getY());

        		if(v == null) {
        			return;
        		}
        		
        		Valuation val = ((Node)v.getUserDatum("Node")).getContent();
        		
        		if(val instanceof Identity) {
        			return;
        		}
        		
        		FlatDialog.this.viewer.addValuationFrame(val);

        	}        	
        };
        
        vv.setGraphMouse(mouse);
        vv.suspend();
		
		/*
		 * Graph positioning:
		 */
        
        center();
        scale();

	}
	
	/*
	 * Centering of the root node:
	 */
	
	private void center() {
		Point2D center = new Point2D.Double(layout.getX(graph.getRoot()),layout.getY(graph.getRoot()));
		Point2D viewCenter = new Point2D.Float(normal.width / 2, 80);
		viewCenter = vv.inverseTransform(viewCenter);
		double xdist = viewCenter.getX() - center.getX();
		double ydist = viewCenter.getY() - center.getY();
		vv.getViewTransformer().translate(xdist, ydist);
		vv.repaint();
	}
	
	/*
	 * Scaling of the graph:
	 */
	
	private void scale() {
        
		/*
         * Find min / max vertex position:
         */
        
        double left = Double.MAX_VALUE;
        double right = Double.MIN_VALUE; 
        for(Vertex v : leaves) {
			left = (layout.getX(v) < left) ? layout.getX(v) : left;
			right = (layout.getX(v) > right) ? layout.getX(v) : right;
		}
       
        /*
         * Fetch graph to panel size:
         */
        
        double graphwidth = right - left;
        ScalingControl scaler = new CrossoverScalingControl();
        
        final int offset = 100;
        final Point point = new Point(normal.width / 2, offset);
        
        // Rescale too large trees:
        
        final float factor1 = 0.8f;
        while(graphwidth > normal.width) {
        	scaler.scale(vv, factor1, point);
        	graphwidth *= factor1;
        }
        
        // Scale too small trees:
        
        int counter = 10;
        final float factor2 = 1.1f;
        while(graphwidth < normal.width - 200 && counter-- > 0) {
        	scaler.scale(vv, factor2, point);
        	graphwidth *= factor2;
        } 
	}
	
	/**
	 * Method that manages the focus of this component.
	 */
	
	/*public boolean isShowing() {
		vv.requestFocus();
		return super.isShowing();
	}
	
	/**
	 * @return The canvas that draws the join tree.
	 */
	
	protected JPanel getCanvas() {
		return vv;
	}
	
	/**
	 * Initializes the menubar.
	 */
	
	private void extendMenu() {
        JMenu modeMenu = new JMenu("Mode");
        getMenu().add(modeMenu);
        
        ButtonGroup group = new ButtonGroup();
        
		// Content Mode Checkbox:
        JRadioButtonMenuItem contentItem = new JRadioButtonMenuItem("Content");
        contentItem.setSelected(true);
        contentItem.setFocusable(false);
        contentItem.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				none = false;
				vv.repaint();
			}
		});
		group.add(contentItem);
		modeMenu.add(contentItem);
        
		// Processor Mode Checkbox:
        JRadioButtonMenuItem procItem = new JRadioButtonMenuItem("Processor");
        procItem.setFocusable(false);
        procItem.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				none = false;
				vv.repaint();
			}
		});

        group.add(procItem);
		modeMenu.add(procItem);	
		
		// No Mode Checkbox:
        JRadioButtonMenuItem noneItem = new JRadioButtonMenuItem("None");
        noneItem.setSelected(none);
        noneItem.setFocusable(false);
        noneItem.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				none = true;
				vv.repaint();
			}
		});
		group.add(noneItem);
		modeMenu.add(noneItem);
		
		// Separator:
		modeMenu.add(new JSeparator());
		
		// Labeling Checkbox:
        JCheckBoxMenuItem labelItem = new JCheckBoxMenuItem("Labels");
        labelItem.setSelected(true);
        labelItem.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				labeling = !labeling;
				vv.repaint();
			}
		});
		modeMenu.add(labelItem);
		
		// Separator:
		modeMenu.add(new JSeparator());
		
		// Freeze Checkbox:
        final JCheckBoxMenuItem freezeItem = new JCheckBoxMenuItem("Freeze");
        freezeItem.setSelected(false);
        freezeItem.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(freezeItem.isSelected()) {
					mouse.setMode(ModalGraphMouse.Mode.PICKING);
				} else {
					mouse.setMode(ModalGraphMouse.Mode.TRANSFORMING);
				}
			}
		});
		modeMenu.add(freezeItem);
		
		/*
		 * Query Menu:
		 */
		
		if(tree.getQueries().size() > 0) {
	        JMenu queryMenu = new JMenu("Queries");
	        getMenu().add(queryMenu);
	        for(Domain query : tree.getQueries()) {
	        	JMenuItem menu = new JMenuItem(query.toString());
	        	menu.addActionListener(new QueryListener(query));
	        	queryMenu.add(menu);
	        }
		}
	}
		
	/**
	 * @see gui.dialogs.Dialog#saveDOT(File,boolean)
	 */
	
	protected void saveDOT(File file) throws IOException {
		Exporter.saveTreeAsDOT(file, tree, true);
	}
	
	/**
	 * 
	 * @author Marc Pouly
	 * @version 1.2
	 */
	
	private class QueryListener implements ActionListener {
		
		private Domain query;
		
		/**
		 * Constructor:
		 * @param query The query domain.
		 */
		
		public QueryListener(Domain query) {
			this.query = query;
		}
		
		/**
		 * 
		 */
		
		public void actionPerformed(ActionEvent arg0) {
			try {
				Valuation v = tree.answer(query);
				viewer.addValuationFrame(v);
			} catch (RemoteException e) {
				viewer.error("Communication Error", e);
				return;
			}
		}
	}
	
    /**
     * Build the graphical structure og the jointree.
     */
	
    private void buildTree(Node node, DirectedSparseVertex child) {
    	DirectedSparseVertex vertex = new DirectedSparseVertex();
    	if(node.getParents().size() == 0) {
        	leaves.add(vertex);	
    	}
        vertex.addUserDatum("Node", node, UserData.SHARED);    	
    	if(child == null) {
            graph = new SparseTree(vertex);
    	} else {
    		graph.addVertex(vertex);
    		Edge edge = new DirectedSparseEdge(child, vertex);
    		graph.addEdge(edge);
    	}
    	for(Node parent : node.getParents()) {
    		buildTree(parent, vertex);
    	} 
    }
}
