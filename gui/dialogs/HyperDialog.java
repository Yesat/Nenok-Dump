package gui.dialogs;

import gui.Viewer;
import gui.hyper.MyGraphPanel;
import gui.util.Exporter;
import hypergraph.graph.GraphImpl;
import hypergraph.graphApi.AttributeManager;
import hypergraph.graphApi.Graph;
import hypergraph.graphApi.GraphException;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.swing.border.MatteBorder;

import nenok.lc.JoinTree;
import nenok.lc.Node;

/**
 * Displays jointrees as a hypergraph.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 565 $<br/>$LastChangedDate: 2008-03-26 14:39:54 +0100 (Mi, 26 Mrz 2008) $
 */

public class HyperDialog extends Dialog {
	
	private JoinTree tree;
	private HyperPanel panel;
		
	/**
	 * Constructor:
	 * @param tree The jointree to display.
	 * @param viewer The main application.
	 */
	
	public HyperDialog(JoinTree tree, Viewer viewer) {
		super(viewer);
		this.tree = tree;
		this.setTitle("Join Tree Hypergraph");
		this.setContentPane(getCanvas());
	}
		
	/**
	 * @return The content pane of this frame.
	 */
	
	protected HyperPanel getCanvas() {
		if(panel == null) {
			this.panel = new HyperPanel();
		}
		return panel;
	}
	
	/**
	 * @see gui.dialogs.Dialog#saveDOT(File)
	 */
	
	protected void saveDOT(File file) throws IOException {
		Exporter.saveTreeAsDOT(file, tree, false);
	}
	
	/**
	 * This class implements the panel to display hypergraphs.
	 * 
	 * @author Marc Pouly
	 * @version 1.1
	 */

	public class HyperPanel extends MyGraphPanel {
		
	    private int counter = 0;
	     
	    /**
	     * Constructor:
	     */
	    
	    public HyperPanel() {
	        super(new GraphImpl());
	        this.setBackground(Color.WHITE);
	        Graph graph = this.getGraph();
	        this.setBorder(new MatteBorder(5,5,5,5,Color.WHITE));
	        Node root = tree.getRoot();
	        buildNodes(root, graph, null);
	    }
	    
	    /**
	     * Method to transform join tree structure to a displayable graph.
	     * @param node The root node of the sub-tree.
	     * @param graph The graph node representation of the child node.
	     * @param childGraphNode
	     */
	    
	    private void buildNodes(Node node, Graph graph, hypergraph.graphApi.Node childGraphNode) {
	        try {
	            hypergraph.graphApi.Node graphNode = graph.createNode(counter+"");
	            graphNode.setLabel(node.getLabel().toString());
	            counter++;
	            if(childGraphNode != null) {
	                graph.createEdge(childGraphNode, graphNode);
	            }
	            Iterator<Node> iter = node.getParents().iterator();
	            while(iter.hasNext()) {
	                Node parent = iter.next();
	                buildNodes(parent, graph, graphNode);
	            }
	        } catch (GraphException e) {
	            return;
	        }
	    }
	    	
		/** 
	     * @see gui.hyper.MyGraphPanel#setHoverNode(hypergraph.graphApi.Node, boolean)
		 */
	    
		public void setHoverNode(hypergraph.graphApi.Node node, boolean repaint) {
			if (getHoverNode() != node) {
				//AppletContext context = explorer.getAppletContext();
				if (node == null) {
					//context.showStatus("");
					setToolTipText(null);
				} else {
					AttributeManager attrMgr = getGraph().getAttributeManager();
					String href = (String) attrMgr.getAttribute("xlink:href",node);
					if (href == null)
						href = "No reference";
					//context.showStatus(href);
					setToolTipText(href);
				}
			}
			super.setHoverNode(node,repaint);
			//if (getHoverNode() == null)
	        //explorer.getAppletContext().showStatus("");
		}
		
		/**
	     * Overwrites a method by an empty body.
	     * @param e A mouse event.
		 */
	    
		protected void logoClicked(MouseEvent e) {
			
		}
		
		/**
		 * @see java.awt.event.MouseMotionListener#mouseMoved(java.awt.event.MouseEvent)
		 */
		
		public void mouseMoved(MouseEvent e) {
			super.mouseMoved(e);
		}
		
		/** 
	     * @see gui.hyper.MyGraphPanel#nodeClicked(int, hypergraph.graphApi.Node)
		 */
		
		public void nodeClicked(int clickCount, hypergraph.graphApi.Node node) {
			super.nodeClicked(clickCount,node);
			/*if (clickCount == 2) {
				AttributeManager attrMgr = getGraph().getAttributeManager();
				String href = (String) attrMgr.getAttribute("xlink:href",node);
				String target = getPropertyManager().getString("gui.hypergraph.applications.hexplorer.GraphPanel.target");
				if (target==null)
					target="_top";
				try {
					//URL url = new URL(explorer.getDocumentBase(),href);				
					//AppletContext context = explorer.getAppletContext();
					//context.showDocument(url,target);
				}
				catch (Exception ex)
				{
					System.out.println(ex);
				}
			}*/
		}
	}
}
