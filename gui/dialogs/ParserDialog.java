package gui.dialogs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import gui.Viewer;
import gui.util.JarClassLoader;
import nenok.Knowledgebase;
import nenok.LCFactory;
import nenok.jtconstr.ConstrException;
import nenok.lc.JoinTree;
import nenok.parser.JT_Parser;
import nenok.parser.KB_Parser;
import nenok.parser.Parser;
import nenok.parser.ResultSet;

/**
 * Internal frame to parse data files.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 565 $<br/>$LastChangedDate: 2008-03-26 14:39:54 +0100 (Mi, 26 Mrz 2008) $
 */

public class ParserDialog extends JDialog {
	
	/**
	 * Parsing mode. Determines if data represents serialized 
	 * knowledgebase or jointree.
	 * 
	 * @author Marc Pouly
	 * @version 1.4
	 */
	
	public enum Mode {
		
		/**
		 * Knowledgebase mode:
		 */
		
		Knowledgebase, 
		
		/**
		 * Jointree mode:
		 */
		
		JoinTree}
	
	private Viewer viewer;
	private File file, parser;
	private Knowledgebase kb;
	private JoinTree tree;
	private Mode mode;
	private File filePath, parserPath;
		
	/**
	 * Constructor:
	 * @param viewer The main application.
	 * @param mode Indicated either knowledgebase of tree parsing.
	 */
	
	public ParserDialog(Viewer viewer, Mode mode) {
		super(viewer, true);
		this.viewer = viewer;
		this.mode = mode;
		
		this.setTitle("Parse Data File");
		this.setBackground(java.awt.Color.white);
		this.setBounds(new java.awt.Rectangle(0,0,0,0));
		this.setResizable(false);

		this.setContentPane(new ParserPanel());
		this.setSize(new Dimension(350,180));
		
        this.setLocation((viewer.getWidth() - this.getWidth()) / 2, (viewer.getHeight() - this.getHeight()) / 3);
		this.setVisible(true);
	}
	
	/**
	 * Content pane.
	 * 
	 * @author Marc Pouly
	 * @version 1.2
	 */

	public class ParserPanel extends JPanel {
		
		private JTextField fileField, parserField;
		private JButton fileButton, parserButton, parseButton;
		
		/**
		 * Constructor:
		 */
		
		public ParserPanel() {
			super();
			initialize();
		}
		
		/**
		 * This method initializes all graphical components:
		 */
		
		private void initialize() {
			
			GridBagConstraints gbc01 = new GridBagConstraints();
			gbc01.fill = java.awt.GridBagConstraints.BOTH;
			gbc01.gridx = 0;
			gbc01.gridy = 0;
			gbc01.weightx = 1.0;
			gbc01.insets = new java.awt.Insets(10,10,5,5);
			
			GridBagConstraints gbc02 = new GridBagConstraints();
			gbc02.fill = java.awt.GridBagConstraints.BOTH;
			gbc02.gridx = 0;
			gbc02.gridy = 1;
			gbc02.weightx = 1.0;
			gbc02.insets = new java.awt.Insets(5,10,5,5);
			
			GridBagConstraints gbc03 = new GridBagConstraints();
			gbc03.fill = java.awt.GridBagConstraints.BOTH;
			gbc03.gridx = 1;
			gbc03.gridy = 1;
			gbc03.insets = new java.awt.Insets(5,5,5,10);
			
			GridBagConstraints gbc04 = new GridBagConstraints();
			gbc04.fill = java.awt.GridBagConstraints.BOTH;
			gbc04.gridx = 0;
			gbc04.gridy = 2;
			gbc04.weightx = 1.0;
			gbc04.insets = new java.awt.Insets(5,10,5,5);
			
			GridBagConstraints gbc05 = new GridBagConstraints();
			gbc05.fill = java.awt.GridBagConstraints.BOTH;
			gbc05.gridx = 0;
			gbc05.gridy = 3;
			gbc05.weightx = 1.0;
			gbc05.insets = new java.awt.Insets(5,10,5,5);
			
			GridBagConstraints gbc06 = new GridBagConstraints();
			gbc06.fill = java.awt.GridBagConstraints.BOTH;
			gbc06.gridx = 1;
			gbc06.gridy = 3;
			gbc06.insets = new java.awt.Insets(5,5,5,10);
			
			GridBagConstraints gbc07 = new GridBagConstraints();
			gbc07.fill = java.awt.GridBagConstraints.VERTICAL;
			gbc07.gridx = 0;
			gbc07.gridy = 4;
			gbc07.gridwidth = 2;
			gbc07.insets = new java.awt.Insets(5,10,10,10);			
			
			this.setLayout(new GridBagLayout());
			
			this.add(getFileLabel(), gbc01);
			this.add(getFileField(), gbc02);
			this.add(getFileButton(), gbc03);
			this.add(getParserLabel(), gbc04);
			this.add(getParserField(), gbc05);
			this.add(getParserButton(), gbc06);
			this.add(getParseButton(), gbc07);
		}
		
		/**
		 * @return The first text label.
		 */
		
		private JLabel getFileLabel() {
			JLabel label = new JLabel();
			if(mode.equals(Mode.Knowledgebase)) {
				label.setText("Choose Knowledgebase File: ");
			} else {
				label.setText("Choose Jointree File: ");
			}
			return label;
		}
		
		/**
		 * @return The text field for the knowledgebase file name.
		 */
		
		private JTextField getFileField() {
			if (fileField == null) {
				fileField = new JTextField();
				fileField.setText(" -");
				fileField.setEditable(false);
				fileField.setBackground(Color.WHITE);
			}
			return fileField;
		}
		
		/**
		 * @return The button to search the data file.
		 */
		
		private JButton getFileButton() {
			if (fileButton == null) {
				fileButton = new JButton();
				fileButton.setText("...");
				fileButton.setFocusPainted(false);
				fileButton.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent e) {
				        JFileChooser fileChooser = new JFileChooser();
				        
				        if(filePath == null) {
				        	fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
				        } else {
				        	fileChooser.setCurrentDirectory(filePath);
				        }
				        
				        fileChooser.setDialogTitle("Choose Knowledgebase File");
				        fileChooser.setAcceptAllFileFilterUsed(false);
				        
				        javax.swing.filechooser.FileFilter all = new javax.swing.filechooser.FileFilter() {
				            public boolean accept(File file) {
				                return true;
				            }

				            public String getDescription() {
				                return " *.*";
				            }
				        }; 
				        javax.swing.filechooser.FileFilter xml = new javax.swing.filechooser.FileFilter() {
				            public boolean accept(File file) {
				                String filename = file.getName();
				                return filename.endsWith(".xml") || file.isDirectory();
				            }

				            public String getDescription() {
				                return " *.xml (XML File)";
				            }
				        }; 
				        
				        fileChooser.setFileFilter(xml);
				        fileChooser.setFileFilter(all);
				        
				        int value = fileChooser.showOpenDialog(viewer);
				        if (value == JFileChooser.APPROVE_OPTION) {
				        	file = fileChooser.getSelectedFile();
				            filePath = file;
				            fileField.setText(" "+file.getName());
				            if(file != null && parser != null) {
								parseButton.setEnabled(true);
							} 
				        }
					}
				});
			}
			return fileButton;
		}

		/**
		 * @return The second text label.
		 */
		
		private JLabel getParserLabel() {
			JLabel label = new JLabel("Choose JAR with Parser Implementation: ");
			return label;
		}
		
		/**
		 * @return The text field for the parser JAR file name.
		 */
		
		private JTextField getParserField() {
			if (parserField == null) {
				parserField = new JTextField();
				parserField.setText(" -");
				parserField.setEditable(false);
				parserField.setBackground(Color.WHITE);
			}
			return parserField;
		}
		
		/**
		 * @return The button to search the parser JAR file.
		 */
		
		private JButton getParserButton() {
			if (parserButton == null) {
				parserButton = new JButton();
				parserButton.setFocusPainted(false);
				parserButton.setText("...");
				parserButton.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent e) {
				        JFileChooser fileChooser = new JFileChooser();
				        
				        if(parserPath == null) {
				        	fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
				        } else {
				        	fileChooser.setCurrentDirectory(parserPath);
				        }
				        
				        fileChooser.setDialogTitle("Choose Parser Class");
				        fileChooser.setAcceptAllFileFilterUsed(false);
				        javax.swing.filechooser.FileFilter format = new javax.swing.filechooser.FileFilter() {
				            public boolean accept(File file) {
				                String filename = file.getName();
				                return filename.endsWith(".jar") || file.isDirectory();
				            }

				            public String getDescription() {
				                return " *.jar (Java Archive)";
				            }
				        };   
				        fileChooser.addChoosableFileFilter(format);
				        int value = fileChooser.showOpenDialog(viewer);
				        if (value == JFileChooser.APPROVE_OPTION) {
				        	parser = fileChooser.getSelectedFile();
				        	parserPath = parser;
				            parserField.setText(" "+parser.getName());
				            if(file != null && parser != null) {
								parseButton.setEnabled(true);
							} 
				        }
					}
				});
			}
			return parserButton;
		}

		/**
		 * @return The button to start the parsing process.
		 */
		
		private JButton getParseButton() {
			if (parseButton == null) {
				parseButton = new JButton();
				parseButton.setFocusPainted(false);
				parseButton.setEnabled(false);
				parseButton.setText("Parse");
				parseButton.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent e) {
						if(file == null || parser == null) {
							parseButton.setEnabled(false);
							return;
						}
											
						Parser instance = null;
						
						/*
						 * For jar files:
						 */
						
						if(parser.getName().endsWith(".jar")) {
							
							try {
								addFile(parser);
							} catch (IOException e1) {
								viewer.error("Class Loader Error", e1);
								return;
							}
							
							JarClassLoader jarLoader = new JarClassLoader(parser.getAbsolutePath());
							try {
								instance = jarLoader.loadParser(file, mode);
							} catch (Exception e1) {
								viewer.error("Class Loader Error", e1);
								return;
							}
							
							if(instance == null) {
								viewer.error("Initialization Problem", "Unable to initialize parser instance. Null obtained.");
								return;
							}
							
							// Parse Knowledgebase:
							if(mode.equals(Mode.Knowledgebase)) {
								ResultSet set = null;
								try {
									set = ((KB_Parser)instance).parse(file);
								} catch (Exception e1) {
									viewer.error("Parsing Error", e1);
									return;
								}
								if(set != null) {
									
									kb = new Knowledgebase(set.vals, file.getName());
									ParserDialog.this.setVisible(false);

								} else {
									viewer.error("Parsing Error", "Obtained null as result set.");
								}
								return;
							}
							
							// Parse Jointree:
							if(mode.equals(Mode.JoinTree)) {
					            try {
									LCFactory factory = new LCFactory();
									tree = factory.rebuild(file, (JT_Parser)instance);
									ParserDialog.this.setVisible(false);
								} catch (ConstrException e1) {
									viewer.error("Construction Error", e1);
									return;
								}
								return;
							}
						} else {
							JOptionPane.showMessageDialog(viewer, "The given file is not a Java archive.", "I/O Error", JOptionPane.WARNING_MESSAGE);
							return;
						}
					}
				});
			}
			return parseButton;
		}
	}
	
	/**
	 * @return The created Knowledgebase.
	 */
	
	public Knowledgebase getKnowledgebase() {
		return kb;
	}
	
	/**
	 * @return The created jointree.
	 */
	
	public JoinTree getJoinTree() {
		return tree;
	}
	
	/**
	 * @return The name of the parsed file.
	 */
	
	public String getFileName() {
		if(file != null) {
			return file.getName();
		}
		return null;
	}
	
	/*
	 * Helper methods:
	 */
	
	/**
	 * Adds a file to the system classpath.
	 * @param file The file to add.
	 * @throws IOException Exception thrown by the classpath modification process.
	 */
	
	private static void addFile(File file) throws IOException {
		addURL(file.toURI().toURL());
	}
	
	/**
	 * Adds a URL to the system classpath.
	 * @param url The URL to add.
	 * @throws IOException Exception thrown by the classpath modification process.
	 */
	 
	private static void addURL(URL url) throws IOException {
		URLClassLoader sysloader = (URLClassLoader)ClassLoader.getSystemClassLoader();
		Class<?> sysclass = URLClassLoader.class;
		try {
			Method method = sysclass.getDeclaredMethod("addURL", new Class<?>[]{URL.class});
			method.setAccessible(true);
			method.invoke(sysloader,new Object[]{ url });
		} catch (Throwable t) {
			t.printStackTrace();
			throw new IOException("Error, could not add URL to system classloader");
		}	
	}
}
