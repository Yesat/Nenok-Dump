package gui.dialogs;

import gui.Viewer;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.MatteBorder;

/**
 * Internal frame to display error messages &amp; stack traces.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 565 $<br/>$LastChangedDate: 2008-03-26 14:39:54 +0100 (Mi, 26 Mrz 2008) $
 */

public class ErrorDialog extends JDialog {
	
	private final String title, message;
		
	/**
	 * Constructor:
	 * @param viewer The main application.
	 * @param title The title of the error dialog.
	 * @param ex The exception that caused the error.
	 */
	
	public ErrorDialog(Viewer viewer, String title, Exception ex) {
		super(viewer, true);
		
		this.message = stackToString(ex);
		this.title = title;
		
		this.setTitle("Error");
		this.setBackground(java.awt.Color.white);
		this.setBounds(new java.awt.Rectangle(0,0,0,0));
		this.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12));
		this.setResizable(true);

		Panel cp = new Panel();
		this.setContentPane(cp);
		this.setSize(new Dimension(700,400));
		
        this.setLocation((viewer.getWidth() - this.getWidth()) / 2, (viewer.getHeight() - this.getHeight()) / 3);
		this.setVisible(true);
	}
	
	/**
	 * Constructor:
	 * @param viewer The main application.
	 * @param title The title of the error dialog.
	 * @param message The error message.
	 */
	
	public ErrorDialog(Viewer viewer, String title, String message) {
		super(viewer, true);
		
		this.message = message;
		this.title = title;
		
		this.setTitle("Error");
		this.setBackground(java.awt.Color.white);
		this.setBounds(new java.awt.Rectangle(0,0,0,0));
		this.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12));
		this.setResizable(true);

		Panel cp = new Panel();
		this.setContentPane(cp);
		this.setSize(new Dimension(500,300));
		
        this.setLocation((viewer.getWidth() - this.getWidth()) / 2, (viewer.getHeight() - this.getHeight()) / 3);
		this.setVisible(true);
	}
	
	/**
	 * Transformes the stack trace to a formatted string.
	 * @param stack The stack trace.
	 * @return A formatted string containing the stack trace.
	 */
	
	private static String stackToString(Throwable t) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		t.printStackTrace(pw);
		pw.flush();
		sw.flush();
		String out = "<html><div style='font-family:courier; font-size:12pt'>"+encodeHTML(sw.toString())+"</div></html>";
		String msg = encodeHTML(t.getMessage());
		if(t.getMessage() != null) {
			out = out.replaceAll(msg, "<span style='color:red'>"+msg+"</span>");
		}
		String result = "";
		Pattern p = Pattern.compile("[^\\(]*.java:[0-9]*");
		Matcher m = p.matcher(out);
		int start = 0;
		while(m.find()) {
			result += out.substring(start, m.start());
			result += "<span style='color:blue'>"+m.group()+"</span>";
			start = m.end();
		}
		result += out.substring(start, out.length());
		return result;
	}
	
	/**
	 * Inner panel of this dialog.
	 * 
	 * @author Marc Pouly
	 * @version 1.0
	 */
	
	private class Panel extends JPanel {
							
		private JLabel image = null;
		private JLabel titleLabel = null;
		private JEditorPane editor = null;
		private JButton closeButton = null;
		private JScrollPane scroller = null;
		
		/**
		 * Constructor:
		 */
		
		public Panel() {
			super();
			initialize();
		}
	
		/**
		 * Initializes this component.
		 */
		
		private void initialize() {		
			GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
			gridBagConstraints4.fill = java.awt.GridBagConstraints.BOTH;
			gridBagConstraints4.gridy = 2;
			gridBagConstraints4.weightx = 1.0;
			gridBagConstraints4.weighty = 1.0;
			gridBagConstraints4.gridwidth = 2;
			gridBagConstraints4.insets = new java.awt.Insets(15,20,5,20);
			gridBagConstraints4.gridx = 0;
	        GridBagConstraints gridBagConstraints21 = new GridBagConstraints();
	        gridBagConstraints21.gridx = 0;
	        gridBagConstraints21.insets = new java.awt.Insets(5,20,10,20);
	        gridBagConstraints21.gridwidth = 2;
	        gridBagConstraints21.fill = java.awt.GridBagConstraints.VERTICAL;
	        gridBagConstraints21.anchor = java.awt.GridBagConstraints.CENTER;
	        gridBagConstraints21.gridy = 4;
	        GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
	        gridBagConstraints11.gridx = 0;
	        gridBagConstraints11.gridwidth = 2;
	        gridBagConstraints11.fill = java.awt.GridBagConstraints.BOTH;
	        gridBagConstraints11.insets = new java.awt.Insets(5,20,5,20);
	        gridBagConstraints11.gridy = 1;
	        GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
	        gridBagConstraints1.ipadx = 1;
	        gridBagConstraints1.insets = new java.awt.Insets(20,5,5,20);
	        gridBagConstraints1.anchor = java.awt.GridBagConstraints.CENTER;
	        gridBagConstraints1.gridx = 1;
	        gridBagConstraints1.gridy = 0;
	        gridBagConstraints1.fill = java.awt.GridBagConstraints.BOTH;
	        GridBagConstraints gridBagConstraints = new GridBagConstraints();
	        gridBagConstraints.gridx = 0;
	        gridBagConstraints.insets = new java.awt.Insets(20,20,5,5);
	        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
	        gridBagConstraints.gridy = 0;
	        this.setLayout(new GridBagLayout());
	        this.add(getImage(), gridBagConstraints);
	        this.add(getTitle(), gridBagConstraints1);
	        this.add(getCloseButton(), gridBagConstraints21);
	        this.add(getScroller(), gridBagConstraints4);
		}
				
		/**
		 * @return The label indicating the error type.
		 */
		
		private JLabel getTitle() {
			if(titleLabel == null) {
		        titleLabel = new JLabel();
		        titleLabel.setText(title);
		        titleLabel.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 14));
			}
			return titleLabel;
		}
		
		/**
		 * @return The label containing the warning symbol.
		 */
		
		private JLabel getImage() {
			if (image == null) {
				image = new JLabel(javax.swing.UIManager.getIcon("OptionPane.errorIcon"));
			}
			return image;
		}

		/**
		 * @return The stack trace text area.
		 */
		
		private JEditorPane getTextArea() {
			if (editor == null) {
				editor = new JEditorPane();
				editor.setContentType("text/html");
				editor.setEditable(false);
				editor.setFocusable(true);
				editor.setText(message);
				editor.setBorder(new MatteBorder(10,10,10,10,editor.getBackground()));
				editor.setCaretPosition(0);
			}
			return editor;
		}	
		
		/**
		 * @return The editor's scroller.
		 */
		
		private JScrollPane getScroller() {
			if (scroller == null) {
				scroller = new JScrollPane();
				scroller.setViewportView(getTextArea());
			}
			return scroller;
		}
		
		/**
		 * @return The button to close this frame.
		 */
		
		private JButton getCloseButton() {
			if (closeButton == null) {
				closeButton = new JButton("Close");
				closeButton.setFocusPainted(false);
				closeButton.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12));
				closeButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						ErrorDialog.this.dispose();
					}
				});
			}
			return closeButton;
		}
	}
	
	/**
	 * HTML string encoding:
	 * @param s The input String.
	 * @return The string encoded as HTML.
	 */
	
	public static String encodeHTML(String s) {
	    StringBuffer out = new StringBuffer();
	    for(int i=0; i<s.length(); i++) {
	        char c = s.charAt(i);
	        if(c > 127 || c=='"' || c=='<' || c=='>' || c=='[' || c==']' || c=='{' || c == '}') {
	           out.append("&#"+(int)c+";");
	        }
	        else {
	            out.append(c);
	        }
	    }
	    return out.toString();
	}
}
