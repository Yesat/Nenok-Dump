package gui.dialogs;

import gui.Viewer;
import gui.util.ImageCanvas;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * About dialog of NENOK.
 * 
 * @author Marc Pouly
 */

public class AboutDialog extends JDialog {
	
	/*
	 * Content:
	 */
	
	private static String VERSION = "2.0";
	
	private static final String CONTENT = ""
		+ "<html>"
		+ "<table>"
		+ "<tr>"
		+ "<td valign='top'>Author:<td>"
		+ "<td>Marc Pouly<br>TCS Research Group<br>University of Fribourg<br>Switzerland</td>"
		+ "</tr>" + "<tr>" + "<td valign='top'>Version:<td>"
		+ "<td>"+VERSION+"</td>" + "</tr>" + "</table>" + "</html>";
	
	private static final String THANKS = ""
		+ "<html>"
		+ "<div width='330'>"
		+ "My special thanks go to Cesar Schneuwly &amp; Antoine de Groote which both contributed to a great extend to this project." 
		+ "</div></html>";

	/**
	 * Constructor:
	 * @param viewer The main application.
	 */
	
	public AboutDialog(Viewer viewer) {
		super(viewer, true);
		
		this.setTitle("About");
		this.setBackground(java.awt.Color.white);
		this.setBounds(new java.awt.Rectangle(0,0,0,0));
		this.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12));
		this.setResizable(true);

		Panel cp = new Panel();
		this.setContentPane(cp);
		this.pack();
		
        this.setLocation((viewer.getWidth() - this.getWidth()) / 2, (viewer.getHeight() - this.getHeight()) / 3);
		this.setVisible(true);
		this.setResizable(false);
	}
	
	/**
	 * The class defining the content pane of this dialog.
	 * 
	 * @author Marc Pouly
	 * @version 1.2
	 */
	
	private class Panel extends JPanel {
		
		private ImageCanvas image = null;
		private JLabel www = null;
		private JLabel text = null;
		private JLabel thanks = null;
		private AboutDialog.Title title = null;
	
		/**
		 * This is the default constructor
		 */
	
		public Panel() {
			super();
			initialize();
		}
		
		/**
		 * @return The TCS image.
		 */
	
		private ImageCanvas getImage() {
			if (image == null) {
				ClassLoader loader = this.getClass().getClassLoader();
				URL url = loader.getResource("tcs.gif");
				if(url == null) {
					System.err.println("File tcs.gif not found.");
				}
				image = new ImageCanvas(new ImageIcon(url));
				image.setPreferredSize(new java.awt.Dimension(130, 100));
			}
			return image;
		}
	
		/**
		 * @return The title 2D Transform.
		 */
		
		private AboutDialog.Title getTitle() {
			if (title == null) {
				title = new AboutDialog.Title();
				title.setPreferredSize(new java.awt.Dimension(10, 45));
			}
			return title;
		}
		
		/**
		 * Initializes this component.
		 */
	
		private void initialize() {
			GridBagConstraints gridBagConstraints7 = new GridBagConstraints();
			gridBagConstraints7.insets = new java.awt.Insets(10,20,0,20);
			gridBagConstraints7.gridwidth = 2;
			gridBagConstraints7.fill = java.awt.GridBagConstraints.BOTH;
			GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
			gridBagConstraints5.gridx = 1;
			gridBagConstraints5.fill = java.awt.GridBagConstraints.BOTH;
			gridBagConstraints5.insets = new java.awt.Insets(10,0,0,20);
			gridBagConstraints5.gridy = 1;
			text = new JLabel();
			text.setText(CONTENT);
			text.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 14));
			GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
			gridBagConstraints3.gridx = 0;
			gridBagConstraints3.gridwidth = 2;
			gridBagConstraints3.anchor = java.awt.GridBagConstraints.WEST;
			gridBagConstraints3.insets = new java.awt.Insets(10,20,10,10);
			gridBagConstraints3.fill = java.awt.GridBagConstraints.HORIZONTAL;
			gridBagConstraints3.gridy = 2;
			www = new JLabel();
			www.setText("<html><a style='text-decoration:none;font-weight:normal' href=\"http://diuf.unifr.ch/nenok\">http://diuf.unifr.ch/nenok</a></html>");
			www.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 14));
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.anchor = java.awt.GridBagConstraints.NORTH;
			gridBagConstraints1.gridx = 0;
			gridBagConstraints1.gridy = 0;
			gridBagConstraints1.gridwidth = 2;
			gridBagConstraints1.insets = new java.awt.Insets(10, 20, 0, 20);
			gridBagConstraints1.fill = java.awt.GridBagConstraints.BOTH;
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 0;
			gridBagConstraints.insets = new java.awt.Insets(10,20,0,0);
			gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
			gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
			gridBagConstraints.gridy = 1;
			
			GridBagConstraints gridBagConstraints8 = new GridBagConstraints();
			gridBagConstraints8.gridx = 0;
			gridBagConstraints8.gridwidth = 2;
			gridBagConstraints8.anchor = java.awt.GridBagConstraints.CENTER;
			gridBagConstraints8.insets = new java.awt.Insets(0,20,10,20);
			gridBagConstraints8.fill = java.awt.GridBagConstraints.HORIZONTAL;
			gridBagConstraints8.gridy = 3;
			thanks = new JLabel();
			thanks.setText(THANKS);
			thanks.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 10));
			
			this.setLayout(new GridBagLayout());
			this.setVisible(true);
			this.add(getTitle(), gridBagConstraints7);
			this.add(getImage(), gridBagConstraints);
			this.add(text, gridBagConstraints5);
			this.add(www, gridBagConstraints3);
			this.add(thanks, gridBagConstraints8);
		}
	}
	
	/**
	 * A JAVA 2D class that defines the title of this dialog.
	 * 
	 * @author Marc Pouly
	 * @version 1.2
	 */

	public static class Title extends JPanel {

		private Color back = new Color(25, 25, 112);

		private TextLayout text = new TextLayout("The NENOK Project", 
				new Font("Helvetica", 1, 36), new FontRenderContext(null, false, false));

		/**
		 * Paints the title.
		 */

		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D) g;
			AffineTransform textAt = new AffineTransform();
			textAt.translate(0, text.getBounds().getHeight());
			g2.setPaint(Color.WHITE);
			g2.fill(text.getOutline(textAt));
			g2.setPaint(back);
			g2.draw(text.getOutline(textAt));
		}
	}
}
