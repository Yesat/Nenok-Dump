package gui.dialogs;

import gui.Viewer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import org.jdesktop.swingworker.SwingWorker;
import javax.swing.border.LineBorder;

/**
 * Abstract JInternalFrame that unifies all jointree displayers.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 565 $<br/>$LastChangedDate: 2008-03-26 14:39:54 +0100 (Mi, 26 Mrz 2008) $
 */

public abstract class Dialog extends JInternalFrame {
	
	protected Viewer viewer;
	
    private JMenuBar menubar;
    private JMenuItem exportItem;
	
	private boolean maximum = false;
    protected final Dimension normal;
	
	/**
	 * Constructor:
	 * @param viewer The main application.
	 */
	
	public Dialog(Viewer viewer) {
		this.viewer = viewer;
		
		this.setBackground(java.awt.Color.white);
		this.setBounds(new java.awt.Rectangle(0,0,0,0));
		this.setBorder(new LineBorder(Color.BLACK));
		
		this.setResizable(true);
		this.setClosable(true);
		this.setIconifiable(true);
		this.setMaximizable(true);

		this.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 12));
		
		this.normal = new Dimension(800, viewer.getControllPanelHeight() - 3);
		
		this.setSize(normal);
		this.setLocation(viewer.getControllPanelWidth() + 30, -1);
		
		this.setJMenuBar(getMenu());
	}
	
	/**
	 * Determines the maximum size of this frame.
	 */
	
	public void setMaximum(boolean max) throws PropertyVetoException {
		if(!maximum) {
			maximum = true;
			Dimension big = new Dimension(viewer.getWidth() - viewer.getControllPanelWidth() - 
					viewer.getInsets().left - viewer.getInsets().right + 2, viewer.getControllPanelHeight() - 3);
			this.setSize(big);
			this.setLocation(viewer.getControllPanelWidth() - 1, -1);
		} else {
			maximum = false;
			this.setSize(normal);
			this.setLocation(viewer.getControllPanelWidth() + 30, -1);
		}
	}
	
	/**
	 * @return The menubar of this window.
	 */
	
	protected JMenuBar getMenu() {
		if (menubar == null) {
			menubar = new JMenuBar();
			
	        JMenu fileMenu = new JMenu("File");
	        menubar.add(fileMenu);
	        
	        // Save As Menu:
	        exportItem = new JMenuItem("Save As ...");
	        exportItem.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent e) {
	                export();
	            }
	        });
	        fileMenu.add(exportItem);
		}
		return menubar;
	}
	
    /**
     * Export procedure to export the current canvas into different formats.
     */
     
    private void export() {
    	    	
		// Open FileChooser:
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Save Graphic As ...");
		fileChooser.setAcceptAllFileFilterUsed(false);
	
		// Add filter allowing .png files:
		javax.swing.filechooser.FileFilter png = new javax.swing.filechooser.FileFilter() {
			public boolean accept(File file) {
				String filename = file.getName();
				return filename.endsWith(".png") || file.isDirectory();
			}
	
			public String getDescription() {
				return " *.png ( Portable Network Graphics)";
			}
		};
		fileChooser.addChoosableFileFilter(png);
	
		// Add filter allowing .dot files:
		javax.swing.filechooser.FileFilter dot = new javax.swing.filechooser.FileFilter() {
			public boolean accept(File file) {
				String filename = file.getName();
				return filename.endsWith(".dot");
			}
	
			public String getDescription() {
				return " *.dot (Graphviz Format)";
			}
		};
		fileChooser.addChoosableFileFilter(dot);
	
		/*
		 * Open dialog:
		 */
	
		int value = fileChooser.showSaveDialog(Dialog.this);
		if (value == JFileChooser.APPROVE_OPTION) {
			String path = fileChooser.getSelectedFile().getAbsolutePath();
	
			/*
			 * PNG files:
			 */
	
			if(fileChooser.getFileFilter() == png) {
				if (!path.endsWith(".png")) {
					path += ".png";
				}
				
				final File file = new File(path);
				
				setCursor(Viewer.HOURGLASS);
				
				/*
				 * This swing worker performs heavy computations in background and
				 * allows the user to continue working on other swing components.
				 */
				
				new SwingWorker<File, Object>() {
					
					@Override
					protected File doInBackground() throws Exception {
						savePNG(file);
						return file;
					}
					
					@Override
					protected void done() {
						setCursor(Viewer.NORMAL); 
					}
				}.execute();
			}
			
			/*
			 * DOT files:
			 */
	
			if(fileChooser.getFileFilter() == dot) {
				if (!path.endsWith(".dot")) {
					path += ".dot";
				}
				
				final File file = new File(path);
				new Thread(new Runnable() {
					public void run() {
						try {
							setCursor(Viewer.HOURGLASS);
							saveDOT(file);
							setCursor(Viewer.NORMAL);
						} catch (Exception e) {
							viewer.error("I/O Error", e);
						}
					}
				}).start();
			}
		}
		
	}
    	
	/**
	 * Exports this canvas as PNG file.
	 * @param file The file into which the panel content is written.
	 */
	
    private void savePNG(File file) throws IOException {
    	BufferedImage image = new BufferedImage(getCanvas().getWidth(), getCanvas().getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g = image.createGraphics();				
        getCanvas().paint(g);
        ImageIO.write(image, "png", file);
        g.dispose();
    }
    
	/**
	 * Exports this canvas as DOT file.
	 * @param file The file into which the panel content is written.
	 */
	
    protected abstract void saveDOT(File file) throws Exception;
	
	/**
	 * @return The drawing canvas of this frame.
	 */
	
    protected abstract JComponent getCanvas();
    
}
