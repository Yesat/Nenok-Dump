package performance;

import java.util.HashSet;
import java.util.Random;

import instances.distances.Distances;
import nenok.Architecture;
import nenok.Knowledgebase;
import nenok.LCFactory;
import nenok.lc.JoinTree;
import nenok.lc.id.IDJoinTree;
import nenok.va.Domain;
import nenok.va.StringVariable;
import nenok.va.Variable;

public class Querying {
	
	// Max. number of variables per factor:
	private static final int FACTOR = 5;
	
	// Total number of variables:
	private static final int UNIVERSE = 100;
	
	// Knowledgebase size:
	private static final int KBSIZE = 25;
		
	// Largest distance:
	private static final int MAX = 10;
	
	// Number of queries:
	private static final int QUERIES = 100;
	
	// Number of queries:
	private static final int REPEAT = 5;	
	
	/**
	 * Main method:
	 * @param args Standard input arguments
	 * @throws Exception Generic Exception
	 */
	
	public static final void main(String[] args) throws Exception {
		
		int width = 0;
		double start, end;

		/*
		 * Knowledgebase:
		 */
		
		Knowledgebase data = data();
		Variable[] vars = Domain.totalDomain(data.toArray()).toArray();
		
		LCFactory factory = new LCFactory();
		factory.setArchitecture(Architecture.Idempotent);
		
		/*
		 * Query set:
		 */
		
		Random generator = new Random();
		HashSet<Domain> queries = new HashSet<Domain>();
				
		for (int j = 0; j < QUERIES; j++) {
			Variable v1 = vars[generator.nextInt(vars.length)];
			Variable v2 = vars[generator.nextInt(vars.length)];
			Domain dom = new Domain(v1, v2);
			if(dom.size() == 2 && !queries.contains(dom)) {
				queries.add(dom);
			} else {
				j--;
			}
		}
		
		double percent = (queries.size()*100)/(vars.length*vars.length/2);
		
		System.out.println("*************************************************");
		System.out.println(" Measurement Setting:");
		System.out.println("*************************************************");
		System.out.println(" Number of variables: "+vars.length);
		System.out.println(" Number of knowledgebase factors: "+data.size());
		System.out.println(" Absolute number of {X,Y} queries: "+queries.size());
		System.out.println(" Relative number of {X,Y} queries: "+percent+"%");
		System.out.println(" Domain size of knowledgebase factors: "+FACTOR);
		System.out.println(" Test repetitions for average time: "+REPEAT);
		
		
		/*
		 * Test 1: New join tree for each query:
		 */
		
		System.out.println();
		System.out.println("*************************************************");
		System.out.println(" Test 1: New join tree for each query:");
		System.out.println("*************************************************");
		
		width = Integer.MAX_VALUE;
		start = System.currentTimeMillis();
		for (int i = 0; i < REPEAT; i++) {
			for (Domain query : queries) {
				JoinTree tree = factory.create(data, query);
				tree.propagate();
				tree.answer(query);
			}
		}
		end = System.currentTimeMillis();
		System.out.println("Execution time: "+((end - start) / REPEAT)+" ms");
		
		
		/*
		 * Test 2: One join tree for all queries:
		 */
		
		System.out.println();
		System.out.println("*************************************************");
		System.out.println(" Test 2: One join tree for all queries:");
		System.out.println("*************************************************");
		
		start = System.currentTimeMillis();
		for (int i = 0; i < REPEAT; i++) {
			JoinTree tree = factory.create(data, queries);
			tree.propagate();
			width = tree.getTreeWidth();
			for (Domain query : queries) {
				tree.answer(query);
			}
		}
		end = System.currentTimeMillis();
		System.out.println("Execution time: "+((end - start) / REPEAT)+" ms");
		System.out.println("Treewidth: "+width);
		
		
		/*
		 * Test 3: Online query answering:
		 */
		
		System.out.println();
		System.out.println("*************************************************");
		System.out.println(" Test 3: New online query answering procedure:");
		System.out.println("*************************************************");
		
		width = Integer.MAX_VALUE;
		start = System.currentTimeMillis();
		for (int i = 0; i < REPEAT; i++) {
			IDJoinTree tree = (IDJoinTree)factory.create(data);
			tree.propagate();
			width = tree.getTreeWidth();
			for (Domain query : queries) {
				Variable[] qvars = query.toArray(); 
				tree.answer(qvars[0], qvars[1]);
			}
		}
		end = System.currentTimeMillis();
		System.out.println("Execution time: "+((end - start) / REPEAT)+" ms");
		System.out.println("Treewidth: "+width);
	}
	
	/**
	 * Generates a random knowledgebase.
	 */
	
	public static Knowledgebase data() {
		
		Random generator = new Random();
		
		/**
		 * Variables:
		 */
		
		StringVariable[] vars = new StringVariable[UNIVERSE];
		for (int i = 0; i < vars.length; i++) {
			vars[i] = new StringVariable("v"+i);
		}
		
		/**
		 * Valuations:
		 */
		
		Distances[] vals = new Distances[KBSIZE];
	
		for (int i = 0; i < vals.length; i++) {
			
			/*
			 * Produce domain:
			 */
			
			HashSet<StringVariable> set = new HashSet<StringVariable>();
			for (int j = 0; j < FACTOR; j++) {
				set.add(vars[generator.nextInt(UNIVERSE)]);
			}
			StringVariable[] dom = set.toArray(new StringVariable[set.size()]);
			
			/*
			 * Produce matrix:
			 */
			
			int[][] matrix = new int[dom.length][dom.length];
			
			for (int j = 0; j < matrix.length; j++) {
				for (int k = 0; k < matrix.length; k++) {
					matrix[j][k] = generator.nextInt(MAX);
				}
			}
			
			vals[i] = new Distances(dom, matrix);
		}
		
		return new Knowledgebase(vals, "Random Instance");
	}
}
