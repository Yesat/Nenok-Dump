package instances.probability;

import nenok.path.KValuation;
import nenok.path.SquareMatrix;
import nenok.semiring.Kleene;
import nenok.semiring.Semiring;
import nenok.va.Variable;

/**
 * This class offers an implementation of the maximum reliability problem in graphs.
 * It is based on the probabilistic (t-norm) semiring that forms a Kleene algebra and therefore extends
 * the {@link KValuation} class.
 *  
 * @author Marc Pouly
 */

public class MaxReliability extends KValuation<Double> {
		
	/*
	 * Kleene Algebra Implementation:
	 */
	
	private static final Kleene<Double> KLEENE = new Kleene<Double>() {
		
		private static final double EPSILON = 0.001;
								
		/**
		 * @see Semiring#add(java.lang.Object, java.lang.Object)
		 */
		
		public Double add(Double e1, Double e2) {
			return Math.max(e1, e2);
		}

		/**
		 * @see Semiring#multiply(java.lang.Object, java.lang.Object)
		 */
		
		public Double multiply(Double e1, Double e2) {
			return e1*e2;
		}

		/**
		 * @see Semiring#valueToString(java.lang.Object)
		 */

	    public String valueToString(Double e) {
	    	return String.format("%.2f", e);
	    }
	    
		/**
		 * @see Kleene#closure(java.lang.Object)
		 */

		public Double closure(Double element) {
			return 1.0;
		}
		
		/**
		 * @see Kleene#unit()
		 */

		public Double unit() {
			return 1.0;
		}
		
		/**
		 * @see Kleene#zero()
		 */

		public Double zero() {
			return 0.0;
		}
		
		@Override
		public boolean isEqual(Double elt1, Double elt2) {
			return Math.abs(elt1 - elt2) < EPSILON;
		}
	};
	
	/**
	 * Constructor:
	 * @param vars The variable array.
	 * @param matrix The adjacency matrix of [0,1] reals.
	 */
	
	public MaxReliability(Variable[] vars, double[][] matrix) {
		super(vars, KLEENE, new SquareMatrix<Double>(vars, convert(matrix), KLEENE), true);
	}
			
	/**
	 * Constructor:
	 * @param vars The domain of this valuation as variable array which determines the matrix order.
	 * @param matrix The adjacency matrix.
	 * @param succ The successor matrix of this Kleene valuation.
	 * @param closure Flag to indicate if a closure has to be computed - this is for optimization issues.
	 */
	
	private MaxReliability(Variable[] vars, SquareMatrix<Double> matrix, Variable[][] pred, boolean closure) {
		super(vars, KLEENE, matrix, pred, closure);
	}	
	
	/**
	 * @see nenok.path.KValuation#create(nenok.va.Variable[], Kleene, Object[][], Variable[][], boolean)
	 */
	
	@Override
	public KValuation<Double> create(Variable[] vars, Kleene<Double> kleene, SquareMatrix<Double> matrix, Variable[][] pred, boolean closure) {
		return new MaxReliability(vars, matrix, pred, closure);
	}
		
	/**
	 * Converts a matrix of [0,1] reals into a matrix of objects.
	 * @param matrix The matrix of [0,1] reals.
	 * @return The according matrix of {@link Double} objects.
	 */
	
	private static Double[][] convert(double[][] matrix) {
		int size = matrix.length;
		Double[][] result = new Double[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if(matrix[i][j] < 0 || matrix[i][j] > 1) {
					throw new IllegalArgumentException("Only probability values between [0,1] accepted. Given: "+matrix[i][j]+".");
				}
				result[i][j] = new Double(matrix[i][j]);
			}
		}
		return result;
	}
}
