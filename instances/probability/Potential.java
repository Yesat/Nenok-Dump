package instances.probability;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JPanel;

import nenok.semiring.Dividability;
import nenok.semiring.Semiring;
import nenok.sva.SRValuation;
import nenok.va.FiniteVariable;
import nenok.va.Representor;
import nenok.va.Scalability;
import nenok.va.Separativity;

/**
 * This class offers an implementation of probability potentials based on the arithmetic semiring of
 * non-negative reals. It thus extends the {@link SRValuation} class. Probability potentials are 
 * scalable and therefore implements {@link Scalability}.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 568 $<br/>$LastChangedDate: 2008-03-26 14:42:35 +0100 (Mi, 26 Mrz 2008) $
 */

public class Potential extends SRValuation<Double> implements Scalability {
	
	/*
	 * Semiring Implementation:
	 */
	
	private static final Dividability<Double> semiring = new Dividability<Double>() {
		
		private static final double EPSILON = 0.0001;
		
		/**
		 * @see Semiring#add(Object, Object)
		 */

		public Double add(Double e1, Double e2) {
			return e1+e2; 
		}
		
		/**
		 * @see Semiring#multiply(Object, Object)
		 */

		public Double multiply(Double e1, Double e2) {
			return e1*e2;
		}
		
		/**
		 * @see nenok.semiring.Semiring#zero()
		 */
		
		public Double zero() {
			return 0.0;
		}
		
		/**
		 * @see Dividability#inverse(Object)
		 */
		
		public Double inverse(Double e) {
			return 1/e;
		}
		
		/**
		 * @see Semiring#valueToString(java.lang.Object)
		 */

	    public String valueToString(Double e) {
	    	return String.format("%.3f", e);
		}
	    
		@Override
		public boolean isEqual(Double elt1, Double elt2) {
			return Math.abs(elt1 - elt2) < EPSILON;
		}
	};
	
	/**
	 * Constructor:
	 * @param var The single variable of this probability potential.
	 * @param values The probability value for each configuration.
	 */
	
	public Potential(FiniteVariable var, double... values) {
		super(new FiniteVariable[] {var}, semiring, convert(values));
	}
	
	/**
	 * Constructor:
	 * @param var The conditioned variable.
	 * @param conditionals The conditioning variables (conditionals).
	 * @param values The probability value for each configuration.
	 */
	
	public Potential(FiniteVariable var, FiniteVariable[] conditionals, double... values) {
		super(merge(var, conditionals), semiring, convert(values));
	}
	
	/**
	 * Private Constructor:
	 * @param vars The variables of this probability potential.
	 * @param values The probability value for each possible configuration.
	 */
	
	private Potential(FiniteVariable[] vars, List<Double> values) {
		super(vars, semiring, values);
	}
	
	/**
	 * Factory method:
	 * @see nenok.sva.SRValuation#create(nenok.va.FiniteVariable[], Semiring, java.util.List)
	 */
	
	@Override
	public SRValuation<Double> create(FiniteVariable[] vars, Semiring<Double> semiring, List<Double> values) {
		return new Potential(vars, values);
	}

	/**
	 * @see nenok.va.Separativity#inverse()
	 */
	
	
	public Separativity inverse() {
		return Separativity.Implementor.getInstance().inverse(this);
	}
	
	/**
	 * @see nenok.va.Scalability#scale()
	 */
	
	public Scalability scale() {
		return Scalability.Implementor.getInstance().scale(this);
	}
	
    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    
	public boolean equals(Object o) {
		if(!(o instanceof Potential)) {
			return false;
		}
		Potential p = (Potential)o;
		if(!Arrays.equals(p.variables, variables)) {
			return false;
		}
		for(int i = 0; i < p.values.size(); i++) {
			double d1 = (p.values.get(i)).doubleValue();
			double d2 = (values.get(i)).doubleValue();
			if(!numericTest(d1, d2)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Converts an array of real numbers into semiring elements of the given type.
	 * @param values The input array of real numbers.
	 * @param type The type of semiring values that are created.
	 * @return A corresponding array of the Arithmetic semiring elements.
	 */
	
	private static List<Double> convert(double[] values) {
		ArrayList<Double> elements = new ArrayList<Double>(values.length);
		for (int i = 0; i < values.length; i++) {
			elements.add(i, new Double(values[i]));
		}
		return elements;
	}
	
	/**
	 * Adds a variable at the beginning of an existing variable array.
	 * @param var The variable to add.
	 * @param vars The existing variable array.
	 * @return The resulting new variable array.
	 */
	
	private static FiniteVariable[] merge(FiniteVariable var, FiniteVariable[] vars) {
		FiniteVariable[] result = new FiniteVariable[vars.length+1];
		result[0] = var;
		for (int i = 1; i < result.length; i++) {
			result[i] = vars[i-1];
		}
		return result;
	}
		
    /**
     * Displays the current potential as a chart.
     * @return The current potential as a chart.
     */
    
	@Representor
    public JPanel displayChart() {
        return new Chart(this);
    }
	
	/**
     * This test compares two numeric double values for equality.
     * They are equal if their distance is smaller than 0.0000001.
     * @param d1 The first value.
     * @param d2 The second value.
     * @return <code>true</code>, if the two values are equal.
     */
    
    public static boolean numericTest(double d1, double d2) {
        double distance = Math.abs(d1 - d2);
        return !(distance > 0.0000001);
    }
}
