package instances.probability;

import java.text.DecimalFormatSymbols;

import nenok.path.QValuation;
import nenok.path.SquareMatrix;
import nenok.path.Vector;
import nenok.semiring.Kleene;
import nenok.semiring.Quasiregular;
import nenok.semiring.Semiring;
import nenok.va.Variable;

/**
 * This class offers an implementation of Markov chains to search for transient and recurrent states.
 * It is based on the quasi-regular, arithmetic semiring of non-negative reals and thus extends the 
 * {@link QValuation} class.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 568 $<br/>$LastChangedDate: 2008-03-26 14:42:35 +0100 (Mi, 26 Mrz 2008) $
 */

public class Markov extends QValuation<Double> {
	
	/*
	 * Constants:
	 */

	private static final DecimalFormatSymbols SYMBOLS = new DecimalFormatSymbols();
	
	/*
	 * Kleene Algebra Implementation:
	 */
	
	private static final Quasiregular<Double> SEMIRING = new Quasiregular<Double>() {
		
		private static final double EPSILON = 0.0001;
		
		/**
		 * @see Semiring#add(java.lang.Object, java.lang.Object)
		 */
		
		public Double add(Double e1, Double e2) {
			if(e1 == Double.MAX_VALUE || e2 == Double.MAX_VALUE) {
				return Double.MAX_VALUE;
			}
			return e1+e2;
		}

		/**
		 * @see Semiring#multiply(java.lang.Object, java.lang.Object)
		 */
		
		public Double multiply(Double e1, Double e2) {
			if(e1 == 0 || e2 == 0) {
				return 0.0;
			}			
			if(e1 == Double.MAX_VALUE || e2 == Double.MAX_VALUE) {
				return Double.MAX_VALUE;
			}
			return e1*e2;
		}

		/**
		 * @see Semiring#valueToString(java.lang.Object)
		 */

	    public String valueToString(Double e) {
			if(e == Double.MAX_VALUE) {
				return SYMBOLS.getInfinity(); 
			}
	    	return String.format("%.2f", e);
	    }
	    
		/**
		 * @see Kleene#closure(java.lang.Object)
		 */

		public Double closure(Double element) {
			if(element == 0) {
				return 1.0;
			}
			return Double.MAX_VALUE;
		}
		
		/**
		 * @see Kleene#unit()
		 */

		public Double unit() {
			return 1.0;
		}
		
		/**
		 * @see Kleene#zero()
		 */

		public Double zero() {
			return 0.0;
		}
		
		@Override
		public boolean isEqual(Double elt1, Double elt2) {
			return Math.abs(elt1 - elt2) < EPSILON;
		}
	};
	
	/**
	 * Constructor:
	 * @param vars The variable array.
	 * @param matrix The adjacency matrix of non-negative reals.
	 */
	
	public Markov(Variable[] vars, double[][] matrix, double[] vector) {
		super(new SquareMatrix<Double>(vars, convert(matrix), SEMIRING), new Vector<Double>(vars, convert(vector), SEMIRING), SEMIRING);
	}
		
	/**
	 * Constructor:
	 * @param vars The variable array.
	 * @param matrix The adjacency matrix of non-negative reals.
	 */
	
	private Markov(SquareMatrix<Double> matrix, Vector<Double> vector) {
		super(matrix, vector, SEMIRING);
	}
	
	/**
	 * @see nenok.path.QValuation#create(nenok.path.SquareMatrix, nenok.path.Vector, nenok.semiring.Quasiregular)
	 */
	
	@Override
	public QValuation<Double> create(SquareMatrix<Double> matrix, Vector<Double> vector, Quasiregular<Double> semiring) {
		return new Markov(matrix, vector);
	}

	/**
	 * Converts a matrix of non-negative reals into a matrix of objects.
	 * @param matrix The matrix of double values.
	 * @return The according matrix of {@link Double} objects.
	 */
	
	private static Double[][] convert(double[][] matrix) {
		int size = matrix.length;
		Double[][] result = new Double[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if(matrix[i][j] < 0) {
					throw new IllegalArgumentException("Only positive reals accepted. Given: "+matrix[i][j]+".");
				}
				result[i][j] = matrix[i][j];
			}
		}
		return result;
	}
	
	/**
	 * Converts a matrix of non-negative reals into a matrix of objects.
	 * @param matrix The matrix of double values.
	 * @return The according matrix of {@link Double} objects.
	 */
	
	private static Double[] convert(double[] vector) {
		int size = vector.length;
		Double[] result = new Double[size];
		for (int i = 0; i < size; i++) {
			if(vector[i] < 0) {
				throw new IllegalArgumentException("Only positive reals accepted. Given: "+vector[i]+".");
			}
			result[i] = vector[i];
		}
		return result;
	}
}
