package instances.probability;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.swing.JPanel;

import nenok.sva.Configuration;

/**
 * This SWING composite extension represents probability potentials 
 * in a graphical way, i.e. as charts.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 568 $<br/>$LastChangedDate: 2008-03-26 13:42:35 +0000 (Mi, 26 Mrz 2008) $
 */

public class Chart extends JPanel {
    
    private double[] probabilities;
    private String[] configurations;
    private int coffset;
        
    /*
     * Distance constants:
     */

    private static final int BAR_WIDTH = 300;
    private static final int BAR_HEIGHT = 15;
    private static final int H_OFFSET = 10;
    private static final int V_OFFSET = 0;
    private static final int P_OFFSET = 10;
    private static final int GAP = 20;
    
    /*
     * Colors:
     */
    
    private static final Color BLUE = new Color(105, 89, 205);
    
    /**
     * Constructor:
     * @param potential The semiring potential to represent as a chart.
     */

    public Chart(Potential potential) {
        super();
        
        /*
         * build probability array:
         */
        
        this.probabilities = new double[potential.getValues().size()];
        
        for (int i = 0; i < probabilities.length; i++) {
            probabilities[i] = (potential.getValues().get(i)).doubleValue();
        }
        
        /*
         * build configurations:
         */
        
        configurations = Configuration.getConfigurations(potential.getVariables());    
        build(); 
    }
    
    /**
     * @see java.awt.Component#paint(java.awt.Graphics)
     */
    
    public void paint(Graphics g) {
        super.paint(g);
        
        int x = coffset;
        int y = V_OFFSET;
        
        Graphics2D g2 = (Graphics2D)g;
        FontMetrics metrics = g2.getFontMetrics();
        
        int cWidth = (configurations.length == 0)? 0 : metrics.stringWidth(configurations[0]);
                                
        /*
         * Draw rectangles and configurations:
         */ 
        
        for (int i = 0; i < probabilities.length; i++) {
            y += GAP;

            //Rectangle:
            g2.setColor(BLUE);
            Rectangle rect = new Rectangle(x, y+i*BAR_HEIGHT, (int)(BAR_WIDTH*probabilities[i]), BAR_HEIGHT);
            g2.fill(rect);
       
            //Configuration:
            g2.setColor(Color.BLACK);
            
            int stringY = y+i*BAR_HEIGHT+BAR_HEIGHT / 2+g2.getFontMetrics().getHeight() / 4;
            g2.drawString(configurations[i], H_OFFSET, stringY);
            
            //Arithmetic:
            String p = (Math.round(probabilities[i] * 1000.) / 1000.)+"";
            g2.drawString(p, x+P_OFFSET+(int)(BAR_WIDTH*probabilities[i]), stringY);
        }
        
        /*
         * Draw vertical line:
         */
        
        g2.setColor(Color.BLACK);
        g2.setStroke(new BasicStroke(2));
        g2.drawLine(cWidth+2*H_OFFSET, 
        		    GAP / 2, 
        		    cWidth+2*H_OFFSET, 
        		    V_OFFSET+probabilities.length*BAR_HEIGHT+probabilities.length*GAP+GAP);
        		    
        
        /*
         * Draw horizontal line:
         */
        
        g2.drawLine(cWidth+H_OFFSET, 
        			(int)(V_OFFSET+probabilities.length*BAR_HEIGHT+probabilities.length*GAP+0.5*GAP), 
        		    cWidth+2*H_OFFSET+BAR_WIDTH+GAP, 
        		    (int)(V_OFFSET+probabilities.length*BAR_HEIGHT+probabilities.length*GAP+0.5*GAP));
    }
    
    /**
     * Builds the chart composite.
     */
    
    private void build() {
        FontMetrics metric = this.getFontMetrics(this.getFont());
        for (int i = 0; i < probabilities.length; i++) {
            String s = configurations[i];
            if(coffset < metric.stringWidth(s) + 2*H_OFFSET) {
                coffset = metric.stringWidth(s) + 2*H_OFFSET;
            }
        }
        int poffset = 0;
        for (int i = 0; i < probabilities.length; i++) {
            String s = probabilities[i]+"";
            if(poffset < metric.stringWidth(s)) {
                poffset = metric.stringWidth(s);
            }
        }
        
        this.setPreferredSize(new Dimension(
        		BAR_WIDTH+2*H_OFFSET+coffset+poffset, 
        		2*V_OFFSET+probabilities.length*BAR_HEIGHT+probabilities.length*GAP+2*GAP));
    }
}
