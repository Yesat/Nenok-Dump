package instances.probability;

import nenok.Knowledgebase;
import nenok.va.Domain;
import nenok.va.FiniteVariable;

/**
 * This class is a content holder for application data.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 586 $<br/>$LastChangedDate: 2008-03-26 15:20:24 +0000 (Mi, 26 Mrz 2008) $
 */

public class Input {
		
	/**
	 * A knowledgebase containing the valuation objects.
	 */
	
	public Knowledgebase knowledgebase;
	
	/**
	 * An array of queries to be answered.
	 */
	
	public Domain[] queries;
	
	/**
	 * @see java.lang.Object#toString()
	 */
	
	@Override
	public String toString() {
		return knowledgebase.toString();
	}
	
	/**
	 * Constructor:
	 * @param potentials An array of potentials that constitutes the knowledgebase.
	 * @param queries An array of queries covered by the knowledgebase.
	 * @param name The name of the modelled example.
	 */
	
	public Input(Potential[] potentials, Domain[] queries, String name) {
		this.knowledgebase = new Knowledgebase(potentials, name);
		this.queries = queries;
	}
	
	/**
	 * @return All input data defined in this file.
	 */
	
	public static Input[] getAllInput() {
		return new Input[] {
			getAsiaInput(),
			getCancerInput(),
			getDogInput(),
			getEarthquakeInput(),
			getMontyHallInput(),
			getOilInput(), 
			getStudfarmInput()
		};
	}
	
	/**
	 * @return Input data of the ASIA example.
	 */
	
	public static Input getAsiaInput() {
		
        /*
         * Frame:
         */
     
		// There are only binary variables:
        String[] frame = { "0", "1" };	
		
        /*
         * Variables:
         */

        //Visit to Asia:
        FiniteVariable a = new FiniteVariable("A", frame);
        //Tuberculosis:
        FiniteVariable t = new FiniteVariable("T", frame);
        //Smoking:
        FiniteVariable s = new FiniteVariable("S", frame);
        //Bronchitis:
        FiniteVariable b = new FiniteVariable("B", frame);
        //Lung cancer:
        FiniteVariable l = new FiniteVariable("L", frame);
        //Either tubercolosis or lung cancer:
        FiniteVariable e = new FiniteVariable("E", frame);
        //Positive X-ray:
        FiniteVariable x = new FiniteVariable("X", frame);
        //Dyspnoea:
        FiniteVariable d = new FiniteVariable("D", frame);
                
        /*
         * Potentials:
         */
        
        double[] probs1 = {0.99, 0.01};
        Potential pot1 = new Potential(a, probs1);
        
        FiniteVariable[] cond1 = {a};
        double[] probs2 = {0.99, 0.95, 0.01, 0.05};
        Potential pot2 = new Potential(t, cond1, probs2);
        
        double[] probs3 = {0.5, 0.5};
        Potential pot3 = new Potential(s, probs3);
        
        FiniteVariable[] cond2 = {s};
        double[] probs4 = {0.99, 0.9, 0.01, 0.1};
        Potential pot4 = new Potential(l, cond2, probs4);
        
        FiniteVariable[] cond3 = {s};
        double[] probs5 = {0.7, 0.4, 0.3, 0.6};
        Potential pot5 = new Potential(b, cond3, probs5);
        
        FiniteVariable[] cond4 = {l, t};
        double[] probs6 = {1, 0, 0, 0, 0, 1, 1, 1};
        Potential pot6 = new Potential(e, cond4, probs6);
        
        FiniteVariable[] cond5 = {e};
        double[] probs7 = {0.95, 0.02, 0.05, 0.98};
        Potential pot7 = new Potential(x, cond5, probs7);   
        
        FiniteVariable[] cond6 = {e, b};
        double[] probs8 = {0.9, 0.2, 0.3, 0.1, 0.1, 0.8, 0.7, 0.9};
        Potential pot8 = new Potential(d, cond6, probs8);
        
        Potential[] potentials = new Potential[] {pot1, pot2, pot3, pot4, pot5, pot6, pot7, pot8};

        /*
         * Queries:
         */
        
        Domain[] queries = new Domain[] {
            new Domain(a),
            new Domain(t),
            new Domain(s),
            new Domain(b),
            new Domain(l),
            new Domain(x),
            new Domain(e),
            new Domain(d)
        };
        
        return new Input(potentials, queries, "Asia");
	}
	
	/**
	 * @return Input data of the CANCER example.
	 */
	
	public static Input getCancerInput() {
		
		/*
         * Frame:
         */
        
        //There are only binary variables:
        String[] frame = { "0", "1" };
                
        /*
         * Variables:
         */

        //Woman suffering from cancer:
        FiniteVariable c = new FiniteVariable("C", frame);
        //Positive guitest result:
        FiniteVariable b = new FiniteVariable("B", frame);
        
        /*
         * Potentials:
         */
        
        FiniteVariable[] cond1 = new FiniteVariable[] {c};
        double[] probs1 = {0.995, 0.1, 0.005, 0.9};
        Potential pot1 = new Potential(b, cond1, probs1);
        
        double[] probs2 = {0.9998, 0.0002};
        Potential pot2 = new Potential(c, probs2);
        
        Potential[] potentials = new Potential[] {pot1, pot2};
        
        /*
         * Queries:
         */
        
        Domain[] queries = new Domain[] {
            new Domain(c),
            new Domain(b)
        };
        
        return new Input(potentials, queries, "Cancer");
	}

	/**
	 * @return Input data of the DOG example.
	 */
	
	public static Input getDogInput() {
		
		/*
         * Frame:
         */
        
        // There are only binary variables:
        String[] frame = { "0", "1" };
                
        /*
         * Variables:
         */

        // The family is out:
        FiniteVariable f = new FiniteVariable("F", frame);
        // Dog has bowel-problem::
        FiniteVariable b = new FiniteVariable("B", frame);
        // The lights are on:
        FiniteVariable l = new FiniteVariable("L", frame);
        // The dog is out:
        FiniteVariable d = new FiniteVariable("D", frame);
        // Hear barking:
        FiniteVariable h = new FiniteVariable("H", frame);
        
        /*
         * Potentials:
         */
         
        double[] probs1 = new double[] {0.85, 0.15};
        Potential pot1 = new Potential(f, probs1);
        
        double[] probs2 = new double[] {0.99, 0.01};
        Potential pot2 = new Potential(b, probs2);
        
        FiniteVariable[] cond1 = new FiniteVariable[] {f};
        double[] probs3 = new double[] {0.95, 0.4, 0.05, 0.6};
        Potential pot3 = new Potential(l, cond1, probs3);       
        
        FiniteVariable[] cond2 = new FiniteVariable[] {f, b};
        double[] probs4 = new double[] {0.7, 0.03, 0.1, 0.01, 0.3, 0.97, 0.9, 0.99};
        Potential pot4 = new Potential(d, cond2, probs4);
        
        FiniteVariable[] cond3 = new FiniteVariable[] {d};
        double[] probs5 = new double[] {0.99, 0.3, 0.01, 0.7};
        Potential pot5 = new Potential(h, cond3, probs5);
        
        Potential[] potentials = new Potential[] {pot1, pot2, pot3, pot4, pot5};
      
        /*
         * Queries:
         */
        
        Domain[] queries = new Domain[] {
            new Domain(f),
            new Domain(b),
            new Domain(l),
            new Domain(d),
            new Domain(h)
        };
        
        return new Input(potentials, queries, "Dog");
	}
	
	/**
	 * @return Input data of the Earthquake example.
	 */	
	
	public static Input getEarthquakeInput() {
		
        /*
         * Frame:
         */
        
        // There are only binary variables:
        String[] frame = { "0", "1" };
                
        /*
         * Variables:
         */
        
        // Burglary
        FiniteVariable b = new FiniteVariable("B", frame);
        // Earthquake
        FiniteVariable e = new FiniteVariable("E", frame);
        // John calls:
        FiniteVariable j = new FiniteVariable("J", frame);
        // Mary calls:
        FiniteVariable m = new FiniteVariable("M", frame);
        // Alarm
        FiniteVariable a = new FiniteVariable("A", frame);
        
        /*
         * Potentials:
         */
            
        double[] probs1 = {0.999, 0.001};
        Potential pot1 = new Potential(b, probs1);
        
        double[] probs2 = {0.998, 0.002};
        Potential pot2 = new Potential(e, probs2);   
        
        FiniteVariable[] cond1 = {a};
        double[] probs3 = {0.95, 0.1, 0.05, 0.9};
        Potential pot3 = new Potential(j, cond1, probs3);
        
        double[] probs4 = {0.99, 0.3, 0.01, 0.7};
        Potential pot4 = new Potential(m, cond1, probs4);   
        
        FiniteVariable[] cond2 = {b, e};
        double[] probs5 = {0.999, 0.71, 0.06,  0.05, 0.001, 0.29, 0.94,  0.95};
        Potential pot5 = new Potential(a, cond2, probs5);
        
        Potential[] potentials = new Potential[] {pot1, pot2, pot3, pot4, pot5};
                          
        /*
         * Queries:
         */
        
        Domain[] queries = new Domain[] {
            new Domain(m),
            new Domain(a),
            new Domain(e),
            new Domain(b),
            new Domain(j)
        };
    
		return new Input(potentials, queries, "Earthquake");
	}
	
	/**
	 * @return Input data of the Montyhall example.
	 */	
	
	public static Input getMontyHallInput() {
		
		/*
         * Frame:
         */
        
        String[] frame = new String[] {"door 1", "door 2", "door 3"};
                
        /*
         * Variables:
         */

        FiniteVariable prize = new FiniteVariable("Prize", frame);
        FiniteVariable selection = new FiniteVariable("First Selection", frame);
        FiniteVariable opens = new FiniteVariable("Monty opens", frame);
        
        /*
         * Potentials:
         */
        
        double[] prob1 = new double[] {0.33, 0.33, 0.33};
        Potential p1 = new Potential(prize, prob1);
        
        Potential p2 = new Potential(selection, prob1);   
        
        double[] prob2 = new double[] {0, 0, 0, 0, 0.5, 1, 0, 1, 0.5, 0.5, 0, 1, 0, 0, 0, 1, 0, 0.5, 0.5, 1, 0, 1, 0.5, 0, 0, 0, 0};
        Potential p3 = new Potential(opens, new FiniteVariable[] {prize, selection}, prob2);  
        
        Potential[] potentials = new Potential[] {p1, p2, p3};

        /*
         * Queries:
         */
        
        Domain[] queries = new Domain[] {
            new Domain(prize),
            new Domain(selection),
            new Domain(opens)
        };
        
        return new Input(potentials, queries, "Monty");
	}
	
	/**
	 * @return Input data of the Oil example.
	 */	
	
	public static Input getOilInput() {
		
		/*
         * Variables:
         */

        FiniteVariable oil = new FiniteVariable("Oil", new String[] {"dry", "wet", "soak"});
        FiniteVariable test = new FiniteVariable("Test", new String[] {"yes", "not"});
        FiniteVariable seismic = new FiniteVariable("Seismic", new String[] {"closed", "open", "diff"});
        
        /*
         * Potentials
         */
            
        double[] prob1 = new double[] {0.5, 0.3, 0.2};
        Potential p1 = new Potential(oil, prob1);
        
        double[] prob2 = new double[] {0.33, 0.1, 0.33, 0.3, 0.33, 0.5, 0.33, 0.3, 
                                       0.33, 0.4, 0.33, 0.4, 0.33, 0.6, 0.33, 0.3, 0.33, 0.1};
        Potential p2 = new Potential(seismic, new FiniteVariable[] {oil, test}, prob2);        
        
        Potential[] potentials = new Potential[] {p1, p2};
        
        /*
         * Queries:
         */
        
        Domain[] queries = new Domain[] {
            new Domain(oil),
            new Domain(test),
            new Domain(seismic)
        };
        
        return new Input(potentials, queries, "Oil");
	}
	
	/**
	 * @return Input data of the ELISA test example.
	 */	
	
	public static Input getElisaInput() {
		
		/*
         * Variables:
         */

        FiniteVariable hiv = new FiniteVariable("HIV", new String[] {"false", "true"});
        FiniteVariable test = new FiniteVariable("Test", new String[] {"neg", "pos"});
        
        /*
         * Potentials
         */
            
        double[] prob1 = new double[] {0.997, 0.003};
        Potential p1 = new Potential(hiv, prob1);
        
        double[] prob2 = new double[] {0.98, 0.01, 0.02, 0.99};
        Potential p2 = new Potential(test, new FiniteVariable[] {hiv}, prob2);        
        
        Potential[] potentials = new Potential[] {p1, p2};
        
        /*
         * Queries:
         */
        
        Domain[] queries = new Domain[] {
            new Domain(test),
        };
        
        return new Input(potentials, queries, "ELISA");
	}
	
	/**
	 * @return Input data of the Studfarm example.
	 */		
	
	public static Input getStudfarmInput() {
		
		/*
         * Frames:
         */
        
        String[] frame1 = {"AA", "Aa"};
        String[] frame2 = {"AA", "Aa", "aa"};
        
        /*
         * Variables:
         */
        
        FiniteVariable a1 = new FiniteVariable("A1", frame1);
        FiniteVariable a2 = new FiniteVariable("A2", frame1);
        
        FiniteVariable ann = new FiniteVariable("Ann", frame1);
        FiniteVariable alan = new FiniteVariable("Alan", frame1);
        FiniteVariable alice = new FiniteVariable("Alice", frame1);
        
        FiniteVariable bill = new FiniteVariable("Bill", frame1);
        FiniteVariable betsy = new FiniteVariable("Betsy", frame1);
        FiniteVariable benny = new FiniteVariable("Benny", frame1);
        FiniteVariable bonnie = new FiniteVariable("Bonnie", frame1);
        
        FiniteVariable carl = new FiniteVariable("Carl", frame1);
        FiniteVariable cecily = new FiniteVariable("Cecily", frame1);
        
        FiniteVariable dennis = new FiniteVariable("Dennis", frame2);
        
        /*
         * Potentials:
         */
            
        //Level 1:
        double[] p1 = {0.99, 0.01};
        Potential p11 = new Potential(a1, p1);
        Potential p12 = new Potential(ann, p1);
        Potential p13 = new Potential(alan, p1);
        Potential p14 = new Potential(alice, p1);
        Potential p15 = new Potential(a2, p1);
        
        //Level 2:
        double[] p2 = {1, 0.5, 0.5, 0.33, 0, 0.5, 0.5, 0.67};
        Potential p21 = new Potential(bill, new FiniteVariable[] {a1,ann}, p2);
        Potential p22 = new Potential(betsy, new FiniteVariable[] {ann,alan}, p2);
        Potential p23 = new Potential(benny, new FiniteVariable[] {alan,alice}, p2);
        Potential p24 = new Potential(bonnie, new FiniteVariable[] {ann,a2}, p2);
        
        Potential p25 = new Potential(carl, new FiniteVariable[] {bill,betsy}, p2);
        Potential p26 = new Potential(cecily, new FiniteVariable[] {benny,bonnie}, p2);
        
        //Level 3:
        double[] p3 = {1, 0.5, 0.5, 0.25, 0, 0.5, 0.5, 0.5, 0, 0, 0, 0.25};
        Potential p31 = new Potential(dennis, new FiniteVariable[] {carl,cecily}, p3);

        //Observation:
        Potential p41 = new Potential(dennis, new double[] {0, 0, 1});
        
        Potential[] potentials = new Potential[] {p11, p12, p13, p14, p15, p21, p22, p23, p24, p25, p26, p31, p41};
        
        /*
         * Queries:
         */
        
        Domain[] queries = new Domain[] {
            new Domain(ann),
            new Domain(alan),
            new Domain(alice),
            new Domain(bill),
            new Domain(betsy),
            new Domain(benny),
            new Domain(bonnie),
            new Domain(carl),
            new Domain(cecily)
        };
        
        return new Input(potentials, queries, "Studfarm");
	}
}
