package instances;

import java.util.Arrays;

import gui.Viewer;
import instances.probability.Input;
import nenok.Architecture;
import nenok.Knowledgebase;
import nenok.LCFactory;
import nenok.lc.JoinTree;
import nenok.va.Domain;

/**
 * Demo application for the use of NENOK.
 * 
 * @author Marc Pouly
 */

public class Demo {

	/**
	 * Main method:
	 * @param args Standard input arguments.
	 * <ul>
	 * 	<li>1. Argument: Execution mode (console | Interactive)</li>
	 * 	<li>2. Argument: Name of the example to be executed.</li>
	 * </ul>
	 */
	
	public static void main(String[] args) {
		
		if(args.length < 2) {
			System.out.println("Missing argument.");
			System.out.println("Usage: Demo (Interactive | Console) example");	
			System.out.println("Examples available: Asia, Dog, Studfarm, Montyhall, Earthquake, Cancer, Oil, Elias, ...");	
			System.exit(1);
		}

        /*
         * Get input data:
         */
        
        Input[] input = new Input[args.length-1];
        
        for (int i = 1; i < args.length; i++) {
        	
    		if(args[i].equalsIgnoreCase("Asia")) {
    			input[i-1] = Input.getAsiaInput();
    		} else if(args[i].equalsIgnoreCase("Dog")) {
    			input[i-1] = Input.getDogInput();
    		} else if(args[i].equalsIgnoreCase("Studfarm")) {
    			input[i-1] = Input.getStudfarmInput();		
    		} else if(args[i].equalsIgnoreCase("Montyhall")) {
    			input[i-1] = Input.getMontyHallInput();	
    		} else if(args[i].equalsIgnoreCase("Earthquake")) {
    			input[i-1] = Input.getEarthquakeInput();	
    		} else if(args[i].equalsIgnoreCase("Cancer")) {
    			input[i-1] = Input.getCancerInput();	
    		} else if(args[i].equalsIgnoreCase("Oil")) {
    			input[i-1] = Input.getOilInput();
    		} else if(args[i].equalsIgnoreCase("Elisa")) {
    			input[i-1] = Input.getElisaInput();
    		} else {
    			System.out.println("Example '"+args[i]+"' not found.");
    			System.exit(1);
    		}
		}
        		
        /*
         * Interactive or Console Mode:
         */
				
        try {
        
            if(args[0].equalsIgnoreCase("Console")) {
            	
                LCFactory factory = new LCFactory();
                factory.setArchitecture(Architecture.Shenoy_Shafer);
                
                for(Input in : input) {
                	
                    JoinTree tree = factory.create(in.knowledgebase, Arrays.asList(in.queries));
                    tree.propagate(true);
                    
                    System.out.println("Marginals of the "+in.knowledgebase.toString()+" example:");
                    System.out.println();
                    
                    for(Domain query : in.queries) {
                        System.out.println("Query: "+query);
                        System.out.println(tree.answer(query));
                    } 
                	
                }
           
    
                
            } else if(args[0].equalsIgnoreCase("Interactive")) {
            	
            	Knowledgebase[] kbs = new Knowledgebase[input.length];
            	for (int j = 0; j < input.length; j++) {
					kbs[j] = input[j].knowledgebase;
				}
                
            	Viewer.display(kbs);
            	
            } else {
            	
    			System.out.println("Wrong first argument: select Interactive or Console");		
    			System.exit(1);
            }
            
        } catch (Exception e1) {
            e1.printStackTrace();
            System.exit(1);
        }
	}
	
}
