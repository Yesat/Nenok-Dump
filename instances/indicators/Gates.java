package instances.indicators;

import nenok.va.FiniteVariable;
import nenok.va.Valuation;

/**
 * An application for indicator functions are binary, logical gates.
 * This class provides static creators for a collection of the most
 * common logical gates. Additionally, there is a creator to build
 * n-bit-adder circuits for variable n.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 681 $<br/>$LastChangedDate: 2009-03-26 19:48:39 +0000 (Do, 26 Mrz 2009) $
 */

public class Gates {
	
	private static int varCounter = 1;
	private static final String VAR = "v";
    private static final String[] frame = new String[] {"0", "1"};
	
	/**
	 * This static method translates an AND-Gate to an indicator function.
	 * @param in1 The first input line.
	 * @param in2 The second input line.
	 * @param out The output line.
	 * @return The indicator function for this gate.
	 */
	
	public static final Indicator AND_GATE(FiniteVariable in1, FiniteVariable in2, FiniteVariable out) {
		FiniteVariable[] vars = new FiniteVariable[] {in1, in2, out};
		int[] indicator = new int[]{1,0,1,0,1,0,0,1};
		return new Indicator(vars, indicator);
	}
	
	/**
	 * This static method translates an OR-Gate to an indicator function.
	 * @param in1 The first input line.
	 * @param in2 The second input line.
	 * @param out The output line.
	 * @return The indicator function for this gate.
	 */
	
	public static final Indicator OR_GATE(FiniteVariable in1, FiniteVariable in2, FiniteVariable out) {
		FiniteVariable[] vars = new FiniteVariable[] {in1, in2, out};
		int[] indicator = new int[]{1,0,0,1,0,1,0,1};
		return new Indicator(vars, indicator);
	}
	
	/**
	 * This static method translates an XOR-Gate to an indicator function.
	 * @param in1 The first input line.
	 * @param in2 The second input line.
	 * @param out The output line.
	 * @return The indicator function for this gate.
	 */
	
	public static final Indicator XOR_GATE(FiniteVariable in1, FiniteVariable in2, FiniteVariable out) {
		FiniteVariable[] vars = new FiniteVariable[] {in1, in2, out};
		int[] indicator = new int[]{1,0,0,1,0,1,1,0};
		return new Indicator(vars, indicator);
	}
	
	/**
	 * This static method translates a NOT-Gate to an indicator function.
	 * @param in The input line.
	 * @param out The output line.
	 * @return The indicator function for this gate.
	 */
	
	public static final Indicator NOT_GATE(FiniteVariable in, FiniteVariable out) {
		FiniteVariable[] vars = new FiniteVariable[] {in, out};
		int[] indicator = new int[]{0,1,1,0};
		return new Indicator(vars, indicator);
	}
	
	/**
	 * This static method translates a 1-Bit-Adder1 circuit to an array of indicator functions.
	 * @param in1 Input line 1.
	 * @param in2 Input line 2.
	 * @param inc Input line for carry bit.
	 * @param out Output line.
	 * @param outc Carry output line.
	 * @return The indicator functions for this circuit.
	 */
	
	public static final Indicator[] Adder_1(FiniteVariable in1, FiniteVariable in2, FiniteVariable inc, FiniteVariable out, FiniteVariable outc) {
		//Intermediate Variables:
		FiniteVariable v1 = new FiniteVariable(VAR+varCounter, frame);
		varCounter++;
		FiniteVariable v2 = new FiniteVariable(VAR+varCounter, frame);
		varCounter++;
		FiniteVariable v3 = new FiniteVariable(VAR+varCounter, frame);
		varCounter++;
		Indicator xor1 = Gates.XOR_GATE(in1, in2, v1);
		Indicator and1 = Gates.AND_GATE(in1, in2, v3);
		Indicator and2 = Gates.AND_GATE(v1, inc, v2);
		Indicator xor2 = Gates.XOR_GATE(v1, inc, out);
		Indicator or1 = Gates.OR_GATE(v2, v3, outc);
		return new Indicator[] {xor1, and1, and2, xor2, or1};
	}
	
	/**
	 * This static method translates a N-Bit-Adder1 circuit to an array of indicator functions.
	 * @param in Array of input lines.
	 * @param inc Input line for carry bit.
	 * @param out Array of output lines.
	 * @param outc Carry output line.
	 * @return The indicator functions for this circuit.
	 */
	
	public static final Indicator[] Adder_N(FiniteVariable[] in, FiniteVariable inc, FiniteVariable[] out, FiniteVariable outc) {
		// Dimension Test1:
		if(in.length != 2*out.length) {
			throw new IllegalArgumentException("Wrong array dimension. Expected: in.length == 2*out.length");
		}
        
		// Compute Adder size and number of gates (5 gates per adder):
		int size = in.length / 2;
		int gates = size*5;
        
		// Prepare output array:
		Indicator[] result = new Indicator[gates];
        
		// Input / Output carry bit:
        FiniteVariable o;
		FiniteVariable c = inc;
        
		for (int i = 0, j = 0; i < size; i++) {
            
			// Output carry bit:
            if(i == size - 1) {
                o = outc;
            } else {
                o = new FiniteVariable(VAR+varCounter, frame);
            }
            
            Indicator[] adder = Adder_1(in[j++], in[j++], c, out[i], o);
            
			// Copy current adder to result set:
			for (int k = i*5, s = 0; k < 5*(i + 1); k++, s++) {
				result[k] = adder[s];
			}
            
            // Set output carry on input carry:
            c = o;
         
		}
		return result;
	}
	
	/**
	 * This static method translates an input line to an indicator function.
	 * @param line The variable representing the line.
	 * @param value The value put on this line.
	 * @return The indicator function for this input line.
	 */
	
	public static final Indicator LINE(FiniteVariable line, int value) {
		FiniteVariable[] vars = new FiniteVariable[] {line};
		int[] indicator = null;
		if(value == 1) {
			indicator = new int[]{0,1};
			return new Indicator(vars, indicator);
		}
		if(value == 0) {
			indicator = new int[]{1,0};
			return new Indicator(vars, indicator);
		}
		throw new IllegalArgumentException("Only values from set {0,1} are accepted. Given: "+value);
	}

    /**
     * @param size The input size of the adder circuit to construct.
     * @return An array of indicators that build an adder circuit of the given input size.
     */
    
    public static Indicator[] adder(int size) {
        
        /*
         * Input Variables:
         */
        
        FiniteVariable[] in = new FiniteVariable[2*size];
        for (int i = 0; i < in.length; i++) {
            in[i] = new FiniteVariable("in"+i, frame);
        }
        FiniteVariable inc = new FiniteVariable("inc", frame);
        
        /*
         * Output Variables:
         */
        
        FiniteVariable[] out = new FiniteVariable[size];
        for (int i = 0; i < out.length; i++) {
            out[i] = new FiniteVariable("out"+i, frame);
        } 
        FiniteVariable outc = new FiniteVariable("outc", frame);
        
        /*
         * Lines:
         */
        
        Valuation[] lines = new Valuation[2*size+1];
        for (int i = 0; i < lines.length - 1; i++) {
            lines[i] = Gates.LINE(in[i], 0);
        }
        lines[lines.length - 1] = Gates.LINE(inc, 0);
        
        /*
         * Adder:
         */
        
        Indicator[] adder = Gates.Adder_N(in, inc, out, outc);
        
        /*
         * Knowledgebase:
         */
        
        Indicator[] vals = new Indicator[adder.length+lines.length];
        System.arraycopy(adder, 0, vals, 0, adder.length);
        System.arraycopy(lines, 0, vals, adder.length, lines.length);
                        
        return vals;
    }
}
