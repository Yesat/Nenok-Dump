package instances.indicators;

import java.util.ArrayList;
import java.util.List;

import nenok.semiring.Dividability;
import nenok.semiring.Optimization;
import nenok.semiring.Semiring;
import nenok.sva.Configuration;
import nenok.sva.OSRValuation;
import nenok.sva.SRValuation;
import nenok.va.FiniteVariable;
import nenok.va.Idempotency;
import nenok.va.Representor;
import nenok.va.Scalability;
import nenok.va.Separativity;

/**
 * Implementation of the valuation algebra that is induced by the Boolean semiring.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 574 $<br/>$LastChangedDate: 2008-03-26 13:50:48 +0000 (Mi, 26 Mrz 2008) $
 */

public class Indicator extends OSRValuation<Boolean> implements Idempotency {
	
	private static final Boole semiring = new Boole();
	
	/*
	 * Semiring Implementation:
	 */
	
	private static final class Boole implements Dividability<Boolean>, Optimization<Boolean> {
		
		/*
		 * Implementors:
		 */
		
		private final Optimization.Implementor<java.lang.Boolean> comparator = new Optimization.Implementor<java.lang.Boolean>(this);
			
		/**
		 * @see nenok.semiring.Semiring#add(java.lang.Object, java.lang.Object)
		 */
		
		public Boolean add(Boolean e1, Boolean e2) {
			return e1 || e2; 
		}
		
		/**
		 * @see Semiring#multiply(java.lang.Object, java.lang.Object)
		 */
		
		public Boolean multiply(Boolean e1, Boolean e2) {
			return e1 && e2;
		}	
		
		/**
		 * @see nenok.semiring.Semiring#zero()
		 */
		
		public Boolean zero() {
			return false;
		}
		
		/**
		 * @see Dividability#inverse(java.lang.Object)
		 */
		
		public Boolean inverse(Boolean e) {
			return e;
		}
		
		/**
		 * @see Optimization#compare(java.lang.Object, java.lang.Object)
		 */

		public int compare(Boolean e1, Boolean e2) {
			return comparator.compare(e1, e2);
		}
		
		/**
		 * @see Semiring#valueToString(java.lang.Object)
		 */

		public String valueToString(java.lang.Boolean element) {
			return element ? "1" : "0";
		}
		
		@Override
		public boolean isEqual(Boolean elt1, Boolean elt2) {
			return elt1.equals(elt2);
		}
	}

	/*
	 * Constants:
	 */
	
	private static final Boolean TRUE = new Boolean(true);
	private static final Boolean FALSE = new Boolean(false);
	
	/**
	 * Constructor:
	 * @param vars The variables of this indicator function.
	 * @param values The indicator value for each configuration.
	 */
	
	public Indicator(FiniteVariable[] vars, int... values) {
		super(vars, semiring, convert(values));
	}
	
	/**
	 * Constructor:
	 * @param vars The variables of this indicator function.
	 * @param values The indicator value for each configuration.
	 */
	
	public Indicator(FiniteVariable[] vars, boolean... values) {
		super(vars, semiring, convert(values));
	}
		
	/**
	 * Private Constructor:
	 * @param vars The variables of this probability potential.
	 * @param values The probability value for each possible configuration.
	 */
	
	private Indicator(FiniteVariable[] vars, List<Boolean> values) {
		super(vars, semiring, values);
	}
	
	/**
	 * Factory method:
	 * @see nenok.sva.SRValuation#create(nenok.va.FiniteVariable[], Semiring, java.util.List)
	 */
	
	@Override
	public SRValuation<Boolean> create(FiniteVariable[] vars, Semiring<Boolean> semiring, List<Boolean> values) {
		return new Indicator(vars, values);
	}
	
	/**
	 * @see nenok.va.Separativity#inverse()
	 */
	
	public Separativity inverse() {
		return Idempotency.Implementor.getInstance().inverse(this);
	}
	
	/**
     * @see nenok.va.Scalability#scale()
     */
    
    public Scalability scale() {
        return Idempotency.Implementor.getInstance().scale(this);
    }
	
	/**
	 * Converts an array of booleans into semiring elements.
	 * @param values The input array of booleans.
	 * @return A corresponding array of Boolean semiring elements.
	 */
	
	private static List<Boolean> convert(boolean[] values) {
		ArrayList<Boolean> elements = new ArrayList<Boolean>(values.length);
		for (int i = 0; i < values.length; i++) {
			if(values[i]) {
				elements.add(i, TRUE);
			} else {
				elements.add(i, FALSE);
			}
		}
		return elements;
	}
	
	/**
	 * Converts an array of integers into elements of the Boolean semiring.
	 * @param values The input array of integers.
	 * @return A corresponding array of Boolean semiring elements.
	 */
	
	private static List<Boolean> convert(int[] values) {
		ArrayList<Boolean> elements = new ArrayList<Boolean>(values.length);
		for (int i = 0; i < values.length; i++) {
			if(values[i] > 0) {
				elements.add(i, TRUE);
			} else {
				elements.add(i, FALSE);
			}
		}
		return elements;
	}
	
	/**
     * @see java.lang.Object#toString()
     */
	
	@Representor
    public String toString() {
        int max = 0;
        String format = "", result = "", line = "";
        
        /*
         * Find string of maximum length:
         */
        
        for (int i = 0; i < variables.length; i++) {
            FiniteVariable v = variables[i];
            if(max < v.toString().length()) {
                max = v.toString().length();
            }
            for (int j = 0; j < v.getFrame().length; j++) {
                if(max < v.getFrame()[j].toString().length()) {
                    max = v.getFrame()[j].toString().length();
                }
            }
        }
        
        max += 2;
        
        /*
         * Prepare format string for title:
         */

        for (int i = 0; i < variables.length; i++) {
        	format += "|%"+(i+1)+"$-"+max+"s";
        }
        format += "|\n";
        
        /*
         * Output header line:
         */
        
        Object[] data = new Object[variables.length];
        for (int i = 0; i < variables.length; i++) {
            data[i] = variables[i];
        }
        result += String.format(format, data);
        
        /*
         * Horizontal line:
         */
        
        for (int i = 0; i < result.length()-1; i++) {
            line += "-";
        }
        line += "\n";
        result = line+result+line;

        /*
         * Prepare format string for configurations:
         */

        format = "";
        for (int i = 0; i < variables.length; i++) {
        	format += "|%"+(i+1)+"$-"+max+"s";
        }
        format += "|\n";
                
        /*
         * Configurations:
         */
        
        for (int i = 0; i < values.size(); i++) {
        	if(values.get(i)) {
        		Object row[] = new Object[data.length];
        		for (int j = 0; j < variables.length; j++) {
        			row[j] = variables[j].getFrame()[Configuration.getSymbolIndex(variables, j,i)].toString();
        		}
        		result += String.format(format, row);
        	}
        }   
        
        result += line;
		return result;
    }
}
