package instances.indicators;

import java.util.Arrays;

import gui.Viewer;
import nenok.Architecture;
import nenok.Knowledgebase;
import nenok.LCFactory;
import nenok.lc.JoinTree;
import nenok.va.Domain;
import nenok.va.FiniteVariable;
import nenok.va.Valuation;

/**
 * This application simulates an n-bit adder . The first argument, either "Console" or 
 * "Interactive", specifies the application mode and the second argument defines the 
 * size of the adder circuit.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 681 $<br/>$LastChangedDate: 2009-03-26 19:48:39 +0000 (Do, 26 Mrz 2009) $
 */

public class Adder {
    
    private static final String[] frame = new String[] {"0", "1"};

    /**
     * @param args Usage: Adder #procs (Interactive | Console) #size
     */
    
    public static void main(String[] args) {
        
        // Verify number of arguments:
        if(args.length != 2) {
            System.out.println("Wrong number of Arguments.");
            System.out.println("Usage: Adder (Interactive | Console) #size");
            System.exit(1);
        }
                
        // Fetch adder size:
        int size = Integer.parseInt(args[1]);
        
        if(size < 1) {
            System.out.println("The adder size should be positive.");
            System.out.println("Usage: Adder (Interactive | Console) #size");
            System.exit(1);
        }        
         
        /*
         * Variables:
         */

        //Input Variables:
        FiniteVariable[] in = new FiniteVariable[2*size];
        for (int i = 0; i < in.length; i++) {
			in[i] = new FiniteVariable("in"+(i+1), frame);
		}
        
        FiniteVariable inC = new FiniteVariable("inC", frame);

        //Output Variables:
        FiniteVariable[] out = new FiniteVariable[size];
        for (int i = 0; i < out.length; i++) {
        	out[i] = new FiniteVariable("out"+(i+1), frame);
		}
        
        FiniteVariable outC = new FiniteVariable("outC", frame);
        
        /*
         * Indicators:
         */
        
        // Adder circuit:
        Indicator[] adder = Gates.Adder_N(in, inC, out, outC);
        
        // Input lines:
        Indicator[] lines = new Indicator[2*size+1];
        for (int i = 0; i < lines.length - 1; i++) {
			lines[i] = Gates.LINE(in[i], 0);
		}
        
        lines[2*size] =  Gates.LINE(inC, 0);

        //Collect knowledgebase:
        Valuation[] indicators = new Indicator[adder.length+lines.length];
        System.arraycopy(adder, 0, indicators, 0, adder.length);
        System.arraycopy(lines, 0, indicators, adder.length, lines.length);
                                
        /*
         * Queries:
         */
        
        Domain[] queries = new Domain[] {
            new Domain(outC)
        };
        
        /*
         * Local Computation:
         */
        
        try {
        	
        	Knowledgebase kb = new Knowledgebase(indicators, size+"-Bit-Adder");
        
	        if(args[0].equalsIgnoreCase("Console")) {
	        
	            LCFactory factory = new LCFactory();
	            factory.setArchitecture(Architecture.Shenoy_Shafer);
	            JoinTree tree = factory.create(kb, Arrays.asList(queries));
	            tree.propagate();
	            
	            /*
	             * Query answering:
	             */
	            
	            System.out.println("Marginals of the Adder Example:\n");
	            
	            for(Domain query : queries) {
	                System.out.println("Query: "+query+"\n"+tree.answer(query));
	            }
	                        
	        } else if(args[0].equalsIgnoreCase("Interactive")) {
	        	            	
	        	Viewer.display(kb);
	        	                
	        } else {
	            System.out.println("Wrong second Argument.");
	            System.out.println("Usage: Adder (Interactive | Console) #size");
	        }
                           
        } catch (Exception e1) {
            e1.printStackTrace();
            System.exit(1);
        }
    }
}
