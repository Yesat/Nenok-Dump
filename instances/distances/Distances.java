package instances.distances;

import java.text.DecimalFormatSymbols;

import nenok.path.KValuation;
import nenok.path.SquareMatrix;
import nenok.semiring.Kleene;
import nenok.semiring.Semiring;
import nenok.va.Variable;

/**
 * This class offers an implementation of distance potentials.
 *  
 * @author Marc Pouly
 */

public class Distances extends KValuation<Integer> {
	
	/*
	 * Constants:
	 */
	
	private static final String INF = "inf";
	private static final DecimalFormatSymbols SYMBOLS = new DecimalFormatSymbols();
	
	/*
	 * Kleene Algebra Implementation:
	 */
	
	private static final Kleene<Integer> KLEENE = new Kleene<Integer>() {
								
		/**
		 * @see Semiring#add(java.lang.Object, java.lang.Object)
		 */
		
		public Integer add(Integer e1, Integer e2) {
			return Math.min(e1,e2); 
		}

		/**
		 * @see Semiring#multiply(java.lang.Object, java.lang.Object)
		 */
		
		public Integer multiply(Integer e1, Integer e2) {
			if(e1.intValue() == Integer.MAX_VALUE || e2.intValue() == Integer.MAX_VALUE) {
				return Integer.MAX_VALUE;
			}
			return e1+e2;
		}

		/**
		 * @see Semiring#valueToString(java.lang.Object)
		 */

	    public String valueToString(Integer e) {
			if(e.intValue() == Integer.MAX_VALUE) {
				return SYMBOLS.getInfinity(); 
			}
	    	return e.toString();
	    }
	    
		/**
		 * @see Kleene#closure(java.lang.Object)
		 */

		public Integer closure(Integer element) {
			return 0;
		}
		
		/**
		 * @see Kleene#unit()
		 */

		public Integer unit() {
			return 0;
		}
		
		/**
		 * @see Kleene#zero()
		 */

		public Integer zero() {
			return Integer.MAX_VALUE;
		}
		
		@Override
		public boolean isEqual(Integer elt1, Integer elt2) {
			return elt1.equals(elt2);
		}
	};
	
	/**
	 * Constructor:
	 * @param vars The variable array.
	 * @param matrix The adjacency matrix of non-negative integers.
	 */
	
	public Distances(Variable[] vars, int[][] matrix) {
		super(vars, KLEENE, new SquareMatrix<Integer>(vars, convert(matrix), KLEENE), true);
	}
	
	/**
	 * Constructor:
	 * @param vars The variable array.
	 * @param matrix The adjacency matrix of non-negative integers as strings or inf.
	 */
	
	public Distances(Variable[] vars, String[][] matrix) {
		super(vars, KLEENE, new SquareMatrix<Integer>(vars, convert(matrix), KLEENE), true);
	}	
	
	/**
	 * Constructor:
	 * @param vars The variable array.
	 * @param matrix The adjacency matrix of non-negative integers.
	 */
	
	private Distances(Variable[] vars, Kleene<Integer> kleene, SquareMatrix<Integer> matrix, Variable[][] pred, boolean closure) {
		super(vars, KLEENE, matrix, pred, closure);
	}	
	
	/**
	 * @see nenok.path.KValuation#create(nenok.va.Variable[], Kleene, Object[][], Variable[][], boolean)
	 */
	
	@Override
	public KValuation<Integer> create(Variable[] vars, Kleene<Integer> kleene, SquareMatrix<Integer> matrix, Variable[][] pred, boolean closure) {
		return new Distances(vars, kleene, matrix, pred, closure);
	}
		
	/**
	 * Converts a matrix of non-negative int values into a matrix of objects.
	 * @param matrix The matrix of int values.
	 * @return The according matrix of {@link Integer} objects.
	 */
	
	private static Integer[][] convert(int[][] matrix) {
		int size = matrix.length;
		Integer[][] result = new Integer[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if(matrix[i][j] < 0) {
					throw new IllegalArgumentException("Only positive integer values accepted. Given: "+matrix[i][j]+".");
				}
				result[i][j] = new Integer(matrix[i][j]);
			}
		}
		return result;
	}
	
	/**
	 * Converts a matrix of string values into a matrix of {@link Integer} objects.
	 * @param matrix The matrix of string values.
	 * @return The according matrix of {@link Integer} objects.
	 */
	
	private static Integer[][] convert(String[][] matrix) {
		int size = matrix.length;
		Integer[][] result = new Integer[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if(matrix[i][j].equalsIgnoreCase(INF)) {
					result[i][j] = Integer.MAX_VALUE;
				} else {
					result[i][j] = new Integer(matrix[i][j]);
					if(result[i][j].intValue() < 0) {
						throw new IllegalArgumentException("Only positive integer values accepted. Given: "+matrix[i][j]+".");
					}
				}
				
			}
		}
		return result;
	}
}
