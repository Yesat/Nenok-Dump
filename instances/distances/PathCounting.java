package instances.distances;

import java.text.DecimalFormatSymbols;

import nenok.path.QValuation;
import nenok.path.SquareMatrix;
import nenok.path.Vector;
import nenok.semiring.Quasiregular;
import nenok.va.Variable;

/**
 * This class offers an implementation of the path counting application.
 * It is based on the quasi-regular, arithmetic semiring of non-negative integers and thus extends the {@link QValuation} class.
 * 
 * @author Marc Pouly
 */

public class PathCounting extends QValuation<Integer> {
		
	/*
	 * Constants:
	 */

	private static final DecimalFormatSymbols SYMBOLS = new DecimalFormatSymbols();
	
	/*
	 * Semiring Implementation:
	 */
	
	private static final Quasiregular<Integer> SEMIRING = new Quasiregular<Integer>() {
								
		/**
		 * @see Semiring#add(java.lang.Object, java.lang.Object)
		 */
		
		public Integer add(Integer e1, Integer e2) {
			if(e1 >= Integer.MAX_VALUE-e1 || e2 >= Integer.MAX_VALUE-e1) {
				return Integer.MAX_VALUE;
			}
			return e1+e2;
		}

		/**
		 * @see Semiring#multiply(java.lang.Object, java.lang.Object)
		 */
		
		public Integer multiply(Integer e1, Integer e2) {
			if(e1.intValue() == 0 || e2.intValue() == 0) {
				return 0;
			}			
			if(e1 >= Integer.MAX_VALUE / e2 || e2 >= Integer.MAX_VALUE / e1) {
				return Integer.MAX_VALUE;
			}
			return e1*e2;
		}

		/**
		 * @see Semiring#valueToString(java.lang.Object)
		 */

	    public String valueToString(Integer e) {
			if(e.intValue() == Integer.MAX_VALUE) {
				return SYMBOLS.getInfinity(); 
			}
	    	return e.toString();
	    }
	    
		/**
		 * @see Kleene#closure(java.lang.Object)
		 */

		public Integer closure(Integer e) {
			if(e.intValue() == 0) {
				return 1;
			}
			return Integer.MAX_VALUE;
		}
		
		/**
		 * @see Kleene#unit()
		 */

		public Integer unit() {
			return 1;
		}
		
		/**
		 * @see Kleene#zero()
		 */

		public Integer zero() {
			return 0;
		}
		
		@Override
		public boolean isEqual(Integer elt1, Integer elt2) {
			return elt1.equals(elt2);
		}
	};
	
	/**
	 * Constructor:
	 * @param vars The variable array.
	 * @param matrix The adjacency matrix of non-negative integers.
	 */
	
	public PathCounting(Variable[] vars, int[][] matrix, int[] vector) {
		super(new SquareMatrix<Integer>(vars, convert(matrix), SEMIRING), new Vector<Integer>(vars, convert(vector), SEMIRING), SEMIRING);
	}
	
	/**
	 * Constructor:
	 * @param vars The variable array.
	 * @param matrix The adjacency matrix of non-negative integers.
	 */
	
	private PathCounting(SquareMatrix<Integer> matrix, Vector<Integer> vector) {
		super(matrix, vector, SEMIRING);
	}
	
	/**
	 * @see nenok.path.QValuation#create(nenok.path.SquareMatrix, nenok.path.Vector, nenok.semiring.Quasiregular)
	 */
	
	@Override
	public QValuation<Integer> create(SquareMatrix<Integer> matrix, Vector<Integer> vector, Quasiregular<Integer> semiring) {
		return new PathCounting(matrix, vector);
	}
	
	/**
	 * Converts a matrix of non-negative reals into a matrix of objects.
	 * @param matrix The matrix of double values.
	 * @return The according matrix of {@link Double} objects.
	 */
	
	private static Integer[][] convert(int[][] matrix) {
		int size = matrix.length;
		Integer[][] result = new Integer[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if(matrix[i][j] != 0 && matrix[i][j] != 1) {
					throw new IllegalArgumentException("Only zero-one entries accepted. Given: "+matrix[i][j]+".");
				}
				result[i][j] = matrix[i][j];
			}
		}
		return result;
	}
	
	/**
	 * Converts a matrix of non-negative reals into a matrix of objects.
	 * @param matrix The matrix of double values.
	 * @return The according matrix of {@link Double} objects.
	 */
	
	private static Integer[] convert(int[] vector) {
		int size = vector.length;
		Integer[] result = new Integer[size];
		for (int i = 0; i < size; i++) {
			if(vector[i] != 0 && vector[i] != 1) {
				throw new IllegalArgumentException("Only zero-one entries accepted. Given: "+vector[i]+".");
			}
			result[i] = vector[i];
		}
		return result;
	}
}
