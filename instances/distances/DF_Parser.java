package instances.distances;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.jdom2.Element;

import nenok.parser.ParserException;
import nenok.parser.XmlParser;
import nenok.va.StringVariable;
import nenok.va.Valuation;
import nenok.va.Variable;

/**
 * This class provides an XML parser implementation for distance functions.
 * 
 * @author Marc Pouly &amp; Antoine De Groote
 */

public class DF_Parser extends XmlParser {

	private static final Integer INFINITY = Integer.MAX_VALUE;

	/**
	 * @see nenok.parser.XmlParser#parseVariable(java.lang.String,java.util.List)
	 */

	public Variable parseVariable(String name, List<Element> content) throws ParserException {
		return new StringVariable(name);
	}

	/**
	 * @see nenok.parser.XmlParser#parseValuation(java.util.List,java.util.Hashtable)
	 */

	public Valuation parseValuation(List<Element> content, Hashtable<String, Variable> variables) throws ParserException {

		ArrayList<StringVariable> vars = new ArrayList<StringVariable>();
		Hashtable<Variable, Integer> distances = new Hashtable<Variable, Integer>();

		for (Element elem : content) {

			if (elem.getName().equals("label")) {
				continue;
			}

			if (elem.getName().equals("cities")) {
				for (Element e : elem.getChildren()) {

					// Current city:
					Variable name = variables.get(e.getText());
					
					// Distance to first city:
					Integer distance = new Integer(e.getAttributeValue("distance"));
					distances.put(name, distance);
				}
			}
		}

		/*
		 * Variable array:
		 */

		Variable reference = null;

		for (Variable v : distances.keySet()) {
			if (distances.get(v).intValue() == 0) {
				reference = v;
			}
			vars.add((StringVariable)v);
		}
		
		/*
		 * Distance matrix:
		 */

		int[][] matrix = new int[vars.size()][vars.size()];

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {

				StringVariable v1 = vars.get(i);
				StringVariable v2 = vars.get(j);

				if (v1.equals(v2)) {
					matrix[i][j] = 0;
				}

				else if (v1.equals(reference)) {
					matrix[i][j] = distances.get(v2);
					
				}

				else if (v2.equals(reference)) {
					matrix[i][j] = distances.get(v1);
				}

				else {
					matrix[i][j] = INFINITY;
				}
			}
		}
		
		return new Distances(vars.toArray(new StringVariable[vars.size()]), matrix);
	}
}
