package instances.regLang;

import nenok.semiring.Kleene;
import nenok.semiring.Semiring;
import nenok.va.Variable;

import nenok.path.KValuation;
import nenok.path.SquareMatrix;

public class RegLang extends KValuation<String>{
	
	/*
	 * Kleene Algebra implementation:
	 */

	private static final Kleene<String> KLEENE = new Kleene<String>() {
		
		/**
		 * @see Semiring#add(java.lang.Object, java.lang.Object)
		 */
		public String add(String s1, String s2) {
			int x = test(s1, s2);
			switch (x) {
			case 10:
				return s2;
			case 20:
				return s1;
			default:
				return "("+s1+"+"+s2+")";
			}
		}

		@Override
		public String closure(String e) {
			return e+"*";
		}

		@Override
		public String unit() {
			return "eps";
		}

		@Override
		public String multiply(String e1, String e2) {
			// TODO Add the 1 and 0 cases
			int x = test(e1,e2);
			switch (x) {
			case 10:
				return(null);
			case 20:
				return(null);
			case 1:
				return(e2);
			case 2:
				return(e1);
			default:
				return(e1+e2);
			}
			
		}

		@Override
		public String zero() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String valueToString(String e) {
			return e;
		}

		@Override
		public boolean isEqual(String e1, String e2) {
			// TODO this is going to be the not fun part
			return e1.equals(e2);
		}
	};
	
	public static int test(String e1, String e2) {
		if (e1.equals("eps")){
			return 1;
		} if (e2.equals(e2)){
			return 2;
		} if (e1.equals(null)) {
			return 10;
		} if (e2.equals(null)){
			return 20;
		} else {
			return 3;
		}
	}

	public RegLang(Variable[] vars, Kleene<String> kleene, SquareMatrix<String> matrix, boolean closure) {
		super(vars, KLEENE, matrix, closure);
	}

	@Override
	public KValuation<String> create(Variable[] vars, Kleene<String> kleene, SquareMatrix<String> matrix,
			Variable[][] succ, boolean closure) {
		return new RegLang(vars, kleene, matrix, closure);
	}


}
