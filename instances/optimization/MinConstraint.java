package instances.optimization;

import java.text.DecimalFormatSymbols;
import java.util.List;

import nenok.Utilities;
import nenok.semiring.Dividability;
import nenok.semiring.Optimization;
import nenok.semiring.Semiring;
import nenok.sva.OSRValuation;
import nenok.va.FiniteVariable;
import nenok.va.Scalability;
import nenok.va.Separativity;

/**
 * This class offers an implementation of weighted constraints taking values from the tropical semiring.
 * 
 * @author Marc Pouly
 */

public class MinConstraint extends OSRValuation<Integer> implements Scalability {
	
	private static final Tropical SEMIRING = new Tropical();
	private static final DecimalFormatSymbols SYMBOLS = new DecimalFormatSymbols();
	
	/*
	 * Semiring Implementation:
	 */
	
	private static final class Tropical implements Optimization<Integer>, Dividability<Integer> {
		
		/*
		 * Implementors:
		 */
		
		private final Optimization.Implementor<Integer> comparator = new Optimization.Implementor<Integer>(this);

		/**
		 * @see Optimization#compare(java.lang.Object, java.lang.Object)
		 */
		
		public int compare(Integer e1, Integer e2) {
			return comparator.compare(e1, e2);
		}
		
		/**
		 * @see Semiring#add(java.lang.Object, java.lang.Object)
		 */
		
		public Integer add(Integer e1, Integer e2) {
			return Math.min(e1,e2);
		}

		/**
		 * @see Semiring#multiply(java.lang.Object, java.lang.Object)
		 */
		
		public Integer multiply(Integer e1, Integer e2) {
			if(e1 >= Integer.MAX_VALUE || e2 >= Integer.MAX_VALUE) {
				return Integer.MAX_VALUE;
			}
			return e1+e2;
		}

		/**
		 * @see nenok.semiring.Semiring#zero()
		 */
		
		public Integer zero() {
			return Integer.MAX_VALUE;
		}
		
		/**
		 * @see Dividability#inverse(java.lang.Object)
		 */
		
		public Integer inverse(Integer e) {
			return -e;
		}
		
		/**
		 * @see Semiring#valueToString(java.lang.Object)
		 */

	    public String valueToString(Integer e) {
			if(e.intValue() == Integer.MAX_VALUE) {
				return SYMBOLS.getInfinity(); 
			}
			if(e.intValue() == Integer.MIN_VALUE) {
				return "-"+SYMBOLS.getInfinity(); 
			}
	    	return e.toString();
	    }
	    
	    @Override
		public boolean isEqual(Integer elt1, Integer elt2) {
			return elt1.equals(elt2);
		}
	};
	
	/**
	 * Constructor:
	 * @param vars The variable of this weighted constraint.
	 * @param values The weight for each configuration.
	 */
	
	public MinConstraint(FiniteVariable[] vars, int... values) {
		super(vars, SEMIRING, Utilities.toList(values));
	}
		
	/**
	 * Private Constructor:
	 * @param vars The variables of this weighted constraint.
	 * @param values The weight for each possible configuration.
	 */
	
	private MinConstraint(FiniteVariable[] vars, List<Integer> values) {
		super(vars, SEMIRING, values);
	}
	
	/**
	 * Factory method:
	 * @see nenok.sva.SRValuation#create(nenok.va.FiniteVariable[], Semiring, java.util.List)
	 */
	
	@Override
	public OSRValuation<Integer> create(FiniteVariable[] vars, Semiring<Integer> semiring, List<Integer> values) {
		return new MinConstraint(vars, values);
	}

	/**
	 * @see nenok.va.Separativity#inverse()
	 */
	
	public Separativity inverse() {
		return Separativity.Implementor.getInstance().inverse(this);
	}
	
	/**
	 * @see nenok.va.Scalability#scale()
	 */
	
	public Scalability scale() {
		return Scalability.Implementor.getInstance().scale(this);
	}
}
