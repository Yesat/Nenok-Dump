package nenok.sva;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nenok.semiring.Optimization;
import nenok.va.Domain;
import nenok.va.FiniteVariable;
import nenok.va.Representor;
import nenok.va.VAException;
import nenok.va.Valuation;

/**
 * Specifies a valuation induced by a totally ordered idempotent semiring. 
 * The according elements are of type {@link Optimization}. 
 * Such semiring induced valuation algebras model optimization problems and allow for dynamic programming tasks.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 560 $<br/>$LastChangedDate: 2008-03-26 14:37:05 +0100 (Mi, 26 Mrz 2008) $
 */

public abstract class OSRValuation<E> extends SRValuation<E> {
	
	private FiniteVariable[] original;
	private int[] extension;
	
	/**
	 * Constructor:
	 * @param vars The variables of this semiring valuation. 
	 * @param values The semiring value for each possible configuration.<br/> 
	 */
	
	public OSRValuation(FiniteVariable[] vars, Optimization<E> semiring, List<E> values) {
		super(vars, semiring, values);
	}
	
	/**
	 * @see nenok.sva.SRValuation#marginalize(nenok.va.Domain)
	 */

	@Override
	public Valuation marginalize(Domain dom) {
		if(label().equals(dom)) {
			
			if(extension == null) {
				original = variables;
				extension = new int[values.size()];
				for (int i = 0; i < extension.length; i++) {
					extension[i] = i;
				}
			}
			
		    return this;
		}
		if(!dom.subSetOf(label())) {
			new VAException("Marginalization impossible: this.label() = "+label()+", argument = "+dom+".");
		}
		// Calculates the difference set between both domains.
		FiniteVariable[] elim = Domain.difference(label(), dom).toArray(FiniteVariable.class);
		
		// Eliminate each variable in the difference set.
		FiniteVariable[] vars = variables;
		ArrayList<E> elements = new ArrayList<E>(values);
		int[] ext = extension;
		
		Optimization<E> semiring = (Optimization<E>)getSemiring();
		
		for (int n = 0; n < elim.length; n++) {
			
			FiniteVariable var = elim[n];
			FiniteVariable[] newVars = new FiniteVariable[vars.length-1];
	        int oldPosition = -1;
	        int size = 1;
	        
	        // Fill the new variable array and calculate the number of configurations of the new potential.
	        for (int i = 0, j = 0; i < vars.length; i++) {
	            if(!vars[i].equals(var)) {
	                size *= vars[i].getFrame().length;
	                newVars[j++] = vars[i];
	            } else {
	                oldPosition = i;
	            }
	        }
	        	        
	        // The list of configuration extensions:
	        int[] newExtensions = new int[size];
	        
	        // The new element array.
	        ArrayList<E> newElements = new ArrayList<E>();
	        int offset = 1;
	        for (int i = oldPosition + 1; i < vars.length; i++) {
	            offset *= vars[i].getFrame().length;
	        }
	        
	        // Calculate the elements of the semiring value & extension array.
	        int blocs = (elements.size() / var.getFrame().length) / offset;
	        int blocLines = size /*newElements.length*/ / blocs;
	        for (int i = 0, index = 0; i < blocs; i++) {
	            int base = i * (elements.size() / blocs);
	            for (int j = 0; j < blocLines; j++) {
	            	E sum = null; 
	                for (int k = 0; k < var.getFrame().length; k++) {   
	                	int position = base + j + k*offset;
	                	if(sum == null) {
	                		sum = elements.get(position);
	                		newExtensions[index] = (ext == null) ? position : ext[position];
	                	} else {
	                		E value = elements.get(position);
	                		if(semiring.compare(value, sum) < 0) {
	                			newExtensions[index] = (ext == null) ? position : ext[position];
	                		}
	                		sum = getSemiring().add(sum, elements.get(position));
	                	}
	                }
	                newElements.add(index, sum);
	                index++;
	            }
	        }
	        vars = newVars;
	        elements = newElements;
	        ext = newExtensions;
		}
				
        // Generate and return the new instance.
        OSRValuation<E> result = (OSRValuation<E>) create(vars, semiring, elements);
        result.extension = ext;
        result.original = (original == null)? variables : original;
                
        return result;
	}

	/**
	 * @return This valuation together with possible extensions as string.
	 */
	
	@Representor
	public String displayExtension() {
		
		if(extension == null) {
			return null;
		}
		
		// Get fields:
		List<E> values = getValues();
		FiniteVariable[] variables = getVariables();
				
        int maxConf = 0, maxExt = 0;
        String format = "", result = "", line = "", vTitle = "Value", eTitle = "Extension";
        
        /*
         * Find string of maximum length:
         */
        
        maxConf = vTitle.length();
        for (int i = 0; i < variables.length; i++) {
            FiniteVariable v = variables[i];
            if(maxConf < v.toString().length()) {
                maxConf = v.toString().length();
            }
            for (int j = 0; j < v.getFrame().length; j++) {
                if(maxConf < v.getFrame()[j].toString().length()) {
                    maxConf = v.getFrame()[j].toString().length();
                }
            }
        }
        maxConf += 2;
                                
        /*
         * Prepare extension string:
         */
        
        String[] ext = new String[extension.length];
        for (int i = 0; i < ext.length; i++) {
        	for(FiniteVariable var : original) {
        		if(Arrays.binarySearch(getVariables(),var) >= 0) {
        			continue;
        		}
        		int index = Configuration.getSymbolIndex(original, Arrays.binarySearch(original, var), extension[i]);
        		Object val = var.getFrame()[index];
        		ext[i] = (ext[i] == null)? var+" = "+val : ext[i]+", "+var+" = "+val;
        	}
        	ext[0] += " ";
		}
        
        maxExt = Math.max(ext[0].length(), eTitle.length());
        maxExt += 2;
        
        /*
         * Prepare format string for titles:
         */

        for (int i = 0; i < variables.length+1; i++) {
        	format += "|%"+(i+1)+"$-"+maxConf+"s";
        }
        format += "|%"+(variables.length+2)+"$-"+maxExt+"s";
        format += "|\n";
        
        /*
         * Output header line:
         */
        
        Object[] data = new Object[variables.length+2];
        for (int i = 0; i < variables.length; i++) {
            data[i] = variables[i];
        }
        data[variables.length] = vTitle;
        data[variables.length+1] = eTitle;
        result += String.format(format, data);
        
        /*
         * Horizontal line:
         */
        
        for (int i = 0; i < result.length()-1; i++) {
            line += "-";
        }
        line += "\n";
        result = line+result+line;
        
        /*
         * Prepare format string for configurations:
         */

        format = "";
        for (int i = 0; i < variables.length; i++) {
        	format += "|%"+(i+1)+"$-"+maxConf+"s";
        }
        format += "|%"+(variables.length+1)+"$-"+maxConf+"s";
        format += "|%"+(variables.length+2)+"$-"+maxExt+"s|\n";
         
        /*
         * Configurations:
         */
        
        for (int i = 0; i < values.size(); i++) {
            Object row[] = new Object[data.length];
            for (int j = 0; j < variables.length; j++) {
                row[j] = variables[j].getFrame()[Configuration.getSymbolIndex(variables, j,i)].toString();
            }
            row[data.length-2] = values.get(i);
            row[data.length-1] = ext[i]; //extension[i]+1;
            result += String.format(format, row);
        }   
        
        result += line;
		return result;
	}

	/**
	 * @return A non-deterministically chosen solution configuration, relative to a subset of variables.
	 */
	
	public String[] getSolutionConfiguration(FiniteVariable[] vars) {
		
		@SuppressWarnings("unchecked")
		OSRValuation<E> val = (OSRValuation<E>)this.marginalize(Domain.EMPTY);
		
		int index = val.extension[0]; 
		index = Configuration.convert(val.extension[0], variables, vars);
		return Configuration.decode(index, variables);
	}
	
	/**
	 * @return A non-deterministically chosen solution configuration, relative to <code>this.getVariables()</code>.
	 */
	
	public String[] getSolutionConfiguration() {
		
		@SuppressWarnings("unchecked")
		OSRValuation<E> val = (OSRValuation<E>)this.marginalize(Domain.EMPTY);
		
		int index = val.extension[0]; 
		return Configuration.decode(index, variables);
	}
	
	/**
	 * @return Largest semiring element in this valuation with respect to canonical semiring order. 
	 */
	
	public E maxValue() {
		E value = values.get(0);
		for (int i = 1; i < values.size(); i++) {
			value = semiring.add(value, values.get(i));
		}
		return value;
	}
	
    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    
	public boolean equals(Object o) {
		if(!(o instanceof OSRValuation)) {
			return false;
		}
		
		// No additional equality checks necessary.
		return super.equals(o);
	}
	
	/**
	 * Returns the solution extension of a configuration relative to some variable array
	 * or <code>null</code> if this factor was not created by an operation of marginalization. 
	 * @param conf The configuration whose extension is returned.
	 * @param rel The variable array that specifies the input configuration's order.
	 * @param result The variable array that specifies the extension's order.
	 * @return A solution extension of the given configuration.
	 */
	
    public String[] getExtension(String[] conf, FiniteVariable[] rel, FiniteVariable[] result) {
    	
    	if(conf.length != rel.length) {
    		throw new IllegalArgumentException("Configuration and variable array are not equally long.");
    	}
    	
    	int c = Configuration.encode(conf, rel);
    	c = Configuration.convert(c, rel, variables);

    	// Extension array may be null if no marginalization has been executed.
    	if(extension == null) {
    		return null;
    	}
    	
    	String[] t = Configuration.decode(extension[c], original);
    	return Configuration.project(t, original, result);
    }
}
