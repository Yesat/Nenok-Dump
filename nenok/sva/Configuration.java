package nenok.sva;

import java.util.Arrays;

import nenok.va.Domain;
import nenok.va.FiniteVariable;

/**
 * Utility class for the representation of configurations.
 * This class encodes configurations (string arrays) into the according integer position of their standard enumeration.
 * 
 * @author Marc Pouly
 */

public class Configuration {
	
	/**
	 * Encodes the given configuration (object array) into an integer.
	 * The variable array defines the configuration's order.
	 * @param conf The configuration to encode.
	 * @param vars The variable array to specify the configuration's order.
	 * @return The encoded configuration relative to the variable array.
	 */
	
	public static int encode(Object[] conf, FiniteVariable[] vars) {
		int result = 0;
		for (int i = 0; i < vars.length; i++) {
			int repeat = 1;
			int symbol = indexOf(vars[i].getFrame(), conf[i]); 
            for (int j = i + 1; j < vars.length; j++) {
                repeat *= vars[j].getFrame().length;
            }           
            result += symbol * repeat;
		}
		return result;
	}
	
	/**
	 * Decodes the given configuration (integer) into an object array.
	 * The variable array defines the configuration's code order.
	 * @param conf The configuration to decode.
	 * @param vars The variable array to specify the configuration's order.
	 * @return The decoded configuration relative to the variable array.
	 */
	
    public static String[] decode(int conf, FiniteVariable[] vars) {
    	if(conf >= FiniteVariable.countConfigurations(vars)) {
    		throw new ArrayIndexOutOfBoundsException("Configuration "+conf+" larger than product of frame cardinality of "+Arrays.toString(vars)+".");
    	}
        String[] result = new String[vars.length];
        for (int i = 0; i < vars.length; i++) {
        	int n = getSymbolIndex(vars, i, conf);
        	result[i] = vars[i].getFrame()[n];
        }
        return result;
    }
	
    /**
     * Converts a configuration encoding relative to some variable array into
     * the encoding relative to another variable array.
     * @param conf The configuration to convert.
     * @param from The variable array to which the input configuration is relative.
     * @param to The variable array to which the output configuration is relative.
     * @return The converted configuration.
     */
	
    public static int convert(int conf, FiniteVariable[] from, FiniteVariable[] to) {
    	if(from.length != to.length) {
    		throw new IllegalArgumentException("Variable arrays are not equal: " +
    				"first array: "+Arrays.toString(from)+", " +
    						"second array: "+Arrays.toString(to)+".");
    	}
    	Object[] c = decode(conf, from);
    	Object[] result = new Object[to.length];
    	for (int i = 0; i < to.length; i++) {
			int index = indexOf(from, to[i]);
			result[i] = c[index];
		}
    	return encode(result, to);	
    }
    
	/**
	 * Projects a configuration given as object array to some sub-set of variables.
	 * @param conf The configuration that has to be projected.
	 * @param from The variable array to which the argument configuration is relative.
	 * @param to The target array to which the projection is executed.
	 * @return The argument configuration projected (and relative) to <code>to</code>.
	 */
    
	public static String[] project(String[] conf, FiniteVariable[] from, FiniteVariable[] to) {
		String[] result = new String[to.length];
		for (int i = 0; i < to.length; i++) {
			int index = indexOf(from, to[i]);
			if(index < 0) {
				return null;
			}
			result[i] = conf[index];
		}
		return result;
	}
    
	/**
	 * Computes the union of two partial configurations.
	 * @param c1 The first configuration.
	 * @param v1 The variable array that specifies the first configuration's order.
	 * @param c2 The second configuration.
	 * @param v2 The variable array that specifies the second configuration's order.
	 * @param result The variable array that specifies the result's order.
	 * @return The union configuration of the two argument configuration relative to the given variable array.
	 */
	
	public static String[] union(String[] c1, FiniteVariable[] v1, String[] c2, FiniteVariable[] v2, FiniteVariable[] result) {
		
		Domain d1 = new Domain(v1);
		Domain d2 = new Domain(v2);
		
		// Tests if the two input variable arrays are distinct.
		if(!Domain.intersection(d1, d2).equals(Domain.EMPTY)) {
			throw new IllegalArgumentException("Input variable arrays are not distinct.");
		}
		
		// Tests if the union of the two input variable arrays is equal to the output variable array.
		if(!Domain.union(d1, d2).equals(new Domain(result))) {
			throw new IllegalArgumentException("Union of input variable arrays is not equal to output variable array.");
		}
		
		String[] conf = new String[result.length];
		for (int i = 0; i < result.length; i++) {
			
			FiniteVariable var = result[i];
			int index = indexOf(v1, var);
			if(index > -1) {
				conf[i] = c1[index];
			} else {
				index = indexOf(v2, var);
				conf[i] = c2[index];
			}
		}
		
		return conf;
	}
	
    /**
     * Transforms configurations into string representation.
     * @param vars The order of variables that defines the configurations.
     * @return Configurations as stings.
     */
    
    public static String[] getConfigurations(FiniteVariable... vars) {
    	
    	int count = 1;
    	for (int i = 0; i < vars.length; i++) {
			count *= vars[i].getFrame().length;
		}
    	
        String[] result = new String[count];
        
        for (int i = 0; i < count; i++) {
            String conf = "";
            for (int j = 0; j < vars.length; j++) {
                conf += vars[j].getFrame()[getSymbolIndex(vars, j, i)].toString();
            }
            result[i] = conf;
        }
        return result;
    }
	
    /**
     * This method returns the index of the variable's value at a specifique configuration.
     * @param vars The order of variables that defines the configuration.
     * @param var The variable caracterized by its position.
     * @param conf The configuration whose index has to be found.
     * @return The symbol of the variable at the given configuration.
     * Example:<br/><code>getSymbol(vars, 0,3)</code> returns the index of variable 0's symbol in vars at configuration 3.
     */
    
    public static int getSymbolIndex(FiniteVariable[] vars, int var, int conf) {
        int repetitions = 1;
        for (int i = var + 1; i < vars.length; i++) {
            repetitions *= vars[i].getFrame().length;
        }
        return (conf / repetitions) % vars[var].getFrame().length;
    }
    
    /**
     * Finds an item in an array of objects.
     * @param objects The array of objects.
     * @param item The item to find.
     * @return The item's position in the given array or <code>-1</code> if the item cannot be found.
     */
    
    public static int indexOf(Object[] objects, Object item) {
    	for (int i = 0; i < objects.length; i++) {
			if(item.equals(objects[i])) {
				return i;
			}
		}
    	return -1;
    }
}
