package nenok.sva;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nenok.va.Domain;
import nenok.va.FiniteVariable;
import nenok.va.Identity;
import nenok.va.Predictability;
import nenok.va.Predictor;
import nenok.va.Representor;
import nenok.va.VAException;
import nenok.va.Valuation;
import nenok.va.Variable;
import nenok.semiring.Semiring;

/**
 * Specifies a semiring induced valuation by leading its principal operations of 
 * marginalization &amp; combination back to the according semiring operations.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 560 $<br/>$LastChangedDate: 2008-03-26 14:37:05 +0100 (Mi, 26 Mrz 2008) $
 */

public abstract class SRValuation<E> implements Valuation, Predictability {
    	
    protected FiniteVariable[] variables;
    protected List<E> values;
    private Domain domain;
    
    protected final Semiring<E> semiring;
    
    // SINGLETON design pattern.
    private static SRValuation<?>.SRPredictor self;
    
    /**
     * Factory method:
	 * @param vars The variables of this semiring valuation. 
	 * @param values The semiring value for each possible configuration.
	 * @return A new instance of this class.
     */
    
    public abstract SRValuation<E> create(FiniteVariable[] vars, Semiring<E> semiring, List<E> values);
        
	/**
	 * Constructor:
	 * @param vars The variables of this semiring valuation. 
	 * @param values The semiring value for each possible configuration.<br/> 
	 * The number of values must match with the product computed from each 
	 * variable's frame cardinality.
	 */
		
	public SRValuation(FiniteVariable[] vars, Semiring<E> semiring, List<E> values) {
		
		/*
		 * Check user input:
		 */
		
		if(vars == null || semiring == null || values == null) {
	        throw new IllegalArgumentException("Invalid arguments: At least one argument is null.");
	    }
	    if(FiniteVariable.countConfigurations(vars) != values.size()) {
	        throw new IllegalArgumentException("Dimension of semiring value array doesn't " +
	        		"match. Given: "+values.size()+", expected: "+FiniteVariable.countConfigurations(vars)+".");
	    }	
	    
	    this.semiring = semiring;
	    
	    /*
	     * Sort variable array:
	     */
	    
		FiniteVariable[] copy = new FiniteVariable[vars.length];
		for (int i = 0; i < vars.length; i++) {
			copy[i] = vars[i];
		}	    
	    
	    Arrays.sort(copy);
	    this.variables = copy;
	    
	    /*
	     * Adapt values to new configuration arrangement.
	     */
	    
	    if(!Arrays.equals(vars, copy)) {
	    	ArrayList<E> newvals = new ArrayList<E>();
	    	int size = values.size();
	    	for (int i = 0; i < size; i++) {
	    		for(int j = 0; j < size; j++) {
	    			int index = Configuration.convert(j, vars, copy);
	    			if(index == i) {
	    				newvals.add(values.get(j));
	    				break;
	    			}
	    		}
			}
	    	
	    	this.values = newvals;
	    	
	    } else {
	    	
	    	this.values = values;
	    	
	    }
	}
	
    /**
     * @see nenok.va.Valuation#label()
     */
	
    public Domain label() {
    	if(domain == null) {
    		domain = new Domain(variables);
    	}
        return domain;
    }
    
    /**
     * @see nenok.va.Valuation#combine(nenok.va.Valuation)
     */
     
    public Valuation combine(Valuation val) {
		
    	if(val instanceof Identity) {
			return this;
		}
    	
		if(!val.getClass().equals(this.getClass())) {
			throw new IllegalArgumentException("Unable to combine two valuations of different type.");
		}
    	
        @SuppressWarnings("unchecked")
		SRValuation<E> arg = (SRValuation<E>)val;
        
        //Number of variable in the new domain:
        int inter = 0;
        for (int i = 0; i < variables.length; i++) {
			if(Arrays.binarySearch(arg.variables, variables[i]) >= 0) {
				inter++;
			}
		}
		//Variable array of the new potential:
		FiniteVariable[] newVars = new FiniteVariable[arg.variables.length + this.variables.length - inter];
		FiniteVariable[] interVars = new FiniteVariable[inter];
        
		//Fill new variable array and compute dimension of values array:
		int elementSize = 1, newVarsCounter = 0, interCounter = 0;
		for (; newVarsCounter < this.variables.length; newVarsCounter++) {
			newVars[newVarsCounter] = this.variables[newVarsCounter];
			elementSize *= this.variables[newVarsCounter].getFrame().length;
		}
		for (int j = 0; j < arg.variables.length; j++) {
			if(Arrays.binarySearch(variables, arg.variables[j]) < 0) {
				newVars[newVarsCounter++] = arg.variables[j];
				elementSize *= arg.variables[j].getFrame().length;
			} else {
				interVars[interCounter++]= arg.variables[j];
			}
		}
		
		//Values of the new valuation:
		ArrayList<E> newvals = new ArrayList<E>(elementSize);
		
		/*
		 * Compute each configuration's value
		 */
			
		int[] symbols = new int[inter];
		int thisConfgurations = FiniteVariable.countConfigurations(this.variables);
		int argConfigurations = FiniteVariable.countConfigurations(arg.variables);
		
		// Determine indexes of intervars:
		int thisIndex[] = new int[symbols.length];
		int argIndex[] = new int[symbols.length];
		for (int i = 0; i < interVars.length; i++) {
			thisIndex[i] = Arrays.binarySearch(variables, interVars[i]);
			argIndex[i] = Arrays.binarySearch(arg.variables, interVars[i]);
		}
		
		for (int i = 0, configurationIndex = 0; i < thisConfgurations; i++) {
		    //Store all variables' indexes of this.variables:
		    for (int j = 0; j < interVars.length; j++) {
		    	symbols[j] = Configuration.getSymbolIndex(variables, thisIndex[j], i);
            }
		    for (int j = 0; j < argConfigurations; j++) {
		        boolean multiply = true;
		        for (int k = 0; k < interVars.length; k++) {
                    multiply &= (Configuration.getSymbolIndex(arg.variables, argIndex[k], j) == symbols[k]);
                }
		        
		        //Perform multiplication:
		        if(multiply) {
		        	E result = semiring.multiply(this.values.get(i), arg.values.get(j));
		            newvals.add(configurationIndex++, result); 
		        }
            }
        }
	
		return create(newVars, semiring, newvals);
    }
    
    /**
     * @see nenok.va.Valuation#marginalize(nenok.va.Domain)
     */
      
	public Valuation marginalize(Domain dom) {
		if(dom.equals(label())) {
		    return this;
		}
		if(!dom.subSetOf(label())) {
			new VAException("Marginalization impossible: this.label() = "+label()+", argument = "+dom+".");
		}
		//Calculates the difference set between both domains.
		Domain d = Domain.difference(label(), dom);
		FiniteVariable[] elim = d.toArray(FiniteVariable.class);
		
		// Eliminate each variable in the difference set.
		
		FiniteVariable[] vars = variables;
		ArrayList<E> elements = new ArrayList<E>(values);
		
		for (int n = 0; n < elim.length; n++) {
			
			FiniteVariable var = elim[n];
			FiniteVariable[] newVars = new FiniteVariable[vars.length-1];
	        int oldPosition = -1;
	        int size = 1;
	        
	        // Fill the new variable array and calculate the number of configurations of the new potential.
	        for (int i = 0, j = 0; i < vars.length; i++) {
	            if(!vars[i].equals(var)) {
	                size *= vars[i].getFrame().length;
	                newVars[j++] = vars[i];
	            } else {
	                oldPosition = i;
	            }
	        }
	        
	        //The new element array.
	        ArrayList<E> newElements = new ArrayList<E>();
	        int offset = 1;
	        for (int i = oldPosition + 1; i < vars.length; i++) {
	            offset *= vars[i].getFrame().length;
	        }
	        
	        // Calculate the elements of the probability & extension array.
	        int blocs = (elements.size() / var.getFrame().length) / offset;
	        int blocLines = size / blocs;
	        for (int i = 0, index = 0; i < blocs; i++) {
	            int base = i * (elements.size() / blocs);
	            for (int j = 0; j < blocLines; j++) {
	                E sum = null; 
	                for (int k = 0; k < var.getFrame().length; k++) {   
	                	int position = base + j + k*offset;
	                	if(sum == null) {
	                		sum = elements.get(position);
	                	} else {
	                		sum = semiring.add(sum, elements.get(position));
	                	}
	                }
	                newElements.add(index, sum);
	                index++;
	            }
	        }
	        
	        vars = newVars;
	        elements = newElements;
		}
		
		return create(vars, semiring, elements);
	}

    /**
     * @see nenok.va.Valuation#weight()
     */
    
    public int weight() {
        return Predictability.Implementor.getInstance().weight(this);
    }
	
    /*
     * Predictablility:
     */
    
    /**
     * @see nenok.va.Predictability#predictor()
     */
    
    public Predictor predictor() {
        if(self == null) {
            self = new SRPredictor();
        }
        return self;
    }
	
    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    
	public boolean equals(Object o) {
		if(!(o instanceof SRValuation)) {
			return false;
		}
		
		@SuppressWarnings("unchecked")
		SRValuation<E> p = (SRValuation<E>)o;
		
		if(!Arrays.equals(p.variables, variables)){
			return false;
		}
		
		for (int i = 0; i < values.size(); i++) {
			if(!semiring.isEqual(values.get(i), p.values.get(i))) {
				return false;
			}
		}
		
		return true;
	}
    
    /**
     * Returns the semiring value of the given configuration. In order to make this
     * method independent from the variable (configuration) order, the variable array 
     * to which the configuration is relativ is given.
     * @param vars The variable array that specifies the configuration's order.
     * @param conf The configuration whose value is returned.
     * @return The value of the given configuration or <code>null</code> if no such 
     * configuration exists.
     */
    
	public E evaluate(FiniteVariable[] vars, Object[] conf) {
		int result = Configuration.encode(conf, vars);
		return evaluate(vars, result);
	}
	
	public E evaluate(FiniteVariable[] vars, int conf) {
		int result = Configuration.convert(conf, vars, variables);
		return values.get(result);
	}
		    
	/**
     * @see java.lang.Object#toString()
     */
	
	@Representor
    public String toString() {
        int max = 0;
        String format = "", result = "", line = "", title = "Value";
        
        /*
         * Find string of maximum length:
         */
        
        for (int i = 0; i < variables.length; i++) {
            FiniteVariable v = variables[i];
            if(max < v.toString().length()) {
                max = v.toString().length();
            }
            for (int j = 0; j < v.getFrame().length; j++) {
                if(max < v.getFrame()[j].toString().length()) {
                    max = v.getFrame()[j].toString().length();
                }
            }
        }
        
        if(max < title.length()) {
            max = title.length();
        }
        
        max += 2;
        
        /*
         * Prepare format string for title:
         */

        for (int i = 0; i < variables.length+1; i++) {
        	format += "|%"+(i+1)+"$-"+max+"s";
        }
        format += "|\n";
        
        /*
         * Output header line:
         */
        
        Object[] data = new Object[variables.length+1];
        for (int i = 0; i < variables.length; i++) {
            data[i] = variables[i];
        }
        data[variables.length] = title;
        result += String.format(format, data);
        
        /*
         * Horizontal line:
         */
        
        for (int i = 0; i < result.length()-1; i++) {
            line += "-";
        }
        line += "\n";
        result = line+result+line;

        /*
         * Prepare format string for configurations:
         */

        format = "";
        for (int i = 0; i < variables.length; i++) {
        	format += "|%"+(i+1)+"$-"+max+"s";
        }
        format += "|%"+(variables.length+1)+"$-"+max+"s|\n";
                
        /*
         * Configurations:
         */
        
        for (int i = 0; i < values.size(); i++) {
            Object row[] = new Object[data.length];
            for (int j = 0; j < variables.length; j++) {
                row[j] = variables[j].getFrame()[Configuration.getSymbolIndex(variables, j,i)].toString();
            }
            row[data.length-1] = semiring.valueToString(values.get(i));
            result += String.format(format, row);
        }   
        
        result += line;
		return result;
    }

    /*
     * Getters:
     */
    
    /**
     * @return The array of semiring values.
     */
    
	public List<E> getValues() {
		return values;
	}
	
    /**
     * @return The semiring that belongs to this instance. 
     */
    
	public Semiring<E> getSemiring() {
		return semiring;
	}	
	
    /**
     * @return The variable array of this semiring valuation.
     */

	public FiniteVariable[] getVariables() {
		return variables;
	}
		
	/**
	 * Implementation of weight predictor class.
	 * 
	 * @author Marc Pouly
	 * @version 1.4
	 */
	
	public class SRPredictor implements Predictor {
	    	    	    
	    /**
	     * @see nenok.va.Predictor#predict(nenok.va.Domain)
	     */
	    
	    public int predict(Domain dom) {
	        if(dom.size() == 0) {
	            return 0;
	        }
	        int result = 1;
	        for(Variable var : dom) {
	            FiniteVariable v = (FiniteVariable)var;
	            result *= v.getFrame().length;
	        }
	        return result;
	    }
	}
}