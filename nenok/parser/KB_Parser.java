package nenok.parser;

import java.io.File;

/**
 * Parser subinterface that parser knowledgebases.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 555 $<br/>$LastChangedDate: 2008-03-26 14:20:40 +0100 (Mi, 26 Mrz 2008) $
 */

public interface KB_Parser extends Parser {
		
    /**
     * Parses the content of the given file and builds a result set, i.e. a wrapper class
     * that bundles valuations and queries (domains) converted from the file content.
     * @param file The file whose content is parsed.
     * @return A bundle that encapsulates all data constructed from the file's content.
     * @throws ParserException Exceptions that are thrown during the parsing process.
     */
    
    public ResultSet parse(File file) throws ParserException;

}
