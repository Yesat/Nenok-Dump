package nenok.parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaderSAX2Factory;

import nenok.Knowledgebase;
import nenok.lc.JoinTree;
import nenok.lc.Node;
import nenok.va.Domain;
import nenok.va.Valuation;
import nenok.va.Variable;

/**
 * This class parses XML input files. It processes their static skeleton and delegates the generic 
 * parts to the user's subclass implementation. This class uses the JDOM framework and demands the XML 
 * file to reference a XML schema instance.
 * 
 * @author Marc Pouly
 */

public abstract class XmlParser implements KB_Parser, JT_Parser {
    
    private Valuation[] vals;
    private Domain[] queries;
    private Hashtable<String, Variable> variables;
    
    /**
     * Constructor:
     */
    
    public XmlParser() {
        variables = new Hashtable<String, Variable>();
    }

    /**
     * @see nenok.parser.Parser#accept(java.io.File)
     */
    
    public boolean accept(File file) {
    	return file.getName().endsWith(".xml");
    }
    
    /**
     * @see nenok.parser.KB_Parser#parse(java.io.File)
     */
    
    public ResultSet parse(File file) throws ParserException {
        try {
            
        	// Deprecated: SAXBuilder builder = new SAXBuilder("org.apache.xerces.parsers.SAXParser", true);
            SAXBuilder builder = new SAXBuilder(new XMLReaderSAX2Factory(true, "org.apache.xerces.parsers.SAXParser")); 
            builder.setFeature("http://apache.org/xml/features/validation/schema", true);
            Document doc = builder.build(file);
            if(doc.getRootElement().getName() != "knowledgebase") {
                throw new ParserException("Semiring <knowledgebase> expected as root element.");
            }
            
            Element root = doc.getRootElement();
            this.vals = new Valuation[root.getChild("valuations").getChildren().size()];
            if(root.getChild("queries") != null) {
            	this.queries = new Domain[root.getChild("queries").getChildren().size()];
            } else {
            	this.queries = new Domain[0];
            }
            
            /*�
             * Parse Variables:
             */
            
            Iterator<Element> varIter = root.getChild("variables").getChildren().iterator();
            while (varIter.hasNext()) {
                Element var = varIter.next();
                String name = var.getAttributeValue("varname");
                ArrayList<Element> content = new ArrayList<Element>();
                Iterator<Element> iter = var.getChildren().iterator();
                while (iter.hasNext()) {
                    content.add(iter.next());
                }
                variables.put(name, parseVariable(name, content));
            }
            
            /*
             * Parse Valuations:
             */
            
            Iterator<Element> valIter = root.getChild("valuations").getChildren().iterator();
            for(int i = 0; valIter.hasNext(); i++) {
                Element val = valIter.next();
                ArrayList<Element> content = new ArrayList<Element>();
                Iterator<Element> iter = val.getChildren().iterator();
                while (iter.hasNext()) {
                    content.add(iter.next());
                }
                vals[i] = parseValuation(val.getChildren(), variables);
            }
            
            /*
             * Parse Queries:
             */
            
            if(root.getChild("queries") != null) {
                Iterator<Element> queryIter = root.getChild("queries").getChildren().iterator();
                for(int i = 0; queryIter.hasNext(); i++) {
                    queries[i] = parseDomain(queryIter.next(), variables);
                }
            } 

            return new ResultSet(vals, queries);
        } catch (JDOMException e) {
            throw new ParserException("Problem in processing of XML data.", e);
        } catch (IOException e) {
            throw new ParserException("Input / Output problem.", e);
        }
    }
          
    /**
     * @see nenok.parser.JT_Parser#parse(java.io.File, nenok.lc.JoinTree.Construction)
     */
    
    public Node parse(File file, JoinTree.Construction data) throws ParserException {
        try {
            //Deprecated: SAXBuilder builder = new SAXBuilder("org.apache.xerces.parsers.SAXParser", true);
            SAXBuilder builder = new SAXBuilder(new XMLReaderSAX2Factory(true, "org.apache.xerces.parsers.SAXParser")); 
            builder.setFeature("http://apache.org/xml/features/validation/schema", true);
            Document doc = builder.build(file);
            if(doc.getRootElement().getName() != "jointree") {
                throw new ParserException("Semiring <jointree> expected as root element.");
            }
            
            Element root = doc.getRootElement();
            
            /*
             * Parse Variables:
             */
            
            Iterator<Element> varIter = root.getChild("variables").getChildren().iterator();
            while (varIter.hasNext()) {
                Element var = varIter.next();
                String name = var.getAttributeValue("varname");
                ArrayList<Element> content = new ArrayList<Element>();
                Iterator<Element> iter = var.getChildren().iterator();
                while (iter.hasNext()) {
                    content.add(iter.next());
                }
                Variable v = parseVariable(name, content);
                variables.put(name, v);
            }
           
            /*
             * Parse Nodes:
             */
            
            ArrayList<Valuation> vals = new ArrayList<Valuation>();
            Hashtable<String, Node> nodes = new Hashtable<String, Node>();
            
            Iterator<Element> nodeIter = root.getChild("nodes").getChildren().iterator();
            while(nodeIter.hasNext()) {
                Element node = nodeIter.next();
                Domain dom = parseDomain(node.getChild("domain"), variables);
                if(node.getChild("valuation") != null) {
                    Valuation val = parseValuation(node.getChild("valuation").getChildren(), variables);
                    Node newnode = data.getNodeInstance(dom, val);
                    nodes.put(node.getAttributeValue("name"), newnode);
                    vals.add(val);
                }
            }
            
            data.knowledgebase = new Knowledgebase(vals.toArray(new Valuation[vals.size()]), file.getName());
            
            /*
             * Build Jointree:
             */
            
            Node result = null;
            
            nodeIter = root.getChild("nodes").getChildren().iterator();
            while(nodeIter.hasNext()) {
                Element node = nodeIter.next();
                Node current = nodes.get(node.getAttributeValue("name"));
                if(node.getAttributeValue("child") != null) {
                    Node child = nodes.get(node.getAttributeValue("child"));
                    current.setChild(child);
                    child.addParent(current);
                } else if(result == null) {
                    result = current;
                } else {
                    throw new ParserException("More than one root node specified in this file.");
                }
            }
            
            return result;
        } catch (JDOMException e) {
            throw new ParserException("Problem in processing of XML data.", e);
        } catch (IOException e) {
            throw new ParserException("Input / Output problem.", e);
        }
    }
        
    /*
     * Delegators:
     */
        
    /**
     * This method converts the informations representing a domain read from an XML file 
     * to a domain object. The &quot;domain&quot; tag's content is given as a
     * <code>org.jdom.Element</code> object.
     * @param domain The content of the &quot;domain&quot; tag read from the XML file.
     * @param variables A hashtable that maps variable names on variable objects.
     * @return A new domain instance created from the tag's content.
     * @throws ParserException Exceptions caused by the parsing process.
     */
    
    public Domain parseDomain(Element domain, Hashtable<String, Variable> variables) throws ParserException {
    	String dom = domain.getText().trim();
    	if(dom.length() == 0) {
    		return new Domain();
    	}
        String[] str = domain.getText().split(" ");
        Variable[] result = new Variable[str.length];
        for(int i = 0; i < result.length; i++) {
        	Variable var = variables.get(str[i]);
        	if(var == null) {
        		throw new ParserException("Valuation domain contains unspecified variable. Variable is: "+str[i]+".");
        	}
            result[i] = var;
        }
        return new Domain(result);
    }
    
    /**
     * This method converts the informations representing a variable read from an XML file 
     * to a variable object. The &quot;variable&quot; tag's content is given as a list of 
     * <code>org.jdom.Element</code> objects.
     * @param name The variable's name (this is part of the file's skeleton).
     * @param content All sub-elements of the &quot;variable&quot; element read from the XML file.
     * @return A new variable instance created from the content list.
     * @throws ParserException Exceptions caused by the parsing process.
     */
    
    public abstract Variable parseVariable(String name, List<Element> content) throws ParserException;
    
    /**
     * This method converts the informations representing a valuation read from an XML file to a 
     * valuation object. The &quot;valuation&quot; tag's content is given as a list of 
     * <code>org.jdom.Element</code> objects.
     * @param content All sub-elements of the &quot;valuation&quot; element read from the XML file.
     * @param variables A hashtable that maps variable names on variable objects.
     * @return A new valuation instance created from the content list.
     * @throws ParserException Exceptions caused by the parsing process. 
     */
    
    public abstract Valuation parseValuation(List<Element> content, Hashtable<String, Variable> variables) throws ParserException;
}
