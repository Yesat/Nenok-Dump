package nenok.parser;

import java.io.File;

/**
 * The is the head interface of the NENOK parser framework.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 555 $<br/>$LastChangedDate: 2008-03-26 14:20:40 +0100 (Mi, 26 Mrz 2008) $
 */

public interface Parser {
	
	/**
	 * Verifies that this file type can be used together with this parser instance.
	 * @param file The file to verify.
	 * @return <code>true</code> if the file extension satisfies the precondition of this parser.
	 */
	
	public boolean accept(File file);
}
