package nenok.parser;

import java.io.File;

import nenok.lc.JoinTree;
import nenok.lc.Node;

/**
 * Parser subinterface that rebuilds jointrees.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 555 $<br/>$LastChangedDate: 2008-03-26 14:20:40 +0100 (Mi, 26 Mrz 2008) $
 */

public interface JT_Parser extends Parser {
		
    /**
     * Rebuilds a jointree which is serialized in the given file.
     * @param file The file containing a serialized jointree.
     * @param data Object that contains all necessary information to build join trees.
     * @return The root node of the jointree reconstructed from the file content.
     * @throws ParserException Exceptions that are thrown during the parsing process.
     */
    
    public Node parse(File file, JoinTree.Construction data) throws ParserException;

}
