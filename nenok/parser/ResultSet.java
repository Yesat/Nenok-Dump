package nenok.parser;

import nenok.va.Domain;
import nenok.va.Valuation;

/**
 * This class is a content holder to store the components that were constructed by the parsing 
 * method. Essentially, it enables the parsing method to return valuations as well as queries.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 555 $<br/>$LastChangedDate: 2008-03-26 14:20:40 +0100 (Mi, 26 Mrz 2008) $
 */

public class ResultSet {
    
    /**
     * An array of valuations.
     */
    
    public Valuation[] vals;
    
    /**
     * An array of queries (i.e. domains).
     */
    
    public Domain[] queries;
    
    /**
     * Constructor:
     * @param vals An array of valuations.
     * @param queries An array of queries (i.e. domains).
     */
    
    public ResultSet(Valuation[] vals, Domain[] queries) {
        this.vals = vals;
        this.queries = queries;
    }
}