package nenok.parser;

import nenok.jtconstr.ConstrException;

/**
 * Exceptions that are thrown during the parsing process.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 555 $<br/>$LastChangedDate: 2008-03-26 14:20:40 +0100 (Mi, 26 Mrz 2008) $
 */

public class ParserException extends ConstrException {
    
    /**
     * Constructor:
     * @param message The exception message.
     */
    
    public ParserException(String message) {
        super(message);
    }
    
    /**
     * Constructor:
     * @param message The exception message.
     * @param cause The (prior) exception to be wrapped.
     */
    
    public ParserException(String message, Throwable cause) {
        super(message, cause);
    }
}
