package nenok;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import nenok.va.Representor;
import nenok.va.Valuation;

/**
 * Utility class that provides a collection of static methods for different purposes.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 550 $<br/>$LastChangedDate: 2008-03-26 14:14:27 +0100 (Mi, 26 Mrz 2008) $
 */

public class Utilities {
	
    /**
     * Helper method to determine if a given class implements a given interface.
     * @param _interface Is this interface implemented by the given class.
     * @param _class Does this class implement the given interface.
     * @return <code>true</code>, if the given class implements the given interface.
     */
    
    public static boolean interfaceOf(Class<?> _interface, Class<?> _class) {
        Class<?> c = _class;
        while(c != null) {
            if(findInterface(_interface, c.getInterfaces())) {
                return true;
            }
            c = c.getSuperclass();
        }
        return false;
    }
    
    /**
     * Helper method to determine if a given class is superclass of another.
     * Convention: Every class is superclass of itself.
     * @param _super Is this class ascendent of the second argument.
     * @param _class Is this class descendent of the former argument.
     * @return <code>true</code>, if the first argument is a superclass of the second.
     */
    
    public static boolean superclassOf(Class<?> _super, Class<?> _class) {
        Class<?> superclass = _class;
    	while (superclass != null) {
            if(superclass.getName().equals(_super.getName())) {
            	return true;
            }
            superclass = superclass.getSuperclass();
         }
    	return false;
    }
    
    /**
     * This static method returns a list of all representators defined in the argument's class. 
     * Representator methods are annotated with {@link Representor}.
     * @param val The valuation class instance to be searched.
     * @return All representators contained in the argument's class.
     */
    
    public static List<Method> getRepresentators(Valuation val) {
        Class<? extends Valuation> c = val.getClass();
        ArrayList<Method> result = new ArrayList<Method>();
        for(Method m : c.getMethods()) {
        	if(m.getParameterTypes().length != 0) {
        		continue;
        	}
        	if(m.getAnnotation(Representor.class) != null) {
        		result.add(m);
        	}
        }
        return result;
    }
        
    /**
     * Is an interface contained in an array of interfaces or in their super interfaces.
     * @param theInterface The interface to find.
     * @param interfaces An array of interfaces.
     * @return <code>true</code>, if the interface has been found.
     */
    
    private static boolean findInterface(Class<?> theInterface, Class<?>[] interfaces) {
        for(Class<?> i : interfaces) {
            if(i.equals(theInterface)) {
                return true;
            }
            if(findInterface(theInterface, i.getInterfaces())) {
                return true;
            }
        }
        return false;
    }

	/**
	 * Searches an array for the given values and returns its index.
	 * @param values The array of values to be searched.
	 * @param val A value whose index has to be found.
	 * @return The index of the values in the array or -1 if the values cannot be found.
	 */
	
	public static int indexOf(Object[] values, Object val) {
		for (int i = 0; i < values.length; i++) {
			if(values[i].equals(val)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Converts an array of integers into a list of {@link Integer} objects.
	 * @param values The input array of integers.
	 * @return A list of {@link Integer} objects respecting the array order.
	 */
	
	public static List<Integer> toList(int[] values) {
		ArrayList<Integer> elements = new ArrayList<Integer>(values.length);
		for (int i = 0; i < values.length; i++) {
			elements.add(i, Integer.valueOf(values[i]));
		}
		return elements;
	}
}
