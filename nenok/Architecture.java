package nenok;

import java.util.Collection;

import nenok.jtconstr.Algorithm;
import nenok.jtconstr.ConstrException;
import nenok.lc.JoinTree;
import nenok.lc.dp.DPJoinTree;
import nenok.lc.hugin.HJoinTree;
import nenok.lc.id.IDJoinTree;
import nenok.lc.id.PathJoinTree;
import nenok.lc.ls.LSJoinTree;
import nenok.lc.ss.BSSJoinTree;
import nenok.lc.ss.SSJoinTree;
import nenok.va.Domain;

/**
 * This class serves as a simplified user interface to access local computation architectures.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 549 $<br/>$LastChangedDate: 2008-03-26 14:11:16 +0100 (Mi, 26 Mrz 2008) $
 */

public enum Architecture {
    
    /**
     * The n-ary Shenoy-Shafer architecture.
     */
    
    Shenoy_Shafer,
    
    /**
     * The binary Shenoy-Shafer architecture.
     */
    
    Binary_Shenoy_Shafer,
    
    /**
     * The Lauritzen-Spiegelhalter architecture.
     */
    
    Lauritzen_Spiegelhalter,
    
    /**
     * The Hugin architecture.
     */
    
    Hugin,
    
    /**
     * The idempotent architecture.
     */
    
    Idempotent,
        
    /**
     * The specialized architecture for path problems.
     */
    
    Paths,
    
    /**
     * The specialized architecture for constraints problems.
     */
    
    Constraints;
    
    /**
     * This method maps enum types on new jointree instances.
     * @param kb The knowledgebase for the new jointree.
     * @param queries The query list for the new jointree.
     * @param algo The construction algorithm to build the jointree.
     * @return A new jointree instance constructed from the above data by use of <code>algo</code>.
     * @throws ConstrException Exceptions thrown by the jointree construction process.
     */
    
    public JoinTree getInstance(Knowledgebase kb, Collection<Domain> queries, Algorithm algo) throws ConstrException {
        switch(this) {
            case Binary_Shenoy_Shafer: return new BSSJoinTree(kb, queries, algo); 
            case Shenoy_Shafer: return new SSJoinTree(kb, queries, algo); 
            case Lauritzen_Spiegelhalter: return new LSJoinTree(kb, queries, algo); 
            case Hugin: return new HJoinTree(kb, queries, algo); 
            case Idempotent: return new IDJoinTree(kb, queries, algo); 
            case Paths: return new PathJoinTree(kb, queries, algo); 
            case Constraints: return new DPJoinTree(kb, queries, algo); 
            default: return null; // will never happen ...
        }
    }
    
    /*
     * @see java.lang.Enum#toString()
     */
    
    public String toString() {
        switch(this) {
            case Binary_Shenoy_Shafer: return "Binary Shenoy-Shafer"; 
            case Shenoy_Shafer: return "Shenoy-Shafer"; 
            case Lauritzen_Spiegelhalter: return "Lauritzen-Spiegelhalter"; 
            case Hugin: return "Hugin"; 
            case Idempotent: return "Idempotent Architecture"; 
            case Paths: return "Architecture for Path Problems"; 
            case Constraints: return "Architecture for Constraints"; 
            default: return "Unknown Architecture"; // will never happen ...
        }
    } 
}
