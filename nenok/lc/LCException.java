package nenok.lc;

import java.rmi.RemoteException;

/**
 * This class represents exceptions occuring during local computation. In Nenok, this exception class acts as 
 * a base class abstracting other more detailled exceptions. LCException stands for Local Computation Exception. 
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 557 $<br/>$LastChangedDate: 2008-03-26 14:23:32 +0100 (Mi, 26 Mrz 2008) $
 */

public class LCException extends RemoteException {
   
    /**
     * Constructor:
     * @param message The exception message.
     */
    
    public LCException(String message) {
        super(message);
    }
    
    /**
     * Constructor: 
     * @param message The exception message.
     * @param cause The (prior) exception to be wrapped.
     */
    
    public LCException(String message, Throwable cause) {
        super(message, cause);
    }
    
    /**
     * Constructor: 
     * @param cause The (prior) exception to be wrapped.
     */
    
    public LCException(Throwable cause) {
        super("", cause);
    }
}
