package nenok.lc.out;

import java.util.ArrayList;
import java.util.List;

import nenok.lc.Node;

/**
 * Jointree representation in ASCII.
 * 
 * @author Heiner Koecker (adapted by Cesar Schneuwly &amp; Marc Pouly)
 * @version $LastChangedRevision: 557 $<br/>$LastChangedDate: 2008-03-26 14:23:32 +0100 (Mi, 26 Mrz 2008) $
 */

public class AsciiTree {
	
	/**
	 * Converts a jointree object to an ASCII string.
	 * @param root The root of the join tree to be drawn.
	 * @param content <code>true</code>, if the node content shall be drawn.
	 * @return The join tree formatted as ASCII string.
	 */
	
	public static String joinTreeToString(Node root, boolean content) {
		Structure structure = new Structure("");
		rootNodeToAsciiJoinTreeArt(root, structure, content);
		return buildTreeRecursiv(structure, "", 1);
	}

	/**
	 * Recursive helper procedure to draw the tree.
	 * @param pStructure Internal structure to store subtrees.
	 * @param pIndent The indent of the subtree.
	 * @param pSepLineCount The line separation counter.
	 * @return The jointree as ASCII string.
	 */
	
	private static final String buildTreeRecursiv(Structure pStructure, String pIndent, int pSepLineCount) {
		final StringBuffer strBuff = new StringBuffer();
		for (int j = 0; j < pSepLineCount; j++) {
			if (pStructure.list.size() != 0) {
				strBuff.append(pIndent + "|" + "\n");
			}
		}
		for (int i = 0; i < pStructure.list.size(); i++) {
			final boolean isLastRun = i >= pStructure.list.size() - 1;
			Structure elem = pStructure.list.get(i);
			strBuff.append(multiLine(pIndent, elem, isLastRun));
			strBuff.append(buildTreeRecursiv(elem, pIndent + (isLastRun ? "  " : "| "),pSepLineCount));
			if (i < pStructure.list.size() - 1 && pSepLineCount > 0) {
				for (int j = 0; j < pSepLineCount; j++) {
					strBuff.append(pIndent + "|" + "\n");
				}
			}
		}
		return strBuff.toString();
	}

	/**
	 * Special treatment of line breaks.
	 * @param pIndent The current indent.
	 * @param pElem The current structure to display.
	 * @return The ASCII string for the current tree component.
	 */
	
	private static final String multiLine(String pIndent, Structure pElem, boolean pIsLastRun) {
		
		if (pElem == null) {
			return "null\n";
		}
		
		StringBuffer strBuff = new StringBuffer();
		final String[] strArr = pElem.toString().split("\n");
		for (int i = 0; i < strArr.length; i++) {
			String str = strArr[i];
			if (i == 0) {
				strBuff.append(pIndent + "+-" + str + '\n');
			} else if (pIsLastRun) {
				strBuff.append(pIndent + "  " + str + '\n');
			} else {
				strBuff.append(pIndent + "| " + str + '\n');
			}
		}
		return strBuff.toString();
	}

	/**
	 * Content holder class. Collects ASCII subtrees temporarily.
	 * 
	 * @author Heiner Koecker (adapted by Cesar Schneuwly &amp; Marc Pouly)
	 * @version 1.0
	 */
	
	private static class Structure {
		
		private String name;
		private List<Structure> list = new ArrayList<Structure>();

		/**
		 * Constructor:
		 * @param name
		 */
		
		private Structure(String name) {
			this.name = name;
		}
	
		/**
		 * @see java.lang.Object#toString()
		 */
		
		public String toString() {
			return name;
		}
	}
	
	/**
	 * Creates single ASCII tree parts.
	 * @param node The current node to be transformed in a string.
	 * @param structure The content holder class.
	 * @param content <code>true</code>, if the node content shall be drawn.
	 */

	private static void rootNodeToAsciiJoinTreeArt(Node node, Structure structure, boolean content) {
		String str = "Label: "+node.getLabel().toString();
		
		if(content) {
			str += ", Content: "+node.getContent().toString();
		}
		
		Structure newStructure = new Structure(str);
		structure.list.add(newStructure);
		if (node.getParents().size() != 0) {
			for (Node n : node.getParents()) {
				rootNodeToAsciiJoinTreeArt(n, newStructure, content);
			}
		}
	}
}
