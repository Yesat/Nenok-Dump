package nenok.lc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import nenok.Adapter;
import nenok.Knowledgebase;
import nenok.Utilities;
import nenok.jtconstr.Algorithm;
import nenok.jtconstr.ConstrException;
import nenok.jtconstr.OSLA_SC;
import nenok.lc.out.AsciiTree;
import nenok.va.Domain;
import nenok.va.Scalability;
import nenok.va.Valuation;

/**
 * This is the head class for the various join tree implementations. 
 * Each join tree class is equivalent to an architecture of local computation.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 557 $<br/>$LastChangedDate: 2008-03-26 14:23:32 +0100 (Mi, 26 Mrz 2008) $
 */

public abstract class JoinTree implements Iterable<Node> {
    
    // Tree components:
    protected Node root;
    
    // Runtime statistics:
    private double collectTime = -1, distributeTime = -1;
    
    // Predicates:
    private boolean isCollected, isDistributed;
   
    // Construction algorithm:
    private Algorithm algorithm;    
    
    // Queries:
    private HashSet<Domain> queries;
   
    // Construction data:
    private Construction cdata;
        
    // Task adapter:
    protected Adapter adapter;
        
    // Scaling mode:
    private boolean scaling = true;
              
    /**
     * Constructor:
     * @param kb The knowledgebase to build a jointree.
     * @param queries The set of queries that must be covered by this join tree.
     * @param algo The construction algorithm to build the tree.
     */
    
    protected JoinTree(Knowledgebase kb, Collection<Domain> queries, Algorithm algo) throws ConstrException {
              
    	// Check arguments:
    	if(queries == null) {
    		throw new ConstrException("Invalid argument: Either knowledgebase or query argument is null.");
    	}
    	    	            	    
        /*
         * Shrink query list, i.e. eliminate redundant &amp; covered queries:
         */
        
        ArrayList<Domain> minQueries = new ArrayList<Domain>(queries);
                
        // Remove redundant queries:
        for(int i = 0; i < minQueries.size(); i++) {
            for (int j = i+1; j < minQueries.size(); j++) {
                if(minQueries.get(i).subSetOf(minQueries.get(j))) {
                    minQueries.remove(i--);
                    break;
                }
                if(minQueries.get(j).subSetOf(minQueries.get(i))) { 
                    minQueries.remove(j--);
                }
            }            
        }
        
        this.queries = new HashSet<Domain>(minQueries);
                              
        /*
         * Create adapter:
         */
        
        adapter = new Adapter();
            
        /*
         * Construction algorithm:
         */
        
        this.algorithm = (algo == null)? new OSLA_SC() : algo;      
        this.cdata = new Construction(kb, minQueries);
        
        /*
         * Test instance acceptability:
         */

        boolean test;
		try {
			test = verify(cdata.knowledgebase.getType());
		} catch (Exception e) {
    		throw new ConstrException("Cannot access the knowledgebase type.", e);
		}
        
    	if(!test) {
    		throw new ConstrException("Cannot use this architecture together with " +
    		"the given knowledgebase.\nMathematical restrictions are violated.");
    	}
        
        /*
         * Launch construction:
         */
        
        this.root = algorithm.run(cdata);       
    }
    
    /*
     * Abstract methods:
     */
    
    /**
     * Performs the scaling operations for this architecture.
     * This method is executed, if the valuations are of type {@link Scalability}.
     * @throws LCException Exception thrown while executing the scaling operation.
     */
    
    public abstract void scale() throws LCException;
        
    /**
     * @return The join tree's architecture type.
     */
    
    public abstract String getArchitecture();
        
    /**
     * Verifies that the given knowledgebase can indeed be used with this architecture.
     * @param type The type of the locators that are contained in the knowledgebase.
     * @return <code>true</code>, if the knowledgebase satifies the mathematical restrictions for this architecture.
     */
    
    public abstract boolean verify(Class<?> type);
        
    /*
     * BUILDER design pattern:
     */
        
	/**
	 * Returns a new node instance corresponding to the architecture type of this jointree.
	 * @param label The label of the new node.
	 * @param factor The content of the new node whose domain is a subset of the node label or <code>null</code>. 
	 * @return The according node instance for this architecture.
	 */
	
	public abstract Node createNode(Domain label, Valuation factor);
	    
	/**
	 * @return The set of queries that were guarantueed to be covered.
	 */
	
	public Collection<Domain> getQueries() {
		return queries;
	}
	
    /*
     * Propagation methods:
     */
    
	/**
	 * Executes collect &amp; distribute propagation.
     * @param args <code>args[0] = false</code> averts scaling if the 
     * valuation algebra instance is of type {@link Scalability}.
	 * @throws LCException Exception thrown during join tree propagation.
     */
	
	public void propagate(boolean... args) throws LCException {
        collect();
		distribute(args);
	}
	
	/**
	 * Executes the collect propagation.
     * @throws LCException Exception thrown during join tree propagation.
     */
	
	public void collect() throws LCException {
        
        if(isCollected) {
            throw new LCException("Collect has already been executed.");
        }
                   
        /*
         * Start collect:
         */
                
        Set<Node> leaves = root.getLeaves();
        double start = System.currentTimeMillis();
		for (Node leaf : leaves) {
            leaf.collect();
        }
                  
		collectTime = System.currentTimeMillis() - start;
        isCollected = true;
	}
	
	/**
	 * Executes the distribute propagation.
     * @param args <code>args[0] = false</code> averts scaling if the 
     * valuation algebra instance is of type {@link Scalability}.
	 * @throws LCException Exception thrown when trying to execute distribute before collect has been terminated.
     */
	
	public void distribute(boolean... args) throws LCException {
	    if(!isCollected) {
	        throw new LCException("Collect algorithm must be executed before distribute.");
        }
        
        /*
         * Perform scaling if needed and possible:
         */
   
	    if(scaling) {
		    Class<?> type;
			try {
				type = cdata.knowledgebase.getType();
			} catch (Exception e) {
				throw new LCException("Cannot access knowledgebase type.");
			}
		    
	        if(args.length == 0 || args[0]) {
	            if(Utilities.interfaceOf(Scalability.class, type)) {
	                scale();
	            }
	        }
	    }
        
        /*
         * Start distribute:
         */
        
		double start = System.currentTimeMillis();
		root.distribute();
		distributeTime = System.currentTimeMillis() - start;
        isDistributed = true;
	}
	
	/**
	 * Method to enable or disable scaling during local computation.
	 * @param activate <code>true</code> to enable scaling, <code>false</code> to disable scaling.
	 */
	
	public void scaling(boolean activate) {
		scaling = activate;
	}
	    
	/*
	 * Query answering:
	 */
	
	/**
	 * Basic query answering procedure: 
	 * This method searches non-deterministically for a jointree node that covers the query and returns the content of this node projected to the query.
	 * @param query The query domain.
	 * @return The objective function projected to the query.
	 * @throws LCException Exception thrown when propagation has not yet been executed.    
     */
	
	public Valuation answer(Domain query) throws LCException {
		
		if(query == null) {
			throw new LCException("Domain expected as argument: given null.");
		}
		
		/*
		 * Only root queries can be answered without distribute:
		 */
		
		if(!isDistributed) {
			
			if(!isCollected) {
				throw new LCException("No query can be answered before collect is executed.");
			}			
			
			if(query.subSetOf(root.getLabel())) {
				throw new LCException("Distribute must be executed to answer this query.");
			}
			
			/*
			 * Answer query from the root content: 
			 */
			
			Valuation val = root.getContent();
			Domain dom = Domain.intersection(query, val.label());
			return val.marginalize(dom);
		}
		
		/*
		 * Find covering node and answer query from its content:
		 */
		
        Node node = root.findCoveringNode(query);
        
        if(node == null) {
        	throw new LCException("No covering node found for the query: "+query+".");
        }
        
        Valuation val = node.getContent();
        Domain dom = Domain.intersection(query, val.label());
        return val.marginalize(dom); 
     }
		
    /*
     * Propagation Statistics:
     */
    
    /**
     * @return The time elapsed while executing collect. <code>-1</code>, if collect has not yet been executed.
     */
    
    public double getCollectTime() {
        return collectTime;
    }
    
    /**
     * @return The time elapsed while executing distribute. <code>-1</code>, if distribute has not yet been executed.
     */
    
    public double getDistributeTime() {
        return distributeTime;
    }
    
    /**
     * @return The time elapsed while executing collect and distribute. <code>-1</code>, if the propagation has not yet been executed.
     */
    
    public double getPropagationTime() {
        return collectTime+distributeTime;
    }
    
    /**
     * @return The adapter of this jointree.
     */
    
    public Adapter getAdapter() {
        return adapter;
    }
     
    /*
     * Join tree construction:
     */
    
    /**
     * @return The construction algorithm having built this join tree.
     */
    
	public Algorithm getConstructionAlgorithm() {
		return algorithm;
	}
	        
	/**
	 * @return The data that was used to construct this join tree.
	 * Implementation note: This method cannot be part of {@link Algorithm}, because
	 * those object must obviously be created beforehand.
	 */
	
	public Construction getConstructionData() {
		return cdata;
	}
	
    /*
     * Tree components:
     */
    
    /**
     * @return The root node of this join tree.
     */
    
    public Node getRoot() {
        return this.root;
    }
    
    /**
     * Changes the root node of this jointree. 
     * @param node The new root node.
     * @param redirect Flag that indicates NENOK if the jointree needs to be redirected.
     * <ul>
     * 	<li><code>true</code>, NENOK redirects the jointree edges to the new root node according to (Kohlas 2003).</li>
     *  <li><code>false</code>, the root node is changed but no redirection is performed.</li>
     * </ul>
     * @throws ConstrException Exception thrown when new root is inappropriate.
     * Attention: if the flag is set to <code>false</code>, the user needs to take the responsibility for the
     * jointree's correctness. This functionality is provided for updating reasons (Schneuwly &amp; Kohlas 2006).
     */
    
    public void setRoot(Node node, boolean redirect) throws ConstrException {
    	if(isCollected && !isDistributed) {
    		throw new ConstrException("Change of root node not possible on collected tree. Execute distribute first.");
    	}
    	if(!createNode(Domain.EMPTY, null).getClass().equals(node.getClass())) {
    		throw new ConstrException("New root node type does not correspond to the jointree architecture.");
    	}
    	
    	// Test on flag:
    	if(!redirect) {
    		this.root = node;
    		return;
    	}
    	
    	Node current = node;
    	Node child = node.getChild();
    	
    	while(current != root) {
    		if(child == null) {
    			throw new ConstrException("The new root node must already be contained in the jointree.");
    		}
    		
    		Node temp = child.getChild();
    		
    		child.setChild(current);
    		current.addParent(child);
    		child.removeParent(current);
    		
    		current = child;
    		child = temp;
    	}

    	root = node;
    	root.setChild(null);
    }
    
	/**
	 * Join trees can iterate over their nodes.
	 * @return The jointree's node iterator.
     * @see java.lang.Iterable#iterator()
	 */
    
    public Iterator<Node> iterator() {
    	Collection<Node> nodes = root.getNodes(new HashSet<Node>());
    	return nodes.iterator();
    }
    
	/*
	 * Informative methods:
	 */
    
    /**
     * @return The treewidth of this join tree, i.e. the size of the largest node label - 1. 
     */
    
    public int getTreeWidth() {
    	return getLargestDomain().size() - 1;
    }
		
	/**
	 * @return The largest domain in this join tree.
	 */
	
	public Domain getLargestDomain() {
        return root.findLargestDomain();
	}
	    
    /**
     * @return The union of all node domains within the tree.
     */
    
    public Domain getTreeDomain() {
        return root.getTotalDomain();
    }
    
    /**
     * @return Counts the number of nodes within the join tree.
     */
	
    public int countNodes() {
        return root.subTreeSize();
    }
    
    /*
     * Display methods.
     */
    
    /**
     * @see java.lang.Object#toString()
     */
    
    public String toString() {
    	return getArchitecture();
    }
    
    /**
     * Prints the jointree as ASCII string.
     * @param content <code>true</code> to print the content of each node. 
     * @return The current jointree as ASCII string.
     */
    
    public String toASCII(boolean content) {
    	return AsciiTree.joinTreeToString(root, content);
    }
    
    /*
     * Predicates:
     */
    
    /**
     * @return <code>true</code>, if the collect algorithm has been executed.
     */
    
    public boolean isCollected() {
        return this.isCollected;
    }
    
    /**
     * @return <code>true</code>, if the distribute algorithm has been executed.
     */
    
    public boolean isDistributed() {
        return this.isDistributed;
    }    
    
    /**
     * @return <code>true</code>, if this join tree is binary.
     */
    
    public boolean isBinary() {
        return root.isBinary();
    }
         
    /*
     * Tranformations:
     */
    
    /**
     * Transforms this join tree to a binary join tree.
     */
    
    public void makeBinary() {
        double start = System.currentTimeMillis();
        root.makeBinary(cdata);
        algorithm.addConstructionTime(System.currentTimeMillis() - start);
    }
    
    /*
     * Inner classes:
     */
    
    /**
     * Inner class wrapping all necessary data for the join tree construction process.
     * 
     * @author Marc Pouly
     */
    
    public class Construction {
        
        /**
         * Knowledgebase covered by this join tree.
         */
        
        public Knowledgebase knowledgebase;
        
        /**
         * List of queries to be covered.
         */ 
        
        public List<Domain> queries;
                
        /**
         * Constructor:
         * @param kb The knowledgebase from which this join tree is built.
         * @param queries List of queries to be covered.
         */
        
        public Construction(Knowledgebase kb, List<Domain> queries) {
            this.knowledgebase = (kb != null)? kb : new Knowledgebase(null, "Empty Knowledgebase."); 
            this.queries = queries;
        }
        
        /**
		 * Delegator method for the construction of new node instances.
         * @param label The label of the new node.
         * @param factor The content of the new node whose domain is a subset of the node label. 
         * @return Returns a {@link Node} instance for the corresponding architecture.
         */
        
        public Node getNodeInstance(Domain label, Valuation factor) {
            return JoinTree.this.createNode(label, factor);
        }

        /**
		 * Delegator method for the construction of new node instances without content.
         * @param label The label of the new node.
         * @return Returns a {@link Node} instance for the corresponding architecture.
         */
        
        public Node getNodeInstance(Domain label) {
            return JoinTree.this.createNode(label, null);
        }
    }
}
