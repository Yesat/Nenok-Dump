package nenok.lc.hugin;

import nenok.Adapter;
import nenok.lc.LCException;
import nenok.lc.Node;
import nenok.va.Domain;
import nenok.va.Separativity;
import nenok.va.Valuation;

/**
 * This is the node class implementation that belongs to the HUGIN architecture.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 557 $<br/>$LastChangedDate: 2008-03-26 14:23:32 +0100 (Mi, 26 Mrz 2008) $
 */

public class HNode extends Node {

    // Internal message cache:
	private int counter;
    private Valuation[] messages;
    
    // Separator:
    private Separativity separator;
    
    /**
     * Constructor:
     * @param label The label of this node.
     * @param adapter The adapter to execute valuation algebra operations.
     * @param factor The content of this node whose domain is a subset of the node label. 
	 * If <code>null</code> is given as node content, the node is initialized by the identity element.
     */
	
	public HNode(Adapter adapter, Domain label, Valuation factor) {
		super(adapter, label, factor);
	}
                
    /**
     * @see nenok.lc.Node#collect()
     */
    
    public void collect() throws LCException { 
        
        // Test, if all parents have sent their message:
        if(counter < this.getParents().size()) {
            return;
        }
                
        // Compute new node content -> messages haven't been initialized for leaves.  
        if(getParents().size() > 0) {
            content = adapter.combine(content, messages);
                    
            // Throw messages away:
            messages = null;
        }
                                   
        // Stop when reaching the root node:
        if(getChild() == null) {
            return;
        }
                     
        // Compute message for the child:
        Domain inter = Domain.intersection(content.label(), getChild().getLabel());  
        separator = (Separativity)adapter.marginalize(content, inter);
                        
        // Send message to child:
        HNode child = (HNode)this.getChild();
        child.receiveParentMessage(separator);
        child.collect();
    }

    /**
     * @see nenok.lc.Node#distribute()
     */
    
    public void distribute() throws LCException {
        for(Node parent : this.getParents()) {
            Valuation message = adapter.marginalize(content, Domain.intersection(this.getLabel(), parent.getLabel()));
        	((HNode)parent).receiveChildMessage(message);
            parent.distribute();
        }
    }
   
    /*
     * Helper methods:
     */
    
    /**
     * Replaces the node content by the scaling factor and clears the message cache.
     * @param factor The scaling factor to replace the node content.
     */
    
    protected void setScalingFactor(Valuation factor) {
        this.content = factor;
    }
    
    /**
     * Method called by parent node to send message to this node.
     * @param message The message to send.
     */
    
    private void receiveParentMessage(Valuation message) {
        if(messages == null) {
            messages = new Valuation[getParents().size()];
        }
        
    	messages[counter++] = message;
    }
    
    /**
     * Method called by child node to send message to this node.
     * @param message The message to send.
     * @throws LCException Generic local computation exception.
     */
    
    private void receiveChildMessage(Valuation message) throws LCException {
        // Inverse of separator:
        Valuation inverse = adapter.inverse(separator);
                    
        // Combine message with inverse of separator:
        Valuation idempotent = adapter.combine(inverse, message);
        
        // Combine idempotent to the node content:
        content = adapter.combine(idempotent, content);
     }
}
