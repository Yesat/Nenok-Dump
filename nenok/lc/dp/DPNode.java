package nenok.lc.dp;

import nenok.Adapter;
import nenok.lc.LCException;
import nenok.lc.Node;
import nenok.sva.Configuration;
import nenok.sva.OSRValuation;
import nenok.va.Domain;
import nenok.va.FiniteVariable;
import nenok.va.Valuation;

/**
 * This is the node class implementation that belongs to the dynamic programming architecture.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 557 $<br/>$LastChangedDate: 2008-03-26 14:23:32 +0100 (Mi, 26 Mrz 2008) $
 */

public class DPNode extends Node {
	
	private int counter;
    private Valuation[] messages;
	private OSRValuation<?> childMessage;
	private String[] partSolConf;
	
    /**
     * Constructor:
     * @param label The label of this node.
     * @param adapter The adapter to execute valuation algebra operations.
     * @param factor The content of this node whose domain is a subset of the node label. 
	 * If <code>null</code> is given as node content, the node is initialized by the identity element.
     */
	
	public DPNode(Adapter adapter, Domain label, Valuation factor) {
		super(adapter, label, factor);
	}
	
	/**
	 * @see nenok.lc.Node#collect()
	 */
	
	@Override
	public void collect() throws LCException {
        
		// Test, if all parents have sent their message:
        if(counter < this.getParents().size()) {
            return;
        }
                
        // Compute new node content -> messages haven't been initialized for leaves.  
        if(getParents().size() > 0) {
            content = adapter.combine(content, messages);
                    
            // Throw messages away:
            messages = null;
        }
         
        // Stop when reaching the root node:
        if(getChild() == null) {
            return;
        }
               
        // Compute message for the child:
        Domain dom = Domain.intersection(content.label(), getChild().getLabel());        
        childMessage = (OSRValuation<?>)adapter.marginalize(content, dom);
                        
        // Send message to child:
        DPNode child = (DPNode)this.getChild();
        child.receiveParentMessage(childMessage);
        child.collect();
	}
	
	/**
	 * @see nenok.lc.Node#distribute()
	 */
	
	@Override
	public void distribute() throws LCException {
		
		/*
		 * For the root node:
		 */
		
		if(getChild() == null) {
			partSolConf = ((OSRValuation<?>)content).getSolutionConfiguration(); 
			for(Node parent : getParents()) {
				parent.distribute();
			}
			return;
		}
		
		/*
		 * For internal nodes:
		 */
		
		DPNode child = (DPNode)getChild();
		OSRValuation<?> childContent = (OSRValuation<?>)child.content;
		
		FiniteVariable[] inter = Domain.intersection(content.label(), child.getLabel()).toArray(FiniteVariable.class);
				
		// Partial configuration from child node projected and relative to inter:
		String[] cch = Configuration.project(child.partSolConf, childContent.getVariables(), inter);
		
		// Extension relative to child message;
		partSolConf = childMessage.getExtension(cch, inter, ((OSRValuation<?>)content).getVariables());
	
		for (Node parent : getParents()) {
			parent.distribute();
		}
	}
	
	/**
	 * Returns the part of the solution configuration that corresponds to the given variable.
	 * Note that the variable must be contained in the label of this node.
	 * @param var The variable onto which the solution configuration is projected.
	 * @return The value of the solution configuration at this variable.
	 * @throws LCException Exception thrown if the given variable is not part of this node label.
	 */
	
	protected String getSolutionConfigurationEntry(FiniteVariable var) throws LCException {
		if(!getLabel().contains(var)) {
			throw new LCException("Variable "+var+" is not covered by this join tree.");
		}
		
		if(partSolConf == null) {
			throw new LCException("Join tree propagation not yet executed.");
		}
		
		FiniteVariable[] vars = ((OSRValuation<?>)content).getVariables();
		return Configuration.project(partSolConf, vars, new FiniteVariable[] {var})[0];
	}
	
    /*
     * Helper methods:
     */
    
    /**
     * Method called by parent node to send message to this node.
     * @param message The message to send.
     */
    
    private void receiveParentMessage(Valuation message) {
        if(messages == null) {
            messages = new Valuation[getParents().size()];
        }
    	messages[counter++] = message;
    }
}
