package nenok.lc.dp;

import java.util.Collection;

import nenok.Knowledgebase;
import nenok.Utilities;
import nenok.jtconstr.Algorithm;
import nenok.jtconstr.ConstrException;
import nenok.lc.JoinTree;
import nenok.lc.LCException;
import nenok.lc.Node;
import nenok.sva.OSRValuation;
import nenok.va.Domain;
import nenok.va.FiniteVariable;
import nenok.va.VAException;
import nenok.va.Valuation;

/**
 * The join tree corresponding to the dynamic programming architecture.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 557 $<br/>$LastChangedDate: 2008-03-26 14:23:32 +0100 (Mi, 26 Mrz 2008) $
 */

public class DPJoinTree extends JoinTree {
	
	/**
	 * Constructor:
     * @param kb The knowledgebase from which this jointree is constructed.
     * @param queries The set of queries that are covered by this join tree.
     * @param algo The construction algorithm.
     * @throws ConstrException Exceptions throws during construction process.
     */
	
	public DPJoinTree(Knowledgebase kb, Collection<Domain> queries, Algorithm algo) throws ConstrException {
		super(kb, queries, algo);
	}

	/**
	 * @see nenok.lc.JoinTree#createNode(nenok.va.Domain, nenok.adapt.Labeled)
	 */
	
	@Override
	public Node createNode(Domain label, Valuation factor) {
		return new DPNode(adapter, label, factor);
	}

	/**
	 * @see nenok.lc.JoinTree#getArchitecture()
	 */
	
	@Override
	public String getArchitecture() {
		return "Dynamic Programming";
	}
	
	/**
	 * Rebuilds the solution configuration from the partial configurations stored in the 
	 * nodes of the join tree. Note that this method presupposes that a full propagation
	 * has been executed.
	 * @param vars The variable array that defines the output configuration's order.
	 * @return A solution configuration built by the dynamic programming architecture.
	 * @throws LCException Exception thrown if propagation has not been executed.
	 */

	public String[] getSolutionConfiguration(FiniteVariable[] vars) throws LCException {
		
		if(!(isCollected() && isDistributed())) {
			throw new LCException("Collect and distribute algorithm must first be executed.");
		}
		
		String[] result = new String[vars.length];
		
		for (int i = 0; i < vars.length; i++) {
			
			DPNode node = (DPNode)root.findCoveringNode(new Domain(vars[i]));
			if(node == null) {
				throw new LCException("Variable "+vars[i]+" is not covered by this join tree.");
			}
			
			result[i] = node.getSolutionConfigurationEntry(vars[i]);
			
		}
		
		return result;
	}
	
	/**
	 * @see nenok.lc.JoinTree#scale()
	 * Scaling is impossible since otherwise, the identification
	 * of solution configurations will fail.
	 */
	
	@Override
	public void scale() throws VAException {
		// do nothing ...
	}

	/**
	 * @see nenok.lc.JoinTree#verify(java.lang.Class)
	 */
	
	@Override
	public boolean verify(Class<?> type) {
		return Utilities.superclassOf(OSRValuation.class, type);
	}
}
