package nenok.lc.id;

import java.util.ArrayList;
import java.util.Collection;

import nenok.Knowledgebase;
import nenok.Utilities;
import nenok.jtconstr.Algorithm;
import nenok.jtconstr.ConstrException;
import nenok.lc.LCException;
import nenok.path.KValuation;
import nenok.va.Domain;
import nenok.va.Variable;

/**
 * A join tree implementation for Kleene Valuation Algebras based on the idempotent architecture.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 557 $<br/>$LastChangedDate: 2008-03-26 14:23:32 +0100 (Mi, 26 Mrz 2008) $
 */

public class PathJoinTree extends IDJoinTree {
	    
	/**
	 * Constructor:
     * @param kb The knowledgebase from which this jointree is constructed.
     * @param queries The set of queries that are covered by this join tree.
     * @param algo The construction algorithm.
     * @throws ConstrException Exceptions throws during construction process.
     */
	
	public PathJoinTree(Knowledgebase kb, Collection<Domain> queries, Algorithm algo) throws ConstrException {
        super(kb, queries, algo);
    }

    /** 
     * @see nenok.lc.JoinTree#verify(java.lang.Class)
     */
    
    public boolean verify(Class<?> type) {
        return Utilities.superclassOf(KValuation.class, type);
    }

    /** 
     * @see nenok.lc.JoinTree#getArchitecture()
	 */
	 
	public String getArchitecture() {
		return "Path Problems";
	}

	/**
	 * Finds a path between a source and a target variable.
	 * @param source The source variable of the path.
	 * @param target The target variable of the path.
	 * @return An array of variables that define the path.
	 * @throws LCException Exception throws when distribute propagation has not yet been executed.  
	 */

	public Variable[] findPath(Variable source, Variable target) throws LCException {
		
		if(!isDistributed()) {
			throw new LCException("Distribute must be executed first.");
		}
		
		ArrayList<Variable> list = new ArrayList<Variable>();
		list.add(source);
		
		// Test procedure:
		// System.out.println("Search path from "+source+" to "+target+":");
		
		Variable current = source;
		while(!current.equals(target)) {
			
			KValuation<?> answer = (KValuation<?>)answer(current, target);
			current = answer.successor(current, target);
			
			// Test procedure:
			// System.out.println("  Intermediate: "+current);
			
			if(list.contains(current) && list.size() > 1) {
				System.exit(-1);
			}
			
			list.add(current);
		}
		
		return list.toArray(new Variable[list.size()]);
	}
}
