package nenok.lc.id;

import java.util.Collection;

import nenok.Knowledgebase;
import nenok.Utilities;
import nenok.jtconstr.Algorithm;
import nenok.jtconstr.ConstrException;
import nenok.lc.JoinTree;
import nenok.lc.LCException;
import nenok.lc.Node;
import nenok.lc.Path;
import nenok.va.Domain;
import nenok.va.Idempotency;
import nenok.va.VAException;
import nenok.va.Valuation;
import nenok.va.Variable;

/**
 * The join tree corresponding to the idempotent architecture.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 557 $<br/>$LastChangedDate: 2008-03-26 14:23:32 +0100 (Mi, 26 Mrz 2008) $
 */

public class IDJoinTree extends JoinTree {
	    
	/**
	 * Constructor:
     * @param kb The knowledgebase from which this jointree is constructed.
     * @param queries The set of queries that are covered by this join tree.
     * @param algo The construction algorithm.
     * @throws ConstrException Exceptions throws during construction process.
     */
	
	public IDJoinTree(Knowledgebase kb, Collection<Domain> queries, Algorithm algo) throws ConstrException {
        super(kb, queries, algo);
    }

    /** 
     * @see nenok.lc.JoinTree#verify(java.lang.Class)
     */
    
    public boolean verify(Class<?> type) {
        return Utilities.interfaceOf(Idempotency.class, type);
    }

    /** 
     * @see nenok.lc.JoinTree#getArchitecture()
	 */
	 
	public String getArchitecture() {
		return "Idempotent";
	}

	/** 
     * @see nenok.lc.JoinTree#scale()
	 */
	
	public void scale() throws VAException {
        // scaling is redundant in idempotent architecture
	}

	/**
	 * @see nenok.lc.JoinTree#createNode(nenok.va.Domain, nenok.adapt.Labeled)
	 */
	
	@Override
	public Node createNode(Domain label, Valuation factor) {
		return new IDNode(adapter, label, factor);
	}

	/**
	 * Specialized query answering procedure: 
	 * This method answers queries which consist of two variables that are not necessarily covered by the same node.
	 * It combines all node factors along the path between two nodes covering the variables.
	 * Time and space complexity bounded by the double of the treewidth are guaranteed.
	 * @param v1 The first variable.
	 * @param v2 The second variable.
	 * @return The objective function projected to the two variables.
	 * @throws LCException Exception throws when distribute propagation has not yet been executed.    
     */
	
	public Valuation answer(Variable v1, Variable v2) throws LCException {
		
		if(!isDistributed()) {
			throw new LCException("Distribute must be executed first.");
		}
		
		Node node1 = root.findCoveringNode(new Domain(v1));
		Node node2 = root.findCoveringNode(new Domain(v2));		
		
		Node[] path = (new Path(node1, node2)).toArray();

		Valuation eta = path[0].getContent();
		Domain first = eta.label();
		
		for (int i = 0; i < path.length-1; i++) {
			Domain inter = Domain.intersection(path[i].getContent().label(), path[i+1].getContent().label());			
			Domain union = Domain.union(first, inter);
			eta = path[i+1].getContent().combine(eta.marginalize(union));
		}
		
		return eta.marginalize(new Domain(v1, v2));
     }
}
