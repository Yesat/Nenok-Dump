package nenok.lc.id;

import nenok.Adapter;
import nenok.lc.LCException;
import nenok.lc.Node;
import nenok.va.Domain;
import nenok.va.Valuation;

/**
 * This is the node class implementation that belongs to the idempotent architecture.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 557 $<br/>$LastChangedDate: 2008-03-26 14:23:32 +0100 (Mi, 26 Mrz 2008) $
 */

public class IDNode extends Node {

    // Internal message cache:
	private int counter;
    private Valuation[] messages;
    
    /**
     * Constructor:
     * @param label The label of this node.
     * @param adapter The adapter to execute valuation algebra operations.
     * @param factor The content of this node whose domain is a subset of the node label. 
	 * If <code>null</code> is given as node content, the node is initialized by the identity element.
     */
	
	public IDNode(Adapter adapter, Domain label, Valuation factor) {
		super(adapter, label, factor);
	}
            
    /**
     * @see nenok.lc.Node#collect()
     */
    
    public void collect() throws LCException { 
    
        // Test, if all parents have sent their message:
        if(counter < this.getParents().size()) {
            return;
        }
        
        // Compute new node content -> messages haven't been initialized for leaves.  
        if(getParents().size() > 0) {
            content = adapter.combine(content, messages); 
                    
            // Throw messages away:
            messages = null;
        }

        
        // Stop when reaching the root node:
        if(getChild() == null) {
            return;
        }
               
        // Compute message for the child:
        Domain dom = Domain.intersection(content.label(), getChild().getLabel());        
        Valuation message = adapter.marginalize(content, dom);
                        
        // Send message to child:
        IDNode child = (IDNode)this.getChild();
        child.receiveParentMessage(message);
        child.collect();
    }

    /**
     * @see nenok.lc.Node#distribute()
     */
    
    public void distribute() throws LCException {
        for(Node parent : this.getParents()) {
            Valuation message = adapter.marginalize(content, Domain.intersection(content.label(), parent.getLabel()));
            ((IDNode)parent).receiveChildMessage(message);
            parent.distribute();
        }
    }
   
    /*
     * Helper methods:
     */
    
    /**
     * Method called by parent node to send message to this node.
     * @param message The message to send.
     */
    
    private void receiveParentMessage(Valuation message) {
        if(messages == null) {
            messages = new Valuation[getParents().size()];
        }
    	messages[counter++] = message;
    }
    
    /**
     * Method called by child node to send message to a parent
     * @param message The message to send.
     * @throws LCException Generic local computation exception.
     */
    
    private void receiveChildMessage(Valuation message) throws LCException {
        content = adapter.combine(content, message);
    }    
}
