package nenok.lc.ls;

import java.util.Collection;

import nenok.Knowledgebase;
import nenok.Utilities;
import nenok.jtconstr.Algorithm;
import nenok.jtconstr.ConstrException;
import nenok.lc.JoinTree;
import nenok.lc.LCException;
import nenok.lc.Node;
import nenok.va.Domain;
import nenok.va.Separativity;
import nenok.va.Valuation;

/**
 * The join tree corresponding to the Lauritzen-Spiegelhalter architecture.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 557 $<br/>$LastChangedDate: 2008-03-26 14:23:32 +0100 (Mi, 26 Mrz 2008) $
 */

public class LSJoinTree extends JoinTree {
	    
	/**
	 * Constructor:
     * @param kb The knowledgebase from which this jointree is constructed.
     * @param queries The set of queries that are covered by this join tree.
     * @param algo The construction algorithm.
     * @throws ConstrException Exceptions throws during construction process.
      */
	
	public LSJoinTree(Knowledgebase kb, Collection<Domain> queries, Algorithm algo) throws ConstrException {
        super(kb, queries, algo);
    }

    /** 
     * @see nenok.lc.JoinTree#verify(java.lang.Class)
     */
    
    public boolean verify(Class<?> type) {
        return Utilities.interfaceOf(Separativity.class, type);
    }

    /** 
     * @see nenok.lc.JoinTree#getArchitecture()
	 */
	 
	public String getArchitecture() {
		return "Lauritzen-Spiegelhalter";
	}

	/** 
     * @see nenok.lc.JoinTree#scale()
	 */
	
	public void scale() throws LCException {
        
        /*
         * Compute inverse (scaling factor):
         */
        
        Valuation result = root.getContent();
        
        if(result.label().size() > 0) {
            result = adapter.marginalize(root.getContent(), Domain.EMPTY);
        }
        
        result = adapter.inverse((Separativity)result);
        
        /*
         * Combine inverse with root content:
         */
        
        ((LSNode)getRoot()).setScalingFactor(adapter.combine(getRoot().getContent(), result));
	}

	/**
	 * @see nenok.lc.JoinTree#createNode(nenok.va.Domain, nenok.adapt.Valuation)
	 */
	
	@Override
	public Node createNode(Domain label, Valuation factor) {
		return new LSNode(adapter, label, factor);
	}
}
