package nenok.lc.ss;

import java.util.LinkedHashMap;
import java.util.Map;

import nenok.Adapter;
import nenok.lc.LCException;
import nenok.lc.Node;
import nenok.va.Domain;
import nenok.va.Valuation;

/**
 * This is the node class implementation that belongs to the Shenoy-Shafer architecture.
 * Internally, this class stores messages within a {@link LinkedHashMap} that reflects the concept
 * of mailboxes in the Shenoy-Shafer architecture. This implementation does not combine incoming 
 * messages to the node content, but only creates these combinations when necessary.
 * 
 * @author Marc Pouly
 */

public class SSNode extends Node {

    // Internal message cache:
    private LinkedHashMap<Node, Valuation> messages;
        
    /**
     * Constructor:
     * @param label The label of this node.
     * @param adapter The adapter to execute valuation algebra operations.
     * @param factor The content of this node whose domain is a subset of the node label. 
	 * If <code>null</code> is given as node content, the node is initialized by the identity element.
     */
	
	public SSNode(Adapter adapter, Domain label, Valuation factor) {
		super(adapter, label, factor);
        int neighbors = (getChild() != null) ? getParents().size()+1 : getParents().size();
        this.messages = new LinkedHashMap<Node, Valuation>(neighbors);
	}
    
    /**
     * @see nenok.lc.Node#collect()
     */
    
    public void collect() throws LCException { 
        
        // Test, if all parents have sent their message:
        if(messages.keySet().size() < this.getParents().size()) {
            return;
        }
                                                  
        // Stop when reaching the root node:
        if(getChild() == null) {
            return;
        }
        
        // Combine messages from parents:
        Valuation message = computeMessage(getChild());
                                       
        // Send message to child:
        ((SSNode)getChild()).receiveMessage(message, this);
        getChild().collect();
    }

    /**
     * @see nenok.lc.Node#distribute()
     */
    
    public void distribute() throws LCException {
        for(Node parent : this.getParents()) {
            Valuation message = computeMessage(parent);
            ((SSNode)parent).receiveMessage(message, this);
            parent.distribute();
        }
    }
    
    /**
     * @see nenok.lc.Node#getContent()
     */
    
    @Override    
    public Valuation getContent() {
    	
    	Valuation result = this.content;
    	for(Map.Entry<Node, Valuation> entry : messages.entrySet()) {
    		result = adapter.combine(result, entry.getValue());              
    	}
    	
    	return result;
    }
    
    /**
     * Replaces the node content by the scaling factor and clears the message cache.
     * @param factor The scaling factor to replace the node content.
     */
    
    protected void setScalingFactor(Valuation factor) {
        this.content = factor;
        messages.clear();
    }
    
    /*
     * Helper methods:
     */
    
    /**
     * Method called by parent node to send message to this node.
     * @param message The message to be sent.
     * @param parent The node from which this message was sent.
     */
    
    private void receiveMessage(Valuation message, Node node){
        messages.put(node, message);
     }
    
    /**
     * Computes the message for a specific neighbor node.
     * @param receiver The neighbor that will receive the computed message.
     * @return The message for the given neighbor node.
     */
    
    private Valuation computeMessage(Node receiver) throws LCException {
        Valuation result = this.content;
        for(Map.Entry<Node, Valuation> entry : messages.entrySet()) {
            if(!entry.getKey().equals(receiver)) {
                result = adapter.combine(result, entry.getValue());              
            }
        }
        return adapter.marginalize(result, Domain.intersection(result.label(), receiver.getLabel()));
    }
}
