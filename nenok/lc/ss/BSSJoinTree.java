package nenok.lc.ss;

import java.util.Collection;

import nenok.Knowledgebase;
import nenok.jtconstr.Algorithm;
import nenok.jtconstr.ConstrException;
import nenok.va.Domain;

/**
 * The join tree corresponding to the binary Shenoy-Shafer architecture.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 557 $<br/>$LastChangedDate: 2008-03-26 14:23:32 +0100 (Mi, 26 Mrz 2008) $
 */

public class BSSJoinTree extends SSJoinTree {
	    
	/**
	 * Constructor:
     * @param kb The knowledgebase from which this jointree is constructed.
     * @param queries The list of queries that are covered by this join tree.
     * @param algo The construction algorithm.
     * @throws ConstrException Exceptions throws during construction process.
     */
	
	public BSSJoinTree(Knowledgebase kb, Collection<Domain> queries, Algorithm algo) throws ConstrException {
        super(kb, queries, algo);
        makeBinary();
    }

	/** 
     * @see nenok.lc.JoinTree#getArchitecture()
	 */
	 
	public String getArchitecture() {
		return "Binary Shenoy-Shafer";
	}
}
