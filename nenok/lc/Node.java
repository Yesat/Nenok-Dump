package nenok.lc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import nenok.Adapter;
import nenok.va.Domain;
import nenok.va.Identity;
import nenok.va.Valuation;

/**
 * This abstract class represents nodes of a join tree. The tree itself is built from a double 
 * linked set of {@link Node} objects and the {@link JoinTree} object points to the root node of the tree.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 557 $<br/>$LastChangedDate: 2008-03-26 14:23:32 +0100 (Mi, 26 Mrz 2008) $
 */

public abstract class Node {
    
    // Unique child node:
    private Node child;
    
    // List of parent nodes:
    private List<Node> parents;
    
    // Node label:
    private final Domain label;
    
    // Node content locator:
    protected Valuation content;
        
    // Adapter:
    protected final Adapter adapter;
                
    /**
     * Constructor:
     * @param label The label of this node.
     * @param adapter The adapter to execute valuation algebra operations.
     * @param factor The content of this node whose domain is a subset of the node label. 
	 * If <code>null</code> is given as node content, the node is initialized by the identity element.
     */
    
    public Node(Adapter adapter, Domain label, Valuation factor) {
    	
    	this.label = label;
    	this.adapter = adapter;
        parents = new ArrayList<Node>();
        
        /*
         * Node content:
         */
                
        if(factor != null) {
        	
            if(!factor.label().subSetOf(label)) {
            	throw new IllegalArgumentException("The domain of the node content is not a subset of the node label.");
            }
        	this.content = factor;
        	
        } else {
        	initEmpty();
        }
    }
              
    /*
     * Propagation methods:
     */
    
    /**
     * Executes the collect algorithm for this node.
     * @throws LCException Exception caused by the collect propagation.
    */
    
    public abstract void collect() throws LCException;
    
    /**
     * Executes the distribute algorithm for this node.
     * @throws LCException Exception caused by the distribute propagation.
     */
    
    public abstract void distribute() throws LCException;
    
    /*
     * Tree components:
     */
    
    /**
     * Assigns a unique child element to this node.
     * @param child The node's new child element.
     */
    
    public void setChild(Node child) {
        this.child = child;
    }
    
    /**
     * @return This node's unique child node.
     */
    
    public Node getChild() {
        return child;
    }
    
    /**
     * Adds a new parent node to this node instance.
     * @param parent The new parent node to add.
     */
    
    public void addParent(Node parent) {
        if(!parents.contains(parent)) {
            parents.add(parent);
        }
        parent.child = this;
    }
    
    /**
     * Adds a new parent node to this node instance at the given position.
     * @param parent The new parent node to add.
     * @param index The position where the new parent is added.
     */
    
    public void addParent(Node parent, int index) {
        if(!parents.contains(parent)) {
            parents.add(index, parent);
        }
        parent.child = this;        
    }
    
    /**
     * Adds new new parent nodes to this node instance.
     * @param parents A list of new parent nodes to add.
     */
    
    public void addParents(List<Node> parents) {
        for(Node parent : parents) {
            this.addParent(parent);
        }
    }
        
    /**
     * @return The parent nodes of this node instance.
     */
    
    public List<Node> getParents() {
        return parents;
    }
    
    /**
     * Removes a parent node from this node instance.
     * @param parent The parent to remove.
     */
    
    protected void removeParent(Node parent) {
        parents.remove(parent);  
    }
        
    /**
     * @return The leaf nodes in the subtree of this node.
     */
    
    public Set<Node> getLeaves() {
        return findLeaves(new HashSet<Node>());
    }
    
    /*
     * Node content:
     */
    
    /**
     * @return <code>true</code>, if this node is not empty.
     */
    
    public boolean isFull() {
    	return !(content instanceof Identity);
    }
    
    /**
     * @return Returns the label of this node.
     */
    
    public Domain getLabel() {
        return label;
    }
           
    /**
     * Moves the content from this node to the target node and removes the content from this node.
     * Important: This method can only be executed on empty taget nodes.
     * @param target The target node to which the content of the source node is combined.
     * @throws LCException Exception thrown when the target node is not empty or when the content label is not a subset of the target node label.
     */
    
    public void moveContent(Node target) throws LCException {
    	
    	if(target.isFull()) {
    		throw new LCException("This method is only allowed for empty target nodes, i.e. taget nodes that contain the identity element.");
    	}
    	
    	if(!this.content.label().subSetOf(target.getLabel())) {
    		throw new LCException("Label of node content to be moved is not a subset of the target node label.");
    	}
    	
    	target.content = this.content;
    	this.initEmpty();
    }
    
    /**
     * Returns the valuation stored in or referenced by this node.
     * This method calls <code>getContent()</code>.
     * @return The valuation that is stored in this node.
     * @throws LCException Wrapper exception.
     */
    
    public Valuation getContent() {
    	return content;
    }
    
    /**
     * Replaces the content of this node by the argument.
     * @param content The new node content.
     */
    
    public void setContent(Valuation content) {
    	this.content = content;
    }
        
    /*
     * Informative methods:
     */
	
    /**
     * @return The number of nodes in the subtree of this node.
     */
	
    public int subTreeSize() {
        int result = 1;
        for(Node node : parents) {
            result += node.subTreeSize();
        }
        return result;
    }
    
    /**
     * @return <code>true</code>, if this node's subtree is binary.
     */
    
    protected boolean isBinary() {
        boolean result = parents.size() <= 2;
        for (Node parent : parents) {
            result &= parent.isBinary();
        }
        return result;
    }
    
    /**
     * @return The largest node label within the subtree of this node.
     */
    
    protected Domain findLargestDomain() {
        Domain result = label;
        for (Node parent : parents) {
            Domain pdom = parent.findLargestDomain();
            if(pdom.size() > result.size()) {
                result = pdom;
            }            
        }
        return result;
    }
    
    /**
     * @return The union of all node domains within the subtree of this node.
     */
    
    protected Domain getTotalDomain() {
        Domain result = label;
        for(Node node : parents) {
            result = Domain.union(result, node.getTotalDomain());
        }
        return result;
    }
           
    /*
     * Query answering:
     */
    
    /**
     * @param query The query label to find.
     * @return A node whose label equals the query or <code>null</code>.
     */
    
    public Node findCoveringNode(Domain query) {
      if(query.subSetOf(label)) {
          return this;
      }
      for (Node parent : parents) {
          Node result = parent.findCoveringNode(query);
          if(result != null) {
              return result;
          }
      }
      return null;
    }
    
    /*
     * Tree transformations:
     */
    
    /**
     * Transforms the subtree of this to a binary tree.
     * @param data The information for the join tree construction process.
     */
    
    protected void makeBinary(JoinTree.Construction data) {
        
        /*
         * Iterate as long as the current node is not binary.
         */
        
        while(getParents().size() > 2) {
            Node first = parents.remove(0);
            Node second = parents.remove(1);
            Node union = data.getNodeInstance(Domain.intersection(Domain.union(first.label, second.label), getLabel()));
            union.addParent(first);
            union.addParent(second);
            addParent(union, parents.size() - 1);
        }
        
        /*
         * Apply this procedure to all parent nodes.
         */
        
        for(Node parent : parents) {
            parent.makeBinary(data);
        }
    }

    /*
     * Helper methods:
     */
    
    /**
     * Initializes jointree nodes when no factor is present.
     */
    
    private void initEmpty() {
    	 this.content = Identity.INSTANCE;
    }
    
    /**
     * Returns the sub-tree of this nodes as an iterable node set.
     * @param nodes An empty collection to be filled.
     * @return The collection containing all nodes of this sub-tree.
     */
    
    protected Collection<Node> getNodes(Collection<Node> nodes) {
    	nodes.add(this);
    	for(Node parent : parents) {
    		parent.getNodes(nodes);
    	}
    	return nodes;
    }
          
    /**
     * Procedure to find all leaves of this node's subtree.
     * @param cache The cache to store leaves.
     * @return The filled cache.
     */
    
    private Set<Node> findLeaves(Set<Node> cache) {
        if(parents.size() == 0) {
            cache.add(this);
            return cache;
        }
        for (Node parent : parents) {
            parent.findLeaves(cache);
        }
        return cache;
    }
}
