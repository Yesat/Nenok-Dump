package nenok.lc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Representation of a path in a join tree.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 557 $<br/>$LastChangedDate: 2008-03-26 14:23:32 +0100 (Mi, 26 Mrz 2008) $
 */

public class Path {
	
	private ArrayList<Node> path;
	
	/**
	 * Constructor:
	 * Builds a path from the given node to the root node.
	 * @param node The start node of the path starting at the root.
	 */
	
	public Path(Node node) {
		path = new ArrayList<Node>();
		path.add(node);
		
		Node child = node.getChild();
		while(child != null) {
			path.add(child);
			child = child.getChild();
		}
	}
	
	/**
	 * Constructor:
	 * @param start The start node of the path.
	 * @param end The end node of the path.
	 */
	
	public Path(Node start, Node end) {
		Path path1 = new Path(start);
		Path path2 = new Path(end);
		path = union(path1.path, path2.path);
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	
	@Override
	public String toString() {
		String result = "";
		Iterator<Node> iterator = path.iterator();
		while(iterator.hasNext()) {
			result += " -> ";
			result += iterator.next().getLabel().toString();
		}
		return result;
	}

	/**
	 * @return The length (number of nodes) in this path.
	 */

	public int length() {
		return path.size();
	}
	
	/**
	 * @see java.lang.Iterable#iterator()
	 */
	
	public Node[] toArray() {
		return path.toArray(new Node[path.size()]);
	}
	
	/*
	 * Helper methods:
	 */
	
	/**
	 * Creates a new path start starts at the first node of <code>path1</code> and ends at the last node of <code>path2</code>.
	 * @param path1 The path whose first node marks the start of the resulting path.
	 * @param path2 The path whose last node marks the final node of the resulting path.
	 */
	
	private ArrayList<Node> union(List<Node> path1, List<Node> path2) {
		
		if((path1 == null) || (path2 == null)) {
			throw new IllegalArgumentException("Null is not a valid argument for this procedure");
		}
		
		int end1 = path1.size()-1;
		int end2 = path2.size()-1;
		
		if((end1 < 0) || (end2 < 0)) {
			throw new IllegalArgumentException("A path of size 0 is impossible.");
		}
		
		// Find first common node of the two paths:
		while(true) {
			
			if((end1 < 0) || (end2 < 0)) {
				break;
			}
						
			// Are the two end nodes are different:
			if(!path1.get(end1).equals(path2.get(end2))) {
				break;
			}
			
			end1--;
			end2--;
			
		}
		
		ArrayList<Node> result = new ArrayList<Node>();
		
		// Add nodes from first path including first common node as connector:
		for (int i = 0; i <= end1+1; i++) {
			result.add(path1.get(i));
		}
		
		// Add nodes from second path in reversed order:
		for(int i = end2; i >= 0; i--) {
			result.add(path2.get(i));
		}
		
		return result;
	}
}
