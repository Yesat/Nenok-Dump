package nenok.jtconstr;

import java.io.File;

import nenok.lc.Node;
import nenok.lc.JoinTree.Construction;
import nenok.parser.JT_Parser;

/**
 * This construction algorithm allows to read and rebuild jointrees that were 
 * serialized into a file.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 554 $<br/>$LastChangedDate: 2008-03-26 14:19:57 +0100 (Mi, 26 Mrz 2008) $
 */

public class Deserializer extends Algorithm {
    
    private File file;
    private JT_Parser parser;

    /**
     * Constructor:
     * @param file The file that contains the serialized jointree.
     * @param parser The parser to deserialize the given file.
     */
    
    public Deserializer(File file, JT_Parser parser) {
        this.file = file;
        this.parser = parser;
    }
    
    /**
     * @see nenok.jtconstr.Algorithm#buildTree(nenok.lc.JoinTree.Construction)
     */

    public Node buildTree(Construction data) throws ConstrException {
        return parser.parse(file, data);
    }
    
    /**
     * @see nenok.jtconstr.Algorithm#getName()
     */

    public String getName() {
        return "File Deserializer";
    }
}
