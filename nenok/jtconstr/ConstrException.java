package nenok.jtconstr;

import nenok.lc.LCException;

/**
 * This class represents exceptions thrown during the join tree construction process.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 554 $<br/>$LastChangedDate: 2008-03-26 14:19:57 +0100 (Mi, 26 Mrz 2008) $
 */

public class ConstrException extends LCException {

	/**
	 * Constructor:
	 * @param message The exception message.
	 */

	public ConstrException(String message) {
		super(message);
	}

	/**
	 * Constructor: 
	 * @param message The exception message.
	 * @param cause The (prior) exception to be wrapped.
	 */

	public ConstrException(String message, Throwable cause) {
		super(message, cause);
	}
}
