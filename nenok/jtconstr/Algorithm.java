package nenok.jtconstr;

import nenok.lc.JoinTree;
import nenok.lc.LCException;
import nenok.lc.Node;
import nenok.va.Domain;

/**
 * The join tree construction algorithm base interface. This is the head class of a 
 * realization of the STRATEGY design pattern, allowing that the same type of join 
 * tree architecture can be constructed with different construction algorithms.
 * <br/><br/>
 * It is important to note that every construction algorithm instance can build exactly one 
 * join tree, since it also serves as an information holder for the construction process.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 554 $<br/>$LastChangedDate: 2008-03-26 14:19:57 +0100 (Mi, 26 Mrz 2008) $
 */

public abstract class Algorithm {
    
	private boolean runnable = true;
    private double constrTime = -1;
	
    /**
	* Method to build join trees. This method can be called exactly once.
	* @param data Object that contains all necessary information to build join trees.
	* @return The root node of the constructed join tree.
	* @throws ConstrException Exceptions caused by the jointree construction process or if this method has already been called.
    */
    
    public Node run(JoinTree.Construction data) throws ConstrException {
    	if(runnable) {
        	double start = System.currentTimeMillis();
        	Node result = buildTree(data);
        	constrTime = System.currentTimeMillis() - start;
        	return result;
    	}
    	throw new ConstrException("Cannot run two times the same construction algorithm instance.");
    }
    
   /**
	* Method to build join trees called from <code>Algorithm.run(data)</code>.
	* The <code>JoinTree.Construction</code> object given as argument contains all necessary 
	* informations for the construction process, this is: the knowledgebase, a set of 
	* queries that need to be covered by the join tree and the {@link Node} builder method 
	* for the type of architecture to be constructed.
	* @param data Object that contains all necessary information to build join trees.
	* @return The root node of the constructed join tree.
	* @throws ConstrException Exceptions caused by the jointree construction process.
    */
	
	public abstract Node buildTree(JoinTree.Construction data) throws ConstrException;

	/**
	 * @return The construction time of the last constructed join tree.
	 */
	
	public double getConstructionTime() {
        return constrTime;
    }
	   
	/**
	 * The running time of delayed tree transformations should sometimes be considered
	 * in the total construction time. This method therefore adds the given number of
	 * milliseconds to the total construction time.
	 * @param time Number of milliseconds to be added to the construction time.
	 */
	
	public void addConstructionTime(double time) {
		constrTime += time;
	}
	
	/**
	 * @return The name of the construction algorithm.
	 */

	public abstract String getName();
	
	/**
	 * Eliminates node chains within a jointree.
	 * @param node The root node of the subtree that has to be optimized.
	 * @param data The construction data that was used to construct the tree.
	 */
	
	public static void optimize(Node node, JoinTree.Construction data) {
		for(int i = 0; i < node.getParents().size(); i++) {
			Node parent = node.getParents().get(i);
			Domain pDomain = parent.getLabel();
			
			// Ignore full & query nodes:
			if(parent.isFull()) {
				continue;
			}
						
			// Remove parent if its domain is a subset of the child domain:
			if(pDomain.subSetOf(node.getLabel()) || (parent.getParents().size() < 2 && !data.queries.contains(parent.getLabel()))) {			
				node.getParents().remove(parent);
				parent.setChild(null);
				for(Node pp : parent.getParents()) {
					pp.setChild(node);
					node.addParent(pp);
				}
				parent.getParents().clear();
				if(parent.isFull()) {
					
					try {
						parent.moveContent(node);
					} catch (LCException e) {
						// Cannot happen ...
						e.printStackTrace();
					}
					
				}
				i--;
				continue;
			} 
		}
		
        // Apply this procedure recursively:
        for(Node parent : node.getParents()) {
            optimize(parent, data);
        }
	}
}
