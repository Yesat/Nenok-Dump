package nenok.jtconstr.vvll;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import nenok.lc.Node;
import nenok.va.Variable;

/**
 * This container is used to construct a vvll. A valuation container containes:
 * <ul>
 *  <li>A list of references to variables being contained in this valuation.</li>
 *  <li>A {@link Node} object that will contain this valuation in the join tree.</li> 
 * </ul>
 * 
 * @author Marc Pouly
 */

public class ValuationContainer {
	
	private ArrayList<VariableContainer> varContainerRefs;
	private Node node;
	
	/**
	 * Constructor:
	 * @param node A new jointree node instance that holds the valuation.
	 */
	
	public ValuationContainer(Node node) {
		this.node = node;
		this.varContainerRefs = new ArrayList<VariableContainer>();
	}
			
	/**
	 * Adds a variable container to the container's internal list.
	 * @param varCon The variable container reference.
	 */
	
	public void addVariableContainer(VariableContainer varCon) {
		if(!contains(varCon)) {
			varContainerRefs.add(varCon);	
		}
	}
	
	/**
	 * Adds a collection of variable container references to the container's internal list.
	 * @param varCons The collection of variable container.
	 */	
	
	public void addVariableContainer(Collection<VariableContainer> varCons) {
		Iterator<VariableContainer> it = varCons.iterator();
		while (it.hasNext()) {
			VariableContainer varCon = it.next();
			addVariableContainer(varCon);
		}
	}
	
	/**
	 * Does the variable container reference list already contain a given variable container.
	 * Uses the equal method in the variable interface.
	 * @param varCon The given variable container.
	 * @return <code>true</code>, if the variable container is already contained in the 
	 * variable container reference list. 
	 */
	
	public boolean contains(VariableContainer varCon) {
		Variable var = varCon.getVariable();
		Iterator<VariableContainer> iter = varContainerRefs.iterator();
		while (iter.hasNext()) {
			VariableContainer vc = iter.next();
			if(var.equals(vc.getVariable())) {
				return true;
			}
		}
		return false;
	}
		
	/**
	 * @return The variable container foreach variable in the valuation's domain.
	 */
	
	public List<VariableContainer> getVariableContainerRefs() {
		return this.varContainerRefs;
	}
	
	/**
	 * @return The container's join tree node.
	 */
	
	public Node getNode() {
		return this.node;
	}
	
	/**
	 * Adds a child node to the container's node.
	 * @param child The child node to add.
	 */
	
	public void addChildNode(Node child) {
		node.addParent(child);
	}
}