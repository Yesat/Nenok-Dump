package nenok.jtconstr.vvll;

import nenok.va.Variable;

/**
 * Objects of this extension of the {@link VariableContainer} class
 * will be stored within a heap. Therefore, the {@link Comparable} interface
 * is implemented additionally. The <code>compareTo(Object o)</code> is based
 * on a cost value representing the costs to eliminate the variable of this
 * container.
 * 
 * @author Marc Pouly
 */

public class VariableContainerHeap extends VariableContainer implements Comparable<VariableContainerHeap> {
	
	private int cost;
	
	/**
	 * Constructor:
	 * Calls super constructor and initializes costs to 0.
	 * @param var The variable of this container.
	 */
	
	public VariableContainerHeap(Variable var) {
		super(var);
		this.cost = 0;
	}

	/**
	 * Sets the variable containers's cost value.
	 * @param value The new cost of the variable.
	 */
	
	public void setCost(int value) {
		this.cost = value;
	}
	
	/*
	 * Comparable interface:
	 */
	
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	
	public int compareTo(VariableContainerHeap o) {
		if (this.cost == o.cost) {
			return 0;	
		}
		if (this.cost > o.cost) {
			return 1;
		}
		return -1;
	}	
}














