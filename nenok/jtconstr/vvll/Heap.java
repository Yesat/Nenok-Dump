package nenok.jtconstr.vvll;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * This class offers a generic implementation of a binary
 * heap data structure. All elements need to implements the 
 * {@link Comparable} interface to guarantee that they can 
 * be ordered to reconstruct the heap structure.
 * 
 * This heap implementation asserts that the smallest elements is 
 * always on top of the heap and removed first. To determine the
 * smallest element, the operation <code>compareTo(Object o)</code>
 * from {@link Comparable} is called.
 * @param <C> Class implementing the {@link Comparable} insterface.
 * 
 * @author Marc Pouly
 */

public class Heap<C extends Comparable<C>> implements Iterable<C>{
	
	private ArrayList<C> heap;
	
	/**
	 * Constructor:
	 * Creates a new empty heap. 
	 */
	
	public Heap() {
		heap = new ArrayList<C>();
	}
	
	/**
	 * Constructor:
	 * Creates a heap from a given collection of {@link Comparable} objects.
	 * @param c The collection of heap elements.
	 */
	
	public Heap(Collection<C> c) {
		heap = new ArrayList<C>(c.size());
		Iterator<C> it = c.iterator();
		while(it.hasNext()) {
			add(it.next());
		}
	}
		
	/**
	 * Adds a single element to the heap.
	 * @param value The element to add.
	 */
	
	public void add(C value) {
		heap.add(value);
		pericolateUp(heap.size()-1);
	}
	
	/**
	 * Adds a collection of elements to the heap.
	 * @param c The collection of elements to add.
	 */
	
	public void add(Collection<C> c) {
		Iterator<C> it = c.iterator();
		while (it.hasNext()) {
			this.add(it.next());
		}
	}
	
	/**
	 * Removes the root (= smallest element) of the heap.
	 * @return The root of the heap.
	 */
	
	public C remove() {
		if (heap.size() == 0) {
			return null;
		}
		C min = heap.get(0);
		heap.set(0, heap.get(heap.size()-1));
		heap.remove(heap.size()-1);
		if(heap.size() > 1) {
			pushDownRoot(0);
		}
		return min;
	}
	
	/**
	 * The iterator of the collection interface.
	 * @return The heap's iterator.
	 */
	
	public Iterator<C> iterator() {
		return heap.iterator();
	}
	
	/**
	 * Returns the number of elements contained in the heap.
	 * @return The heap's size.
	 */
	
	public int size() {
		return heap.size();
	}
	
	/**
	 * Updates the heap, if the given node has changed.
	 * @param index The node to update.
	 */
	
	public void update(int index) {
		pericolateUp(index);
		pushDownRoot(index);
	}
	
	/**
	 * Returns the index of the given object in the heap.
	 * @param c The object to search.
	 * @return The object's position.
	 */
	
	public int getPosition(C c) {
		return heap.indexOf(c);
	}
	
	/*
	 * Helper functions:
	 */
	
	/**
	 * Finds the index of the parent element.
	 * @param i The index of the current child element.
	 * @return The index of the parent element.
	 */
	
	private static int parentOf(int i) {
		return (i-1)/2;
	}
	
	/**
	 * Finds the index of left child element.
	 * @param i The index of the current element.
	 * @return The index of the left child element.
	 */	
	
	private static int leftChild(int i) {
		return 2*i+1;
	}
	
	/**
	 * Finds the index of right child element.
	 * @param i The index of the current element.
	 * @return The index of the right child element.
	 */		
	
	private static int rightChild(int i) {
		return 2*(i+1);
	}
	
	/**
	 * Procedure to reconstruct heap structure upwards.
	 * @param leaf The position of the last added leaf.
	 */
	
	private void pericolateUp(int leaf) {
		if(leaf <= 0) {
			return;
		}
		int parent = parentOf(leaf);
		C value = heap.get(leaf);
		//Search position for current element in heap.
		while(leaf > 0 && value.compareTo(heap.get(parent)) < 0) {
			heap.set(leaf, heap.get(parent));
			leaf = parent;
			parent = parentOf(leaf);
		}
		heap.set(leaf, value);
	}
	
	/**
	 * Procedure to reconstruct heap structure downwards.
	 * @param root The position of the node to push down.
	 */
	
	private void pushDownRoot(int root) {
		int heapSize = heap.size();
		C value = heap.get(root);
		while(root < heapSize) {
			int childPos = leftChild(root);
			if(childPos < heapSize) {
				if((rightChild(root) < heapSize) && ((heap.get(childPos+1)).compareTo(heap.get(childPos)) < 0)) {
					childPos++;
				}
				if((heap.get(childPos)).compareTo(value) < 0) {
					heap.set(root, heap.get(childPos));
					root = childPos;
				} else {
					heap.set(root, value);
					return;
				}
			} else {
				heap.set(root, value);
				return;
			}
		}
	}	
}