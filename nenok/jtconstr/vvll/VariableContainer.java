package nenok.jtconstr.vvll;

import java.util.ArrayList;
import java.util.List;

import nenok.va.Variable;

/**
 * This container is used to construct a vvll during the join tree creation process.
 * These variable container will be elements of a heap, therefore we need to implement 
 * the comparable interface. A VariableContainer contains:
 * <ul>
 * 	<li>A reference to its variable.</li>
 *  <li>A list of references to valuations wich contain this variable.</li>
 * </ul>
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 554 $<br/>$LastChangedDate: 2008-03-26 14:19:57 +0100 (Mi, 26 Mrz 2008) $
 */

public class VariableContainer {
	
	private Variable variable;
	private ArrayList<ValuationContainer> vcRefs;
		
	/**
	 * Constructor:
	 * @param var The Variable of this container.
	 */
	
	public VariableContainer(Variable var) {
		this.variable = var;
		this.vcRefs = new ArrayList<ValuationContainer>();
	}
	
	/**
	 * Adds a valuation container reference to the internal list.
	 * @param valCon The valuation container to add.
	 */
		
	public void addValuationContainer(ValuationContainer valCon) {
		if(!vcRefs.contains(valCon)) {
			vcRefs.add(valCon);
		}
	}
	
	/**
	 * Returns the container's variable.
	 * @return The container's variable.
	 */
	
	public Variable getVariable() {
		return this.variable;
	}
	
	/**
	 * Returns the container's valuation reference list.
	 * @return The list of all valuation container containing the container's variable.
	 */
	
	public List<ValuationContainer> getValuationContainerRefs() {
		return vcRefs;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	
	public boolean equals(Object o) {
		if(!(o instanceof VariableContainer)) {
			return false;
		}
		Variable var = ((VariableContainer)o).variable;
		return variable.equals(var);
	}

	/**
	 * @see java.lang.Object#toString()
	 */

	public String toString() {
		return "Container of variable "+variable.toString();
	}
}














