package nenok.jtconstr.vvll;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import nenok.lc.JoinTree;
import nenok.lc.Node;
import nenok.va.Domain;
import nenok.va.Valuation;
import nenok.va.Variable;

/**
 * The Variable-Valuation-Link-List (vvll) environment is used during the join tree construction process. A vvll contains:
 * <ul>
 * <li>A list of {@link ValuationContainer} objects.</li>
 * <li>A list of {@link VariableContainer} objects.</li>
 * </ul>
 * 
 * @author Marc Pouly
 */

public class VVLL_A {

    private ArrayList<VariableContainer> varcList;
    private ArrayList<ValuationContainer> valcList;

    /**
     * Constructor:
     * @param data Object containing necessary informations for the join tree construction process.
     */

    public VVLL_A(JoinTree.Construction data) {
        
        varcList = new ArrayList<VariableContainer>();
        valcList = new ArrayList<ValuationContainer>();

        // Compute total domain:
        Domain dom = Domain.union(data.knowledgebase.getDomain(), Domain.union(data.queries));

        // Foreach locator: create a new valuation container and add it to the list.
        for(Valuation loc : data.knowledgebase.toArray()) {
        	Node node = data.getNodeInstance(loc.label(), loc);
            valcList.add(new ValuationContainer(node));
        }
        
        // Foreach query: create a new valuation container and add it to the list.
        for (Domain query : data.queries) {
        	Node node = data.getNodeInstance(query);
            valcList.add(new ValuationContainer(node));
        }

        // Foreach variable: create a new variable container and add it to the temporary list.
		for(Variable v : dom) {
			varcList.add(new VariableContainerHeap(v));
		}

        // Construction of the binding: Variable->Valuation:
        for(VariableContainer varCon : varcList) {
            Variable var = varCon.getVariable();
            for(ValuationContainer valCon : valcList) {
                dom = valCon.getNode().getLabel();
                if (dom.contains(var)) {
                    varCon.addValuationContainer(valCon); 
                }
            }
        }
        
		//Construction of the binding: Valuation->Variable:
		for(ValuationContainer valCon : valcList) {
			for(Variable var : valCon.getNode().getLabel()) {
				for(VariableContainer varCon : varcList) {
					if(var.equals(varCon.getVariable())) {
						valCon.addVariableContainer(varCon);
					}
				}				
			}
		}
    }

    /**
     * Adds a new valuation container to the internal valuation container list.
     * @param vc The valuation container to add.
     */

    public void addValuationContainer(ValuationContainer vc) {
        int position = valcList.size();
        valcList.add(position, vc);
    }

    /**
     * Removes the variable container which belongs to the variable having the
     * lowest elimination costs.
     * @param var The variable whose container has to be removed.
     * @return The removed VariableContainer.
     */

    public VariableContainer removeVariableContainer(Variable var) {
        Iterator<VariableContainer> it = varcList.iterator();
        while (it.hasNext()) {
            VariableContainer vc = it.next();
            if (vc.getVariable().equals(var)) {
                varcList.remove(vc);
                return vc;
            }
        }
        return null;
    }

    /**
     * Removes a given valuation container from the internal valuation container list.
     * @param vc The valuation container to remove.
     * @return <code>true</code>, if the given valuation container existed.
     */

    public boolean removeValuationContainer(ValuationContainer vc) {
        return valcList.remove(vc);
    }

    /**
     * @return The number of valuation containers in the vvll.
     */

    public int getValuationContainerSize() {
        return this.valcList.size();
    }

    /**
     * @return The number of variable containers in the vvll.
     */

    public int getVariableContainerSize() {
        return this.varcList.size();
    }

    /**
     * Returns the position of a given element in the heap.
     * @param cp The element whose position is asked.
     * @return The element's position.
     */

    public int getPosition(VariableContainer cp) {
        return this.varcList.indexOf(cp);
    }

    /**
     * @return A list all nodes currently insered in the valuation container list.
     */

    public List<Node> getValuationContainerNodes() {
        ArrayList<Node> result = new ArrayList<Node>();
        for(ValuationContainer vc : valcList) {
        	result.add(vc.getNode());
        }
        return result;
    }
}