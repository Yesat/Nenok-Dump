package nenok.jtconstr.vvll;

import java.util.ArrayList;
import java.util.List;

import nenok.lc.JoinTree;
import nenok.lc.Node;
import nenok.va.Domain;
import nenok.va.Valuation;
import nenok.va.Variable;

/**
 * The Variable-Valuation-Link-List (vvll) environment is used during 
 * the join tree construction process. A vvll contains:
 * <ul>
 * 	<li>A list of {@link ValuationContainer} objects.</li>
 *  <li>A heap of {@link VariableContainerHeap} objects.</li>
 * </ul>
 * 
 * @author Marc Pouly
 */

public class VVLL_H {
		   
	private Heap<VariableContainerHeap> varcList;
	private ArrayList<ValuationContainer> valcList;
		
    /**
     * Constructor:
     * @param data Object containing necessary informations for the join tree construction process.
     */
	
	public VVLL_H(JoinTree.Construction data) {
        	
		varcList = new Heap<VariableContainerHeap>();
		valcList = new ArrayList<ValuationContainer>();
		
		//Compute total domain:
        Domain dom = Domain.union(data.knowledgebase.getDomain(), Domain.union(data.queries));
		
		//A temporary list to store the variable container. As long as they do not have costs, they can't be added to the heap.
		ArrayList<VariableContainerHeap> tempList = new ArrayList<VariableContainerHeap>();
		
        // Foreach locator: create a new valuation container and add it to the list.
        for(Valuation loc : data.knowledgebase.toArray()) {
        	Node node = data.getNodeInstance(loc.label(), loc);
            valcList.add(new ValuationContainer(node));
        }
		
		//Foreach query: create a new valuation container and add it to the list.
        for (Domain query : data.queries) {
        	Node node = data.getNodeInstance(query);
            valcList.add(new ValuationContainer(node));
        }
				
		//Foreach variable: create a new variable container and add it to the temporary list.
		for(Variable v : dom) {
			tempList.add(new VariableContainerHeap(v));
		}
		
		//Construction of the binding: Variable->Valuation:
		ArrayList<Domain> doms = new ArrayList<Domain>();
		for(VariableContainerHeap varCon : tempList) {
			int counter = 0;
			Variable var = varCon.getVariable();
			for(ValuationContainer valCon : valcList) {
				dom = valCon.getNode().getLabel();
				if(dom.contains(var)) {
					varCon.addValuationContainer(valCon);
					//Calculation of variable's costs
					counter++;
					doms.add(dom);
				}
			}
            
			//the current variable is a leaf -> costs = 0
			if (counter == 1) {
				varCon.setCost(0);
			}
            
			//the variable's cost = cardinality of the union of all domains, containing the current variable.
			else {
				varCon.setCost(Domain.union(doms).size());
			}
			doms.clear();
		}
		
		//Add the content of the temporary variable list to the heap.
		varcList.add(tempList);
		
		//Construction of the binding: Valuation->Variable:
		for(ValuationContainer valCon : valcList) {
			for(Variable var : valCon.getNode().getLabel()) {
				for(VariableContainerHeap varCon : varcList) {
					if(var.equals(varCon.getVariable())) {
						valCon.addVariableContainer(varCon);
					}
				}				
			}
		}
	}
	
	/**
	 * Adds a new valuation container to the internal valuation container list.
	 * @param vc The valuation container to add.
	 */
	
	public void addValuationContainer(ValuationContainer vc) {
		int position = valcList.size();
		valcList.add(position, vc);			
	}
	
	/**
	 * Removes the variable container which belongs to the
	 * variable having the lowest elimination costs.
	 * @return The removed VariableContainer_H.
	 */
	
	public VariableContainerHeap removeVariableContainer() {
		return varcList.remove();
	}
	
	/**
	 * Removes a given valuation container from the internal valuation container list.
	 * @param vc The valuation container to remove.
	 * @return <code>true</code>, if the given valuation container existed.
	 */
	
	public boolean removeValuationContainer(ValuationContainer vc) {
		return valcList.remove(vc);
	}
	
	/**
	 * Returns the size of the internal valuation list.
	 * @return The number of valuations in the vvll.
	 */
	
	public int getValuationContainerSize() {
		return this.valcList.size();
	}
	
	/**
	 * Returns the size of the internal variable list.
	 * @return The number of variables in the vvll.
	 */
	
	public int getVariableContainerSize() {
		return this.varcList.size();
	}
	
	/**
	 * Updates the inner heap structure.
	 * @param node The heap position that changed and that has to be updated.
	 */
	
	public void updateHeap(int node) {
		varcList.update(node);
	}
	
	/**
	 * Returns the position of a given element in the heap.
	 * @param cp The element whose position is asked.
	 * @return The element's position.
	 */
	
	public int getHeapPosition(VariableContainerHeap cp) {
		return this.varcList.getPosition(cp);
	}
	
    /**
     * @return A list all nodes currently insered in the valuation container list.
     */

    public List<Node> getValuationContainerNodes() {
        ArrayList<Node> result = new ArrayList<Node>();
        for(ValuationContainer vc : valcList) {
        	result.add(vc.getNode());
        }
        return result;
    }
}