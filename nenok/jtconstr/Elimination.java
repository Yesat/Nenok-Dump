 package nenok.jtconstr;

import nenok.va.Variable;

/**
 * This class provides a abstraction for all construction algorithms
 * that are based on variables elimination.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 554 $<br/>$LastChangedDate: 2008-03-26 14:19:57 +0100 (Mi, 26 Mrz 2008) $
 */

public abstract class Elimination extends Algorithm {
    
    /**
     * @return The elimination sequence used by this algorithm.
     */
    
    public abstract Variable[] getEliminationSequence();
}
