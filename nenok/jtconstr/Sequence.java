package nenok.jtconstr;

import java.util.Iterator;
import java.util.List;

import nenok.jtconstr.vvll.VVLL_A;
import nenok.jtconstr.vvll.ValuationContainer;
import nenok.jtconstr.vvll.VariableContainer;
import nenok.lc.JoinTree;
import nenok.lc.Node;
import nenok.va.Domain;
import nenok.va.Variable;

/**
 * The static sequence join tree construction algorithm. The user predefines an elimination 
 * sequence in the constructor of this algorithm class. The join tree will be construted by 
 * variable elimination respecting the order of the variable array.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 554 $<br/>$LastChangedDate: 2008-03-26 14:19:57 +0100 (Mi, 26 Mrz 2008) $
 */

public class Sequence extends Elimination {

	private Variable[] elimSequence;
	
	/**
	 * Constructor:
	 * @param sequence The elimination sequence.
	 */
	
	public Sequence(Variable[] sequence) {
		this.elimSequence = sequence;
	}
	
	/**
     * @see nenok.jtconstr.Algorithm#buildTree(nenok.lc.JoinTree.Construction)
	 */
	
	public Node buildTree(JoinTree.Construction data) throws ConstrException {
		        
		// Construct VVLL:
		VVLL_A vvll = new VVLL_A(data);
		
		// Has a valid elimination sequence been chosen:
		Domain elim = new Domain(elimSequence);
        Domain total = Domain.union(data.knowledgebase.getDomain(), Domain.union(data.queries));
        if(!elim.subSetOf(total)) {
            throw new ConstrException("Invalide construction sequence. The variables "+Domain.difference(elim, total)+" are not contained in the knowledgebase.");   
        }
        if(elim.size() != total.size()) {
            throw new ConstrException("The elimination sequence does not include all knowledgebase variables.");   
        }
		
		// This empty node is used to connect multiple roots to one single root at the end of the algorithm:
		Node emptyNode = data.getNodeInstance(Domain.EMPTY);
		
        // Result variable:
        Node root = emptyNode;

		// If one domain of the initial valuation set is empty, connect it directly at this place.
		Iterator<Node> it = vvll.getValuationContainerNodes().iterator();
		while (it.hasNext()) {
			Node node = it.next();
			if (node.getLabel().size() == 0) {
				emptyNode.addParent(node);
				node.setChild(emptyNode);
			}
		}

		// Foreach variable container:
		int size = vvll.getVariableContainerSize();
        for (int i = 0; i < size; i++) {

			// Eliminate the container specified by the elimination sequence:
			VariableContainer varCon = vvll.removeVariableContainer(elimSequence[i]);
			Variable var = varCon.getVariable();

			// Calculate the domain of the new element.
			Domain[] nodeDomains = new Domain[varCon.getValuationContainerRefs().size()];
			int counter = 0;
			Iterator<ValuationContainer> valit = varCon.getValuationContainerRefs().iterator();
			while (valit.hasNext()) {
				ValuationContainer valCon = valit.next();
				nodeDomains[counter] = valCon.getNode().getLabel();
				counter++;
			}
			
            List<Variable> vlist = Domain.union(nodeDomains).asList();
            vlist.remove(var);
			Domain newDomain = new Domain(vlist); 
			
			Domain unionDomain = Domain.union(nodeDomains);
			            
			Node unionNode = data.getNodeInstance(unionDomain);

			// Construct a neutral valuation with this domain and add it to the vvll.
			Node node = data.getNodeInstance(newDomain);
			ValuationContainer neutralValCon = new ValuationContainer(node);
			vvll.addValuationContainer(neutralValCon);

			// Add variable container references to the neutral valuation container.
			// Foreach valuation container in the current variable container:
			int valSize = varCon.getValuationContainerRefs().size();
			for (int l = 0; l < valSize; l++) {

				ValuationContainer valCon = varCon.getValuationContainerRefs().get(l);

				// Remove current variable from valuation's variable container list.
				valCon.getVariableContainerRefs().remove(varCon);
				neutralValCon.addVariableContainer(valCon.getVariableContainerRefs());

				// unionNode is the new father node of all valCon's node.	
				unionNode.addParent(valCon.getNode());
			}

			// The neutral element is the new father node of unionNode.
			neutralValCon.addChildNode(unionNode);

			// If the neutral element has an empty domain, this node is directly connected with the root node.
			if ((newDomain.size() == 0) && (i != size - 1)) {
				emptyNode.addParent(neutralValCon.getNode());
			}

			// If the current variable is the last variable:
			if (i == size - 1) {

				if (emptyNode.getParents().isEmpty()) {

					// the node in the neutralValCon is the jointree root:
					root = neutralValCon.getNode();

				} else {
					// there are other empty nodes:
					emptyNode.addParent(neutralValCon.getNode());
					root = emptyNode;
				}
			}

			// Foreach valuation container in the current variable container:
			for (int l = 0; l < valSize; l++) {

				ValuationContainer valCon = varCon.getValuationContainerRefs().get(l);

				// Foreach of its variable containers:
				int innerVarSize = valCon.getVariableContainerRefs().size();
				for (int j = 0; j < innerVarSize; j++) {

					VariableContainer innerVarCon = valCon.getVariableContainerRefs().get(j);

					// Replace the current valuation from variable's valuation container list by the new neutral element.
					innerVarCon.getValuationContainerRefs().remove(valCon);
					innerVarCon.addValuationContainer(neutralValCon);

				}
				// Remove the valuation container.
				vvll.removeValuationContainer(valCon);
			}
		}
        
        // Shrink jointree:
        optimize(root, data);
        
		return root;
	}
    
	/**
     * @see nenok.jtconstr.Elimination#getEliminationSequence()
     */
    
    public Variable[] getEliminationSequence() {
        return elimSequence;
    }
    
    /**
     * @see nenok.jtconstr.Algorithm#getName()
	 */

	public String getName() {
		return "Static Sequence Algorithm";
	}
}
