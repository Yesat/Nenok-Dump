package nenok.jtconstr;

import java.util.ArrayList;
import java.util.List;

import nenok.jtconstr.vvll.VVLL_H;
import nenok.jtconstr.vvll.ValuationContainer;
import nenok.jtconstr.vvll.VariableContainerHeap;
import nenok.lc.JoinTree;
import nenok.lc.Node;
import nenok.va.Domain;
import nenok.va.Valuation;
import nenok.va.Variable;

/**
 * The OSLA-SC (One-Step-Look-Ahead Smallest-Clique) join tree construction 
 * algorithm. 
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 554 $<br/>$LastChangedDate: 2008-03-26 14:19:57 +0100 (Mi, 26 Mrz 2008) $
 */

public class OSLA_SC extends Elimination {
	
	private Variable[] elimSequence;

    /**
     * @see nenok.jtconstr.Algorithm#buildTree(nenok.lc.JoinTree.Construction)
     */
    
	public Node buildTree(JoinTree.Construction data) throws ConstrException {
        				
		// Construct VVLL:
		VVLL_H vvll = new VVLL_H(data);
		
		// Elimination sequence:
		elimSequence = new Variable[vvll.getVariableContainerSize()];

		// This empty node is used to connect multiple roots to one single root at the end of the algorithm:
		Node emptyNode = data.getNodeInstance(Domain.EMPTY);
		
        // Result variable:
        Node root = emptyNode;

		// If one domain of the initial valuation set is empty, connect it directly at this place.
        for(Node node : vvll.getValuationContainerNodes()) {
            if (node.getLabel().size() == 0) {
                emptyNode.addParent(node);
                node.setChild(emptyNode);
            }            
        }
                
        int size = vvll.getVariableContainerSize();

		// Foreach variable container:
		for (int i = 0; i < size; i++) {

			// Eliminate the container with lowest costs.
			VariableContainerHeap varCon = vvll.removeVariableContainer();
			Variable var = varCon.getVariable();

			// Store the eliminated variable. 
			elimSequence[i] = var;
			
			// Calculate the domain of the new element.
            int counter = 0;
            Domain[] nodeDomains = new Domain[varCon.getValuationContainerRefs().size()];
            for(ValuationContainer valCon : varCon.getValuationContainerRefs()) {
                nodeDomains[counter] = valCon.getNode().getLabel();
                counter++;               
            }
            
            Domain unionDomain = Domain.union(nodeDomains);
            
            List<Variable> vlist = unionDomain.asList();
            vlist.remove(var);
			Domain newDomain = new Domain(vlist); 
		
            Node unionNode = data.getNodeInstance(unionDomain);

			// Construct a neutral valuation with this domain and add it to the vvll.
            Node node = data.getNodeInstance(newDomain);
            ValuationContainer neutralValCon = new ValuationContainer(node);
            vvll.addValuationContainer(neutralValCon);

			// Add variable container references to the neutral valuation container.
			// Foreach valuation container in the current variable container:
			for(ValuationContainer valCon : varCon.getValuationContainerRefs()) {
			
				// Remove current variable from valuation's variable container list.
				valCon.getVariableContainerRefs().remove(varCon);
				neutralValCon.addVariableContainer(valCon.getVariableContainerRefs());

				// unionNode is the new father node of all valCon's node.	
                unionNode.addParent(valCon.getNode());
			}

			// The neutral element is the new father node of unionNode.
            neutralValCon.addChildNode(unionNode);

			// If the neutral element has an empty domain, this node is directly connected with the root node.
            if ((newDomain.size() == 0) && (i != size - 1)) {
                emptyNode.addParent(neutralValCon.getNode());
            }

			// If the current variable is the last variable:
			if (i == size - 1) {

				if (emptyNode.getParents().isEmpty()) {

					// the node in the neutralValCon is the jointree root:
					root = neutralValCon.getNode();

				} else {
					// there are other empty nodes:
					emptyNode.addParent(neutralValCon.getNode());
					root = emptyNode;
				}
			}

			// Foreach valuation container in the current variable container:
			for(ValuationContainer valCon : varCon.getValuationContainerRefs()) {

				// Foreach of its variable containers:
				int innerVarSize = valCon.getVariableContainerRefs().size();
				for (int j = 0; j < innerVarSize; j++) {

					VariableContainerHeap innerVarCon = (VariableContainerHeap) valCon.getVariableContainerRefs().get(j);

					// Replace the current valuation from variable's valuation container list by the new neutral element.
					innerVarCon.getValuationContainerRefs().remove(valCon);
					innerVarCon.addValuationContainer(neutralValCon);

					// Calculate new costs of the variable container. Cardinality of the total union of each valuation's domain.

					int innerValSize = innerVarCon.getValuationContainerRefs().size();
					nodeDomains = new Domain[innerValSize];
					ArrayList<Valuation> vals = new ArrayList<Valuation>();

					for (int k = 0; k < innerValSize; k++) {
						ValuationContainer innerValCon = innerVarCon.getValuationContainerRefs().get(k);
						Valuation content = innerValCon.getNode().getContent();
						vals.add(content);
						nodeDomains[k] = innerValCon.getNode().getLabel();
					}

					if (vals.size() == 1 || vals.size() == 0) {
						
                        // If the current variable is a leaf
						innerVarCon.setCost(0);
					} else {
						
                        // If the current variable is not a leaf
						innerVarCon.setCost(Domain.union(nodeDomains).size());
					}
					
                    // After changing the costs, the heap must be updated.
					vvll.updateHeap(vvll.getHeapPosition(innerVarCon));

					vals.clear();
				}
				
                // Remove the valuation container.
				vvll.removeValuationContainer(valCon);
			}
		}
	
        // Optimize join tree:
        optimize(root, data);
		return root;
	}

    /**
     * @see nenok.jtconstr.Algorithm#getName()
     */

	public String getName() {
		return "OSLA SC";
	}

    /**
     * @see nenok.jtconstr.Elimination#getEliminationSequence()
     */
    
	public Variable[] getEliminationSequence() {
		return elimSequence;
	}
}
