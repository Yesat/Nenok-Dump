package nenok.va;

import java.io.Serializable;

/**
 * This marker interface represents a generic variable. Variable objects
 * are serializable such that they can be transmitted between network hosts.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 559 $<br/>$LastChangedDate: 2008-03-26 14:25:18 +0100 (Mi, 26 Mrz 2008) $
 */

public interface Variable extends Serializable {
    
	// Marker interface ...
    
}
