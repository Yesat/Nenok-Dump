package nenok.va;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * The domain of a valuation is a set of variables and this class offers a
 * corresponding pre-implementation. Domains are serializable such that they 
 * can be transmitted between network hosts. Furthermore, this type is immutable
 * for synchronization issues.
 * 
 * @author Marc Pouly
 */

public final class Domain implements Serializable, Iterable<Variable> {
	
	/**
	 * Empty domain as a constant.
	 */
	
	public static final Domain EMPTY = new Domain();

	/*
	 * Fields:
	 */
	
	private final HashSet<Variable> domain; 

	/**
	 * Constructor:
	 * @param vars An arbitrary number of variables.
	 */

	public Domain(Variable... vars) {
		domain = new HashSet<Variable>(vars.length);
		for(Variable v : vars) {
			domain.add(v);
		}
	}
	
	/**
	 * Constructor:
	 * @param varrays An arbitrary number of variable arrays.
	 */

	public Domain(Variable[] array1, Variable[]... varrays) {
		domain = new HashSet<Variable>();
		for(Variable v : array1) {
			domain.add(v);
		}
		for(Variable[] vars : varrays) {
			for(Variable v : vars) {
				domain.add(v);
			}
		}
	}
	
	/**
	 * Constructor:
	 * @param vars A collection of variables.
	 */
	
	public Domain(Collection<Variable> vars) {
		this.domain = new HashSet<Variable>(vars);
	}
	
	/**
	 * Internal Constructor for performance reasons.
	 * @param domain The hashset that constitutes a domain's field.
	 */
	
	private Domain(HashSet<Variable> domain) {
		this.domain = domain;
	}
	
	/**
	 * @return The domain's size, i.e. the number of variables in the current domain.
	 */

	public int size() {
		return domain.size();
	}

	/**
	 * @return The domain's variable iterator.
     * @see java.lang.Iterable#iterator()
	 */
	
	public Iterator<Variable> iterator() {
		return this.domain.iterator();
	}
    
    /**
     * Tests, if this domain is a subset of the domain given as argument, i.e. if all variables of 
     * <code>this</code> are contained within the argument.
     * @param dom The domain which is tested for superset.
     * @return <code>true</code>, if <code>this</code> is a subset of <code>dom</code>.
     */
    
    public boolean subSetOf(Domain dom) {
        return dom.domain.containsAll(this.domain);
    }
    
    /**
     * Does the current domain contain a given variable.
     * Attention: This method is very slow - don't use it if possible.
     * @param var The variable to find in the current domain.
     * @return <code>true</code>, if <code>this</code> contains the given variable.
     */

    public boolean contains(Variable var) {
        return domain.contains(var);
    }

    /**
     * Returns the current domain as an array of variables.
     * @return An array containing the domain's variables.
     */
    
    public Variable[] toArray() {
    	Variable[] result = new Variable[this.size()];
        Iterator<Variable> it = this.iterator();
        for(int i = 0; it.hasNext(); i++) {
            result[i] = it.next();
        }
        return result;
    }
    
    /**
     * Returns the current domain as an array of variables.
     * @param <T> The exact tape of the return variable array.
     * @param c The array type of the result.
     * @return An array containing the domain's variables.
     */
   
	@SuppressWarnings("unchecked")
    public <T extends Variable> T[] toArray(Class<T> c) {
		T[] result = (T[])Array.newInstance(c, this.size());
        Iterator<Variable> it = this.iterator();
        for(int i = 0; it.hasNext(); i++) {
            result[i] = (T)it.next();
        }
        return result;
    }
    
    /**
     * Returns the current domain as a list of variables.
     * @return A list containing the domain's variables.
     */
    
    public List<Variable> asList() {
    	return new ArrayList<Variable>(this.domain);
    }
    
    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    
    public boolean equals(Object o) {
        if(!(o instanceof Domain)) {
            return false;
        }
        if(this == o) {
        	return true;
        }
        Domain dom = (Domain)o;
        return this.subSetOf(dom) && dom.subSetOf(this);
    }
    
    /**
     * @see java.lang.Object#toString()
     */

    public String toString() {
        String result = "{";
        for(Iterator<Variable> it = domain.iterator(); it.hasNext();) {
            Variable var = it.next();
            result += var.toString();
            if(it.hasNext()) {
                result += ", ";
            }
        }
        return result+"}";
    }
    
    /**
     * @see java.lang.Object#hashCode()
     */
    
    public int hashCode() {
    	return domain.hashCode();
    }
    
    /*
     * Static methods:
     */
    
	/**
	 * Computes the union of the two domains.
	 * @param dom1 The first domain.
	 * @param dom2 The second domain.
	 * @return The union of <code>dom1</code> and <code>dom2</code>.
	 */

	public static Domain union(Domain dom1, Domain dom2) {
        HashSet<Variable> result = new HashSet<Variable>();
        result.addAll(dom1.domain);
        result.addAll(dom2.domain);
    	return new Domain(result);
	}
    
    /**
     * Computes the union of an array of domains.
     * @param domains An array of domains.
     * @return The domain corresponding to the union of all array elements.
     */
    
    public static Domain union(Domain[] domains) {
    	if(domains.length == 0) {
    		return EMPTY;
    	}
    	if(domains.length == 1) {
    		return domains[0];
    	}
    	HashSet<Variable> result = new HashSet<Variable>();
        for(int i = 0; i < domains.length; i++) {
        	result.addAll(domains[i].domain);
        }
    	return new Domain(result);
    }
    
    /**
     * Computes the union of a collection of domains.
     * @param domains An collection of domains.
     * @return The domain corresponding to the union of all argument domains.
     */
    
    public static Domain union(Collection<Domain> domains) {
    	if(domains.size() == 0) {
    		return EMPTY;
    	}
        HashSet<Variable> result = new HashSet<Variable>();
        for(Domain dom : domains) {
        	result.addAll(dom.domain);
        }
    	return new Domain(result);
    }
 
    /**
     * Computes the union domain of an array of {@link Valuation}s.
     * @param factors An array of valuations.
     * @return The domain corresponding to the union of all factor domains.
     */
    
    public static Domain union(Valuation[] factors) {
    	ArrayList<Domain> result = new ArrayList<Domain>();
        for(int i = 0; i < factors.length; i++) {
        	result.add(factors[i].label());
        }
    	return Domain.union(result);
    }
    
	/**
     * Computes the intersection of the two domains.
	 * @param dom1 The first domain.
	 * @param dom2 The second domain.
	 * @return The intersection of <code>dom1</code> and <code>dom2</code>.
	 */
	
	public static Domain intersection(Domain dom1, Domain dom2) {
		HashSet<Variable> result = new HashSet<Variable>();
		result.addAll(dom1.domain);
		result.retainAll(dom2.domain);
		return new Domain(result);
	}
	
	/**
     * Computes the difference of the two domains.
	 * @param dom1 The first domain.
	 * @param dom2 The second domain.
	 * @return The difference <code>dom1 / dom2</code>.
	 */
	
	public static Domain difference(Domain dom1, Domain dom2) {
        HashSet<Variable> result = new HashSet<Variable>(dom1.domain);
        result.removeAll(dom2.domain);
		return new Domain(result);
	}
	
	/**
	 * Computes the union domain of a collection of valuations.
	 * @param vals The collection of valuations.
	 * @return The domain corresponding to the union of all valuation's labels.
	 */

	public static Domain totalDomain(Collection<Valuation> vals) {
		HashSet<Variable> result = new HashSet<Variable>();
		for (Valuation val : vals) {
			result.addAll(val.label().domain);
		}
		return new Domain(result);
	}
	
	/**
	 * Computes the union domain of an array of valuations.
	 * @param vals The array of valuations.
	 * @return The domain corresponding to the union of all valuation's labels.
	 */	
	
	public static Domain totalDomain(Valuation[] vals) {
		HashSet<Variable> result = new HashSet<Variable>();
		for (Valuation val : vals) {
			result.addAll(val.label().domain);
		}
		return new Domain(result);
	}
				
	/**
	 * Builds the powerset of the domain given as argument.
	 * @param dom The domain whose powerset shall be built.
	 * @return The powerset of the given domain.
	 */
	
	public static List<Domain> powerSet(Domain dom) {
		ArrayList<Domain> result = new ArrayList<Domain>(), temp = new ArrayList<Domain>();
		for(Variable v : dom) {
			temp.clear();
			for(Domain d : result) {
				HashSet<Variable> set = new HashSet<Variable>(d.domain);
				set.add(v);
				temp.add(new Domain(set));
			}
			result.addAll(temp);
			Domain d = new Domain(v);
			result.add(d);
		}
		result.add(new Domain());
		return result;
	}
}