package nenok.va;

/**
 * This interface makes a valuation algebra weight predictable. Weight predictors only
 * depend on the domain of a valuation and not on the valuation object itself. One must
 * be able to call the weight predictor, even if no corresponding valuation exists.
 * This actually demands a static implementation. On the other hand, each valuation algebra 
 * possesses its own weight predictor, which makes the static design approach impossible.
 * <br/><br/>
 * The workaround presented here is simple. We create weight predictors as an external object
 * and this class simply returns such an object that effectively computes the weight. Doing so, 
 * we may give a pre-implementation of the weight function within this class' implementor.
 * Use it in the following way:
 * <code>
 * public int weight() {
 *    return Predictability.Implementor.getInstance().weight(this);
 * }
 * </code>
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 559 $<br/>$LastChangedDate: 2008-03-26 14:25:18 +0100 (Mi, 26 Mrz 2008) $
 */

public interface Predictability extends Valuation {
 
    /**
     * @return A predictor instance corresponding to this valuation algebra.
     */
    
    public Predictor predictor();
    
    /**
     * Delegator class for the {@link Predictability} interface.
     * 
     * @author Marc Pouly
     * @version 1.4
     */
    
    public class Implementor {
    	
    	// SINGLETON design pattern:
    	private static Implementor implementor;
    	    	
    	/**
    	 * Constructor:
    	 */
    	
    	private Implementor() {}
    	
    	/**
    	 * @return An instance of the implementor class.
    	 */
    	
    	public static Implementor getInstance() {
    		if(implementor == null) {
    			implementor = new Implementor();
    		}
    		return implementor;
    	}

        /**
         * Pre-implementation of the weight method.
         * @param val The valuations whose weight has to be computed.
         * @return The weight of the given valuation.
         */
        
        public int weight(Predictability val) {
        	return val.predictor().predict(val.label());
        }    
    }   
}
