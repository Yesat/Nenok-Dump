package nenok.va;

/**
 * Specifies an idempotent valuation. Idempotency is basically a trivial division, 
 * therefore this interface extends {@link Separativity} and provides furthermore 
 * a pre-implementation of the division operator. The same holds for scalability since 
 * all idempotent valuations are trivially scaled. However, to prevent the user to be 
 * reliant on abstract classes (because multiple inheritance is not allowed in JAVA), 
 * the interface offers a delegator inner class that implements division and scaling. 
 * This delegator can be used as follows:
 * <code>
 * public Separativity inverse() {
 *    return Idempotency.Implementor.getInstance().inverse(this);
 * }
 * </code>
 * or
 * <code>
 * public Scalability scale() {
 *    return Idempotency.Implementor.getInstance().scale(this);
 * }
 * </code>
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 559 $<br/>$LastChangedDate: 2008-03-26 14:25:18 +0100 (Mi, 26 Mrz 2008) $
 */

public interface Idempotency extends Scalability {
    
    /*
     * Marker interface:
     */
    
    /**
     * Delegator class for the {@link Idempotency} interface.
     * 
     * @author Marc Pouly
     * @version 1.4
     */
    
    public class Implementor {
    	
    	// SINGLETON design pattern:
    	private static Implementor implementor = new Implementor();
    	    	
    	/**
    	 * @return An instance of the implementor class.
    	 */
    	
    	public static Implementor getInstance() {
    		return implementor;
    	}

        /**
         * Pre-implementation of the {@link Separativity} interface.
         * @param factor The factor whose inverse has to be computed.
         * @return The inverse of the factor.
         */
        
        public Separativity inverse(Idempotency factor) {
            return factor;
        }
        
        /**
         * Pre-implementation of the {@link Scalability} interface.
         * @param factor The factor whose scale has to be computed.
         * @return The scale of the factor.
         */
        
        public Scalability scale(Idempotency factor) {
            return factor;
        }        
    }
}