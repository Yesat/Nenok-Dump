package nenok.va;

/**
 * This implementation of the valuation interface represents an identity element in a
 * valuation algebra. It has been shown that all valuation algebras can artificially be 
 * extended to have such an identity element. Since identity elements are unique, the 
 * constructor's visibility is set to private and the single instance is avaiable through
 * the constant <code>Identity.INSTANCE</code>.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 559 $<br/>$LastChangedDate: 2008-03-26 14:25:18 +0100 (Mi, 26 Mrz 2008) $
 */

public final class Identity implements Idempotency {
	
	/**
	 * Unique instance of an identity element.
	 */
	
	public static final Identity INSTANCE = new Identity(); 
	
	/**
	 * Constructor:
	 */
	
	private Identity() {}
	
    /**
     * @see nenok.va.Valuation#weight()
     */
    
	public int weight() {
        return 0;
    }
	
    /**
	 * @see nenok.va.Valuation#label()
	 */

	public Domain label() {
		return Domain.EMPTY;
	}
	
	/**
	 * @see nenok.va.Valuation#combine(nenok.va.Valuation)
	 */

	public Valuation combine(Valuation val) {
        return val;
	}
	
	/**
	 * The marginalization to the empty set is the only possible 
	 * marginalization for identity elements.
	 * @see nenok.va.Valuation#marginalize(nenok.va.Domain)
	 */

	public Valuation marginalize(Domain dom) throws VAException {
		if(dom.size() == 0) {
			return INSTANCE;
		} 
		throw new VAException("Only marginalization to the empty domain is defined for identity elements.");	
	}
			
    /**
	 * @see java.lang.Object#toString()
	 */
	
	@Representor
	public String toString() {
		return "Identity Element";
	}
		    
    /**
     * @see nenok.va.Scalability#scale()
     */
    
    public Scalability scale() {
        return INSTANCE;
    }

    /**
     * @see nenok.va.Separativity#inverse()
     */
    
    public Separativity inverse() {
		return INSTANCE;
    }
}
