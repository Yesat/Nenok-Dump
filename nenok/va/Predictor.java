package nenok.va;

import java.io.Serializable;

/**
 * This interface represents weight predictors. It computes the weight of
 * a valuation from its domain. We recommend strongly to consider the following
 * design propositions:
 * <ul>
 * <li>Predictor objects are transmitted between network hosts. To assert smallest 
 * possible communication costs, we recomment to decouple this object completely from 
 * the implementation of the {@link Valuation} object.
 * </li>
 * <li>In fact, only one single predictor instance is needed. We recommend therefore 
 * to implement this class as a SINGLETON design pattern.<li>
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 559 $<br/>$LastChangedDate: 2008-03-26 14:25:18 +0100 (Mi, 26 Mrz 2008) $
 */

public interface Predictor extends Serializable {
    
    /**
     * Predicts the weight of a valuation from its domain.
     * @param dom The domain to compute the valuation's weight.
     * @return The predicted weight of the valuation with domain <code>dom</code>.
     */
    
    public int predict(Domain dom);
}
