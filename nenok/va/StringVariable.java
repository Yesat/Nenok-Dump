package nenok.va;

/**
 * This is a pre-implementation of the variable interface and represents named variables. 
 * Such variables are used (for example) in the context of kleene valuation algebras to identify matrix entries. 
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 559 $<br/>$LastChangedDate: 2008-03-26 14:25:18 +0100 (Mi, 26 Mrz 2008) $
 */

public class StringVariable implements Variable, Comparable<StringVariable> {
	
	private final String name;
	
	/**
	 * Constructor:
	 * @param name The name of the variable. 
	 */
	
	public StringVariable(String name) {
		this.name = name;
	}
	
	/**
	 * @see java.lang.Object#hashCode()
	 */
		
	public int hashCode() {
		return name.hashCode();
	}
	
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	
	public boolean equals(Object o) {
		return (o instanceof StringVariable) ? this.compareTo((StringVariable)o) == 0 : false;
	}
	
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	
	public int compareTo(StringVariable arg) {
		return name.compareTo(arg.name);
	}
	
	/**
	 * Converts the current variable to a string.
	 * @return The variable's string value.
	 */

	public String toString() {
		return name;
	}
}
