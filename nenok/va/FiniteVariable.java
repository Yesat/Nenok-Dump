package nenok.va;

/**
 * This is an extension of named variables to variables with finite frames. 
 * Such variables are used (for example) in the context of semiring valuation algebras to represent configurations.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 559 $<br/>$LastChangedDate: 2008-03-26 14:25:18 +0100 (Mi, 26 Mrz 2008) $
 */

public class FiniteVariable extends StringVariable {
	
	private final String[] frame;
	
	/**
	 * Constructor:
	 * @param name The name of the variable.
	 * @param frame The variable's possible values.	 
	 */
	
	public FiniteVariable(String name, String... frame) {
		super(name);
		this.frame = frame;
	}
	
	/**
	 * Returns the frame of this discrete variable.
	 * @return The variable's frame.
	 */
	
	public String[] getFrame() {
		return this.frame;
	}
	
    /**
     * Computes the number of configurations of the given variable array.
     * @param vars An array of variables with finite frames.
     * @return The number of configurations that can be built from the argument array.
     */
    
    public static int countConfigurations(FiniteVariable[] vars) {
        int size = 1;
        for (int i = 0; i < vars.length; i++) {
            size *= vars[i].getFrame().length;
        }
        return size;           
    }
}
