package nenok.va;

/**
 * Specifies a scalable valuation. Scaling is basically an application of the division 
 * operator and therefore this interface extends {@link Separativity}. Based on the 
 * division operator in its super-interface, the scaling operation can be pre-implemented. 
 * In order to prevent the user to be reliant on abstract classes (because multiple inheritance 
 * is not allowed in JAVA), this interface offers a delegator inner class that reduces scaling on
 * the division operator. Therefore, the user can implement this interface as follows:
 * <code>
 * public Scalability scale() {
 *    return Scalability.Implementor.getInstance().scale(this);
 * }
 * </code>
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 559 $<br/>$LastChangedDate: 2008-03-26 14:25:18 +0100 (Mi, 26 Mrz 2008) $
 */

public interface Scalability extends Separativity {
	
	/**
	 * Scales the current valuation. 
	 * @return The scaled version of this valuation.
	 */
	
	public Scalability scale();
        
    /**
     * Delegator class for the {@link Scalability} interface.
     * 
     * @author Marc Pouly
     * @version 1.1
     */
    
    public class Implementor {
    	
    	// SINGLETON design pattern:
    	private static Implementor implementor = new Implementor();
    	    	
    	/**
    	 * Constructor:
    	 */
    	
    	private Implementor() {}
    	
    	/**
    	 * @return An instance of the implementor class.
    	 */
    	
    	public static Implementor getInstance() {
    		return implementor;
    	}
        
        /**
         * Pre-implementation of the {@link Scalability} interface. In detail: It computes the inverse 
         * of the current valuation with respect to the empty domain and returns the combination of
         * <code>this</code> and the inverse element. The original valuation stays untouched.
         * @param scaler The valuation to scale.
         * @return The combination of <code>scaler</code> and its inverse.
         */
        
        public Scalability scale(Scalability scaler) {
        	try {
				Scalability v = (Scalability)scaler.marginalize(Domain.EMPTY);
				v = (Scalability)v.inverse();
				return (Scalability)(scaler.combine(v));
			} catch (VAException e) {
				// Cannot happen ...
				e.printStackTrace();
				return null;
			}
        }
    }
}
