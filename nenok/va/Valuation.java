package nenok.va;

/**
 * This interface specifies a general valuation by its principal operations of 
 * labeling, marginalization &amp; combination. Ensure that all axioms of a 
 * valuation algebra are respected when implementing this interface. Additionally, 
 * this interface provides a weight function in order to compute communication costs.
 * Valuation objects are serializable such that they can be transmitted between 
 * network hosts.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 559 $<br/>$LastChangedDate: 2008-03-26 14:25:18 +0100 (Mi, 26 Mrz 2008) $
 */

public interface Valuation {
	
	/**
	 * @return The domain of this object.
	 */
	
	public Domain label();
	
	/**
	 * Weight function:
	 * @return The valuation's weight.
	 */
	
	public int weight();
		
	/**
	 * Combination of valuations.
	 * @param val The second valuation involved in the combination.
	 * @return The combination of <code>this</code> and <code>val</code>.
	 */
	
	public Valuation combine(Valuation val);
	
	/**
	 * Marginalization of a valuation onto a given domain.
	 * @param dom The domain onto this valuation is marginalized.
	 * @return The marginalization of <code>this</code> to <code>dom</code>.
	 * @throws VAException Exception occuring when a marginalization is performed onto an illegal domain.
	 * Throwing this exception is the recommended way to implement partial marginalization.
	 */
	
	public Valuation marginalize(Domain dom) throws VAException;
}
