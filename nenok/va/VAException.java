package nenok.va;

import nenok.lc.LCException;

/**
 * This class represents exceptions occuring during computations in a valuation algebra. 
 * VAException stands therefore for Valuation Algebra Exception. Typically, such exceptions 
 * are thrown if illegal valuation algebra operations are performed.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 559 $<br/>$LastChangedDate: 2008-03-26 14:25:18 +0100 (Mi, 26 Mrz 2008) $
 */

public class VAException extends LCException {
   
    /**
     * Constructor:
     * @param message The exception message.
     */
    
    public VAException(String message) {
        super(message);
    }
    
    /**
     * Constructor:
     * @param message The exception message.
     * @param cause The (prior) exception to be wrapped.
     */
    
    public VAException(String message, Throwable cause) {
        super(message, cause);
    }
}
