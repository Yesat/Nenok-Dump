package nenok.va;

import java.util.ArrayList;

import nenok.semiring.Dividability;
import nenok.sva.SRValuation;

/**
 * This interface equips a valuation algebra with some concept of division.
 * NENOK does not distinguish between separative and regular valuation algebras,
 * both are represented by this interface. The division itself is realized by a 
 * single method that computes the inverse element of the current valuation.
 * 
 * @author Marc Pouly
 */

public interface Separativity extends Valuation {
	
	/**
	 * Returns an inverse element for the current valuation.
	 * @return The inverse valuation of the given domain.
	 */
	
	public Separativity inverse();
	
	/**
	 * Implementor class of this interface for separative semiring valuations.
	 * 
	 * @author Marc Pouly
	 * @version 1.4
	 */
	
	public class Implementor {
		
		// Singleton design pattern:
		private static Implementor implementor;
		
		/**
		 * Constructor:
		 */
		
		private Implementor() {}
		
		/**
		 * @return An instance of this implementor class.
		 */
		
		public static Implementor getInstance() {
			if(implementor == null) {
				implementor = new Implementor();
			}
			return implementor;
		}
		
		/**
		 * @param val The separative semiring valuation.
		 * @return The inverse element of a separative semiring valuation.
		 */
		
		public <E> Separativity inverse(SRValuation<E> val) {
			Dividability<E> semiring = (Dividability<E>)val.getSemiring(); 
			ArrayList<E> inverses = new ArrayList<E>();
	    	for (int i = 0; i < val.getValues().size(); i++) {
				inverses.add(semiring.inverse(val.getValues().get(i)));
			}
	    	return (Separativity)val.create(val.getVariables(), semiring, inverses);
		}
	}
}

