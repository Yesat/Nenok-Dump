package nenok.path;

import nenok.semiring.Quasiregular;
import nenok.va.Domain;
import nenok.va.Identity;
import nenok.va.Predictor;
import nenok.va.Representor;
import nenok.va.VAException;
import nenok.va.Valuation;

/**
 * Specifies a quasi-regular semiring induced valuation algebra by leading its principal operations of 
 * marginalization &amp; combination back to the according operations in a quasi-regular semiring.
 * 
 * @author Marc Pouly
 */

public abstract class QValuation<E> implements Valuation {
	
	private final SquareMatrix<E> matrix;
	private final Vector<E> vector;
	private final Quasiregular<E> semiring;
	
	/*
	 * Predictor Implementation:
	 */
	
	private static final Predictor predictor = new Predictor() {
		
		/**
		 * @see nenok.va.Predictor#predict(nenok.va.Domain)
		 */
		
		public int predict(Domain dom) {
			return dom.size()*dom.size();
		};
	};
		
	/**
	 * Constructor:
	 * Each quasi-regular valuation consists of a square matrix and a vector of equal domain.
	 * @param matrix The matrix of this valuation.
	 * @param vector The vector of this valuation.
	 * @param semiring The quasi-regular semiring for the elementary operations.
	 */
	
	public QValuation(SquareMatrix<E> matrix, Vector<E> vector, Quasiregular<E> semiring) {
		this.vector = vector;
		this.matrix = matrix;
		this.semiring = semiring;		
		if(!vector.label().equals(matrix.label())) {
			throw new IllegalArgumentException("Vector and matrix domains do not correspond.");
		}
	}
	
	/**
	 * Factory method:
	 * Each quasi-regular valuation consists of a square matrix and a vector of equal domain.
	 * @param matrix The matrix of this valuation.
	 * @param vector The vector of this valuation.
	 * @param semiring The quasi-regular semiring for the elementary operations.
	 * @return A new instance of this class.
	 */
	
	public abstract QValuation<E> create(SquareMatrix<E> matrix, Vector<E> vector, Quasiregular<E> semiring);
	
	/**
	 * @see nenok.adapt.Labeled#label()
	 */

	public Domain label() {
		return vector.label();
	}
	
	/**
	 * @see nenok.va.Valuation#weight()
	 */

	public int weight() {
		return predictor().predict(label());
	}

	/**
	 * @see nenok.va.Predictability#predictor()
	 */

	public Predictor predictor() {
		return predictor;
	}
	
	/**
	 * @see nenok.va.Valuation#combine(nenok.va.Valuation)
	 */
	
	public Valuation combine(Valuation arg) {
		
		if(arg instanceof Identity) {
			return this;
		}
		
		if(!arg.getClass().equals(this.getClass())) {
			throw new IllegalArgumentException("Unable to combine two valuations of different type.");
		}
		
		@SuppressWarnings("unchecked")
		QValuation<E> val = (QValuation<E>)arg;
		
		Matrix<E> matrix = val.matrix.add(this.matrix, semiring);
		Vector<E> vector = val.vector.add(this.vector, semiring);		
		
		return create((SquareMatrix<E>)matrix, vector, semiring);
	}
	
	/**
	 * @see nenok.va.Valuation#marginalize(nenok.va.Domain)
	 */
	
	public Valuation marginalize(Domain dom) throws VAException {
		
		Domain diff = Domain.difference(label(), dom);
		
		/*
		 * Matrix component:
		 */
		
		Matrix<E> restrict = matrix.restrict(dom, diff);
		Matrix<E> closure = matrix.restrict(diff).closure(semiring);
		
		Matrix<E> resmat = restrict.multiply(closure, semiring);
		resmat = resmat.multiply(matrix.restrict(diff, dom), semiring);
		resmat = resmat.add(matrix.restrict(dom, dom), semiring);
		
		/*
		 * Vector component:
		 */
	
		Vector<E> resvec = vector.restrict(diff);
		resvec = closure.multiply(resvec, semiring);
		resvec = restrict.multiply(resvec, semiring);
		resvec = resvec.add(vector.restrict(dom), semiring);
		
		return create((SquareMatrix<E>)resmat, resvec, semiring);
	}
		
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof QValuation)) {
			return false;
		}
		
		@SuppressWarnings("unchecked")
		QValuation<E> arg = (QValuation<E>)obj;
		
		return matrix.equals(arg.matrix) && vector.equals(arg.vector);
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	
	@Representor @Override
	public String toString() {
		return "TODO: Implementation still missing.";
	}
		
		//int max1 = 0;
		
        /*
         * Find string of maximum length in distance matrix:
         */
		
		/*Variable[] vars = matrix.row;
        
        for (int i = 0; i < vars.length; i++) {
        	int length = vars[i].toString().length();
            if(length > max1) {
            	max1 = length;
            }
            
			length = semiring.valueToString(vector.values[i]).length();
			if(length > max1) {
				max1 = length;
			}
            
            for (int j = 0; j < vars.length; j++) {
				length = semiring.valueToString(matrix.matrix[i][j]).length();
				if(length > max1) {
					max1 = length;
				}
			}
        }
        
        max1++;
        
        String gap = "     ";
        String format1 = "", result = "", line1 = "", line2 = "";
        String format2 = "";
		
        /*
         * Prepare format string for title:
         */

        /*for (int i = 0; i < vars.length+1; i++) {
        	format1 += "|%"+(i+1)+"$-"+max1+"s";
        }
        format1 += "|";
        format2 += "|%"+0+"$-"+max1+"s";
        format2 += "|\n";

        /*
         * Output header line:
         */
        
        /*Object[] data = new Object[vars.length+1];
        for (int i = 0; i < vars.length; i++) {
            data[i+1] = vars[i];
        }
        data[0] = "";
        result += String.format(format1, data);
        result += gap;
        result += String.format(format2, data);
        
        /*
         * Horizontal line:
         */
        
        /*int length1 = String.format(format1, data).length();
        for (int i = 0; i < length1; i++) {
            line1 += "-";
        }
        int length2 = String.format(format2, data).length();
        for (int i = 0; i < length2-1; i++) {
            line2 += "-";
        }        
        
        String line = line1+gap+line2+"\n";
        result = line+result+line;
        
        /*
         * Prepare format string for configurations:
         */

        /*format1 = "|%"+1+"$-"+max1+"s";
        format2 = "|%"+1+"$"+max1+"s";
        for (int i = 0; i < vars.length; i++) {
        	format1 += "|%"+(i+2)+"$"+max1+"s";
        }
        format1 += "|";
        format2 += "|\n";
        
        /*
         * Configurations:
         */
        
        /*for (int i = 0; i < vars.length; i++) {
            Object row1[] = new Object[data.length];
            Object row2[] = new Object[1];
            for (int j = 1; j < vars.length+1; j++) {
                row1[j] = semiring.valueToString(matrix.matrix[j-1][i]);
            }
            
            row2[0] = semiring.valueToString(vector.get(vars[i]));
            
            row1[0] = vars[i];

            result += String.format(format1, row1);
            result += gap;
            result += String.format(format2, row2);
        }   
        
        result += line;     
		return result;
    }*/
}
