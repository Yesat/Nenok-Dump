package nenok.path;

import nenok.Utilities;
import nenok.semiring.Kleene;
import nenok.va.Domain;
import nenok.va.Idempotency;
import nenok.va.Identity;
import nenok.va.Predictability;
import nenok.va.Predictor;
import nenok.va.Representor;
import nenok.va.Scalability;
import nenok.va.Separativity;
import nenok.va.VAException;
import nenok.va.Valuation;
import nenok.va.Variable;

/**
 * Specifies a Kleene algebra induced valuation algebra by leading its principal operations of 
 * marginalization &amp; combination back to the according operations in a Kleene algebra.
 * 
 * @author Marc Pouly
 */

public abstract class KValuation<E> implements Idempotency, Predictability {
	
	private final SquareMatrix<E> matrix;
	private final Variable[][] predecessor;
	private final Kleene<E> semiring;
	
	/*
	 * Predictor Implementation:
	 */
	
	private static final Predictor predictor = new Predictor() {
		
		/**
		 * @see nenok.va.Predictor#predict(nenok.va.Domain)
		 */
		
		public int predict(Domain dom) {
			return dom.size()*dom.size();
		};
	};
	
	/**
	 * Constructor:
	 * @param vars The domain of this valuation as variable array which determines the matrix order.
	 * @param kleene A Kleene algebra.
	 * @param matrix The adjacency matrix.
	 * @param closure Flag to indicate if a closure has to be computed - this is for optimization issues.
	 */
	
	public KValuation(Variable[] vars, Kleene<E> kleene, SquareMatrix<E> matrix, boolean closure) {
		this(vars, kleene, matrix, null, closure);
	}
	
	/**
	 * Constructor:
	 * @param vars The domain of this valuation as variable array which determines the matrix order.
	 * @param kleene A Kleene algebra.
	 * @param matrix The adjacency matrix.
	 * @param succ The successor matrix of this Kleene valuation.
	 * @param closure Flag to indicate if a closure has to be computed - this is for optimization issues.
	 */
	
	public KValuation(Variable[] vars, Kleene<E> kleene, SquareMatrix<E> matrix, Variable[][] succ, boolean closure) {
		
		this.matrix = matrix;
		this.semiring = kleene;
		
		if(succ != null && succ.length != vars.length) {
			throw new IllegalArgumentException("Dimensions wrong !!!");
		}
		
		if(succ == null) {
			int size = matrix.label().size();
			this.predecessor = new Variable[size][size];
			
			for (int i = 0; i < predecessor.length; i++) {
				for (int j = 0; j < predecessor.length; j++) {
					predecessor[i][j] = vars[i];
				}
			}

		} else {
			this.predecessor = succ;
		}
			
		if(closure) {
			this.closure(); 
		}
	}
	
	/**
	 * Factory method:
	 * @param vars The domain of this valuation as variable array which determines the matrix order.
	 * @param kleene A Kleene algebra.
	 * @param matrix The adjacency matrix.
	 * @param succ The successor matrix of this Kleene valuation.
	 * @param closure Flag to indicate if a closure has to be computed - this is for optimization issues.
	 * @return A new instance of the appropriate subclass.
	 */
	
	public abstract KValuation<E> create(Variable[] vars, Kleene<E> kleene, SquareMatrix<E> matrix, Variable[][] succ, boolean closure);
	
	/**
	 * @see nenok.adapt.Labeled#label()
	 */

	public Domain label() {
		return matrix.label();
	}
	
	/**
	 * @see nenok.va.Valuation#marginalize(nenok.va.Domain)
	 */
	
	public Valuation marginalize(Domain dom) throws VAException {
		if(dom.equals(label())) {
		    return this;
		}
		if(!dom.subSetOf(label())) {
			new VAException("Marginalization impossible: this.label() = "+label()+", argument = "+dom+".");
		}
		
		Variable[] vars = matrix.getRowVariables();
		
		// Fill new variable array:
		Variable[] newvars = dom.toArray();
		for (int i = 0, j = 0; i < vars.length; i++) {
			if(!dom.contains(vars[i])) {
				continue;
			}
			newvars[j++] = vars[i];
		}
		
		// Fill new adjacency and predecessor matrix:
		
		@SuppressWarnings("unchecked")
		E[][] newmatrix = (E[][])(new Object[newvars.length][newvars.length]);
		
		Variable[][] newpred = new Variable[newvars.length][newvars.length];
		
		for (int i = 0; i < newvars.length; i++) {
			int j = Utilities.indexOf(vars, newvars[i]); 
			for (int k = 0; k < vars.length; k++) {
				int l = Utilities.indexOf(newvars, vars[k]);
				if(l < 0) {
					continue;
				}
				newmatrix[i][l] = matrix.values[j][k];
				newpred[i][l] = predecessor[j][k];
			}
		}
		
		SquareMatrix<E> m = new SquareMatrix<E>(newvars, newmatrix, semiring);
		
		return create(newvars, semiring, m, newpred, false); 
		
	}
	
	/**
	 * @see nenok.va.Valuation#combine(nenok.va.Valuation)
	 */
	
	public Valuation combine(Valuation arg) {
		
		if(arg instanceof Identity) {
			return this;
		}
		
		if(!arg.getClass().equals(this.getClass())) {
			throw new IllegalArgumentException("Unable to combine two valuations of different type.");
		}
		
		@SuppressWarnings("unchecked")
		KValuation<E> val = (KValuation<E>)arg;
		
        // New variable array:
        Domain newdom = Domain.union(label(), val.label());
        Variable[] newvars = newdom.toArray();
        
        // New adjacency and predecessor matrix:
		@SuppressWarnings("unchecked")
		E[][] newmatrix = (E[][])(new Object[newvars.length][newvars.length]);
		Variable[][] succ = new Variable[newvars.length][newvars.length];
        
        for (int i = 0; i < newmatrix.length; i++) {
        	
    		int k1 = Utilities.indexOf(matrix.getRowVariables(), newvars[i]);
    		int k2 = Utilities.indexOf(val.matrix.getRowVariables(), newvars[i]);
        	
        	for (int j = 0; j < newmatrix.length; j++) {
        		
        		/*
        		 * Get value from first factor:
        		 */
        		        		
        		if(k1 >= 0) {
            		int l1 = Utilities.indexOf(matrix.getRowVariables(), newvars[j]);
            		if(l1 >= 0) {
            			newmatrix[i][j] = matrix.values[k1][l1]; 
            			succ[i][j] = predecessor[k1][l1]; 
            		}	
        		}

        		/* 
        		 * Get value from second factor:
        		 */
        		        		
        		if(k2 >= 0) {
            		int l2 = Utilities.indexOf(val.matrix.getRowVariables(), newvars[j]);
            		if(l2 >= 0) {
            			E value = val.matrix.values[k2][l2];
            			
            			if(newmatrix[i][j] == null) {
            				newmatrix[i][j] = value;
            				succ[i][j] = val.predecessor[k2][l2];
            			} else {
            				
            				E sum = semiring.add(newmatrix[i][j], value);
            				
            				if(sum.equals(value)) {
            					succ[i][j] = val.predecessor[k2][l2];
            				} 
            				
            				newmatrix[i][j] = sum;
            			}
            			
            			//newmatrix[i][j] = (newmatrix[i][j] == null) ? value : kleene.add(newmatrix[i][j], value); 
            		}  
            	}
        		
        		/*
        		 * Vacuous Extension:
        		 */
        		
        		if(newmatrix[i][j] == null) {
        			newmatrix[i][j] = semiring.zero();
        			succ[i][j] = newvars[i];
        		}
        	}
		}
        
		SquareMatrix<E> m = new SquareMatrix<E>(newvars, newmatrix, semiring);
		
		return create(newvars, semiring, m, succ, true);
	}
	
	/**
	 * @see nenok.va.Scalability#scale()
	 */
	
	public Scalability scale() {
		return Idempotency.Implementor.getInstance().scale(this);
	}
	
	/**
	 * @see nenok.va.Separativity#inverse()
	 */

	public Separativity inverse() {
		return Idempotency.Implementor.getInstance().inverse(this);
	}
		
	/**
	 * @see nenok.va.Valuation#weight()
	 */

	public int weight() {
		return predictor().predict(label());
	}
	
	/**
	 * @see nenok.va.Predictability#predictor()
	 */

	public Predictor predictor() {
		return predictor;
	}
	
	/**
	 * Computes the closure of this kleene valuation.
	 */
	
	public void closure() {
		
		Variable[] vars = matrix.getRowVariables();
		
		for (int k = 0; k < vars.length; k++) {
			for (int i = 0; i < vars.length; i++) {
				for (int j = 0; j < vars.length; j++) {
					
					E closure = semiring.closure(matrix.values[k][k]);
					E value = semiring.multiply(matrix.values[i][k], semiring.multiply(closure, matrix.values[k][j]));
					E sum = semiring.add(matrix.values[i][j], value);
					
					// Test procedure:
					// System.out.print(matrix[i][j]+" min ("+matrix[i][k]+" + "+closure+" + "+matrix[k][j]+")");
					// System.out.println(" = "+sum);
					
					if(!matrix.values[i][j].equals(sum)) {
						
						// Test procedure:
						// System.out.println("Change predecessor because "+sum+" better than "+matrix[i][j]);
						
						predecessor[i][j] = predecessor[k][j];
					}
					
					matrix.values[i][j] = sum;
									
				}
			}
		}
		
		for (int i = 0; i < vars.length; i++) {
			E sum = semiring.add(matrix.values[i][i], semiring.unit());
			if(!sum.equals(matrix.values[i][i])) {
				predecessor[i][i] = vars[i];
				matrix.values[i][i] = sum;
			}	
		}
	}
		
	/**
	 * @see java.lang.Object#toString()
	 */
	
	@Representor @Override
	public String toString() {
		
		int max1 = 0;
		Variable[] vars = matrix.getRowVariables();
		
        /*
         * Find string of maximum length in distance matrix:
         */
        
        for (int i = 0; i < vars.length; i++) {
        	int length = vars[i].toString().length();
            if(length > max1) {
            	max1 = length;
            }
            for (int j = 0; j < vars.length; j++) {
				length = semiring.valueToString(matrix.values[i][j]).length();
				if(length > max1) {
					max1 = length;
				}
			}
        }
        
        max1++;
        
        String gap = "     ";
        String format1 = "", result1 = "", line = "";
        String format2 = "";
		
        /*
         * Prepare format string for title:
         */

        for (int i = 0; i < vars.length+1; i++) {
        	format1 += "|%"+(i+1)+"$-"+max1+"s";
        	format2 += "|%"+(i+1)+"$-"+max1+"s";
        }
        format1 += "|";
        format2 += "|\n";

        /*
         * Output header line:
         */
        
        Object[] data = new Object[vars.length+1];
        for (int i = 0; i < vars.length; i++) {
            data[i+1] = vars[i];
        }
        data[0] = "";
        result1 += String.format(format1, data);
        result1 += gap;
        result1 += String.format(format2, data);
        
        /*
         * Horizontal line:
         */
        
        for (int i = 0; i < (result1.length()-1-gap.length()) / 2; i++) {
            line += "-";
        }
        line = line+gap+line+"\n";
        result1 = line+result1+line;
        
        /*
         * Prepare format string for configurations:
         */

        format1 = "|%"+1+"$-"+max1+"s";
        format2 = "|%"+1+"$-"+max1+"s";
        for (int i = 0; i < vars.length; i++) {
        	format1 += "|%"+(i+2)+"$"+max1+"s";
        	format2 += "|%"+(i+2)+"$"+max1+"s";
        }
        format1 += "|";
        format2 += "|\n";
        
        /*
         * Configurations:
         */
        
        for (int i = 0; i < vars.length; i++) {
            Object row1[] = new Object[data.length];
            Object row2[] = new Object[data.length];
            for (int j = 1; j < vars.length+1; j++) {
                row1[j] = semiring.valueToString(matrix.values[i][j-1]);
                row2[j] = predecessor[i][j-1];
            }
            row1[0] = vars[i];
            row2[0] = vars[i];
            result1 += String.format(format1, row1);
            result1 += gap;
            result1 += String.format(format2, row2);
        }   
        
        result1 += line;     
		return result1;
    }

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	
	@Override
	public boolean equals(Object obj) {
		if(!obj.getClass().equals(this.getClass())) {
			return false;
		}
		KValuation<?> arg = (KValuation<?>)obj;
		return matrix.equals(arg.matrix);
	}
	
	/**
	 * Returns the successor variable for the path represented by the source and target variable.
	 * @param source The variable where the path starts.
	 * @param target The target variable where the path ends.
	 * @return The successor of the source variable, i.e. the first variable visited on the path from source to target.
	 */
	
	public Variable successor(Variable source, Variable target) {
		int j = Utilities.indexOf(matrix.getRowVariables(), source);
		int i = Utilities.indexOf(matrix.getRowVariables(), target);
		if(i < 0 || j < 0) {
			return null;
		}
		return predecessor[i][j];
	}
}
