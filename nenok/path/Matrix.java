package nenok.path;

import nenok.Utilities;
import nenok.semiring.Semiring;
import nenok.va.Domain;
import nenok.va.Variable;

/**
 * This class provides a default implementation of general (i.e. not necessarily square) labeled matrices with semiring values.
 * 
 * @author Marc Pouly
 */

public class Matrix<E> {
	
	private final Semiring<E> semiring;
	protected final E[][] values;
	private final Variable[] row, col;
		
	/**
	 * Constructor:
	 * @param row The variable array specifying the row variable order of the matrix.
	 * @param column The variable array specifying the column variable order of the matrix.
	 * @param matrix A matrix of semiring values.
	 */

	public Matrix(Variable[] row, Variable[] column, E[][] matrix, Semiring<E> semiring) {
		this.row = row;
		this.col = column;
		this.values = matrix;
		this.semiring = semiring;
		
		if(matrix == null) {
			throw new IllegalArgumentException("Matrix value is null.");
		}
		
		if(matrix.length != row.length) {
			throw new IllegalArgumentException("Number of row variables does not correspond to matrix shape.");
		}
		
		if(matrix.length > 0 && matrix[0].length != col.length) {
			throw new IllegalArgumentException("Number of column variables does not correspond to matrix shape.");
		}
	}
	
	/**
	 * Factory Method:
	 * @param row The variable array specifying the row variable order of the matrix.
	 * @param column The variable array specifying the column variable order of the matrix.
	 * @param matrix A matrix of semiring values.
	 * @return A new instance of this class.
	 */
	
	public Matrix<E> create(Variable[] row, Variable[] column, E[][] matrix) {
		return new Matrix<E>(row, column, matrix, semiring);
	}
	
	/**
	 * Creates a new instance that corresponds to the sum of this matrix with the argument.
	 * @param m The argument summand.
	 * @param semiring The semiring to perform the elementary operations.
	 * @return The sum of the two argument matrices.
	 */
	
	public Matrix<E> add(Matrix<E> m, Semiring<E> semiring) {
		Variable[] nrow = new Domain(row, m.row).toArray();
		Variable[] ncol = new Domain(col, m.col).toArray();
		
		@SuppressWarnings("unchecked")
		E[][] result = (E[][])new Object[nrow.length][ncol.length];
		
        for (int i = 0; i < nrow.length; i++) {
        	
        	int k1 = Utilities.indexOf(row, nrow[i]);
        	int k2 = Utilities.indexOf(m.row, nrow[i]);
        	
        	for (int j = 0; j < ncol.length; j++) {
        		
        		result[i][j] = semiring.zero();
        		
        		/*
        		 * Get value from first factor:
        		 */
        		        		
        		if(k1 >= 0) {
            		int l1 = Utilities.indexOf(col, ncol[j]);
            		if(l1 >= 0) {
            			result[i][j] = semiring.add(result[i][j], values[k1][l1]);
            		}	
        		}
        		
        		/* 
        		 * Get value from second factor:
        		 */
        		        		
        		if(k2 >= 0) {
            		int l2 = Utilities.indexOf(m.col, ncol[j]);
            		if(l2 >= 0) {
            			result[i][j] = semiring.add(result[i][j], m.values[k2][l2]);
            		}  
            	}
        	}
        }
        return create(nrow, ncol, result);
	}
	
	/**
	 * Creates a new instance that corresponds to the product of this matrix with the argument.
	 * @param m The argument factor.
	 * @param semiring The semiring to perform the elementary operations.
	 * @return The product of the two argument matrices.
	 */
	
	public Matrix<E> multiply(Matrix<E> m, Semiring<E> semiring) {
		Domain d1 = new Domain(col);
		Domain d2 = new Domain(m.row);
		if(!d1.equals(d2)) {
			throw new IllegalArgumentException("Matrix dimensions do not correspond.");
		}
		
		@SuppressWarnings("unchecked")
		E[][] result = (E[][])new Object[row.length][m.col.length];
		
		for (int i = 0; i < row.length; i++) {
			for (int j = 0; j < m.col.length; j++) {
				result[i][j] = semiring.zero();
				int k2 = Utilities.indexOf(m.col, m.col[j]);
				for (int k = 0; k < col.length; k++) {
					
		        	int k1 = Utilities.indexOf(m.row, col[k]);
					E prod = semiring.multiply(values[i][k], m.values[k1][k2]);
					result[i][j] = semiring.add(result[i][j], prod);
				}
			}
		}
		return create(row, m.col, result);
	}

	/**
	 * Creates a vector that corresponds to the product of this matrix and the argument vector.
	 * @param vector The argument vector.
	 * @param semiring The semiring to perform the elementary operations.
	 * @return The product of this matrix and the argument vector.
	 */
	
	public Vector<E> multiply(Vector<E> vector, Semiring<E> semiring) {
		Domain d1 = new Domain(col);
		if(!d1.equals(vector.label())) {
			throw new IllegalArgumentException("Matrix and vector dimensions do not correspond.");
		}
		
		@SuppressWarnings("unchecked")
		E[] result = (E[])new Object[row.length];
		
		for (int i = 0; i < row.length; i++) {
			result[i] = semiring.zero();
			for (int j = 0; j < col.length; j++) {
				//int k2 = StringVariable.indexOf(vector.row, col[j]);
				E prod = semiring.multiply(values[i][j], vector.get(col[j]));
				result[i] = semiring.add(result[i], prod);
			}	
		}
		return new Vector<E>(row, result, semiring);
	}
	
	/**
	 * Creates a new instance that corresponds to a restriction of this matrix.
	 * @param rowvars The restriction for the row variables.
	 * @param colvars The restriction for the column variables.
	 * @return The sub-matrix with respect to the specified row and column variables.
	 */
	
	public Matrix<E> restrict(Domain rowvars, Domain colvars) {
		Variable[] newrow = rowvars.toArray();
		Variable[] newcol = colvars.toArray();
		
		@SuppressWarnings("unchecked")
		E[][] result = (E[][])new Object[newrow.length][newcol.length];
		
		for (int i = 0; i < newrow.length; i++) {
			int k1 = Utilities.indexOf(row, newrow[i]);
			for (int j = 0; j < newcol.length; j++) {
				int k2 = Utilities.indexOf(col, newcol[j]);	
				if(k1 < 0) {
					throw new IllegalArgumentException("The given row argument is not a subset of the matrix row.");
				}
				if(k2 < 0) {
					throw new IllegalArgumentException("The given column argument is not a subset of the matrix column.");
				}
				result[i][j] = values[k1][k2];	
			}
		}
		return create(newrow, newcol, result);
	}
	
	/**
	 * Returns the matrix value specified by the two variables.
	 * @param source The source variable for the row entry.
	 * @param target The target variable for the column entry.
	 * @return The matrix value at the specified position.
	 */
		
	public E value(Variable source, Variable target) {
		int s = Utilities.indexOf(row, source);
		if(s < 0) {
			throw new IllegalArgumentException("The variable "+source+" is not a valid row variable.");
		}
		int t = Utilities.indexOf(col, target);
		if(t < 0) {
			throw new IllegalArgumentException("The variable "+target+" is not a valid column variable.");
		}
		return values[s][t];
	}
	
	/**
	 * @return The variables of the matrix rows in their appropriate order.
	 */
	
	public Variable[] getRowVariables() {
		return row;
	}
	
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Matrix)) {
			return false;
		}

		Matrix<?> arg = (Matrix<?>)obj;

		/*
		 * Compare domains:
		 */
		
		Domain r1 = new Domain(row);
		Domain r2 = new Domain(arg.row);
		Domain c1 = new Domain(col);
		Domain c2 = new Domain(arg.col);
		if(!r1.equals(r2) || !c1.equals(c2)) {
			return false;
		}
		
		/*
		 * Compare matrices:
		 */
		
		for (int i = 0; i < row.length; i++) {
			int l = Utilities.indexOf(arg.row, row[i]);
			for (int j = 0; j < col.length; j++) {
				int k = Utilities.indexOf(arg.col, col[j]);
				
				@SuppressWarnings("unchecked")
				E elt = (E)arg.values[l][k];
				
				if(!semiring.isEqual(values[i][j], elt)) {
					System.out.println(values[i][j]+" different from "+arg.values[l][k]);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * String conversion of this matrix.
	 * @param semiring The semiring to perform the string conversions of the semiring values.
	 * @return This matrix converted into a string.
	 */
	
	public String toString(Semiring<E> semiring) {
		
		int max = 0;
		
        /*
         * Find string of maximum length in distance matrix:
         */
        
        for (int i = 0; i < row.length; i++) {
        	int length = row[i].toString().length();
            if(length > max) {
            	max = length;
            }
            for (int j = 0; j < col.length; j++) {
				length = semiring.valueToString(values[i][j]).length();
				if(length > max) {
					max = length;
				}
			}
        }
        
        max++;
        String format = "", result = "", line = "";
		
        /*
         * Prepare format string for title:
         */

        for (int i = 0; i < col.length+1; i++) {
        	format += "|%"+(i+1)+"$-"+max+"s";
        }
        format += "|\n";

        /*
         * Output header line:
         */
        
        Object[] data = new Object[col.length+1];
        for (int i = 0; i < col.length; i++) {
            data[i+1] = col[i];
        }
        data[0] = "";
        result += String.format(format, data);
        
        /*
         * Horizontal line:
         */
        
        for (int i = 0; i < (result.length()-1) / 2; i++) {
            line += "-";
        }
        line = line+line+"\n";
        result = line+result+line;
        
        /*
         * Prepare format string for configurations:
         */

        format = "|%"+1+"$-"+max+"s";
        for (int i = 0; i < col.length; i++) {
        	format += "|%"+(i+2)+"$"+max+"s";
        }
        format += "|\n";
        
        /*
         * Configurations:
         */
        
        for (int i = 0; i < row.length; i++) {
        	Object conf[] = new Object[col.length+1];
	        for (int j = 1; j < conf.length; j++) {
	            conf[j] = semiring.valueToString(values[i][j-1]);
	        }
	        conf[0] = row[i];
	        result += String.format(format, conf);
        }
        
        result += line;     
		return result;
    }
}
