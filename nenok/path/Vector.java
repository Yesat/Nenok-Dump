package nenok.path;

import nenok.Utilities;
import nenok.semiring.Semiring;
import nenok.va.Domain;
import nenok.va.Variable;

/**
 * This class provides a default implementation of labeled vectors with semiring values.
 * 
 * @author Marc Pouly
 */

public class Vector<E> {
	
	private Domain domain;
	private Semiring<E> semiring;
	private final E[] values;
	private final Variable[] row;
		
	/**
	 * Constructor:
	 * @param vars The variable array specifying the variable order of the vector.
	 * @param values The vector values respecting the variable array order.
	 */

	public Vector(Variable[] vars, E[] values, Semiring<E> semiring) {
		this.row = vars;
		this.values = values;
		this.semiring = semiring;
		if(row.length != values.length) {
			throw new IllegalArgumentException("Lengths of variable array and vector do not match.");
		}
	}
	
	/**
	 * @return The domain or label of this vector.
	 */
	
	public Domain label() {
		if(domain == null) {
			domain = new Domain(row);
		}
		return domain;
	}
		
	/**
	 * Creates a new instance that corresponds to the sum of this vector with the argument.
	 * @param m The argument summand.
	 * @param semiring The semiring to perform the elementary operations.
	 * @return The sum of this and the argument vector.
	 */
	
	public Vector<E> add(Vector<E> m, Semiring<E> semiring) {
		Variable[] nrow = new Domain(row, m.row).toArray();		
		
		@SuppressWarnings("unchecked")
		E[] result = (E[]) new Object[nrow.length];
		
        for (int i = 0; i < result.length; i++) {
        	int k1 = Utilities.indexOf(row, nrow[i]);
        	int k2 = Utilities.indexOf(m.row, nrow[i]);
        	result[i] = semiring.zero();
    		if(k1 >= 0) {
        		result[i] = semiring.add(result[i], values[k1]);
    		}
    		if(k2 >= 0) {
        		result[i] = semiring.add(result[i], m.values[k2]);
        	}
        }
        return new Vector<E>(nrow, result, semiring);
	}
		
	/**
	 * Creates a new instance that corresponds to a restriction of this vector.
	 * @param dom The domain of the resulting sub-vector.
	 * @return The sub-vector with respect to the specified variables.
	 */
	
	public Vector<E> restrict(Domain dom) {
		Variable[] newvars = dom.toArray();
		
		@SuppressWarnings("unchecked")
		E[] result = (E[]) new Object[newvars.length];
		
		for (int i = 0; i < newvars.length; i++) {
			int k1 = Utilities.indexOf(row, newvars[i]);
			if(k1 < 0) {
				throw new IllegalArgumentException("The given row argument is not a subset of the matrix row.");
			}
			result[i] = values[k1];
		}
		return new Vector<E>(newvars, result, semiring);
	}
	
	/**
	 * Returns the vector value specified by the two variables.
	 * @param index The index variable for the vector entry.
	 * @return The vector value at the specified position.
	 */
	
	public E get(Variable index) {
		int s = Utilities.indexOf(row, index);
		if(s < 0) {
			throw new IllegalArgumentException("The variable "+index+" is not a valid variable.");
		}
		return values[s];
	}
	
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Vector)) {
			return false;
		}

		
		@SuppressWarnings("unchecked")
		Vector<E> arg = (Vector<E>)obj;

		/*
		 * Compare domains:
		 */
		
		Domain r1 = new Domain(row);
		Domain r2 = new Domain(arg.row);
		if(!r1.equals(r2)) {
			return false;
		}
		
		/*
		 * Compare matrices:
		 */
		
		for (int i = 0; i < row.length; i++) {
			int l = Utilities.indexOf(arg.row, row[i]);
			
			if(!semiring.isEqual(values[i], arg.values[l])) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * String conversion of this vector.
	 * @param semiring The semiring to perform the string conversions of the semiring values.
	 * @return This vector converted into a string.
	 */

	public String toString(Semiring<E> semiring) {
		
		int max = 0;
		
        /*
         * Find string of maximum length in distance matrix:
         */
        
        for (int i = 0; i < row.length; i++) {
        	int length = row[i].toString().length();
            if(length > max) {
            	max = length;
            }
			length = semiring.valueToString(values[i]).length();
			if(length > max) {
				max = length;
			}
        }
        
        max++;
        String format = "", result = "", line = "";
		
        /*
         * Prepare format string for title:
         */

        for (int i = 0; i < row.length; i++) {
        	format += "|%"+(i+1)+"$-"+max+"s";
        }
        format += "|\n";

        /*
         * Output header line:
         */
        
        result += String.format(format, (Object[])row);
        
        /*
         * Horizontal line:
         */
        
        for (int i = 0; i < (result.length()-1) / 2; i++) {
            line += "-";
        }
        line = line+line+"\n";
        result = line+result+line;
        
        /*
         * Uncomment for right-shift:
         */

        /*format = "";
        for (int i = 0; i < row.length; i++) {
        	format += "|%"+(i+1)+"$"+max+"s";
        }
        format += "|\n";*/
        
        result += String.format(format, values);  
        result += line;     
		return result;
    }
}
