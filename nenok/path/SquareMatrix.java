package nenok.path;

import nenok.semiring.Quasiregular;
import nenok.semiring.Semiring;
import nenok.va.Domain;
import nenok.va.Variable;

/**
 * This class provides a default implementation of square, labeled matrices with semiring values.
 * 
 * @author Marc Pouly
 */

public class SquareMatrix<E> extends Matrix<E> {
	
	private Domain domain;
	
	/**
	 * Constructor:
	 * @param vars The variable array specifying the row and column variable order of the matrix.
	 * @param matrix A matrix of semiring values.
	 */

	public SquareMatrix(Variable[] vars, E[][] matrix, Semiring<E> semiring) {
		super(vars, vars, matrix, semiring);
	}
	
	/**
	 * @see nenok.path.Matrix#create(nenok.va.Variable[], nenok.va.Variable[], E[][])
	 */
	
	public Matrix<E> create(Variable[] row, Variable[] column, E[][] matrix, Semiring<E> semiring) {
		return new SquareMatrix<E>(row, matrix, semiring);
	};
	
	/**
	 * Creates a new instance that corresponds to a restriction of this matrix.
	 * @param dom The restriction for the row and column variables.
	 * @return The sub-matrix with respect to the specified row and column variables.
	 */
	
	public SquareMatrix<E> restrict(Domain dom) {
		return (SquareMatrix<E>)super.restrict(dom,dom);
	}
	
	/**
	 * @return The domain or label of this matrix.
	 */
	
	public Domain label() {
		if(domain == null) {
			domain = new Domain(getRowVariables());
		}
		return domain;
	}
	
	/**
	 * Creates a new instance that corresponds to the closure of this matrix.
	 * Important: Computing closures is only possible for square matrices.
	 * @param semiring The semiring to perform the elementary operations.
	 * @return The closure of the given matrix.
	 */
	
	public Matrix<E> closure(Quasiregular<E> semiring) {
		
		E[][] result = values.clone();
		Variable[] row = getRowVariables();
		
		for (int k = 0; k < row.length; k++) {
			for (int i = 0; i < row.length; i++) {
				for (int j = 0; j < row.length; j++) {
					
					E closure = semiring.closure(result[k][k]);
					E value = semiring.multiply(result[i][k], semiring.multiply(closure, result[k][j]));
					
					// Test procedure:
					// Object sum = semiring.add(matrix[i][j], value);
					// System.out.print(matrix[i][j]+" min ("+matrix[i][k]+" + "+closure+" + "+matrix[k][j]+")");
					// System.out.println(" = "+sum);
					
					result[i][j] = semiring.add(result[i][j], value);
									
				}
			}
		}
			
		for (int i = 0; i < row.length; i++) {
			result[i][i] = semiring.add(result[i][i], semiring.unit());
		}
		
		return new SquareMatrix<E>(row, result, semiring);
	}
}
