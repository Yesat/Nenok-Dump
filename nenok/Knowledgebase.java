package nenok;

import java.io.Serializable;

import nenok.va.Domain;
import nenok.va.Identity;
import nenok.va.Valuation;

/**
 * Knowledgebases allow to collect valuations under an identifies
 * that is referred to as the knowledgebase's name. Subclasses refine 
 * this type to knowledgebases with either local or remote data.
 * Note also that knowledgebases are immutable and serializable.
 * 
 * @author Marc Pouly
 * @param <T> Type to distinguish between remote and local knowledgebases.
 */

public class Knowledgebase implements Serializable {
	
	private Class<?> type;
	private final String name;
	private final Valuation[] valuations;
			
	/**
	 * Constructor:
	 * @param name The identifier of this knowledgebase.
	 * @param valuations The content of this knowledgebase.
	 */
	
	public Knowledgebase(Valuation[] vals, String name) {
		this.name = name;
		this.valuations = (vals != null) ? vals : new Valuation[] {};
	}
	
	/**
	 * @return The content of this knowledgebase as an array of valuations.
	 */

	public Valuation[] toArray() {
		return valuations;
	}
	
	/**
	 * @return The total domain of this knowledgebase.
	 */

	public Domain getDomain() {
		return (valuations == null) ? Domain.EMPTY : Domain.totalDomain(valuations);
	}

	/**
	 * @return The type of valuations that is contained in this knowledgebase.
	 */

	public Class<?> getType() {
		if(type == null) {
			
			if(valuations == null || valuations.length == 0) {
				type = Identity.class;
				return type;
			}
			
	    	for(Valuation val : valuations) {
	    		if(!(val instanceof Identity)) {
	        		type = val.getClass();
	        		break;
	    		}
	    	}
		}
		return type;
	}
	
	/**
	 * @return The size (i.e. number of elements) of this knowledgebase.
	 */

	public int size() {
		return valuations.length;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	
	@Override
	public String toString() {
		return name;
	}
}