package nenok.semiring;

/**
 * This interface represents a separative semiring, respectively its specializations as cancellative or regular or idempotent semiring.
 *
 * @author Marc Pouly
 */

public interface Dividability<E> extends Semiring<E> {
	
	/**
     * This method computes the inverse of a semiring elements.
     * @param elt The element to be inverted.
     * @return The inverse of the given element.
	 */
	
	public E inverse(E elt);
}
