package nenok.semiring;

/**
 * This interface represents a quasi-regular semiring described by its two fundamental 
 * semiring operations of addition and multiplication, and the additional closure operation.
 * 
 * @author Marc Pouly
 */

public interface Quasiregular<E> extends Semiring<E> {
	
	/**
	 * Computes the closure of the given element.
	 * @param elt The element whose closure has to be computed.
	 * @return The closure of the given element.
	 */
	
	public E closure(E elt);
	
	/**
	 * @return The unit element of this Kleene algebra.
	 */
	
	public E unit();
}
