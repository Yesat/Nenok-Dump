package nenok.semiring;

/**
 * This interface represents a generic Kleene algebra by extending {@link Quasiregular}.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision:$
 *
 * @param <E>
 */

public interface Kleene<E> extends Quasiregular<E> {
	
}
