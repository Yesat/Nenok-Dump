package nenok.semiring;

/**
 * Idempotent semiring that contain a total, natural order define a total ordering a + b = max(a,b). 
 * This natural semiring order is reflected by this interface. 
 * Furthermore, this interface presupposes that the ordering relation is strictly monotonic.
 * 
 * @author Marc Pouly
 */

public interface Optimization<E> extends Semiring<E> {
	
	/**
	 * The total order of an optimization semiring. Note, this must be consistent with equality check in Semiring.
	 * @param elt1 The first semiring element.
	 * @param elt2 The second semiring element.
	 * @return -1, if the first argument is smaller than the second, 0 if both arguments are equal and +1 otherwise.
	 */
	
	public int compare(E elt1, E elt2);
	
	/**
	 * Implementor class:
	 * 
	 * @author Marc Pouly
	 */
	
	public class Implementor<E> {
		
		private final Optimization<E> semiring;
		
		/**
		 * Constructor:
		 */
		
		public Implementor(Optimization<E> semiring) {
			this.semiring = semiring;
		}
				
		/**
		 * Implementation of the total natural order for optimization semirings.
		 * @param elt1 The first semiring value.
		 * @param elt2 The second semiring value.
		 * @return see {@link Optimization#compare(java.lang.Object,java.lang.Object)}
		 */
		
		public int compare(E elt1, E elt2) {
			if(semiring.isEqual(elt1, elt2)) {
				return 0;
			}
			E sum = semiring.add(elt1, elt2);
			return (semiring.isEqual(sum, elt1)) ? -1 : 1;
		}
	}
}
