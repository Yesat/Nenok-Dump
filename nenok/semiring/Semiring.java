package nenok.semiring;

import java.io.Serializable;

/**
 * This interface represents a generic commutative semiring type described by its two fundamental operations of addition and multiplication.
 * 
 * @author Marc Pouly
 */

public interface Semiring<E> extends Serializable {
    
    /**
     * This method computes the addition of two semiring elements.
     * @param elt1 The first summand.
     * @param elt2 The second summand.
     * @return The result of the addition.
     */
    
    public E add(E elt1, E elt2);
        
    /**
     * This method computes the multiplication of two semiring elements.
     * @param elt1 The first factor.
     * @param elt2 ment2 The second factor.
     * @return The result of the multiplication.
     */
    
    public E multiply(E elt1, E elt2);
    
	/**
	 * @return The zero element of this Kleene algebra.
	 */
	
	public E zero();
    
    /**
     * @param elt The element whose string conversion is required.
     * @return The semiring element represented as a string.
     */
    
    public String valueToString(E elt);
    
    /**
     * @param elt1 The first semiring element.
     * @param elt2 The second semiring element.
     * @return True, if both elements are equal by value.
     */
    
    public boolean isEqual(E elt1, E elt2);
    
}
