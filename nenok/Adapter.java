package nenok;

import nenok.lc.LCException;
import nenok.va.Domain;
import nenok.va.Separativity;
import nenok.va.VAException;
import nenok.va.Valuation;

/**
 * An adapter class that counts the number of executed operations.
 * 
 * @author Marc Pouly
 */

public class Adapter {
	
    private int combinations, marginalizations, inverses;
	
	/**
	 * Inverse operation.
	 * @param factor The factor whose inverse has to be computed.
	 * @return The inverse of the factor.
	 * @throws LCException Generic local computation exception.
	 */

    public Separativity inverse(Separativity factor) {
    	inverses++;
        return factor.inverse();
    }
	
	/**
	 * Combination operation.
	 * @param factor The first factor of the combination.
	 * @param factors The remaining factors to be combined.
	 * @return The combination of all factors given as argument. 
	 * The implementation of this combination should respect the array's order.
	 */
	
    public Valuation combine(Valuation factor, Valuation... factors) {
    	Valuation result = factor;
    	for (int i = 0; i < factors.length; i++) {
			result = result.combine(factors[i]);
			combinations++;
		}
        return result;
    }
	
    /**
     * Marginalization operation.
     * @param factor The factor to be marginalized.
     * @param dom The domain onto the marginalization is performed.
     * @return The marginal of the argument relativ to the given domain.
	 * @throws LCException Generic local computation exception.
     */
	
    public Valuation marginalize(Valuation factor, Domain dom) throws VAException {
        marginalizations++;
        return factor.marginalize(dom);
    }
	
    /**
     * @return Returns the number of combinations.
     */
    
    public int getCombinations() {
    	return combinations;
    }
    
    /**
     * @return Returns the number of division operations.
     */
    
    public int getInverses() {
        return inverses;
    }
    
    /**
     * @return Returns the number of marginalizations.
     */
    
    public int getMarginalizations() {
        return marginalizations;
    }
}
