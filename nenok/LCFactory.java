package nenok;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import nenok.jtconstr.Algorithm;
import nenok.jtconstr.ConstrException;
import nenok.jtconstr.Deserializer;
import nenok.jtconstr.OSLA_SC;
import nenok.lc.JoinTree;
import nenok.parser.JT_Parser;
import nenok.parser.KB_Parser;
import nenok.parser.ParserException;
import nenok.parser.ResultSet;
import nenok.va.Domain;
import nenok.va.Valuation;

/**
 * FACTORY class to build jointrees. Various methods to build join trees with different 
 * parameter lists are offered by this class. Basically, its main purposes is to simplify 
 * the user's life by preventing him from the details of using default values as parameters.
 *  
 * @author Marc Pouly
 * @version $LastChangedRevision: 550 $<br/>$LastChangedDate: 2008-03-26 14:14:27 +0100 (Mi, 26 Mrz 2008) $
 */

public final class LCFactory {
    
    private Algorithm algo;
    private Architecture architecture;
    
    /**
     * Constructor:
     * Constructs binary Shenoy-Shafer join trees with an OSLA_SC construction algorithm.
     */
    
    public LCFactory() {
    	this(Architecture.Shenoy_Shafer);
    }
            
    /**
     * Constructor:
     * Constructs jointrees of the given architecture with an OSLA_SC construction algorithm. 
     * @param architecture The architecture of local computation, i.e. the corresponding architecture's enum type.
     */
    
    public LCFactory(Architecture architecture) {
        this.architecture = (architecture == null)? Architecture.Shenoy_Shafer : architecture;
    }
    
    /*
     * Create methods with Knowledgebases:
     */
    
    /**
     * Create method of this FACTORY class. Builds a jointree that corresponds to this factory's architecture from the given 
     * knowledgebase and query list. The factory's construction algorithm is used to build the tree. 
     * @param kb The knowledgebase from which the jointree is build.
     * @param queries A list of domains that are ensured to be covered by the resulting jointree.
     * @return A jointree instance that corresponds to this factory's architecture.
     * @throws ConstrException Exceptions thrown by the jointree construction process.
     */
    
    public JoinTree create(Knowledgebase kb, Collection<Domain> queries) throws ConstrException {
    	if(algo == null) {
    		return architecture.getInstance(kb, queries, new OSLA_SC());
    	} 
		JoinTree tree = architecture.getInstance(kb, queries, algo);
		algo = null;
		return tree;
    }
    
    /**
     * Create method of this FACTORY class. Builds a jointree that corresponds to this factory's architecture from the given 
     * knowledgebase and query list. The factory's construction algorithm is used to build the tree. 
     * @param kb The knowledgebase from whom the jointree is build.
     * @param queries An array of domains that are ensured to be covered by the resulting jointree.
     * @return A jointree instance that corresponds to this factory's architecture.
     * @throws ConstrException Exceptions thrown by the jointree construction process.
     */
    
    public JoinTree create(Knowledgebase kb, Domain... queries) throws ConstrException {
    	if(algo == null) {
    		return architecture.getInstance(kb, Arrays.asList(queries), new OSLA_SC());
    	} 
		JoinTree tree = architecture.getInstance(kb, Arrays.asList(queries), algo);
		algo = null;
		return tree;
    }
    
    /*
     * Create methods with Files:
     */
    
    /**
     * Create method of this FACTORY class. Builds a jointree that corresponds to this factory's architecture
     * from the parsed file content. The parsed {@link Valuation} objects will be stored on the processor given
     * as argument. The factory's construction algorithm is used to build the tree.
     * @param file The file to parse.
     * @param parser The user defined parser instance.
     * @return A jointree instance that corresponds to this factory's architecture.
     * @throws ConstrException Exceptions thrown by the jointree construction process.
     * @throws ParserException Exceptions caused by the parsing process.
     */
    
    public JoinTree create(File file, KB_Parser parser) throws ConstrException, ParserException {
        if(!parser.accept(file)) {
        	throw new ParserException("This parser does not accept the given file.");
        }
    	ResultSet result = parser.parse(file);
    	Knowledgebase kb = new Knowledgebase(result.vals, file.getName());
        return create(kb, Arrays.asList(result.queries));
    }
        
    /**
     * Deserializes a jointree from a file by use of the given parser instance.
     * @param file The file containing a serialized jointree.
     * @param parser The parser for the deserialization process.
     * @return A jointree instance that corresponds to this factory's architecture.
     * @throws ConstrException Exceptions thrown by the jointree construction process.
     */
   
    public JoinTree rebuild(File file, JT_Parser parser) throws ConstrException {
        if(!parser.accept(file)) {
        	throw new ConstrException("This parser does not accept the given file.");
        }
    	Algorithm tmp = this.algo;
        this.algo = new Deserializer(file, parser);
        JoinTree tree = create(new Knowledgebase(new Valuation[] {}, file.getName()), new ArrayList<Domain>());
        this.algo = tmp;
        return tree;
    }

    /*
     * Getter &amp; Setter:
     */
    
    /**
     * Method to change the current factory's local computation architecture.
     * @param architecture The new local computation architecture.
     */
    
    public void setArchitecture(Architecture architecture) {
        this.architecture = architecture;
    }

    /**
     * Method to change the current factory's jointree construction algorithm.
     * @param algo The new jointree construction algorithm.
     */
    
    public void setConstructionAlgorithm(Algorithm algo) {
        this.algo = algo;
    }
}
