package test;

import java.util.ArrayList;

import org.junit.Test;

import junit.framework.TestCase;
import nenok.va.Domain;
import nenok.va.Valuation;

/**
 * Test suite for semiring instances.
 * 
 * @author Marc Pouly
 */

public class Test_Instances extends TestCase {
	
	private static final ArrayList<Generator.Data> data = new ArrayList<Generator.Data>();
	
	/*
	 * Build Test Instances:
	 */
	
	static {
		
		// Random semiring valuation algebras:
		for(Generator_SVA.Type type : Generator_SVA.Type.values()) {
			data.add(Generator_SVA.build(type));
		}
		
		// Kleene valuation algebras:
		data.add(Generator_KVA.distances());
		data.add(Generator_KVA.reliabilities());
		
	}
	
	/**
	 * Test: Shenoy-Shafer Architecture:
	 * @throws Exception Generic Exception
	 */
	
	@Test
	public void testOperations() throws Exception {
				
		for(Generator.Data instance : data) {
			
			Valuation[] kb = instance.kb.toArray();
			
			for (int i = 0; i < kb.length-1; i++) {
				
				// Marginalization:
				Valuation c = kb[i].marginalize(Domain.EMPTY);
				assertNotNull(c);
				
				// Combination:
				Valuation c1 = kb[i].combine(kb[i+1]);
				Valuation c2 = kb[i].combine(kb[i+1]);
				assertEquals(c1,c2);
			}		
		}
	}
	
}
