package test;

import java.util.ArrayList;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import junit.framework.TestCase;
import nenok.Architecture;
import nenok.LCFactory;
import nenok.Utilities;
import nenok.lc.JoinTree;
import nenok.lc.dp.DPJoinTree;
import nenok.lc.id.PathJoinTree;
import nenok.path.KValuation;
import nenok.sva.Configuration;
import nenok.sva.OSRValuation;
import nenok.va.Domain;
import nenok.va.FiniteVariable;
import nenok.va.Idempotency;
import nenok.va.Separativity;
import nenok.va.StringVariable;
import nenok.va.Valuation;
import nenok.va.Variable;

/**
 * Test suite for local computation.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 691 $<br/>$LastChangedDate: 2009-04-02 21:15:27 +0100 (Do, 02 Apr 2009) $
 */

public class Test_LC extends TestCase {
	
	private static final ArrayList<Generator.Data> data = new ArrayList<Generator.Data>();
	
	/*
	 * Build Test Instances:
	 */
	
	static {
		
		// Random semiring valuation algebras:
		for(Generator_SVA.Type type : Generator_SVA.Type.values()) {
			data.add(Generator_SVA.build(type));
		}
		
		// Random Kleene valuation algebras:
		data.add(Generator_KVA.distances());
		data.add(Generator_KVA.reliabilities());
		
	}
	
	/**
	 * Test: Shenoy-Shafer Architecture:
	 * @throws Exception Generic Exception
	 */
	
	@Test
	public void testSS() throws Exception {
				
		for(Generator.Data instance : data) {
			
			// Local Computation:		
			LCFactory factory = new LCFactory(Architecture.Shenoy_Shafer);
			JoinTree tree1 = factory.create(instance.kb, instance.queries);
			tree1.scaling(false);
			tree1.propagate();
			
			factory.setArchitecture(Architecture.Binary_Shenoy_Shafer);
			JoinTree tree2 = factory.create(instance.kb, instance.queries);
			tree2.scaling(false);
			tree2.propagate();
			
			Valuation objective = instance.getObjective();
			
			// Test Query Answering:
			for(Domain query : instance.queries) {
				Valuation p1 = tree1.answer(query);
				Valuation p2 = tree2.answer(query);
				Valuation p3 = objective.marginalize(query);
				Assert.assertEquals(instance+": Compare Marginals SS <-> BSS", p1, p2);
				Assert.assertEquals(instance+": Compare Marginals SS <-> naive", p1, p3);
			}
			
			System.out.println(tree1.getArchitecture()+" test for "+instance+": ok !");
		}
		
		System.out.println();
	}
	
	/**
	 * Test: Lauritzen-Spiegelhalter Architecture:
	 * @throws Exception Generic Exception
	 */
	
	@Test
	public void testLS() throws Exception {
		
		for(Generator.Data instance : data) {
						
			// Filter Separative Valuation Algebras:
			if(!Utilities.interfaceOf(Separativity.class, instance.kb.getType())) {
				continue;
			}
			
			// Local Computation:			
			LCFactory factory = new LCFactory(Architecture.Lauritzen_Spiegelhalter);
			JoinTree tree = factory.create(instance.kb, instance.queries);
			tree.scaling(false);
			tree.propagate();
			
			Valuation objective = instance.getObjective();
								
			// Test Query Answering:
			for(Domain query : instance.queries) {
				Valuation p1 = tree.answer(query);
				Valuation p2 = objective.marginalize(query);
				Assert.assertEquals(instance+": Compare Marginals LS <-> naive", p1, p2);
			}
			
			System.out.println(tree.getArchitecture()+" test for "+instance+": ok !");
		}
		
		System.out.println();
	}
	
	/**
	 * Test: Hugin Architecture:
	 * @throws Exception Generic Exception
	 */
	
	@Test
	public void testHugin() throws Exception {
		
		for(Generator.Data instance : data) {
			
			// Filter Separative Valuation Algebras:
			if(!Utilities.interfaceOf(Separativity.class, instance.kb.getType())) {
				continue;
			}
			
			// Local Computation:			
			LCFactory factory = new LCFactory(Architecture.Hugin);
			JoinTree tree = factory.create(instance.kb, instance.queries);
			tree.scaling(false);
			tree.propagate();
			
			Valuation objective = instance.getObjective();
								
			// Test Query Answering:
			for(Domain query : instance.queries) {
				Valuation p1 = tree.answer(query);
				Valuation p2 = objective.marginalize(query);
				Assert.assertEquals(instance+": Compare Marginals Hugin <-> naive", p1, p2);
			}
			
			System.out.println(tree.getArchitecture()+" test for "+instance+": ok !");
		}
		
		System.out.println();
	}
	
	/**
	 * Test: Idempotent Architecture with standard Query Answering:
	 * @throws Exception Generic Exception
	 */
	
	@Test
	public void testID() throws Exception {
		
		for(Generator.Data instance : data) {
			
			// Filter Idempotent Valuation Algebras:
			if(!Utilities.interfaceOf(Idempotency.class, instance.kb.getType())) {
				continue;
			}
			
			// Local Computation:			
			LCFactory factory = new LCFactory(Architecture.Idempotent);
			JoinTree tree = factory.create(instance.kb, instance.queries);
			tree.propagate();
			
			Valuation objective = instance.getObjective();
								
			// Tests with special Query Answering Procedure:
			for(Domain query : instance.queries) {
				Valuation p1 = tree.answer(query);
				Valuation p2 = objective.marginalize(query);
				Assert.assertEquals(instance+": Compare Marginals ID <-> naive", p1, p2);
			}
			
			System.out.println(tree.getArchitecture()+" test for "+instance+": ok !");
		}
		
		System.out.println();
		
	}
	
	/**
	 * Test: Idempotent Architecture with specialized Query Answering for Kleene Valuation Algebras:
	 * @throws Exception Generic Exception
	 */
	
	@Test
	public void testPaths() throws Exception {
		
		for(Generator.Data instance : data) {
			
			// Filter Kleene Valuation Algebras:
			if(!Utilities.superclassOf(KValuation.class, instance.kb.getType())) {
				continue;
			}
			
			// Local Computation:			
			LCFactory factory = new LCFactory(Architecture.Paths);
			PathJoinTree tree = (PathJoinTree)factory.create(instance.kb);
			tree.propagate();
			
			KValuation<?> objective = (KValuation<?>)instance.getObjective();
			// System.out.println(objective);
			
			Random random = new Random();
			Variable[] vars = objective.label().toArray();

			for (int i = 0; i < vars.length*vars.length; i++) {
				
				// Distance Queries:
				StringVariable source = (StringVariable)vars[random.nextInt(vars.length)];
				StringVariable target = (StringVariable)vars[random.nextInt(vars.length)];
				Valuation p1 = tree.answer(source, target);
				Valuation p2 = objective.marginalize(new Domain(source,target));
				Assert.assertEquals(instance+": Compare Marginals ID <-> naive", p1, p2);
				
				// Check Paths of Length 0:
				Assert.assertEquals(source, objective.successor(source, source));
				Assert.assertEquals(target, objective.successor(target, target));
				
				// Test Procedure:
				// StringVariable[] path = tree.findPath(source, target);
				// System.out.println("Path: "+source+" -> "+target+": "+Arrays.toString(path));				
			}
			
			System.out.println(tree.getArchitecture()+" test for "+instance+" with path queries: ok !");
		}
		
		System.out.println();
		
	}
	
	/**
	 * Test: Solution Extensions and Solution Configurations:
	 * @throws Exception Generic Exception
	 */
	
	@Test
	public void testExtension() throws Exception {
	
		for(Generator.Data instance : data) {
			
			// Filter Optimization Semiring Valuation Algebras:
			if(!Utilities.superclassOf(OSRValuation.class, instance.kb.getType())) {
				continue;
			}
			
			// Test Solution Configuration for every factor:
			for(Valuation v : instance.kb.toArray()) {
				
				OSRValuation<?> val = (OSRValuation<?>)v; 
				FiniteVariable[] vars = val.label().toArray(FiniteVariable.class);
				Object[] sol = val.getSolutionConfiguration(vars);
				Object max = val.evaluate(vars, Configuration.encode(sol, vars));
				Assert.assertEquals(max, val.maxValue());
				
			}
			
			System.out.println("Configuration test for "+instance+": ok !");
		}
		
		System.out.println();
		
	}
	
	/**
	 * Test: Satisfiability Architecture:
	 * @throws Exception Generic Exception
	 */
	
	@Test
	public void testSatisfiability() throws Exception {
		
		for(Generator.Data instance : data) {
			
			// Filter Optimization Semiring Valuation Algebras:
			if(!Utilities.superclassOf(OSRValuation.class, instance.kb.getType())) {
				continue;
			}
		
			OSRValuation<?> objective = (OSRValuation<?>)instance.getObjective();
			FiniteVariable[] vars = objective.label().toArray(FiniteVariable.class);
			
			// Find Solution Configuration with Local Computation:			
			LCFactory factory = new LCFactory(Architecture.Constraints);
			DPJoinTree tree = (DPJoinTree)factory.create(instance.kb);
			tree.propagate();
			Object v1 = objective.evaluate(vars, tree.getSolutionConfiguration(vars));
			Object v2 = objective.evaluate(vars, objective.getSolutionConfiguration(vars));
			Assert.assertEquals(instance+"", v1, v2);
			
			System.out.println(tree.getArchitecture()+" test for "+instance+" for Solution Configuration: ok !");
				
		}
	}
}
