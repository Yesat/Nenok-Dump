package test;

import java.util.HashSet;
import java.util.Random;

import instances.indicators.Indicator;
import instances.optimization.MaxConstraint;
import instances.optimization.MinConstraint;
import instances.optimization.MinimaxConstraint;
import nenok.Knowledgebase;
import nenok.va.Domain;
import nenok.va.FiniteVariable;
import nenok.va.Valuation;
import nenok.va.Variable;

/**
 * This class generates semiring valuation algebra knowledgebases from random data for testing purposes.
 * 
 * @author Marc Pouly
 */

public class Generator_SVA extends Generator {
	
	// Frame:
	private static final String[] FRAME = new String[] {"0", "1"};
	
	// Max. number of variables per factor:
	private static final int FACTOR = 3;
	
	// Total number of variables:
	private static final int OBJECTIVE = 20;
	
	// Knowledgebase size:
	private static final int KBSIZE = 5;
	
	// Largest semiring value:
	private static final int MAX = 100;
	
	// Number of queries:
	private static final int QUERIES = 5;
	
	/**
	 * Enum type to determine instance type.
	 * 
	 * @author Marc Pouly
	 * @version $LastChangedRevision: 691 $<br/>$LastChangedDate: 2009-04-02 21:15:27 +0100 (Do, 02 Apr 2009) $
	 */
	
	public enum Type {
		
		/**
		 * Tropical semiring with maximization:
		 */
		
		ARCTIC,
		
		/**
		 * Tropical semiring with minimization:
		 */
		
		TROPICAL,
		
		/**
		 * Bottleneck algebra: 
		 */
		
		BOTTLENECK,
		
		/**
		 * Boolean semiring:
		 */
		
		BOOLEAN;
		
		/**
		 * Builds a semiring instance according to the given value.
		 * @param vars The variable array of the semiring valuation.
		 * @param values The value for each configuration.
		 * @return A semiring instance with the given value.
		 */
		
		public Valuation getInstance(FiniteVariable[] vars, int[] values) {
			switch (this) {
			case ARCTIC:
				return new MaxConstraint(vars, values);
			case TROPICAL:
				return new MinConstraint(vars, values);
			case BOTTLENECK:
				return new MinimaxConstraint(vars, values);
			case BOOLEAN:
				int[] bools = new int[values.length];
				for (int i = 0; i < values.length; i++) {
					bools[i] = values[i] % 2;
				}
				return new Indicator(vars, bools);
			default:
				throw new IllegalArgumentException("Unknown enum type.");
			}
		}
		
		/**
		 * @see java.lang.Enum#toString()
		 */
		
		@Override
		public String toString() {
			switch (this) {
			case ARCTIC:
				return "Max. Constraints with Arctic Semiring";
			case TROPICAL:
				return "Min. Constraints with Tropical Semiring";
			case BOTTLENECK:
				return "Minimax Constraints";
			case BOOLEAN:
				return "Indicator Functions";
			default:
				return "Unknown Enum Type.";
			}
		}
	}
	
	/**
	 * Generates a random knowledgebase.
	 */
	
	public static Data build(Type type) {
		
		Data data = new Generator_SVA().new Data(type.toString());
		
		Random generator = new Random();
		
		/**
		 * Variables:
		 */
		
		FiniteVariable[] vars = new FiniteVariable[OBJECTIVE];
		for (int i = 0; i < vars.length; i++) {
			vars[i] = new FiniteVariable("v"+(i+1), FRAME);
		}
		
		/**
		 * Valuations:
		 */
		
		Valuation[] vals = new Valuation[KBSIZE];
	
		for (int i = 0; i < vals.length; i++) {
			
			int dom = generator.nextInt(FACTOR);
			HashSet<Variable> set = new HashSet<Variable>();
			
			for (int j = 0; j < dom; j++) {
				int index = generator.nextInt(vars.length);
				set.add(vars[index]);
			}
			
			int[] values = new int[(int)Math.pow(2, set.size())];
			
			for (int j = 0; j < values.length; j++) {
				values[j] = generator.nextInt(MAX);
			}
			
			vals[i] = type.getInstance(set.toArray(new FiniteVariable[set.size()]), values);
		}
		
		data.kb = new Knowledgebase(vals, "Random Instance");
		
		/**
		 * Queries:
		 */
		
		data.queries = new Domain[QUERIES];
		Variable[] total = Domain.union(vals).toArray();
		
		for (int j = 0; j < data.queries.length; j++) {
			
			Variable v1 = total[generator.nextInt(total.length)];
			Variable v2 = total[generator.nextInt(total.length)];
			data.queries[j] = new Domain(v1, v2);
			
		}
		
		return data;
	}
}
