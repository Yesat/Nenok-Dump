package test;

import org.junit.Test;

import instances.probability.Input;
import junit.framework.TestCase;
import nenok.Architecture;
import nenok.Knowledgebase;
import nenok.LCFactory;
import nenok.lc.JoinTree;
import nenok.va.Domain;
import nenok.va.Identity;
import nenok.va.Scalability;
import nenok.va.Valuation;

/**
 * Test unit for local computation with probability potentials.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 572 $<br/>$LastChangedDate: 2008-03-26 13:49:10 +0000 (Mi, 26 Mrz 2008) $
 */

public class Test_PP_LC extends TestCase {
	
	private Input[] input = Input.getAllInput();
	
	/**
	 * Test: Shenoy-Shafer Architecture.
	 * @throws Exception Generic Exception
	 */
	
	@Test
	public void testSS() throws Exception {
		for (Input data : input) {
			
			// Naive approach:
			Valuation result = combineAll(data.knowledgebase);

			// Local Computation:			
			LCFactory factory = new LCFactory(Architecture.Shenoy_Shafer);
			JoinTree tree1 = factory.create(data.knowledgebase, data.queries);
			tree1.propagate(true);
			
			factory.setArchitecture(Architecture.Binary_Shenoy_Shafer);
			JoinTree tree2 = factory.create(data.knowledgebase, data.queries);
			tree2.propagate(true);
			
			// Tests:
			for(Domain query : data.queries) {
				Valuation p1 = tree1.answer(query);
				Valuation p2 = tree2.answer(query);
				Valuation p3 = result.marginalize(query);
				assertEquals("Compare Marginals SS <-> BSS", p1, p2);
				assertEquals("Compare Marginals SS <-> naive", p1, ((Scalability)p3).scale());
			}
		}
	}
	
	/**
	 * Test: Lauritzen-Spiegelhalter Architecture.
	 * @throws Exception Generic Exception
	 */
	
	@Test
	public void testLS() throws Exception {
		for (Input data : input) {
			
			// Naive approach:
			Valuation result = combineAll(data.knowledgebase);

			// Local Computation:			
			LCFactory factory = new LCFactory(Architecture.Lauritzen_Spiegelhalter);
			JoinTree tree1 = factory.create(data.knowledgebase, data.queries);
			tree1.propagate(true);
										
			// Tests:
			for(Domain query : data.queries) {
				Valuation p1 = tree1.answer(query);
				Valuation p2 = result.marginalize(query);
				assertEquals("Compare Marginals LS <-> naive", p1, ((Scalability)p2).scale());
			}
		}
	}
		
	/**
	 * Test: Hugin Architecture.
	 * @throws Exception Generic Exception
	 */
	
	@Test
	public void testHugin() throws Exception {
		for (Input data : input) {
			
			// Naive approach:
			Valuation result = combineAll(data.knowledgebase);

			// Local Computation:			
			LCFactory factory = new LCFactory(Architecture.Hugin);
			JoinTree tree1 = factory.create(data.knowledgebase, data.queries);
			tree1.propagate(true);
							
			// Tests:
			for(Domain query : data.queries) {
				Valuation p1 = tree1.answer(query);
				Valuation p2 = result.marginalize(query);
				assertEquals("Compare Marginals Hugin <-> naive", p1, ((Scalability)p2).scale());
			}
		}
	}		
		
	/**
	 * Computes the combination of an array of valuations naively.
	 * @param kb The valuations to combine.
	 * @return The total combination.
	 * @throws Exception Generic exception.
	 */
	
	public static Valuation combineAll(Knowledgebase kb) throws Exception {
		Valuation[] vals = kb.toArray();
		Valuation result = Identity.INSTANCE;
		for (int i = 0; i < vals.length; i++) {
			result = result.combine(vals[i]);
		}
		return result;
	}
}
