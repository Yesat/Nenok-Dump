package test;

import org.junit.Test;

import instances.distances.Distances;
import junit.framework.TestCase;
import nenok.va.Valuation;

/**
 * Test suite for the implementation of dustance potentials.
 * 
 * @author Marc Pouly
 * @version $LastChangedRevision: 681 $<br/>$LastChangedDate: 2009-03-26 19:48:39 +0000 (Do, 26 Mrz 2009) $
 */

public class TestKVA extends TestCase {
		
	/**
	 * Test addition & combination:
	 */
	
	@Test
	public void testCommutativity() throws Exception {
				
		Generator.Data data = Generator_KVA.distances();
		Valuation[] vals = data.kb.toArray();
		for (int i = 0; i < vals.length-1; i++) {
			
			Distances d1 = (Distances)vals[i];
			Distances d2 = (Distances)vals[i+1];
			
			// Commutativity of Combination
			assertEquals(d1.combine(d2), d2.combine(d1));
		}
		
		System.out.println("Commutativity test: ok !");
	}
}
