package test;

import nenok.Knowledgebase;
import nenok.va.Domain;
import nenok.va.Identity;
import nenok.va.Valuation;

/**
 * This class abstracts random instance generators.
 * 
 * @version $LastChangedRevision: 691 $<br/>$LastChangedDate: 2009-04-02 21:15:27 +0100 (Do, 02 Apr 2009) $
 */

public abstract class Generator {
	
	/**
	 * Data collector.
	 * 
	 * @author Marc Pouly
	 * @version $LastChangedRevision: 691 $<br/>$LastChangedDate: 2009-04-02 21:15:27 +0100 (Do, 02 Apr 2009) $
	 */
	
	public class Data {
		
		/**
		 * The knowledgebase:
		 */
		
		public Knowledgebase kb;
		
		/**
		 * The query set: 
		 */
		
		public Domain[] queries;
				
		/**
		 * The name of this test instance.
		 */
		
		private String name;
		
		/**
		 * Constructor:
		 * @param name The name of this test instance.
		 */
		
		public Data(String name) {
			this.name = name;
		}
		
		/**
		 * @see java.lang.Object#toString()
		 */
		
		@Override
		public String toString() {
			return name;
		}
		
		/**
		 * Computes the objective function.
		 * @return The objective function.
		 * @throws Exception Generic Exception
		 */

		public Valuation getObjective() throws Exception {
			Valuation objective = Identity.INSTANCE;
			for (Valuation val : kb.toArray()) {
				objective = objective.combine(val);
			}
			return objective;
		}
	}
}
