package test;

import java.util.HashSet;
import java.util.Random;

import instances.distances.Distances;
import instances.probability.MaxReliability;
import nenok.Knowledgebase;
import nenok.va.Domain;
import nenok.va.StringVariable;
import nenok.va.Variable;

/**
 * This class generates distance functions knowledgebases from random data for testing purposes.
 * 
 * @version $LastChangedRevision: 691 $<br/>$LastChangedDate: 2009-04-02 21:15:27 +0100 (Do, 02 Apr 2009) $
 */

public class Generator_KVA extends Generator {
	
	// Max. number of variables per factor:
	private static final int FACTOR = 5;
	
	// Total number of variables:
	private static final int OBJECTIVE = 10;
	
	// Knowledgebase size:
	private static final int KBSIZE = 20;
		
	// Largest distance:
	private static final int MAX = 100;
	
	// Number of queries:
	private static final int QUERIES = 10;
	
	/**
	 * Generates a knowledgebase of distances.
	 */
	
	public static Data distances() {
		
		Random generator = new Random();
		Data data = new Generator_KVA().new Data("Distances");
		
		/**
		 * Variables:
		 */
		
		StringVariable[] vars = new StringVariable[OBJECTIVE];
		for (int i = 0; i < vars.length; i++) {
			vars[i] = new StringVariable("v"+i);
		}
		
		/**
		 * Valuations:
		 */
		
		Distances[] vals = new Distances[KBSIZE];
	
		for (int i = 0; i < vals.length; i++) {
			
			/*
			 * Produce domain:
			 */
			
			HashSet<StringVariable> set = new HashSet<StringVariable>();
			for (int j = 0; j < FACTOR; j++) {
				set.add(vars[generator.nextInt(OBJECTIVE)]);
			}
			StringVariable[] dom = set.toArray(new StringVariable[set.size()]);
			
			/*
			 * Produce matrix:
			 */
			
			int[][] matrix = new int[dom.length][dom.length];
			
			for (int j = 0; j < matrix.length; j++) {
				for (int k = 0; k < matrix.length; k++) {
					matrix[j][k] = generator.nextInt(MAX-1)+1;
				}
			}
			
			vals[i] = new Distances(dom, matrix);
		}
		
		data.kb = new Knowledgebase(vals, "Distances");
		
		/**
		 * Queries:
		 */
		
		data.queries = new Domain[QUERIES];
		Variable[] total = Domain.union(vals).toArray();
		
		for (int j = 0; j < data.queries.length; j++) {
			
			Variable v1 = total[generator.nextInt(total.length)];
			Variable v2 = total[generator.nextInt(total.length)];
			data.queries[j] = new Domain(v1, v2);
			
		}
		
		return data;
	}

	/**
	 * Generates a knowledgebase of network reliabilities.
	 */

	public static Data reliabilities() {
		
		Random generator = new Random();
		Data data = new Generator_KVA().new Data("Reliabilities");
		
		/**
		 * Variables:
		 */
		
		StringVariable[] vars = new StringVariable[OBJECTIVE];
		for (int i = 0; i < vars.length; i++) {
			vars[i] = new StringVariable("v"+i);
		}
		
		/**
		 * Valuations:
		 */
		
		MaxReliability[] vals = new MaxReliability[KBSIZE];
	
		for (int i = 0; i < vals.length; i++) {
			
			/*
			 * Produce domain:
			 */
			
			HashSet<StringVariable> set = new HashSet<StringVariable>();
			for (int j = 0; j < FACTOR; j++) {
				set.add(vars[generator.nextInt(OBJECTIVE)]);
			}
			StringVariable[] dom = set.toArray(new StringVariable[set.size()]);
			
			/*
			 * Produce matrix:
			 */
			
			double[][] matrix = new double[dom.length][dom.length];
			
			for (int j = 0; j < matrix.length; j++) {
				for (int k = 0; k < matrix.length; k++) {
					matrix[j][k] = Math.random();
				}
			}
			
			vals[i] = new MaxReliability(dom, matrix);
		}
		
		data.kb = new Knowledgebase(vals, "Random Instance");
		
		/**
		 * Queries:
		 */
		
		data.queries = new Domain[QUERIES];
		Variable[] total = Domain.union(vals).toArray();
		
		for (int j = 0; j < data.queries.length; j++) {
			
			Variable v1 = total[generator.nextInt(total.length)];
			Variable v2 = total[generator.nextInt(total.length)];
			data.queries[j] = new Domain(v1, v2);
			
		}
		
		return data;
	}
}
