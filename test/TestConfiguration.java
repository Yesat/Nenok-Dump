package test;

import static org.junit.Assert.assertArrayEquals;

import java.util.Random;

import org.junit.Test;

import junit.framework.JUnit4TestAdapter;
import junit.framework.TestCase;
import nenok.sva.Configuration;
import nenok.va.FiniteVariable;

/**
 * Test suite for the implementation of configurations.
 * 
 * @author Marc Pouly
 */

public class TestConfiguration extends TestCase {
	
	private static final int REPEAT = 100;
	private final Random generator = new Random();
	
	private FiniteVariable a = new FiniteVariable("A", new String[] {"a", "-a"});
	private FiniteVariable b = new FiniteVariable("B", new String[] {"b"});
	private FiniteVariable c = new FiniteVariable("C", new String[] {"0", "1"});
	private FiniteVariable d = new FiniteVariable("X", new String[] {"1", "2", "3"});
	private FiniteVariable e = new FiniteVariable("Y", new String[] {"a", "b", "c"});
	private FiniteVariable f = new FiniteVariable("Z", new String[] {"w", "x", "y", "z"});
	
	/**
	 * Test unit for the encoding and decoding of configurations.
	 */
	
	@Test
	public void testEncoding() {
		
		FiniteVariable[] vars = new FiniteVariable[] {a,b,c,d,e,f};
		
		for (int i = 0; i < REPEAT; i++) {
			
			// Build configuration:
			Object[] conf = new Object[vars.length];
			for (int j = 0; j < vars.length; j++) {
				int index = generator.nextInt(vars[j].getFrame().length);
				conf[j] = vars[j].getFrame()[index];
			}
			
			// Encode & decode:
			int c = Configuration.encode(conf, vars);
			Object[] copy = Configuration.decode(c, vars);
			assertArrayEquals(conf, copy);
		}
	}
	
	/**
	 * Test unit for the conversion of configurations.
	 */
	
	@Test
	public void testConversion() {
		
		FiniteVariable[] from = new FiniteVariable[] {a,b,c,d,e,f};
		FiniteVariable[] to = new FiniteVariable[] {f,c,d,a,e,b};
		assertEquals(from.length, to.length);
		
		int bound = 1;
		for (FiniteVariable var : from) {
			bound *= var.getFrame().length;
		}
		
		for (int i = 0; i < REPEAT; i++) {
			int c1 = generator.nextInt(bound);
			int c2 = Configuration.convert(c1, from, to);
			int c3 = Configuration.convert(c2, to, from);
			assertEquals(c1, c3);
		}
	}
	
	/**
	 * Test unit for the projection of configurations.
	 */
	
	@Test
	public void testProjection() {
		
		FiniteVariable[] to = new FiniteVariable[] {c,d,f};
		FiniteVariable[] from = new FiniteVariable[] {a,b,c,d,e,f};
		
		for (int i = 0; i < REPEAT; i++) {
			
			String[] conf = new String[from.length];
			String[] proj = new String[to.length];
			
			for (int j = 0; j < from.length; j++) {
				int index = generator.nextInt(from[j].getFrame().length);
				conf[j] = from[j].getFrame()[index];
				
				int k = Configuration.indexOf(to, from[j]);
				if(k > -1) {
					proj[k] = from[j].getFrame()[index];
				}
			}
			
			assertArrayEquals(proj, Configuration.project(conf, from, to));
		}
	}
	
	/**
	 * Test unit for the union of configurations.
	 */
	
	@Test
	public void testUnion() {
		
		FiniteVariable[] v1 = new FiniteVariable[] {c, b, a};
		FiniteVariable[] v2 = new FiniteVariable[] {f, e, d};
		FiniteVariable[] result = new FiniteVariable[] {a,b,c,d,e,f};
		
		for (int i = 0; i < REPEAT; i++) {
			
			String[] c1 = new String[v1.length];
			for (int j = 0; j < v1.length; j++) {
				int index = generator.nextInt(v1[j].getFrame().length);
				c1[j] = v1[j].getFrame()[index];
			}
			
			//System.out.println(Arrays.toString(c1));
			
			String[] c2 = new String[v2.length];
			for (int j = 0; j < v2.length; j++) {
				int index = generator.nextInt(v2[j].getFrame().length);
				c2[j] = v2[j].getFrame()[index];
			}	
			
			//System.out.println(Arrays.toString(c2));
			
			String[] union = Configuration.union(c1, v1, c2, v2, result);
			
			//System.out.println(Arrays.toString(union));
			
			assertArrayEquals(Configuration.project(union, result, v1), c1);
			assertArrayEquals(Configuration.project(union, result, v2), c2);			
		}
	}
	
	/**
	 * @return The suite of this unit test class.
	 */
	
	public static junit.framework.Test suite() {
		return new JUnit4TestAdapter(TestConfiguration.class);
	}
}
