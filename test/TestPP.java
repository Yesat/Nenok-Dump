package test;

import org.junit.Test;

import instances.probability.Input;
import junit.framework.TestCase;
import nenok.va.Domain;
import nenok.va.Identity;
import nenok.va.Valuation;

/**
 * Test unit for simple computations with probability potentials.
 * 
 * @author Marc Pouly
 */

public class TestPP extends TestCase {
			
	/**
	 * Test: Combination rule.
	 * @throws Exception
	 */
	
	@Test
	public void testCombination() throws Exception {
		
		Valuation[] kb = Input.getAsiaInput().knowledgebase.toArray();
		
		for(int i = 0; i < kb.length; i++) {
			for(int j = 0; j < kb.length; j++) {
				Valuation v1 = kb[i].combine(kb[j]);
				Valuation v2 = kb[j].combine(kb[i]);
				assertEquals(v1,v2);
			}
		}
	}
	
	/**
	 * Test: Marginalization rule:
	 * @throws Exception
	 */
	
	@Test
	public void testMarginalization() throws Exception {
		
		Valuation result = Identity.INSTANCE;
		Domain[] queries = Input.getCancerInput().queries;
		Valuation[] kb = Input.getCancerInput().knowledgebase.toArray();
		
		for(int i = 0; i < kb.length; i++) {
			result = result.combine(kb[i]);	
		}
		
		Valuation v1 = result.marginalize(Domain.EMPTY);
		
		for(int i = 0; i < queries.length; i++) {
			Valuation v2 = result.marginalize(queries[i]);
			v2 = v2.marginalize(Domain.EMPTY);
			assertEquals(v1,v2);
		}
	}
}
